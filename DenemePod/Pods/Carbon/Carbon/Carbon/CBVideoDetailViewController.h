//
//  CBVideoDetailViewController.h
//  Carbon
//
//  Created by Semih Cihan on 06/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseNewsDetailViewController.h"
#import "CBVideoDetailLogic.h"


typedef NS_ENUM(NSInteger, CBVideoDetailType)
{
    CBVideoDetailType2 = 2,
    CBVideoDetailType3 = 3,
};

@interface CBVideoDetailViewController : CBBaseNewsDetailViewController

@property (nonatomic, strong) CBVideoDetailLogic *logic;
@property (nonatomic, assign) CBVideoDetailType videoDetailType;


@end
