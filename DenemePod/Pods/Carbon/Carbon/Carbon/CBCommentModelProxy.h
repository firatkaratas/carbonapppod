//
//  CBCommentModelProxy.h
//  Carbon
//
//  Created by Semih Cihan on 17/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelProxy.h"

@protocol CBCommentModelProxyProtocol <NSObject>

@optional

- (NSString *)commentIdJSONTransformerOverridden:(id)object;
- (NSString *)commentJSONTransformerOverridden:(id)object;
- (NSString *)dateJSONTransformerOverridden:(id)object;
- (NSString *)usernameJSONTransformerOverridden:(id)object;

@end


@interface CBCommentModelProxy : CBBaseModelProxy <CBCommentModelProxyProtocol>

+ (instancetype)sharedInstance;

- (id)commentIdJSONTransformer:(id)object;
- (id)commentJSONTransformer:(id)object;
- (id)dateJSONTransformer:(id)object;
- (id)usernameJSONTransformer:(id)object;

@end
