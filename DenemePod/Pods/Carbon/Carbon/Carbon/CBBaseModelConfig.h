//
//  CBBaseModelConfig.h
//  Carbon
//
//  Created by Semih Cihan on 16/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBBaseModelConfig : CBBaseConfig

/**
 Holds the information to be used for mapping the additional parameters.
 */
@property (nonatomic, strong) NSDictionary *additionalJSONKeyPathsByPropertyKey;

@end
