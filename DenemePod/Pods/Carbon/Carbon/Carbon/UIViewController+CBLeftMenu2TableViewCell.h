//
//  UIViewController+CBLeftMenu2TableViewCell.h
//  Carbon
//
//  Created by Semih Cihan on 18/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CBLeftMenu2TableViewCell, CBCategoryModel;

@interface UIViewController (CBLeftMenu2TableViewCell)

- (void)configureCell:(CBLeftMenu2TableViewCell *)cell withModel:(CBCategoryModel *)model;

@end
