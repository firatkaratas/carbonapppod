//
//  CBViewControllerBuilder.h
//  Carbon
//
//  Created by Necati Aydın on 23/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu2ContentViewController.h"
#import "CBFeedViewController.h"

@class CBGalleryLogic,CBDetailContainerLogic,CBBaseNewsDetailLogic, CBVideoDetailLogic, CBCommentLogic, CBGuideDayModel,CBAuthorDetailLogic;

@interface CBViewControllerBuilder : NSObject

/**
 Root view controller to be given to the window.
 */
+ (UIViewController *)rootViewController;

/**
 Root view controller to be given to the window if the application is opened with push.
 */
+ (UIViewController *)rootViewControllerForPush;

/**
 CBFeedViewController to display feeds.
 */
+ (UIViewController *)feedViewControllerWithDelegate:(id<CBMenuContentViewControllerDelegate>)delegate;

/**
 CBFeedViewController to display authors.
 */
+ (UIViewController *)authorFeedViewControllerWithAuthorId:(NSString *)authorId;

/**
 CBLeftMenu1ViewController instance. (Left Menu Type 1).
 */
+ (UIViewController *)leftMenu1ViewControllerWithDelegate:(id<CBLeftMenuContainedDelegate>)delegate rootCategory:(CBCategoryModel *)categoryModel;

/**
 CBLeftMenu2ViewController instance. (Left Menu Type 2).
 */
+ (UIViewController *)leftMenu2ViewControllerWithDelegate:(id<CBLeftMenuContainedDelegate>)delegate rootCategory:(CBCategoryModel *)categoryModel;

/**
 CBLeftMenu2ContentViewController instance.
 */
+ (UIViewController *)leftMenu2ContentViewControllerWithDelegate:(id<CBLeftMenu2ContentViewControllerDelegate>)delegate rootCategory:(CBCategoryModel *)categoryModel;

/**
 CBGalleryViewController instance to display a set of photos.
 */
+ (UIViewController *)galleryViewControllerWithLogic:(CBGalleryLogic *)logic;

/**
 CBGalleryViewController instance to be used when a push notification sent.
 */
+ (UIViewController *)galleryViewControllerForPushWithFeedId:(NSString *)feedId;

/**
 CBDetailContainerViewController instance to contain detail view controllers.
 */
+ (UIViewController *)detailContainerViewControllerWithLogic:(CBDetailContainerLogic *)logic
                                               startingIndex:(NSInteger)index;
/**
 CBNewsDetailViewControler instance.
 */
+ (UIViewController *)newsDetailViewControllerWithLogic:(CBBaseNewsDetailLogic *)logic;

/**
 CBNewsDetailViewControler instance to be used after push notifications.
 */
+ (UIViewController *)newsDetailViewControllerForPushWithFeedId:(NSString *)feedId;

/**
 CBVideoDetailViewController instance.
 */
+ (UIViewController *)videoDetailViewControllerWithLogic:(CBVideoDetailLogic *)logic;

/**
 CBVideoDetailViewController instance to be used after push notifications.
 */
+ (UIViewController *)videoDetailViewControllerForPushWithFeedId:(NSString *)feedId;

/**
 CBAuthorDetailViewController instance.
 */
+ (UIViewController *)authorDetailViewControllerWithLogic:(CBAuthorDetailLogic *)logic;

/**
 CBAuthorDetailViewController instance to be used after push notifications.
 */
+ (UIViewController *)authorDetailViewControllerForPushWithFeedId:(NSString *)feedId;

/**
 CBCommentViewController instance.
 */
+ (UIViewController *)commentViewControllerWithLogic:(CBCommentLogic *)logic;

/**
 CBSettingsViewController instance.
 */
+ (UIViewController *)settingsViewController;

/**
 CBInfoViewController instance.
 */
+ (UIViewController *)infoViewController;

/**
 CBWebViewController instance.
 @param request NSURLRequest to be loaded by the webview.
 @param title Title of the news to be used when sharing.
 */
+ (UIViewController *)webViewWithUrlRequest:(NSURLRequest *)request title:(NSString *)title;

/**
 CBGuideViewController instance.
 */
+ (UIViewController *)guideViewControllerWithDelegate:(UIViewController <CBMenuContentViewControllerDelegate> *)delegate;

/**
 CBGuideContentViewController instance.
 */
+ (UIViewController *)guideContentViewControllerWithGuideDayModel:(CBGuideDayModel *)guideDayModel;

@end
