//
//  CBLeftMenuContainerViewController.h
//  Carbon
//
//  Created by Necati Aydın on 17/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "RESideMenu.h"
#import "CBBaseLogic.h"
#import "CBLeftMenu.h"

@class CBLeftMenuContainerLogic, CBGuideViewController;

/**
 It contains the left menu and feed. Should be used as root view controller.
 */
@interface CBLeftMenuContainerViewController : RESideMenu <RESideMenuDelegate, CBDataLoaderDelegate, CBMenuContentViewControllerDelegate>

/**
 Logic of the view controller.
 */
@property (nonatomic, strong) CBLeftMenuContainerLogic *logic;

/**
 If the app is started with push.
 */
@property (nonatomic, assign) BOOL startedWithPush;

/**
 Returns the widht of the side menu's content width when the left menu is displayed. It only works for portrait mode.
 */
- (CGFloat)getCalculatedContentViewInPortraitWidth;


@end
