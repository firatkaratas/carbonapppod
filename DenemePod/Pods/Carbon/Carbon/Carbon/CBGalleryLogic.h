//
//  CBGalleryLogic.h
//  Carbon
//
//  Created by Necati Aydın on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"

@class CBGalleryDetailModel, CBGalleryLogic;


@interface CBGalleryLogic : CBBaseLogic <CBDataLoaderProtocol>

@property (nonatomic, strong) NSString *feedId;
@property (nonatomic, strong) CBGalleryDetailModel *data;
@property (nonatomic, weak) id <CBDataLoaderDelegate> delegate;

@end
