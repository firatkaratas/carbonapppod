//
//  CBFeedCell.m
//  Carbon
//
//  Created by Necati Aydın on 31/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedCell.h"
#import "UIView+Separator.h"

@implementation CBFeedCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.separators = [self.contentView addFrameWithLineWidth:kCellBorderLineWidth];
}

+ (CGSize)sizeForWidth:(CGFloat)width
{
    return CGSizeMake(width, width * [self cellRatio]);
}

+ (CGFloat)cellRatio
{
    NSAssert(NO, @"cellRatio: MUST be implemented for CBFeedCell subclass \"%@\".",[self class]);
    return 0;
}

@end
