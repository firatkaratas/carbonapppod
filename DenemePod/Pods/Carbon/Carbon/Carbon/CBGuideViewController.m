//
//  CBLiveStreamViewController.m
//  Carbon
//
//  Created by Necati Aydın on 08/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGuideViewController.h"
#import "CBGuideDayModel.h"
#import "CBGuideModel.h"
#import "UIView+LayoutConstraints.h"
#import "CBGuideContentViewController.h"
#import "CBViewControllerBuilder.h"
#import "CBNavigationBarStyler.h"
#import "CBLeftMenuContainerViewController.h"
#import "UIView+Loading.h"
#import "CBGuideStyleConfig.h"

@interface CBGuideViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate, CBLoadingActionProtocol>

@property (nonatomic, weak) IBOutlet UIView *daysView;
@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic, strong) NSMutableArray *buttons;
@property (nonatomic, strong) UIImageView *buttonSelector;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *collectionViewBottomSpace;


@end

@interface CBGuideViewController (Styling)

@end

@implementation CBGuideViewController(Styling)

- (void)style
{
    CBGuideStyleConfig *config = [CBGuideStyleConfig sharedConfig];
    
    self.view.backgroundColor = config.backgroundColor;
    self.daysView.backgroundColor = config.backgroundColor;
    
    NSArray *buttons = [self buttons];
    for(UIButton *button in buttons)
    {
        [button setTitleColor:config.daysTextColor forState:UIControlStateNormal];
        button.titleLabel.font = config.daysTextFont;
    }
}

@end


@implementation CBGuideViewController

@dynamic logic;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureButtons];

    [self style];

    [self startMMAAdWithEntityName:[self entityNameWithoutType]];
    
    [CBNavigationBarStyler styleWithTitleLogo:self.navigationItem];
    [CBNavigationBarStyler styleLeftNavigationItemWithMenuIcon:self.navigationItem target:self action:@selector(menuButtonTapped)];
    
    [self.logic loadData];
    
}

- (void)configureButtons
{
    self.buttonSelector = [[UIImageView alloc] init];
    [self.buttonSelector setContentMode:UIViewContentModeCenter];
    [self.daysView addSubview:self.buttonSelector];
    [self.buttonSelector setHidden:YES];

    NSMutableDictionary *viewsDictionary = [NSMutableDictionary dictionary];
    self.buttons = [[NSMutableArray alloc] initWithCapacity:7];
    
    for (int i = 0; i < 7; i++) {
        UIButton *button = [[UIButton alloc] init];
        [button setTranslatesAutoresizingMaskIntoConstraints:NO];
        [button setHeightConstraint:44.f];
        [self.daysView addSubview:button];
        [self.buttons addObject:button];
        [button centerYInSuperview];
        [button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [viewsDictionary setObject:button
                            forKey:[NSString stringWithFormat:@"button%d", i]];
        
        switch (i) {
            case 0:
                [button setTitle:NSLocalizedString(@"monday", nil) forState:UIControlStateNormal];
                break;

            case 1:
                [button setTitle:NSLocalizedString(@"tuesday", nil) forState:UIControlStateNormal];
                break;
                
            case 2:
                [button setTitle:NSLocalizedString(@"wednesday", nil) forState:UIControlStateNormal];
                break;
                
            case 3:
                [button setTitle:NSLocalizedString(@"thursday", nil) forState:UIControlStateNormal];
                break;
                
            case 4:
                [button setTitle:NSLocalizedString(@"friday", nil) forState:UIControlStateNormal];
                break;
                
            case 5:
                [button setTitle:NSLocalizedString(@"saturday", nil) forState:UIControlStateNormal];
                break;
                
            case 6:
                [button setTitle:NSLocalizedString(@"sunday", nil) forState:UIControlStateNormal];
                break;
                
            default:
                break;
        }
    }
    
    [self.daysView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:
      @"|[button0][button1(==button0)][button2(==button0)][button3(==button0)][button4(==button0)][button5(==button0)][button6(==button0)]|"
                                             options:kNilOptions
                                             metrics:nil
                                               views:viewsDictionary]];
    
    [self.view layoutIfNeeded];
}

#pragma mark - Ads

- (NSString *)entityNameWithoutType
{
    return @"Guide-MMA";
}

- (void)adViewDidLoad:(SLAdView *)adView {
    [super adViewDidLoad:adView];
    
    self.collectionViewBottomSpace.constant = SLAdSizeMMA.height;
    [self.pageViewController.view setNeedsLayout];
    [self.pageViewController.view layoutIfNeeded];
    [self.view addSubview:adView];
    [adView distanceBottomToSuperview:0];
    [adView centerXInSuperview];

}

- (void)adViewDidFailToLoad:(SLAdView *)adView {
    [super adViewDidFailToLoad:adView];
    self.collectionViewBottomSpace.constant = 0;
}


#pragma mark - Action

- (void)menuButtonTapped
{
    [self.delegate menuContentViewControllerTappedMenuButton:self];
}

- (void)buttonTapped:(id)sender
{
    NSArray *buttons = [self buttons];
    for(NSInteger i = 0; i < buttons.count; i++)
    {
        UIButton *button = buttons[i];
        if(button == sender)
        {
            [self buttonTappedAtIndex:i];
        }
    }
}

- (void)buttonTappedAtIndex:(NSInteger)index
{
    [self selectButtonAtIndex:index];
    
    [self selectTheDayWithIndex:index];
}

- (void)selectButtonAtIndex:(NSInteger)index
{
    NSArray *buttons = [self buttons];
    for(UIButton *button in buttons)
    {
        button.enabled = YES;
    }
    
    UIButton *button = buttons[index];
    button.enabled = NO;
    
    [self.buttonSelector setImage:[UIImage imageNamed:@"blue_box" inBundle:[NSBundle frameworkBundle]]];
    [self.buttonSelector setFrame:button.frame];
    [self.buttonSelector setHidden:NO];
}

#pragma mark - Data Loading

- (void)dataLoaded
{
    [self addPageViewController];
    
    [self buttonTappedAtIndex:[self.logic currentDayOfWeek]];
    
}

- (void)dataLoading
{
    
}

- (void)dataLoadedWithError:(NSString *)errorMessage
{
    [self.view showErrorMessage:errorMessage actionTarget:self];
}

- (void)errorViewTapped:(UIGestureRecognizer *)recognizer {
    [self.view dismissErrorView];
    [self.logic loadData];
}

#pragma mark - 

- (void)selectTheDayWithIndex:(NSInteger)index
{
    UIViewController *vc = [self viewControllerWithDayOfWeekIndex:index];
    [self.pageViewController setViewControllers:@[vc]
                                      direction:UIPageViewControllerNavigationDirectionForward animated:NO
                                     completion:nil];

}

- (UIViewController *)viewControllerWithDayOfWeekIndex:(NSInteger)dayOfWeek
{
    CBGuideDayModel *guideDayModel = self.logic.data[dayOfWeek];
    UIViewController *vc = [CBViewControllerBuilder guideContentViewControllerWithGuideDayModel:guideDayModel];
    return vc;
}

- (void)addPageViewController
{
    UIPageViewController *pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];

    pageViewController.dataSource = self;
    pageViewController.delegate = self;
    
    [self.contentView addSubview:pageViewController.view];
    pageViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    [pageViewController.view distanceBottomToSuperview:0];
    [pageViewController.view distanceTopToSuperview:0];
    [pageViewController.view distanceLeftToSuperview:0];
    [pageViewController.view distanceRightToSuperview:0];
    
    [self addChildViewController:pageViewController];
    [pageViewController didMoveToParentViewController:pageViewController];
    
    self.pageViewController = pageViewController;
}

#pragma mark - Page View Controller

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    CBGuideContentViewController *current = (CBGuideContentViewController *)viewController;
    NSInteger currentDayOfWeek = [current.guideDayModel.dayOfTheWeek integerValue];
    NSInteger targetDayOfWeek = (currentDayOfWeek + 6) % 7;
    return [self viewControllerWithDayOfWeekIndex:targetDayOfWeek];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    CBGuideContentViewController *current = (CBGuideContentViewController *)viewController;
    NSInteger currentDayOfWeek = [current.guideDayModel.dayOfTheWeek integerValue];
    NSInteger targetDayOfWeek = (currentDayOfWeek + 1 ) % 7;
    return [self viewControllerWithDayOfWeekIndex:targetDayOfWeek];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    CBGuideContentViewController *current = pageViewController.viewControllers[0];
    NSInteger currentDayOfWeek = [current.guideDayModel.dayOfTheWeek integerValue];
    
    [self selectButtonAtIndex:currentDayOfWeek];
}

@end
