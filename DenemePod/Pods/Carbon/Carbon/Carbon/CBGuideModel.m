//
//  CBGuideModel.m
//  Carbon
//
//  Created by Semih Cihan on 08/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGuideModel.h"
#import "CBGuideModelConfig.h"
#import "CBNetworkConfig.h"
#import "CBGuideModelProxy.h"

@implementation CBGuideModel

+ (CBBaseModelConfig *)config
{
    return [CBGuideModelConfig sharedConfig];
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"title" : [CBGuideModelConfig sharedConfig].title,
             @"imageUrl" : [CBGuideModelConfig sharedConfig].imageUrl,
             @"tag" : [CBGuideModelConfig sharedConfig].tag,
             @"time" : [CBGuideModelConfig sharedConfig].time
             };
}

+ (NSValueTransformer *)titleJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGuideModelProxy sharedInstance] titleJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)tagJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGuideModelProxy sharedInstance] tagJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)imageUrlJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGuideModelProxy sharedInstance] imageUrlJSONTransformer:object];
        return [NSURL URLWithString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)timeJSONTransformer
{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id(id object)
            {
                return [[CBGuideModelProxy sharedInstance] timeJSONTransformer:object];
            } reverseBlock:^id(id obj) {
                return obj;
            }];
}

@end
