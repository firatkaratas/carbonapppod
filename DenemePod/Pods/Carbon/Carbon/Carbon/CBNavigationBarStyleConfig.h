//
//  CBNavigationBarConfig.h
//  Carbon
//
//  Created by Necati Aydın on 16/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBNavigationBarStyleConfig : CBBaseConfig

/**
 Background color of the navigation bar.
 */
@property (nonatomic, strong) UIColor *backgroundColor;

/**
 Color of the navigation bar buttons
 */
@property (nonatomic, strong) UIColor *tintColor;

/**
 @description Color of the title of the navigation bar.
 */
@property (nonatomic, strong) UIColor *titleColor;

/**
 @description Font of the title of the navigation bar.
 */
@property (nonatomic, strong) UIFont *titleFont;

/**
 @description Color of the bar button items of the navigation bar.
 */
@property (nonatomic, strong) UIColor *barButtonItemColor;

/**
 Determines the status bar color. When set to YES status bar text color is dark, otherwise it's light.
 */
@property (assign, nonatomic) BOOL statusBarHasLightContent;

@end
