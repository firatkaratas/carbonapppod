//
//  CBVideoDetailStyleConfig.h
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBVideoDetailStyleConfig : CBBaseConfig

/**
 Background color for the complete view.
 */
@property (nonatomic, strong) UIColor *viewBackgroundColor;

/**
 Color of the title text.
 */
@property (nonatomic, strong) UIColor *titleColor;

/**
 Font of the title text.
 */
@property (nonatomic, strong) UIFont *titleFont;

/**
 Color of the spot text.
 */
@property (nonatomic, strong) UIColor *spotColor;

/**
 Font of the spot text.
 */
@property (nonatomic, strong) UIFont *spotFont;

/**
 Color of the date text.
 */
@property (nonatomic, strong) UIColor *dateColor;

/**
 Font of the date text.
 */
@property (nonatomic, strong) UIFont *dateFont;

/**
 Color of the content text.
 */
@property (nonatomic, strong) NSString *contentColor;

/**
 Font of the content text.
 */
@property (nonatomic, strong) UIFont *contentFont;

/**
 Color of the view count text.
 */
@property (nonatomic, strong) UIColor *viewCountColor;

/**
 Font of the view count text.
 */
@property (nonatomic, strong) UIFont *viewCountFont;

/**
 @description Background color of the feedSmallImage cells.
 */
@property (nonatomic, strong) UIColor *cellBackgroundColor;

/**
 @description Color of the borders of the cells.
 */
@property (nonatomic, strong) UIColor *cellBorderColor;

/**
 @description Color of the title of the feedSmallImage cells.
 */
@property (nonatomic, strong) UIColor *cellTitleColor;

/**
 @description Font of the title of the feedSmallImage cells.
 */
@property (nonatomic, strong) UIFont *cellTitleFont;

/**
 @description Color of the category label in the feedSmallImage cell.
 */
@property (nonatomic, strong) UIColor *cellCategoryColor;

/**
 @description Font of the category label in the feedSmallImage cell.
 */
@property (nonatomic, strong) UIFont *cellCategoryFont;

/**
 @description Color of the spot label in the feedSmallImage cell.
 */
@property (nonatomic, strong) UIColor *cellSpotColor;

/**
 @description Font of the spot label in the feedSmallImage cell.
 */
@property (nonatomic, strong) UIFont *cellSpotFont;

/**
 @description Color of the suggested videos background view.
 */
@property (nonatomic, strong) UIColor *suggestedVideosBackgroundColor;

/**
 @description Color of the suggested videos title.
 */
@property (nonatomic, strong) UIColor *suggestedVideosTitleColor;

/**
 @description Font of the suggested videos title.
 */
@property (nonatomic, strong) UIFont *suggestedVideosTitleFont;

/**
 Video thumbnail scrolls with the content.
 */
@property (nonatomic, assign) BOOL thumbnailScrollsWithContent;

@end
