//
//  CBJSONAdapter.h
//  Carbon
//
//  Created by Necati Aydın on 28/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "MTLJSONAdapter.h"
#import "CBBaseModelConfig.h"

@interface CBJSONAdapter : MTLJSONAdapter

- (id)initWithJSONDictionary:(NSDictionary *)JSONDictionary modelClass:(Class)modelClass error:(NSError *__autoreleasing *)error config:(CBBaseModelConfig *)config;

@end
