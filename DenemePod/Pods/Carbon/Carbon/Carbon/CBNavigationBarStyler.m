//
//  CBNavigationBarStyler.m
//  Carbon
//
//  Created by Necati Aydın on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNavigationBarStyler.h"
#import "UIDevice+Additions.h"
#import "CBNavigationBarStyleConfig.h"
#import "UIImage+Loading.h"

@implementation CBNavigationBarStyler

+ (void)styleNavigationBarColors
{
    [[UINavigationBar appearance] setTintColor:[CBNavigationBarStyleConfig sharedConfig].tintColor];
    [[UINavigationBar appearance] setBarTintColor:[CBNavigationBarStyleConfig sharedConfig].backgroundColor];
    [[UIApplication sharedApplication] setStatusBarStyle:[CBNavigationBarStyleConfig sharedConfig].statusBarHasLightContent ? UIStatusBarStyleLightContent : UIStatusBarStyleDefault];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[CBNavigationBarStyleConfig sharedConfig].barButtonItemColor];
}

+ (void)styleNavigationBarTitle {
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSFontAttributeName:[CBNavigationBarStyleConfig sharedConfig].titleFont,
                                                           NSForegroundColorAttributeName:[CBNavigationBarStyleConfig sharedConfig].titleColor
                                                           }];
}

+ (void)styleLeftNavigationItemWithMenuIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action
{
    [self styleLeftNavigationItem:navigationItem imageName:@"left_menu" target:target action:action];
}

+ (void)styleRightNavigationItemWithShareIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action {
    [self styleRightNavigationItem:navigationItem imageName:@"share_white" target:target action:action];
}

+ (void)styleRightNavigationItemWithStreamIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action {
    [self styleRightNavigationItem:navigationItem imageName:@"live" target:target action:action];
}

+ (void)styleNavigationItemWithBackIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action
{
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"white_arrow_back" inBundle:[NSBundle frameworkBundle]] style:UIBarButtonItemStyleBordered target:target action:action];
    navigationItem.leftBarButtonItem = item;
    item.imageInsets = UIEdgeInsetsMake(0, -6, 0, 0);
}

+ (void)styleNavigationItemWithCloseIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action
{
    [self styleLeftNavigationItem:navigationItem imageName:@"close" target:target action:action];
}

+ (void)styleWithTitleLogo:(UINavigationItem *)navigationItem
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title_bar_logo" inBundle:[NSBundle frameworkBundle]]];
    navigationItem.titleView = imageView;
}

+ (void)styleRightNavigationItem:(UINavigationItem *)navigationItem
            commentButtonAction:(SEL)commentAction
                shareButtonAction:(SEL)shareAction
                          target:(id)target {
    
    [self styleRightNavigationItem:navigationItem
                 firstButtonAction:commentAction
                  firstButtonImage:[UIImage imageNamed:@"white_comment" inBundle:[NSBundle frameworkBundle]]
                secondButtonAction:shareAction
                 secondButtonImage:[UIImage imageNamed:@"share_white" inBundle:[NSBundle frameworkBundle]]
                            target:target];
}

+ (void)styleRightNavigationItem:(UINavigationItem *)navigationItem
            settingsButtonAction:(SEL)settingsAction
                infoButtonAction:(SEL)infoAction
                          target:(id)target {
    
    [self styleRightNavigationItem:navigationItem
                 firstButtonAction:settingsAction
                  firstButtonImage:[UIImage imageNamed:@"settings" inBundle:[NSBundle frameworkBundle]]
                secondButtonAction:infoAction
                 secondButtonImage:[UIImage imageNamed:@"info" inBundle:[NSBundle frameworkBundle]]
                            target:target];
}

#pragma mark - private

+ (void)styleLeftNavigationItem:(UINavigationItem *)navigationItem imageName:(NSString *)imageName target:(id)target action:(SEL)action
{
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:imageName inBundle:[NSBundle frameworkBundle]] style:UIBarButtonItemStyleBordered target:target action:action];
    navigationItem.leftBarButtonItem = item;
}

+ (void)styleRightNavigationItem:(UINavigationItem *)navigationItem imageName:(NSString *)imageName target:(id)target action:(SEL)action
{
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:imageName inBundle:[NSBundle frameworkBundle]] style:UIBarButtonItemStyleBordered target:target action:action];
    navigationItem.rightBarButtonItem = item;
}

+ (void)styleRightNavigationItem:(UINavigationItem *)navigationItem
            firstButtonAction:(SEL)settingsAction
                firstButtonImage:(UIImage *)firstButtonImage
                secondButtonAction:(SEL)infoAction
               secondButtonImage:(UIImage *)secondButtonImage
                          target:(id)target {
    
    UIBarButtonItem *firstItem = [[UIBarButtonItem alloc] initWithImage:firstButtonImage style:UIBarButtonItemStyleBordered target:target action:settingsAction];
    [firstItem setImageInsets:UIEdgeInsetsMake(0.f, 0.f, 0.f, -10.f)];
    
    UIBarButtonItem *secondItem = [[UIBarButtonItem alloc] initWithImage:secondButtonImage style:UIBarButtonItemStyleBordered target:target action:infoAction];
    [secondItem setImageInsets:UIEdgeInsetsMake(0.f, -10.f, 0.f, 0.f)];
    
    navigationItem.rightBarButtonItems = @[secondItem, firstItem];
}

@end

