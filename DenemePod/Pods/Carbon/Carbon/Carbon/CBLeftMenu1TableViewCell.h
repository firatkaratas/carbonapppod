//
//  CBLeftMenuTableViewCell.h
//  Carbon
//
//  Created by naydin on 18.02.2015.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseTableViewCell.h"

@interface CBLeftMenu1TableViewCell : CBBaseTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIView *highlightView;
@property (strong, nonatomic) UIColor *selectedTextColor;
@property (strong, nonatomic) UIColor *textColor;
@property (strong, nonatomic) UIFont *selectedTextFont;
@property (strong, nonatomic) UIFont *textFont;
@property (strong, nonatomic) UIColor *arrowColor;
@property (strong, nonatomic) UIColor *selectedArrowColor;

@end
