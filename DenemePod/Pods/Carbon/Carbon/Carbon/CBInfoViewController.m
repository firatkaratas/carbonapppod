//
//  CBInfoViewController.m
//  Carbon
//
//  Created by Semih Cihan on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBInfoViewController.h"
#import "CBNavigationBarStyler.h"
#import "CBInfoStyleConfig.h"
#import "NSString+Additions.h"
#import "CBInfoCell.h"
#import "UITableView+Additions.h"
#import "UIViewController+CBInfoCell.h"
#import "UIView+Loading.h"
#import "UIView+LayoutConstraints.h"
#import "UIView+Separator.h"
#import "CBViewControllerBuilder.h"
#import "UIView+NibLoading.h"

static const CGFloat kFooterWebViewHeight = 300.f;
static const CGFloat kBottomBarHeight = 50;
static const CGFloat kBottomBarButtonWidth = 29;
static const CGFloat kBottomBarButtonLeftPadding = 10;
static const CGFloat kBottomBarHorizontalSpacing = 10;

#pragma mark - Styling

@interface CBInfoViewController(Styling)

- (void)style;
- (void)styleInfoCell:(CBInfoCell *)cell;

@end

@interface CBInfoViewController () <UIWebViewDelegate>

@property (strong, nonatomic) NSMutableDictionary *cellHeights;

@end

@implementation CBInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.cellHeights = [@{} mutableCopy];
    
    [CBNavigationBarStyler styleNavigationItemWithCloseIcon:self.navigationItem target:self action:@selector(closeButtonTapped:)];
    [CBNavigationBarStyler styleWithTitleLogo:self.navigationItem];
    [self style];
    
    UIWebView *webView;
    if (self.logic.htmlInfoUrl) { //webview
        CGRect headerFrame = [UIScreen mainScreen].bounds;
        headerFrame.size.height -= CGRectGetHeight(self.navigationController.navigationBar.frame);
        UIView *tableHeaderView = [[UIView alloc] initWithFrame:headerFrame];
        
        if(self.logic.socialMediaEnabled)
        {
            headerFrame.size.height -= kBottomBarHeight;
        }

        webView = [[UIWebView alloc] initWithFrame:headerFrame];
        [tableHeaderView addSubview:webView];
        
        [webView loadRequest:[NSURLRequest requestWithURL:self.logic.htmlInfoUrl]];
        webView.scrollView.scrollEnabled = YES;

        [self.tableView setDelegate:nil];
        [self.tableView setDataSource:nil];
        self.tableView.bounces = NO;
        self.tableView.scrollEnabled = NO;
        
        [self.tableView setTableHeaderView:tableHeaderView];
        
    } else { //tableview
        
        [self.tableView registerClassForDefaultReuseIdentifier:[CBInfoCell class]];
        
        if (self.logic.mapImageUrl) {
            CGFloat footerHeight = 0;
            if([self.logic socialMediaEnabled])
            {
                footerHeight = kFooterWebViewHeight + kBottomBarHeight;
            }
            else
            {
                footerHeight = kFooterWebViewHeight;
            }
            UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), footerHeight)];
            webView = [[UIWebView alloc] initWithFrame:CGRectMake(0.f, 0.f, CGRectGetWidth(self.view.frame), kFooterWebViewHeight)];
            [footerView addSubview:webView];
            [webView loadRequest:[NSURLRequest requestWithURL:self.logic.mapImageUrl]];
            webView.scrollView.scrollEnabled = NO;

            [self.tableView setTableFooterView:footerView];
        }
    }
    
    [webView setBackgroundColor:[UIColor clearColor]];
    webView.opaque = NO;
    webView.scrollView.bounces = NO;
    webView.scalesPageToFit = YES;
    webView.contentMode = UIViewContentModeScaleAspectFit;
    
    if([self.logic socialMediaEnabled])
    {
        [self addBottomBar];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

#pragma mark - Bottom Bar

- (void)addBottomBar
{
    UIView *bottomBar = [[UIView alloc] init];
    UILabel *label = [UILabel new];
    label.text = NSLocalizedString(@"Bizi takip edin", nil);
    label.font = [CBInfoStyleConfig sharedConfig].bottomBarTextFont;
    label.textColor = [CBInfoStyleConfig sharedConfig].bottomBarTextColor;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    [bottomBar addSubview:label];
    [label distanceLeftToSuperview:kBottomBarButtonLeftPadding];
    [label centerYInSuperview];
    
    UIView *separator = [bottomBar addSeparatorAtTopWithLineWidth:0.5f];
    separator.backgroundColor = [CBInfoStyleConfig sharedConfig].separatorColor;
    
    bottomBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self.navigationController.view addSubview:bottomBar];
    bottomBar.backgroundColor = [CBInfoStyleConfig sharedConfig].backgroundColor;
    [bottomBar distanceBottomToSuperview:0];
    [bottomBar setHeightConstraint:kBottomBarHeight];
    [bottomBar distanceLeftToSuperview:0];
    [bottomBar distanceRightToSuperview:0];
    
    NSMutableArray *socialMedias = [self.logic socialMediaTypes];
    NSArray *reverseSocialMedias = [[socialMedias reverseObjectEnumerator] allObjects];
    for(NSInteger i = 0; i < reverseSocialMedias.count; i++)
    {
        CGFloat x = i * kBottomBarButtonWidth + kBottomBarButtonLeftPadding + i * kBottomBarHorizontalSpacing;
        
        UIButton *button = [[UIButton alloc] init];
        button.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSNumber *mediaNumber = reverseSocialMedias[i];
        CBInfoSocialMedia socialMedia = [mediaNumber integerValue];
        
        UIImage *image = [self imageForSocialMedia:socialMedia];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:[self selectorForSocialMedia:socialMedia] forControlEvents:UIControlEventTouchUpInside];
        
        [bottomBar addSubview:button];
        [button setWidthConstraint:kBottomBarButtonWidth];
        [button setHeightConstraint:kBottomBarButtonWidth];
        [button distanceRightToSuperview:x];
        [button centerYInSuperview];
    }
    
    [self.tableView setContentInset:UIEdgeInsetsMake(0.f, 0.f, kBottomBarHeight, 0.f)];
}


- (UIImage *)imageForSocialMedia:(CBInfoSocialMedia)socialMedia
{
    if(socialMedia == CBInfoSocialMediaFacebook)
    {
        return [UIImage imageNamed:@"fb" inBundle:[NSBundle frameworkBundle]];
    }
    else if(socialMedia == CBInfoSocialMediaTwitter)
    {
        return [UIImage imageNamed:@"twitter" inBundle:[NSBundle frameworkBundle]];
    }
    else if(socialMedia == CBInfoSocialMediaGooglePlus)
    {
        return [UIImage imageNamed:@"gplus" inBundle:[NSBundle frameworkBundle]];
    }
    else if(socialMedia == CBInfoSocialMediaYouTube)
    {
        return [UIImage imageNamed:@"youtube" inBundle:[NSBundle frameworkBundle]];
    }
    
    return nil;
}

- (SEL)selectorForSocialMedia:(CBInfoSocialMedia)socialMedia
{
    if(socialMedia == CBInfoSocialMediaFacebook)
    {
        return @selector(facebookButtonTapped);
    }
    else if(socialMedia == CBInfoSocialMediaTwitter)
    {
        return @selector(twitterButtonTapped);
    }
    else if(socialMedia == CBInfoSocialMediaGooglePlus)
    {
        return @selector(googlePlusButtonTapped);
    }
    else if(socialMedia == CBInfoSocialMediaYouTube)
    {
        return @selector(youTubeButtonTapped);
    }
    
    return nil;
}

#pragma mark - Action

- (void)facebookButtonTapped
{
    [self presentWebViewControllerWithUrl:[self.logic urlForSocialMedia:CBInfoSocialMediaFacebook]];
}

- (void)twitterButtonTapped
{
    [self presentWebViewControllerWithUrl:[self.logic urlForSocialMedia:CBInfoSocialMediaTwitter]];
}

- (void)googlePlusButtonTapped
{
    [self presentWebViewControllerWithUrl:[self.logic urlForSocialMedia:CBInfoSocialMediaGooglePlus]];
}

- (void)youTubeButtonTapped
{
    [self presentWebViewControllerWithUrl:[self.logic urlForSocialMedia:CBInfoSocialMediaYouTube]];
}

- (void)presentWebViewControllerWithUrl:(NSURL*)url
{
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    UIViewController *vc = [CBViewControllerBuilder webViewWithUrlRequest:request title:nil];
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.logic.titles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.cellHeights[@(indexPath.section + 1)]) {
        return [self.cellHeights[@(indexPath.section + 1)] floatValue];
    } else {
        return 0.f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CBInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CBInfoCell class])];
    [cell.webView setTag:indexPath.section + 1];
    [cell.webView setDelegate:self];
    [self styleInfoCell:cell];
    [self configureCell:cell text:[self.logic informationForSection:indexPath.section]];
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.logic titleForSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return nil;
}

#pragma mark - Actions

- (void)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {

    if (!self.cellHeights[@(webView.tag)]) {
        NSString *height = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"];
        self.cellHeights[@(webView.tag)] = @([height floatValue] + (2 * kVerticalWebViewMargin));
    }
    if (self.cellHeights.count == [self.tableView numberOfSections]) {
        
        static dispatch_once_t once;
        __weak __typeof(self)weakSelf = self;
        dispatch_once(&once, ^{
            [weakSelf.tableView reloadData];
        });
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    } else {
        return YES;
    }
}

@end

@implementation CBInfoViewController(Styling)

- (void)style
{
    CBInfoStyleConfig *config = [CBInfoStyleConfig sharedConfig];
    self.view.backgroundColor = config.backgroundColor;
    self.tableView.backgroundColor = config.backgroundColor;
}

- (void)styleInfoCell:(CBInfoCell *)cell
{
    CBInfoStyleConfig *config = [CBInfoStyleConfig sharedConfig];
    cell.font = config.cellLabelFont;
    cell.textColor = config.cellLabelColor;
    cell.backgroundColor = config.cellBackgroundColor;
}

@end
