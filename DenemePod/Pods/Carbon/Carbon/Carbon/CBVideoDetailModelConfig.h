//
//  CBVideoDetailModelConfig.h
//  Carbon
//
//  Created by Semih Cihan on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@interface CBVideoDetailModelConfig : CBBaseModelConfig

/**
 Id for the model.
 */
@property (strong, nonatomic) NSString *detailId;

/**
 Date of the news.
 */
@property (strong, nonatomic) NSString *date;

/**
 Category of the news.
 */
@property (strong, nonatomic) NSString *category;

/**
 Title of the news.
 */
@property (strong, nonatomic) NSString *title;

/**
 Secondary title of the news.
 */
@property (strong, nonatomic) NSString *spot;

/**
 Image url of the news.
 */
@property (strong, nonatomic) NSString *imageUrl;

/**
 Url of the news.
 */
@property (strong, nonatomic) NSString *url;

/**
 Url of the video.
 */
@property (strong, nonatomic) NSString *videoUrl;

/**
 HTML content of the news.
 */
@property (strong, nonatomic) NSString *content;

/**
 View count of the news.
 */
@property (strong, nonatomic) NSString *viewCount;

/**
 Duration of the video.
 */
@property (strong, nonatomic) NSString *duration;

/**
 Suggested videos.
 */
@property (strong, nonatomic) NSString *suggestedVideos;


@end
