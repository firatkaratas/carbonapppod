//
//  CBCommentStyleConfig.h
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBCommentStyleConfig : CBBaseConfig

/**
 @description Background color of the main view.
 */
@property (nonatomic, strong) UIColor *viewBackgroundColor;

/**
 @description Background color of the cells.
 */
@property (nonatomic, strong) UIColor *cellBackgroundColor;

/**
 @description Color of the borders of the cells.
 */
@property (nonatomic, strong) UIColor *cellBorderColor;

/**
 @description Color of the comment label of the cells.
 */
@property (nonatomic, strong) UIColor *commentColor;

/**
 @description Font of the comment label of the cells.
 */
@property (nonatomic, strong) UIFont *commentFont;

/**
 @description Color of the detail label in the cell.
 */
@property (nonatomic, strong) UIColor *detailColor;

/**
 @description Font of the detail label in the cell.
 */
@property (nonatomic, strong) UIFont *detailFont;

@end
