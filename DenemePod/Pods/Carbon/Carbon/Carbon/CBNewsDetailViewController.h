//
//  CBDetailViewController.h
//  Carbon
//
//  Created by Necati Aydın on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseNewsDetailViewController.h"
#import "CBDetailContainerViewController.h"

typedef NS_ENUM(NSInteger, CBNewsDetailType)
{
    CBNewsDetailType2 = 2,
    CBNewsDetailType3 = 3,
};

@interface CBNewsDetailViewController : CBBaseNewsDetailViewController

@property (nonatomic, assign) CBNewsDetailType newsDetailType;

@end
