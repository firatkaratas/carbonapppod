//
//  CBAuthorDetailStyleConfig.h
//  Carbon
//
//  Created by Necati Aydın on 11/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBAuthorDetailStyleConfig : CBBaseConfig

/**
 Background color for the complete view.
 */
@property (nonatomic, strong) UIColor *viewBackgroundColor;

/**
 Separator color for the bar which includes the control buttons.
 */
@property (nonatomic, strong) UIColor *bottomBarSeparatorColor;

/**
 Color of the title text.
 */
@property (nonatomic, strong) UIColor *titleColor;

/**
 Font of the title text.
 */
@property (nonatomic, strong) UIFont *titleFont;

/**
 Color of the spot text.
 */
@property (nonatomic, strong) UIColor *spotColor;

/**
 Font of the spot text.
 */
@property (nonatomic, strong) UIFont *spotFont;

/**
 Color of the date text.
 */
@property (nonatomic, strong) UIColor *dateColor;

/**
 Font of the date text.
 */
@property (nonatomic, strong) UIFont *dateFont;

/**
 Color of the content text.
 */
@property (nonatomic, strong) NSString *contentColor;

/**
 Font of the content text.
 */
@property (nonatomic, strong) UIFont *contentFont;

@end
