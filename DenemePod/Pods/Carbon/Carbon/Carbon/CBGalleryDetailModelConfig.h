//
//  CBGalleryDetailModelConfig.h
//  Carbon
//
//  Created by Semih Cihan on 27/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@interface CBGalleryDetailModelConfig : CBBaseModelConfig

/**
 Id for the model.
 */
@property (strong, nonatomic) NSString *detailId;

/**
 Date of the news.
 */
@property (strong, nonatomic) NSString *date;

/**
 Category of the news.
 */
@property (strong, nonatomic) NSString *category;

/**
 Photos in the gallery.
 */
@property (strong, nonatomic) NSString *photos;

/**
 Share url of the gallery.
 */
@property (strong, nonatomic) NSString *url;

/**
 Title of the news.
 */
@property (strong, nonatomic) NSString *title;

@end
