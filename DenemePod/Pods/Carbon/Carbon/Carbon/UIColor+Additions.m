//
//  UIColor+Additions.m
//  Carbon
//
//  Created by Necati Aydın on 07/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIColor+Additions.h"
#import "CBColorPalette.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIColorAdditions)

@implementation UIColor (Additions)

+ (UIColor *)colorWithCommaSeparatedString:(NSString *)colorString
{
    NSArray *arr = [colorString componentsSeparatedByString:@","];
    
    if(!(arr.count == 3 || arr.count== 4))
    {
        return nil;
    }
    
    NSString *redStr = arr[0];
    redStr = [redStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    CGFloat red = [redStr floatValue]/255.0;
    
    NSString *greenStr = arr[1];
    greenStr = [greenStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    CGFloat green = [greenStr floatValue]/255.0;
    
    NSString *blueStr = arr[2];
    blueStr = [blueStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    CGFloat blue = [blueStr floatValue]/255.0;
    
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1];
    
    //color with alpha
    if(arr.count == 4)
    {
        NSString *alphaStr = arr[3];
        alphaStr = [alphaStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        CGFloat alpha = [alphaStr floatValue];
        
        color = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
    }
    
    return color;
}

+ (UIColor *)colorByReadingFromPaletteWithString:(NSString *)paletteKey
{
    NSArray *arr = [paletteKey componentsSeparatedByString:@","];
    if (arr.count > 0) {
        NSString *paletteKeyTrimmed = [arr[0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        UIColor *color = [CBColorPalette colorWithKey:paletteKeyTrimmed];
        
        //no alpha value
        if (arr.count == 1) {
            return color;
        } else { //alpha value exists
            return [color colorWithAlphaComponent:[(NSString *)arr[1] floatValue]];
        }
    } else {
        return nil;
    }
}

@end
