//
//  UIColor+Additions.h
//  Carbon
//
//  Created by Necati Aydın on 07/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor (Additions)

/**
 Converts a string with rgba values to UIColor;
 
 @param colorString A string representing a color. Color values must be between 0 and 255. Alpha value is optional and between 0 and 1. For example: 223,220,12,1 or 223,220,12 are valid.
 
 @return UIColor object.
 
 */
+ (UIColor *)colorWithCommaSeparatedString:(NSString *)colorString;


/**
 @description Creates a color from the given key by reading it from the palette.
 @param paletteKey The key to be used for CBColorPalette to fetch the color codes.
 @return Created color.
 */
+ (UIColor *)colorByReadingFromPaletteWithString:(NSString *)paletteKey;

@end
