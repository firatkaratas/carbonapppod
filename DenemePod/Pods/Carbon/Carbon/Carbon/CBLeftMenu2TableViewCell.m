//
//  CBLeftMenu2TableViewCell.m
//  Carbon
//
//  Created by Semih Cihan on 18/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu2TableViewCell.h"
#import "UIView+Separator.h"

@interface CBLeftMenu2TableViewCell()

@end

@implementation CBLeftMenu2TableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.separator = [self.contentView addSeparatorAtTopWithLineWidth:kCellBorderLineWidth];
}

+ (CGFloat)cellHeight
{
    return 52.f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if(selected)
    {
        UIImage *image = [UIImage imageNamed:@"leftmenu2_arrow_forward_selected" inBundle:[NSBundle frameworkBundle]];
        self.arrowImageView.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    else
    {
        UIImage *image = [UIImage imageNamed:@"leftmenu2_arrow_forward" inBundle:[NSBundle frameworkBundle]];
        self.arrowImageView.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    
}

@end
