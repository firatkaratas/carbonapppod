//
//  UIView+LayoutConstraints.h
//  Carbon
//
//  Created by Necati Aydın on 12/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIView(LayoutConstraints)

/**
 @description Adds a layout constraint between the left of the view and the right of the given view.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)distance:(CGFloat)distance toLeftView:(UIView *)view;

/**
 @description Adds a layout constraint between the left of the view and the left of the superview.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)distanceLeftToSuperview:(CGFloat)distance;

/**
 @description Adds a layout constraint between the right of the view and the left of the given view.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)distance:(CGFloat)distance toRightView:(UIView *)view;

/**
 @description Adds a layout constraint between the right of the view and the right of the superview.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)distanceRightToSuperview:(CGFloat)distance;

/**
 @description Adds a layout constraint between the top of the view and the bottom of the given view.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)distance:(CGFloat)distance toTopView:(UIView *)view;

/**
 @description Adds a layout constraint between the top of the view and the top of the superview.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)distanceTopToSuperview:(CGFloat)distance;

/**
 @description Adds a layout constraint between the bottom of the view and the top of the given view.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)distance:(CGFloat)distance toBottomView:(UIView*)view;

/**
 @description Adds a layout constraint between the bottom of the view and the bottom of the superview.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)distanceBottomToSuperview:(CGFloat)distance;

/**
 @description Adds a height layout constraint to the given view.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)setHeightConstraint:(CGFloat)height;

/**
 @description Adds a width layout constraint to the given view.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)setWidthConstraint:(CGFloat)width;

/**
 Centers the view in the x-axis within its superview. Adds the constraint to the superview.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)centerXInSuperview;

/**
 Centers the view in the y-axis within its superview. Adds the constraint to the superview.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)centerYInSuperview;

/**
 @description Adds a constraint that makes the width of the view equal to its superview.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)equalWidthWithSuperView:(UIView *)superview;

/**
 @description Adds a constraint that makes the width of the view equal to its superview.
 @return The layout constraint created by this method.
 */
- (NSLayoutConstraint *)equalWidthWithSuperView:(UIView *)superview multiplier:(CGFloat)multiplier;

@end
