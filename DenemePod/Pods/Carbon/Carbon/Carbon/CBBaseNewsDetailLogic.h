//
//  CBFeedDetailLogic.h
//  Carbon
//
//  Created by Necati Aydın on 20/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"

@class CBNewsDetailModel, CBCommentLogic;

@interface CBBaseNewsDetailLogic : CBBaseLogic <CBDataLoaderProtocol, CBDetailContainedLogicProtocol>

/**
 Delegate of CBNewsDetailLogic.
 */
@property (nonatomic, weak) id<CBDataLoaderDelegate> delegate;

/**
 NewsDetail object loaded by the logic.
 */
@property (nonatomic, strong) CBNewsDetailModel *newsDetail;

/**
 Feed id of the news detail.
 */
@property (nonatomic, strong) NSString *feedId;

/**
 Creates and returns comment logic.
 @return CBCommentLogic object with the detail id.
 */
- (CBCommentLogic *)getCommentLogic;

@end
