//
//  FontManager.m
//  Carbon
//
//  Created by Necati Aydın on 07/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFontManager.h"
#import <CoreText/CoreText.h>
#import "CBFontPalette.h"

@implementation CBFontManager

+ (void)loadFonts
{
    CBFontPalette *palette = [CBFontPalette sharedConfig];

    for(NSString *fontKey in palette.plistDictionary.allKeys)
    {
        NSArray *fontArray = palette.plistDictionary[fontKey];
        NSString *fontName = fontArray[0];
        NSString *fontType = fontArray[1];
        [self loadFontWithName:fontName ofType:fontType];
    }
}

+ (void)loadFontWithName:(NSString *)fontName ofType:(NSString *)type
{
    NSString *fontPath = [[NSBundle mainBundle] pathForResource:fontName ofType:type];
    if(fontPath)
    {
        NSData *inData = [NSData dataWithContentsOfFile:fontPath];
        CFErrorRef error;
        CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)inData);
        CGFontRef font = CGFontCreateWithDataProvider(provider);
        if (! CTFontManagerRegisterGraphicsFont(font, &error)) {
            CFStringRef errorDescription = CFErrorCopyDescription(error);
            DebugLog(@"Failed to load font: %@", errorDescription);
            CFRelease(errorDescription);
        } else {
            CFRelease(font);
            CFRelease(provider);
        }
    }
    
}

@end
