//
//  CBCategoryModel.m
//  Carbon
//
//  Created by Semih Cihan on 06/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBCategoryModel.h"
#import "CBCategoryModelConfig.h"
#import "CBCategoryModelProxy.h"

@interface CBCategoryModel ()

@property (strong, nonatomic) NSNumber *hasFeedNumber;

@end

@implementation CBCategoryModel

+ (CBBaseModelConfig *)config
{
    return [CBCategoryModelConfig sharedConfig];
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name" : [CBCategoryModelConfig sharedConfig].name,
             @"categoryId" : [CBCategoryModelConfig sharedConfig].categoryId,
             @"hasFeedNumber" : [CBCategoryModelConfig sharedConfig].hasFeed,
             @"subcategories" : [CBCategoryModelConfig sharedConfig].subcategories,
             @"hasFeed" : [NSNull null],
             @"contentTypeNumber":[CBCategoryModelConfig sharedConfig].contentType,
             };
}

+ (NSValueTransformer *)nameJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBCategoryModelProxy sharedInstance] nameJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)subcategoriesJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
    
        NSArray *rawSubcategoriesArray = [[CBCategoryModelProxy sharedInstance] subcategoriesJSONTransformer:object];
        
        if (rawSubcategoriesArray) {
            NSError *error;
            NSArray *modelArray = [MTLJSONAdapter modelsOfClass:CBCategoryModel.class fromJSONArray:rawSubcategoriesArray error:&error];
            if (error) {
                DebugLog(@"Error in subcategoriesJSONTransformer in CBCategoryModel, error description: %@", error.localizedDescription);
                return nil;
            }
            return modelArray;
        } else {
            return nil;
        }
        
    } reverseBlock:^id (CBCategoryModel *subcategories) {
        return nil;
    }];
}

+ (NSValueTransformer *)contentTypeNumberJSONTransformer
{

    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBCategoryModelProxy sharedInstance] contentTypeNumberJSONTransformer:object];
        return [CBBaseModel stringOrNumberToNumber:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)categoryIdJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBCategoryModelProxy sharedInstance] categoryIdJSONTransformer:object];
        return [CBBaseModel stringOrNumberToString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)hasFeedNumberJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBCategoryModelProxy sharedInstance] hasFeedNumberJSONTransformer:object];
        return [CBBaseModel stringOrNumberToBoolNumber:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}


#pragma mark - other methods

- (CBCategoryModelContentType)contentType
{
    return self.contentTypeNumber ? self.contentTypeNumber.integerValue : CBCategoryModelContentTypeFeed;
}

- (BOOL)hasFeed {
    return [self.hasFeedNumber boolValue];
}

- (void)setHasFeed:(BOOL)hasFeed {
    _hasFeedNumber = @(hasFeed);
}

- (BOOL)isEqual:(id)object {
    return [object isKindOfClass:[self class]] && [self.categoryId isEqualToString:((CBCategoryModel *)object).categoryId] && [self.name isEqualToString:((CBCategoryModel *)object).name];
}

- (NSUInteger)hash {
    return [self.name hash] ^ [self.categoryId hash];
}

#pragma mark - Helpers

- (CBCategoryModel *)searchCategoryForCategoryId:(NSString *)categoryId {
    NSMutableArray *stack = [@[] mutableCopy];
    NSMutableDictionary *discoveredCategories = [@{} mutableCopy];
    [stack addObject:self];
    while (stack.count > 0) {
        CBCategoryModel *poppedCategory = stack.lastObject;
        [stack removeLastObject];
        if (!discoveredCategories[poppedCategory.categoryId]) {
            if ([poppedCategory.categoryId isEqualToString:categoryId]) {
                return poppedCategory;
            }
            discoveredCategories[poppedCategory.categoryId] = @YES;
            for (CBCategoryModel *adjacentCategory in poppedCategory.subcategories) {
                [stack addObject:adjacentCategory];
            }
        }
    }
    return nil;
}

- (NSArray *)findAllSubcategoryIds {
    NSMutableArray *categoryIds = [@[] mutableCopy];
    NSMutableArray *stack = [@[] mutableCopy];
    NSMutableDictionary *discoveredCategories = [@{} mutableCopy];
    [stack addObject:self];
    while (stack.count > 0) {
        CBCategoryModel *poppedCategory = stack.lastObject;
        [stack removeLastObject];
        if (!discoveredCategories[poppedCategory.categoryId]) {
            [categoryIds addObject:poppedCategory.categoryId];
            discoveredCategories[poppedCategory.categoryId] = @YES;
            for (CBCategoryModel *adjacentCategory in poppedCategory.subcategories) {
                [stack addObject:adjacentCategory];
            }
        }
    }
    return categoryIds;
}

- (BOOL)hasSubcategory
{
    return self.subcategories && self.subcategories.count > 0;
}

@end
