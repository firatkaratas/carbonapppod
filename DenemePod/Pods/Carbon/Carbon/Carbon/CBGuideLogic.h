//
//  CBGuideLogic.h
//  Carbon
//
//  Created by Necati Aydın on 08/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"

@interface CBGuideLogic : CBBaseLogic <CBDataLoaderProtocol>

/**
 Data for FeedViewController instances.
 */
@property (nonatomic, strong) NSArray *data;

/**
 Delegate of CBGuideLogic.
 */
@property (nonatomic, weak) id<CBDataLoaderDelegate> delegate;

/**
 Returns an integer 0 to 6 indicating the current day of week.
 @return NSInteger 0 to 6 indicating the current day of week. 0 for Monday, 6 for Sunday.
 */
- (NSInteger)currentDayOfWeek;

@end
