//
//  CBDetailContainerLogic.m
//  Carbon
//
//  Created by Necati Aydın on 20/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBDetailContainerLogic.h"
#import "CBFeedModel.h"
#import "CBBaseNewsDetailLogic.h"
#import "CBVideoDetailLogic.h"
#import "CBAuthorDetailLogic.h"

@interface CBDetailContainerLogic()

@property (nonatomic, copy) NSMutableArray *feedModels;
@property (nonatomic, copy) NSMutableDictionary *detailLogics;

@end

@implementation CBDetailContainerLogic

- (instancetype)initWithFeedModels:(NSArray *)feedModels
{
    self = [super init];
    if(self)
    {
        _feedModels = [NSMutableArray arrayWithArray:feedModels];
        _detailLogics = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (CBBaseLogic *)logicWithIndex:(NSInteger)index {
    CBFeedModel *model  = self.feedModels[index];
    
    CBBaseLogic<CBDetailContainedLogicProtocol> *logic = self.detailLogics[model.feedId];
    if (!logic) { //if it hasnt been created yet
        switch (model.type) {
            case FeedTypeArticle:
            {
                logic = [[CBBaseNewsDetailLogic alloc] initWithFeedId:model.feedId];
                break;
            }
                
            case FeedTypeVideo:
            {
                logic = [[CBVideoDetailLogic alloc] initWithFeedId:model.feedId];
                break;
            }
            case FeedTypeAuthor:
            {
                logic = [[CBAuthorDetailLogic alloc] initWithFeedId:model.feedId];
                break;
            }
            default:
                break;
        }
        
        self.detailLogics[model.feedId] = logic;
    }
    return logic;
}

- (CBBaseLogic *)logicAfterLogic:(CBBaseLogic<CBDetailContainedLogicProtocol> *)logic {
    NSInteger nextIndex;
    BOOL found = NO;
    int i;
    for (i = 0; i < self.feedModels.count; i++)
    {
        CBFeedModel *feed = self.feedModels[i];
        if ([feed.feedId isEqualToString:logic.feedId]){
            found = YES;
            break;
        }
    }
    
    if (found) {
        nextIndex = i + 1;
    } else {
        return nil;
    }
    
    if (nextIndex < self.feedModels.count) {
        return [self logicWithIndex:nextIndex];
    } else {
        return nil;
    }
}

- (CBBaseLogic *)logicBeforeLogic:(CBBaseLogic<CBDetailContainedLogicProtocol> *)logic {
    NSInteger previousIndex;
    BOOL found = NO;
    int i;
    for (i = 0; i < self.feedModels.count; i++) {
        CBFeedModel *feed = self.feedModels[i];
        if ([feed.feedId isEqualToString:logic.feedId]){
            found = YES;
            break;
        }
    }
    
    if (found) {
        previousIndex = i - 1;
    } else {
        return nil;
    }
    
    if (previousIndex >= 0) {
        return [self logicWithIndex:previousIndex];
    } else {
        return nil;
    }
}

- (NSInteger)indexOfLogic:(CBBaseLogic<CBDetailContainedLogicProtocol> *)logic {
    if (logic) {
        BOOL found = NO;
        int i = 0;
        for(; i < self.feedModels.count; i++) {
            CBFeedModel *model = self.feedModels[i];
            if([model.feedId isEqualToString:logic.feedId]) {
                found = YES;
                break;
            }
        }
        return (found) ? i : -1;
    } else {
        return -1;
    }
}

@end
