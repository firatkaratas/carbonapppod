//
//  CBGalleryViewController.m
//  Carbon
//
//  Created by Necati Aydın on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGalleryViewController.h"
#import "CBFeedModel.h"
#import "UIView+NibLoading.h"
#import "UIView+LayoutConstraints.h"
#import "CBGalleryDetailModel.h"
#import "CBGalleryPhotoModel.h"
#import "CBNetworkManager.h"
#import "UIView+Separator.h"
#import "CBGalleryDetailStyleConfig.h"
#import "UIView+Loading.h"
#import "CBGalleryContentViewController.h"
#import "UIViewController+Fading.h"
#import "CBViewControllerBuilder.h"
#import "CBAnalyticsManager.h"
#import "CBFeedModel.h"
#import "UIDevice+Additions.h"

static const CGFloat kDismissPrecisionHeight = 50;
static const CGFloat kShareButtonWidth = 44;

/**
 To be calculated for the alpha ratio when the scroll view scrolled.
 */
static const CGFloat kDismissAlphaPrecisionHeight = 200;

/**
 If the page is scrolled by the kMinPageScrollRatio the page is scrolled. For example if it is 0.25, and the page is scrolled by the quarter of the page it goes to the next or previous page.
 */
static const CGFloat kMinPageScrollRatio = 0.25;

@interface CBGalleryViewController (Styling)

- (void)style;

@end

@interface CBGalleryViewController () <UIScrollViewDelegate, CBImageViewControllerDelegate, CBLoadingActionProtocol>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *imageCountLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) UIView *separator;

@property (nonatomic, strong) NSMutableArray *galleryContentViewControllers;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, strong) NSMutableArray *pageLoad;

@property (nonatomic, assign) BOOL hideBars;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (strong, nonatomic) UIView *blackView;
@property (assign, nonatomic) BOOL statusBarHidden;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *shareButtonWidth;

@end

@implementation CBGalleryViewController

@dynamic logic;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.hideBars = NO;
    self.separator = [self.bottomView addSeparatorAtTopWithLineWidth:0.5];
    
    [self style];

    self.blackView = [[UIView alloc] initWithFrame:self.view.bounds];
    [self.blackView setBackgroundColor:[UIColor blackColor]];
    [self.view insertSubview:self.blackView atIndex:0];
    
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [self.backgroundImageView setImage:self.backgroundImage];
    [self.backgroundImageView setHidden:YES];
    [self.view insertSubview:self.backgroundImageView atIndex:0];
    self.statusBarHidden = YES;
    self.shareButtonWidth.constant = 0;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)prefersStatusBarHidden
{
    return self.statusBarHidden;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.logic loadData];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self setNeedsStatusBarAppearanceUpdate];
}


#pragma mark - Data

- (void)dataLoading {
    [self.view showLoadingViewWithCustomIndicatorColor:[UIColor whiteColor]];
}

- (void)dataLoaded
{
    [self.view dismissLoadingView];
    
    NSInteger __block photoCount = self.logic.data.photoCount;
    
    __weak typeof(self) weakSelf = self;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^
    {
        self.shareButtonWidth.constant = self.logic.data.url == nil ? 0 : kShareButtonWidth;
        self.shareButton.hidden = self.logic.data.url == nil;
        
        [self.shareButton layoutIfNeeded];
        
        [weakSelf layoutImageViewControllerForPhotoCount:photoCount];
        weakSelf.imageCountLabel.text = [NSString stringWithFormat:@"1/%ld",(long)photoCount];
        
        weakSelf.pageLoad = [NSMutableArray new];
        for(NSInteger i = 0; i < photoCount; i++)
        {
            [self.pageLoad addObject:@NO];
        }
        
        [weakSelf pageDisplayed:0];
    }];
    
    //analytics
    [CBAnalyticsManager feedDetailActionWithCategoryOfTheFeed:(self.startedWithPush) ? @"From Push" : nil
                                          categoryOfTheDetail:self.logic.data.category.name
                                                     newsType:[CBFeedModel feedTypeStringOfFeedType:FeedTypeGallery]];

}

- (void)dataLoadedWithError:(NSString *)error
{
    [self.view showErrorMessage:error actionTarget:self];
}

#pragma mark - CBLoadingActionProtocol

- (void)errorViewTapped:(UIGestureRecognizer *)recognizer {
    [self.view dismissErrorView];
    [self.logic loadData];
}

#pragma mark - Gallery Content View Controller

- (void)pageDisplayed:(NSInteger)page
{
    self.imageCountLabel.text = [NSString stringWithFormat:@"%ld/%ld",
                                 (long)self.currentPage+1,
                                 (long)[self.logic.data photoCount]];
    
    [self hideBars:self.hideBars];

    [self loadImageAtPageIfNotLoaded:page];
    [self loadImageAtPageIfNotLoaded:page+1];
}

- (void)loadImageAtPageIfNotLoaded:(NSInteger)page
{
    if(page >= self.pageLoad.count || page < 0)
    {
        return;
    }
    
    NSNumber *pageLoad = self.pageLoad[page];
    if([pageLoad boolValue])
    {
        return;
    }
    self.pageLoad[page] = @YES;
    
    CBGalleryPhotoModel __block *photo = [self.logic.data.photos objectAtIndex:page];
    CBGalleryContentViewController __block *vc = self.galleryContentViewControllers[page];
    __weak typeof(self) weakSelf = self;
    [vc.view showLoadingViewWithCustomIndicatorColor:[UIColor whiteColor]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^
    {
        NSData *data = [NSData dataWithContentsOfURL:photo.imageUrl];
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [vc.view dismissLoadingView];
            UIImage *image = [UIImage imageWithData:data];

            [weakSelf configureGalleryContentViewController:vc
                                         withGalleryPhoto:photo
                                                    image:image];
            [weakSelf.backgroundImageView setHidden:NO];
        });
    });
}

- (void)dismiss
{
    self.scrollView.delegate = nil;
    [self dismissViewControllerByFadingOut];
}

- (void)layoutImageViewControllerForPhotoCount:(NSInteger)photoCount
{
    self.currentPage = 0;
    [self.contentView equalWidthWithSuperView:self.scrollView multiplier:photoCount];
    
    NSMutableArray *galleryContentViewControllers = [NSMutableArray new];
    self.galleryContentViewControllers = galleryContentViewControllers;
    for(NSInteger i = 0; i < photoCount; i++)
    {
        CBGalleryContentViewController *vc = [CBGalleryContentViewController instanceWithoutImage];
        vc.delegate = self;
        vc.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self addChildViewController:vc];

        [galleryContentViewControllers addObject:vc];
        [vc didMoveToParentViewController:self];
        [self.contentView addSubview:vc.view];

        
        [vc.view distanceTopToSuperview:0];
        [vc.view distanceBottomToSuperview:0];
        [vc.view equalWidthWithSuperView:self.scrollView];

        
        if(i == 0)
        {
            [vc.view distanceLeftToSuperview:0];
        }
        else
        {
            CBImageViewController *previous = galleryContentViewControllers[i-1];
            [vc.view distance:0 toLeftView:previous.view];
        }
    }
}


- (void)configureGalleryContentViewController:(CBGalleryContentViewController *)galleryContentViewController
                             withGalleryPhoto:(CBGalleryPhotoModel *)photo
                                        image:(UIImage *)image
{
    [galleryContentViewController addImage:image];
    
    if(![photo hasDescription])
    {
        [galleryContentViewController disableTextView];
    }
    else
    {
        CBGalleryDetailStyleConfig *config = [CBGalleryDetailStyleConfig sharedConfig];
        NSString *htmlStr = [NSString stringWithFormat:
                             @"<style>\
                             p{font-family:'%@';\
                             font-size:%f;\
                             color:rgb(%@)}\
                             body {\
                             background-color: black;\
                             }\
                             </style>\
                             <html>\
                             <body>\
                             %@\
                             </body>\
                             </html>",
                             config.descriptionFont.fontName,
                             config.descriptionFont.pointSize,
                             config.descriptionTextColor,
                             photo.photoDescription ? photo.photoDescription : @""];
        
        htmlStr = [NSString stringWithFormat:@"<p>%@</p>",htmlStr];
        [galleryContentViewController loadHtmlString:htmlStr];
    }
}

#pragma mark - Scroll View

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    CBGalleryContentViewController *gallery = self.galleryContentViewControllers[self.currentPage];
    
    if(((contentOffsetY + kDismissPrecisionHeight < 0 ) || (contentOffsetY - kDismissPrecisionHeight > 0)) && (gallery.scrollView.zoomScale == 1))
    {
        [self dismiss];
        return;
    }
    
    [scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat contentOffsetX = scrollView.contentOffset.x;
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    self.statusBarHidden = YES;
    //increase alpha while dismissing
    CBGalleryContentViewController *galleryContentViewController = self.galleryContentViewControllers[self.currentPage];
    if(galleryContentViewController.scrollView.zoomScale == 1)
    {
        CGFloat alphaValue = (kDismissAlphaPrecisionHeight - ABS(contentOffsetY)) / kDismissAlphaPrecisionHeight;
        ((UIViewController *)self.galleryContentViewControllers[self.currentPage]).view.backgroundColor = [UIColor clearColor];
        self.scrollView.alpha = alphaValue;
        self.blackView.alpha = alphaValue;
        self.bottomView.alpha = alphaValue;
        self.statusBarHidden = NO;
    }
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    //direction lock
    if(ABS((ABS(contentOffsetX)-self.currentPage*CGRectGetWidth(scrollView.frame))) > ABS(contentOffsetY))
    {
        contentOffsetY = 0;
    }
    else
    {
        contentOffsetX = self.currentPage * CGRectGetWidth(scrollView.frame);
    }
    [scrollView setContentOffset:CGPointMake(contentOffsetX, contentOffsetY) animated:NO];
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    CGFloat width = CGRectGetWidth(scrollView.frame);
    
    if(scrollView.contentOffset.x > self.currentPage * width + width * kMinPageScrollRatio )
    {
        self.currentPage++;
    }
    else if(scrollView.contentOffset.x < self.currentPage * width - width/4)
    {
        self.currentPage--;
    }

    if(self.currentPage < 0)
    {
        self.currentPage = 0;
    }
    if(self.currentPage >= [self.logic.data photoCount])
    {
        self.currentPage = self.logic.data.photoCount - 1;
    }
    
    [scrollView setContentOffset:CGPointMake(self.currentPage * CGRectGetWidth(scrollView.frame), 0) animated:YES];
    
    [self pageDisplayed:self.currentPage];
}


#pragma mark - Rotation

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    CGFloat targetSelfWidth = 0;
    CGFloat currentWidth = CGRectGetWidth(self.view.frame);
    CGFloat currentHeight = CGRectGetHeight(self.view.frame);
    
    if(!UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
    {
        targetSelfWidth = MAX(currentWidth, currentHeight);
    }
    else
    {
        targetSelfWidth = MIN(currentWidth, currentHeight);
    }
    
    CGFloat correctHorizontalOffset = (self.currentPage) * targetSelfWidth;
    CGPoint contentOffset = CGPointMake(correctHorizontalOffset, 0);
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration
                     animations:^
    {
        [weakSelf.scrollView setContentOffset:contentOffset animated:NO];
    }];
    
}

#pragma mark - BottomBars

- (void)switchHideBars
{
    self.hideBars = !self.hideBars;
    [self hideBars:self.hideBars];
}

- (void)hideBars:(BOOL)hide
{
    for(CBGalleryContentViewController *galleryContentViewController in self.galleryContentViewControllers)
    {
        [galleryContentViewController hideTextView:hide];
    }
    self.bottomView.alpha = hide ? 0 : 1;
}

#pragma mark - Actions

- (IBAction)backButtonTapped:(id)sender
{
    if(self.startedWithPush)
    {
        [UIApplication sharedApplication].delegate.window.rootViewController = [CBViewControllerBuilder rootViewControllerForPush];
        return;
    }
    
    if(self.navigationController)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)shareButtonTapped:(id)sender
{
    [CBAnalyticsManager shareButtonTapped];
    
    NSMutableArray *activityItems = [NSMutableArray new];
    if(self.logic.data.title)
    {
        [activityItems addObject:self.logic.data.title];
    }
    if(self.logic.data.url)
    {
        [activityItems addObject:self.logic.data.url];
    }
    
    if(activityItems.count)
    {
        UIActivityViewController *controller =
        [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        controller.excludedActivityTypes = @[UIActivityTypeAddToReadingList, UIActivityTypeCopyToPasteboard];
        
        if(IPAD)
        {
            if([UIDevice iOS8AndHigher])
            {
                controller.popoverPresentationController.sourceView = self.shareButton;
                controller.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
            }
            else
            {
                return;
            }
        }
        
        
        if(self.logic.data.title)
        {
            [controller setValue:self.logic.data.title forKey:@"subject"];
        }
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - Gallery Content View Controller Delegate

- (void)imageViewControllerTapped:(CBImageViewController *)imageViewController
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^
    {
        [weakSelf switchHideBars];
    }];
}

@end

@implementation CBGalleryViewController (Styling)

- (void)style
{
    self.view.backgroundColor = [CBGalleryDetailStyleConfig sharedConfig].viewBackgroundColor;
    self.blackView.backgroundColor = [CBGalleryDetailStyleConfig sharedConfig].viewBackgroundColor;
    self.bottomView.backgroundColor = [CBGalleryDetailStyleConfig sharedConfig].bottomBarBackgroundColor;
    self.separator.backgroundColor = [CBGalleryDetailStyleConfig sharedConfig].bottomBarSeparatorColor;
    self.imageCountLabel.textColor = [CBGalleryDetailStyleConfig sharedConfig].bottomBarPhotoCountTextColor;
    self.imageCountLabel.font = [CBGalleryDetailStyleConfig sharedConfig].bottomBarPhotoCountFont;
}

@end