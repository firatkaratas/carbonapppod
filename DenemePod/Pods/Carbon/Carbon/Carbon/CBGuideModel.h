//
//  CBGuideModel.h
//  Carbon
//
//  Created by Semih Cihan on 08/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"

/**
 @description Represents an item that belongs to a day in the guide.
 */
@interface CBGuideModel : CBBaseModel

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSURL *imageUrl;
@property (strong, nonatomic) NSString *tag;
@property (strong, nonatomic) NSString *time;

@end
