//
//  CBPListReader.m
//  Carbon
//
//  Created by Necati Aydın on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"
#import <UIKit/UIKit.h>
#import "NSObject+Property.h"
#import "UIColor+Additions.h"
#import "UIFont+Additions.h"

@interface CBBaseConfig ()

/**
 @description When set to YES and when there is no matching value for a property in the plist, the value of the property is set to the name of the property by default.
 */
@property (assign, nonatomic) BOOL useDefaultsForNils;

@property (assign, nonatomic) BOOL isMapped;

@end

@implementation CBBaseConfig

- (instancetype)initByUsingDefaultsForNils {
    self = [super init];
    
    if(self)
    {
        self.useDefaultsForNils = YES;
    }
    
    return self;
}

+ (instancetype)sharedConfig
{
    CBBaseConfig *config = [super sharedConfig];
    if(!config.isMapped)
    {
        [config mapPlistToClass];
        config.isMapped = YES;
    }
    
    return config;
}

- (NSDictionary *)plistDictionary
{
    if(!super.plistDictionary)
    {
        if(self.useDefaultsForNils)
        {
            super.plistDictionary = [self propertyDictionaryByUsingDefaultForNils];
        }
        else
        {
            super.plistDictionary = [self propertyDictionary];
        }
    }
    
    return super.plistDictionary;
}

/**
 Maps all the plist dictionary values to class properties.
 */
- (void)mapPlistToClass
{
    if([self class] == [CBBaseConfig class])//do not map for the base class
    {
        return;
    }
    
    NSArray *names = [self allPropertyNames];
    NSDictionary *dict = self.plistDictionary;
    
    for(NSString *propertyName in names)
    {
        NSObject *object = dict[propertyName];
        
        if (self.useDefaultsForNils) {
            if (object && [object isKindOfClass:[NSString class]] && [(NSString *)object isEqualToString:@""]) {
                object = nil;
            }
            [self mapObject:(object ? object : propertyName) toPropertyKey:propertyName];
        } else {
            if(!object)
            {
                NSAssert(NO, @"Given key \"%@\" in config file does not exist in the plist %@.", propertyName, NSStringFromClass([self class]));
            }
            [self mapObject:object toPropertyKey:propertyName];
        }
    }
}

/**
 Maps the given object to the property with the given name.
 @param obj Object to be mapped.
 @param propertyName Name of the property to be mapped.
 */
- (void)mapObject:(id)obj toPropertyKey:(NSString *)propertyName
{
    Class propertyClass = [self classOfPropertyNamed:propertyName];
    if(propertyClass && [self classOfPropertyNamed:propertyName] == [UIColor class])
    {
        NSString *colorString = obj;
        UIColor *color = [UIColor colorWithCommaSeparatedString:colorString];
        if(!color)
        {
            color = [UIColor colorByReadingFromPaletteWithString:colorString];
        }
        [self setValue:color forKey:propertyName];
    }
    else if(propertyClass && [self classOfPropertyNamed:propertyName] == [UIFont class])
    {
        NSString *fontString = obj;
        UIFont *font = [UIFont fontWithCommaSeparatedString:fontString];
        if(!font)
        {
            font = [UIFont fontByReadingFromPaletteWithString:fontString];
        }
        
        NSAssert(font != nil, @"Error loading the font with the given config key: %@",fontString);
        
        [self setValue:font forKey:propertyName];
    }
    else
    {
        [self setValue:obj forKey:propertyName];
    }
}

@end
