//
//  CBGuideDayModelConfig.h
//  Carbon
//
//  Created by Semih Cihan on 08/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@interface CBGuideDayModelConfig : CBBaseModelConfig

/**
 Date of the weekly guide.
 */
@property (strong, nonatomic) NSString *date;

/**
 Guide items of the guide, each item represents a day.
 */
@property (strong, nonatomic) NSString *guideItems;

/**
 An integer between 0 and 6 to indicate the day of the week. 0 for Monday, 6 for Sunday.
 */
@property (strong, nonatomic) NSString *dayOfTheWeek;

@end
