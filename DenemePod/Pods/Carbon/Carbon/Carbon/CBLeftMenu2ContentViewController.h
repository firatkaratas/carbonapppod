//
//  CBLeftMenu2ContentViewController.h
//  Carbon
//
//  Created by Semih Cihan on 18/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu.h"

@class CBLeftMenu2ContentViewController;


@protocol CBLeftMenu2ContentViewControllerDelegate <NSObject>

/**
 Called when a category is selected.
 */
- (void)leftMenuContent:(CBLeftMenu2ContentViewController *)leftMenuContentViewController didSelectCategory:(CBCategoryModel *)categoryModel;

/**
 Called when the back button is tapped.
 */
- (void)leftMenuContentTappedBackButton:(CBLeftMenu2ContentViewController *)leftMenuViewController;

@end


/**
 Displays the subcategories for the given root category.
 */
@interface CBLeftMenu2ContentViewController : UIViewController

/**
 Root category for the content view. It should not be necesserily the same as the systems root category. Multiple instances of this class can be used by using recursion among categories.
 */
@property (strong, nonatomic) CBCategoryModel *rootCategory;

/**
 Delegate for the view controller.
 */
@property (weak, nonatomic) id<CBLeftMenu2ContentViewControllerDelegate> delegate;

/**
 Back button to be used to go to the previous content view controller.
 */
@property (nonatomic, weak) IBOutlet UIButton *backButton;

/**
 Selects the first row in the table view. It only affects UI. It does not trigger selection related events.
 */
- (void)selectFirstRow;

/**
 Hides the top bar which includes the title.
 */
- (void)hideTopBar;

@end
