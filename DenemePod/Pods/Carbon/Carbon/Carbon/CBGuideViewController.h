//
//  CBLiveStreamViewController.h
//  Carbon
//
//  Created by Necati Aydın on 08/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBSeamlessViewController.h"
#import "CBGuideLogic.h"
#import "CBLeftMenu.h"

@class CBLeftMenuContainerViewController;

@interface CBGuideViewController : CBSeamlessViewController <CBDataLoaderDelegate>

@property (nonatomic, strong) CBGuideLogic *logic;
@property (nonatomic, weak) id<CBMenuContentViewControllerDelegate> delegate;

@end
