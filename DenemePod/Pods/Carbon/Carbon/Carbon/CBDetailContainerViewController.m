//
//  CBDetailContainerViewController.m
//  Carbon
//
//  Created by Necati Aydın on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBDetailContainerViewController.h"
#import "CBDetailContainerLogic.h"
#import "CBNavigationBarStyler.h"
#import "CBBaseLogic.h"
#import "CBNewsDetailViewController.h"
#import "CBVideoDetailViewController.h"
#import "CBViewControllerBuilder.h"
#import "CBVideoDetailStyleConfig.h"
#import "CBAuthorDetailLogic.h"

@interface CBDetailContainerViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@end

@implementation CBDetailContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [CBNavigationBarStyler styleWithTitleLogo:self.navigationItem];
    [CBNavigationBarStyler styleNavigationItemWithBackIcon:self.navigationItem
                                                        target:self
                                                        action:@selector(backButtonTapped:)];
    
    self.dataSource = self;
    self.delegate = self;
    
    UIViewController *vc = [self detailViewControllerWithLogic:[self.logic logicWithIndex:self.currentIndex] direction:UIPageViewControllerNavigationDirectionForward];
    if([vc conformsToProtocol:@protocol(CBDetailContainedViewControllerProtocol)])
    {
        [self.navigationController setNavigationBarHidden:[((id<CBDetailContainedViewControllerProtocol>)vc) shouldHideNavigationBar] animated:NO];
    }

    [self setViewControllers:@[vc]
                   direction:UIPageViewControllerNavigationDirectionForward
                    animated:NO
                  completion:nil];

}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[CBBaseViewController class]]) {

        return [self detailViewControllerWithLogic:(CBBaseLogic *)[self.logic logicAfterLogic:((CBBaseViewController *)viewController).logic] direction:UIPageViewControllerNavigationDirectionForward];
    } else {
        
        return nil; //returning nil will prevent willTransitionToViewControllers to be called
    }
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[CBBaseViewController class]]) {
     
        return [self detailViewControllerWithLogic:(CBBaseLogic *)[self.logic logicBeforeLogic:((CBBaseViewController *)viewController).logic] direction:UIPageViewControllerNavigationDirectionReverse];
    } else {
        
        return nil; //returning nil will prevent willTransitionToViewControllers to be called
    }
}

- (UIViewController *)detailViewControllerWithLogic:(id)logic direction:(UIPageViewControllerNavigationDirection)direction
{
    if (logic != nil || direction == UIPageViewControllerNavigationDirectionReverse) {
        
        if ([logic class] == [CBBaseNewsDetailLogic class]) {
            
            return [CBViewControllerBuilder newsDetailViewControllerWithLogic:(CBBaseNewsDetailLogic *)logic];
        } else if ([logic class] == [CBVideoDetailLogic class]) {
            
            return [CBViewControllerBuilder videoDetailViewControllerWithLogic:(CBVideoDetailLogic *)logic];
        } else if ([logic class] == [CBAuthorDetailLogic class]) {
            
            return [CBViewControllerBuilder authorDetailViewControllerWithLogic:(CBAuthorDetailLogic *)logic];
        }
        else {
            
            return [[UIViewController alloc] init]; //asking for the previos vc of the first feed detail
        }
    } else { // this case happens when asking for the view controller after the last feed
        
        return nil;
    }
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    UIViewController *vc = self.viewControllers[0];
    if([vc conformsToProtocol:@protocol(CBDetailContainedViewControllerProtocol)])
    {
        [self.navigationController setNavigationBarHidden:[((id<CBDetailContainedViewControllerProtocol>)vc) shouldHideNavigationBar] animated:NO];
    }
    else
    {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    }
}


- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers {
    
    UIViewController *vc = pendingViewControllers[0];
    if ([vc isKindOfClass:[CBBaseViewController class]]) {
        
        NSInteger transitioningIndex = [self.logic indexOfLogic:((CBBaseViewController *)pendingViewControllers[0]).logic]; //candidate index to be shown
        if(transitioningIndex < self.currentIndex)// if going back pop vc
        {
            [self backButtonTapped:nil];
        }
        else
        {
            self.currentIndex = transitioningIndex;
        }
    }
    else
    {
        
        [self backButtonTapped:nil]; // called for the previos vc of the first feed detail, should pop vc
    }

}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - action

- (void)backButtonTapped:(id)sender
{
    //ios7 bugfix: If you use [self.navigationController popViewControllerAnimated:YES] then the system still tries to show the previous vc. In ios8 view controller pops before trying to show it, so there is no problem.
    NSMutableArray *viewControllers = [self.navigationController.viewControllers mutableCopy];
    [viewControllers removeLastObject];
    [self.navigationController setViewControllers:viewControllers animated:YES];
}

@end

