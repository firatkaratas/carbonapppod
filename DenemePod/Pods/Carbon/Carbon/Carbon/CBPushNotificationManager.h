//
//  CBPushNotificationManager.h
//  Carbon
//
//  Created by Necati Aydin on 5.3.15.
//  Copyright (c) 2014 Mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBPushNotificationManager : NSObject

/**
 Returns the current situation for notification enabling.
 */
+ (BOOL)notificationsTurnedOn;

/**
 Turns on the push notifications.
 */
+ (void)turnOnNotifications;

/**
 Turns off the notifications.
 */
+ (void)turnOffNotifications;

/**
 Should be used in the app delegate's (application:didFinishLaunchingWithOptions) method.
 @return NO if it starts with a push notification, YES if it is a regular start;
 */
+ (BOOL)handleDidFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

/**
 Should be used in the app delegate's (application:didRegisterForRemoteNotificationsWithDeviceToken) method.
 */
+ (void)handleDidRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;

/**
 Should be used in the app delegate's (application:didReceiveRemoteNotification) method.
 */
+ (void)handleDidReceiveRemoteNotification:(NSDictionary *)userInfo;

@end
