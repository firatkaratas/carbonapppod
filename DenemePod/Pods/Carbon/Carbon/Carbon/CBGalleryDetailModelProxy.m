//
//  CBGalleryDetailModelProxy.m
//  Carbon
//
//  Created by Semih Cihan on 17/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGalleryDetailModelProxy.h"

@implementation CBGalleryDetailModelProxy

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[CBGalleryDetailModelProxy alloc] init];
    });
    return sharedInstance;
}

- (id)detailIdJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(detailIdJSONTransformerOverridden:)]) {
        return [self detailIdJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)categoryJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(categoryJSONTransformerOverridden:)]) {
        return [self categoryJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)titleJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(titleJSONTransformerOverridden:)]) {
        return [self titleJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)dateJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(dateJSONTransformerOverridden:)]) {
        return [self dateJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)urlJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(urlJSONTransformerOverridden:)]) {
        return [self urlJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)photosJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(photosJSONTransformerOverridden:)]) {
        return [self photosJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

@end
