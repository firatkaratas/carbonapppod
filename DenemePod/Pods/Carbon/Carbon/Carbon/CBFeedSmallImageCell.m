//
//  CBFeedSmallImageCell.m
//  Carbon
//
//  Created by Semih Cihan on 10/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedSmallImageCell.h"
#import "UIView+Separator.h"

@implementation CBFeedSmallImageCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.separators = [self addFrameWithLineWidth:kCellBorderLineWidth];
}

+ (CGFloat)cellRatio
{
    return 100.f/300.f;
}

+ (CGSize)sizeForWidth:(CGFloat)width
{
    return CGSizeMake(width, width * [CBFeedSmallImageCell cellRatio]);
}

@end
