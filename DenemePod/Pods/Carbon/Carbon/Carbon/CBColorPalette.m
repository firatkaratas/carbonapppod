//
//  CBColorPalette.m
//  Carbon
//
//  Created by Necati Aydın on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBColorPalette.h"
#import "UIColor+Additions.h"

@implementation CBColorPalette

+ (UIColor *)colorWithKey:(NSString *)key
{
    CBColorPalette *palette = [self sharedConfig];
    NSString *colorString = [palette.plistDictionary objectForKey:key];
    return [UIColor colorWithCommaSeparatedString:colorString];
}

@end
