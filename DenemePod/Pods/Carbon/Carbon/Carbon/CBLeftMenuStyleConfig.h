//
//  CBLeftMenuStyleConfig.h
//  Carbon
//
//  Created by naydin on 19.02.2015.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBLeftMenuStyleConfig : CBBaseConfig

/**
 Background color for the view.
 */
@property (nonatomic, strong) UIColor *viewBackgroundColor;

/**
 Title color for the page header.
 */
@property (nonatomic, strong) UIColor *headerTitleColor;

/**
  Title font for the page header.
 */
@property (nonatomic, strong) UIFont *headerTitleFont;

/**
 Text color for the menu item's text.
 */
@property (nonatomic, strong) UIColor *menuItemTextColor;

/**
 Text font for the menu item's text.
 */
@property (nonatomic, strong) UIFont *menuItemTextFont;

/**
 Text color for the menu item's text for highlighted state.
 */
@property (nonatomic, strong) UIColor *menuItemHighlightTextColor;

/**
 Text font for the menu item's text for highlighted state.
 */
@property (nonatomic, strong) UIFont *menuItemHighlightTextFont;

/**
 Separator color between the page header and the menu.
 */
@property (nonatomic, strong) UIColor *separatorColor;

/**
 Color for the menu item highlighting view.
 */
@property (nonatomic, strong) UIColor *highlightViewColor;

/**
 Tint color for the arrow image.
 */
@property (nonatomic, strong) UIColor *arrowColor;

/**
 Tint color for the arrow image highlighted state.
 */
@property (nonatomic, strong) UIColor *highlightArrowColor;

@end
