//
//  AppDelegate.m
//  CarbonApp
//
//  Created by Necati Aydın on 29/12/14.
//  Copyright (c) 2014 mobilike. All rights reserved.
//

#import "CBAppDelegate.h"
#import "CBViewControllerBuilder.h"
#import "AppDelegateTaskManager.h"
#import "CBPushNotificationManager.h"
#import "CBPushModel.h"
#import "CBFeedModel.h"

@interface CBAppDelegate ()

@end

@implementation CBAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [AppDelegateTaskManager applicationDidFinishLaunchingTasks];


    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    BOOL regularStart = [CBPushNotificationManager handleDidFinishLaunchingWithOptions:launchOptions];
    
    if(regularStart)
    {

        self.window.rootViewController = [CBViewControllerBuilder rootViewController];
    }

    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Orientation

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

#pragma mark - Push

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [CBPushNotificationManager handleDidRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [CBPushNotificationManager handleDidReceiveRemoteNotification:userInfo];
}

- (void)startAppWithPush:(CBPushModel *)pushModel
{
    UIViewController *vc = nil;
    if(pushModel.feedType == FeedTypeArticle)
    {
        vc = [CBViewControllerBuilder newsDetailViewControllerForPushWithFeedId:pushModel.feedId];
    }
    else if(pushModel.feedType == FeedTypeGallery)
    {
        vc = [CBViewControllerBuilder galleryViewControllerForPushWithFeedId:pushModel.feedId];
    }
    else if(pushModel.feedType == FeedTypeVideo)
    {
        vc = [CBViewControllerBuilder videoDetailViewControllerForPushWithFeedId:pushModel.feedId];
    }
    else if(pushModel.feedType == FeedTypeAuthor)
    {
        vc = [CBViewControllerBuilder authorDetailViewControllerForPushWithFeedId:pushModel.feedId];
    }
    [[UIApplication sharedApplication] delegate].window.rootViewController = vc;
}

@end
