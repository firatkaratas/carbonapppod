//
//  CBFeed3Cell.m
//  Carbon
//
//  Created by Necati Aydın on 06/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeed3Cell.h"

@implementation CBFeed3Cell

+ (CGFloat)cellRatio
{
    return 170.f/300.f;
}

@end
