//
//  CBHTTPRequestOperationManager.h
//  Carbon
//
//  Created by Semih Cihan on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"
@class CBNewsDetailModel, CBVideoDetailModel, CBGalleryDetailModel, CBCategoryModel,CBAuthorDetailModel, CBNetworkRequest;

typedef void (^SuccessBlockWithOperation)(AFHTTPRequestOperation *operation, id responseObject);
typedef void (^ErrorBlockWithOperation)(AFHTTPRequestOperation *operation, NSError *error);
typedef void (^FailureBlock)(NSError *error);

@interface CBNetworkManager : AFHTTPRequestOperationManager

/**
 Returns the singleton shared instance.
 @return Singleton object.
 */
+ (instancetype)sharedInstance;

/**
 Override this method to make all the configurations you want to make on the CBNetworkManager knowing it is a subclass of AFHTTPRequestOperationManager.
 */
- (void)makeInitializationConfigurations;

/**
 Cancels all the requests in the operation queue of the CBNetworkManager.
 @return void
 */
- (void)cancelRunningServices;

/**
 Cancels all the network requests with the given instance's url.
 @param request CBNetworkRequest instance to be cancelled.
 */
- (void)cancelRunningServicesWithRequest:(CBNetworkRequest *)request;

/**
 Makes a network request and handles the response with the given success and failure blocks.
 @param request The request object.
 @param success The block to be executed if the request is successfull.
 @param failure The block to be executed if the request is not successfull.
 */
- (void)makeRequest:(CBNetworkRequest *)request
       successBlock:(SuccessBlockWithOperation)success
       failureBlock:(ErrorBlockWithOperation)failure;

/**
 Fetches news detail data from the server.
 @param detailId News detail id.
 @param successBlock The block that will be executed when/if the request successfully finishes.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getNewsDetailById:(NSString *)detailId
             successBlock:(void (^)(CBNewsDetailModel *categoryModel))successBlock
             failureBlock:(FailureBlock)failureBlock;

/**
 Fetches video detail data from the server.
 @param detailId News detail id.
 @param successBlock The block that will be executed when/if the request successfully finishes.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getVideoDetailById:(NSString *)detailId
             successBlock:(void (^)(CBVideoDetailModel *videoDetailModel))successBlock
             failureBlock:(FailureBlock)failureBlock;

/**
 Fetches author detail data from the server.
 @param detailId News detail id.
 @param successBlock The block that will be executed when/if the request successfully finishes.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getAuthorDetailById:(NSString *)detailId
               successBlock:(void (^)(CBAuthorDetailModel *authorDetailModel))successBlock
               failureBlock:(FailureBlock)failureBlock;

/**
 Fetches comments of a news detail from the server.
 @param detailId Detail id of the feed associated with the comments being fetched.
 @param successBlock The block that will be executed when/if the request successfully finishes. Objects in NSArray of successBlock are of class CBCommentModel.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getCommentsByDetailId:(NSString *)detailId
                 successBlock:(void (^)(NSArray *comments))successBlock
                 failureBlock:(FailureBlock)failureBlock;

/**
 Fetches gallery detail for the given feedId from the server.
 @param feedId Feed ID.
 @param successBlock The block that will be executed when/if the request successfully finishes.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getGalleryDetailById:(NSString *)feedId
                       successBlock:(void (^)(CBGalleryDetailModel *galleryDetailModel))successBlock
                       failureBlock:(FailureBlock)failureBlock;

/**
 Fetches categories from the server.
 @param successBlock The block that will be executed when/if the request successfully finishes.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getCategoriesWithSuccessBlock:(void (^)(NSArray *categories))successBlock
                     failureBlock:(FailureBlock)failureBlock;
/**
 Fetches feeds of the category with the given categoryId from the server.
 @param categoryId Id of the category to be fetched.
 @param pageNumber Indicates the page of the feeds to be fetched.
 @param pageSize Indicates the page size of the feeds to be fetched.
 @param successBlock The block that will be executed when/if the request successfully finishes. Objects in NSArray of successBlock are of class CBFeedModel.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getCategoryFeedWithCategoryId:(NSString *)categoryId
                           pageNumber:(NSString *)pageNumber
                             pageSize:(NSString *)pageSize
                         successBlock:(void (^)(NSArray *feeds))successBlock
                         failureBlock:(FailureBlock)failureBlock;

/**
 Fetches feeds of the author with the given authorId from the server.
 @param authorId Id of the author to be fetched.
 @param pageNumber Indicates the page of the feeds to be fetched.
 @param pageSize Indicates the page size of the feeds to be fetched.
 @param successBlock The block that will be executed when/if the request successfully finishes. Objects in NSArray of successBlock are of class CBFeedModel.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getAuthorFeedWithAuthorId:(NSString *)authorId
                       pageNumber:(NSString *)pageNumber
                         pageSize:(NSString *)pageSize
                     successBlock:(void (^)(NSArray *))successBlock
                     failureBlock:(FailureBlock)failureBlock;

/**
 Fetches live stream url. If a direct url is given it returns the liveStreamDirectUrl in the NetworkConfig.plist. Otherwise it uses the liveStreamUrl to fetch the url.
 @param successBlock The block that will be executed when/if the request successfully finishes.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getLiveStreamWithSuccessBlock:(void (^)(NSURL *streamUrl))successBlock
                         failureBlock:(FailureBlock)failureBlock;

/**
 Fetches tv guide.
 @param successBlock The block that will be executed when/if the request successfully finishes. Guide array holds 7 guide objects.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getGuideWithSuccessBlock:(void (^)(NSArray *guide))successBlock
                         failureBlock:(FailureBlock)failureBlock;

#pragma mark - Network handlers

/**
 Handler for the get request. This method can be overridden in the subclasses for customized handling, or can be used for custom network call.
 @param responseObject Response dictionary.
 @param error Error pointer.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handleGetWithOperation:(AFHTTPRequestOperation *)operation
                responseObject:(id)responseObject
                         error:(NSError *)error
                  successBlock:(SuccessBlockWithOperation)success
                  failureBlock:(ErrorBlockWithOperation)failure;

/**
 Handler for the post request. This method can be overridden in the subclasses for customized handling, or can be used for custom network call.
 @param responseObject Response dictionary.
 @param error Error pointer.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handlePostWithOperation:(AFHTTPRequestOperation *)operation
                 responseObject:(id)responseObject
                          error:(NSError *)error
                   successBlock:(SuccessBlockWithOperation)success
                   failureBlock:(ErrorBlockWithOperation)failure;

@end
