//
//  CBHomeLogic.m
//  Carbon
//
//  Created by Semih Cihan on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedLogic.h"
#import "CBNetworkManager.h"
#import "CBFeedModel.h"
#import "CBDetailContainerLogic.h"
#import "CBGalleryLogic.h"

@interface CBFeedLogic ()

/**
 @description YES if loading in progress, NO otherwise.
 */
@property (assign, nonatomic) BOOL isLoading;

/**
 @description NO if there is no more data to load, YES otherwise.
 */
@property (assign, nonatomic) BOOL hasMorePageToLoad;

@end

@implementation CBFeedLogic
@dynamic delegate;

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        _isLoading = NO;
        _hasMorePageToLoad = YES;
        _currentPage = 0;
        _data = [@[] mutableCopy];
    }
    return self;
}

- (CBBaseLogic *)getLogicWithIndex:(NSInteger)index
{
    CBFeedModel *feed = self.data[index];
    if(feed.type == FeedTypeGallery)
    {
        CBGalleryLogic *logic = [[CBGalleryLogic alloc] init];
        logic.feedId = feed.feedId;
        return logic;
    }
    else
    {
        NSArray *feeds = [self feedModelsByExcludingGalleryStartingFromIndex:index];
        CBDetailContainerLogic *logic = [[CBDetailContainerLogic alloc] initWithFeedModels:feeds];
        return logic;
    }
}

- (NSInteger)findFeedId:(NSString *)feedId inFeedModels:(NSArray *)feeds
{
    for(NSInteger i = 0; i < feeds.count; i++)
    {
        CBFeedModel *feed = feeds[i];
        if([feedId isEqualToString:feed.feedId])
        {
            return i;
        }
    }
    return  -1;
}

- (NSArray *)feedModelsByExcludingGalleryStartingFromIndex:(NSInteger)index
{
    NSMutableArray *feeds = [NSMutableArray new];
    NSArray *tempFeeds = [self.data subarrayWithRange:NSMakeRange(index, self.data.count - index)];
    for(CBFeedModel *feed in tempFeeds)
    {
        if([feed isKindOfClass:[CBBaseModel class]] && feed.type != FeedTypeGallery)
        {
            [feeds addObject:feed];
        }
    }
    return feeds;
}

- (void)makeFetchRequestWithId:(NSString *)categoryId page:(NSInteger)page successBlock:(void (^)(NSArray *feeds))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
    [[CBNetworkManager sharedInstance] getCategoryFeedWithCategoryId:categoryId pageNumber:[@(page) stringValue] pageSize:nil successBlock:successBlock failureBlock:failureBlock];
}

- (void)loadDataByRefreshing:(BOOL)refresh
{
    self.isLoading = YES;
    
    __weak typeof(self) weakSelf = self;
    
    [self makeFetchRequestWithId:self.category.categoryId
                            page:refresh ? 0 : self.currentPage
                    successBlock:^(NSArray *feeds)
     {
         weakSelf.hasMorePageToLoad = (feeds && feeds.count > 0);

         if (refresh) {
             [weakSelf.data removeAllObjects];
             weakSelf.currentPage = 0;
         }
         
         NSInteger location = weakSelf.data.count;
         [weakSelf.data addObjectsFromArray:feeds];
         if (weakSelf.data.count == 0) {
             [weakSelf.delegate dataLoadedWithError:NSLocalizedString(@"No news in the feed.", nil)];
         } else {
             [weakSelf.delegate dataLoadedForRange:NSMakeRange(location, feeds.count)];
         }
         weakSelf.isLoading = NO;
     } failureBlock:^(NSError *error) {
         [weakSelf.delegate dataLoadedWithError:NSLocalizedString(@"Unable to fetch the news.", nil)];
         weakSelf.isLoading = NO;
     }];
}

- (void)getLiveStreamUrl {
    self.isLoading = YES;

    __weak typeof(self) weakSelf = self;
    [[CBNetworkManager sharedInstance] getLiveStreamWithSuccessBlock:^(NSURL *liveStreamUrl)
     {
        
         [weakSelf.delegate liveStreamUrlLoaded:liveStreamUrl];
         weakSelf.isLoading = NO;
     } failureBlock:^(NSError *error) {
         
         [weakSelf.delegate liveStreamLoadedWithError:NSLocalizedString(@"Unable to fetch the live stream url.", nil)];
         weakSelf.isLoading = NO;
     }];
}

#pragma mark - CBDataLoaderWithPagingAndRefreshProtocol

- (BOOL)loadNextPage {
    
    if (self.hasMorePageToLoad && !self.isLoading) {
        self.isLoading = YES;
        self.currentPage++;
        [self loadDataByRefreshing:NO];
        [self.delegate dataLoadingWithPaging];
    }
    
    return self.hasMorePageToLoad;
}

- (void)refreshData {
    if (!self.isLoading) {
        [self loadDataByRefreshing:YES];
        [self.delegate dataLoading];
    }
}

- (void)loadData
{
    [self loadDataByRefreshing:NO];
    [self.delegate dataLoading];
}

@end
