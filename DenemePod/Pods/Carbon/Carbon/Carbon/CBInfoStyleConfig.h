//
//  CBInfoStyleConfig.h
//  Carbon
//
//  Created by Semih Cihan on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBInfoStyleConfig : CBBaseConfig

/**
 Background color of the view.
 */
@property (strong, nonatomic) UIColor *backgroundColor;

/**
 Cell label color. NSString because we use this for a webview.
 */
@property (strong, nonatomic) NSString *cellLabelColor;

/**
 Cell label font.
 */
@property (strong, nonatomic) UIFont *cellLabelFont;

/**
 Cell background color.
 */
@property (strong, nonatomic) UIColor *cellBackgroundColor;

/**
 Separator color for the bottom bar.
 */
@property (strong, nonatomic) UIColor *separatorColor;

/**
 Follow us label text color.
 */
@property (nonatomic, strong) UIColor *bottomBarTextColor;

/**
 Follow us label font.
 */
@property (nonatomic, strong) UIFont *bottomBarTextFont;

@end
