//
//  CBNetworkRequestBuilder.h
//  Carbon
//
//  Created by Necati Aydın on 26/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBNetworkRequest.h"

@class CBNetworkConfig;

/**
 Network request builder class. You can write a category for NetworkRequestBuilder, an empty subclass of this class, to override the  methods of this class.
 */
@interface CBNetworkRequestBuilder : NSObject

/**
 Set the config object for network urls. The default value is read from the config plist if not set.
 @param config
 */
+ (void)setConfig:(CBNetworkConfig *)config;

/**
 Creates a CBNetworkRequest instance for the news detail request.
 @param detailId Detail Id.
 @return CBNetworkRequest instance.
 */
+ (CBNetworkRequest *)newsDetailRequestWithId:(NSString *)detailId;

/**
 Creates a CBNetworkRequest instance for the video detail request.
 @param detailId Detail Id.
 @return CBNetworkRequest instance.
 */
+ (CBNetworkRequest *)videoDetailRequestWithId:(NSString *)detailId;

/**
 Creates a CBNetworkRequest instance for the author detail request.
 @param detailId Detail Id.
 @return CBNetworkRequest instance.
 */
+ (CBNetworkRequest *)authorDetailRequestWithId:(NSString *)detailId;

/**
 Creates a CBNetworkRequest instance for the comments request.
 @param detailId Detail Id.
 @return CBNetworkRequest instance.
 */
+ (CBNetworkRequest *)commentsRequestWithId:(NSString *)detailId;

/**
 Creates a CBNetworkRequest instance for the gallery detail request.
 @param detailId Detail Id.
 @return CBNetworkRequest instance.
 */
+ (CBNetworkRequest *)galleryDetailRequestWithId:(NSString *)detailId;

/**
 Creates a CBNetworkRequest instance for the categories request.
 @return CBNetworkRequest instance.
 */
+ (CBNetworkRequest *)cateogiresRequest;

/**
 Creates a CBNetworkRequest instance for the category feed request.
 @param categoryId Category Id.
 @param pageNumber Page number of the feed, if nil then it is considered as there is no paging in feeds.
 @param pageSize Page size of the feed, if page number is nil, then this is not used. If page number is not nil and page size is nil, then it is considered that the page size is specified by the server.
 @return CBNetworkRequest instance.
 */
+ (CBNetworkRequest *)categoryFeedRequestWithId:(NSString *)categoryId
                                     pageNumber:(NSString *)pageNumber
                                       pageSize:(NSString *)pageSize;

/**
 Creates a CBNetworkRequest instance for the author feed request.
 @param authorId Author Id.
 @param pageNumber Page number of the feed, if nil then it is considered as there is no paging in feeds.
 @param pageSize Page size of the feed, if page number is nil, then this is not used. If page number is not nil and page size is nil, then it is considered that the page size is specified by the server.
 @return CBNetworkRequest instance.
 */
+ (CBNetworkRequest *)authorFeedRequestWithId:(NSString *)authorId
                                   pageNumber:(NSString *)pageNumber
                                     pageSize:(NSString *)pageSize;

/**
 Creates a CBNetworkRequest instance for the live stream request.
 @return CBNetworkRequest instance.
 */
+ (CBNetworkRequest *)liveStreamRequest;

/**
 Creates a CBNetworkRequest instance for the guide request.
 @return CBNetworkRequest instance.
 */
+ (CBNetworkRequest *)guideRequest;

@end
