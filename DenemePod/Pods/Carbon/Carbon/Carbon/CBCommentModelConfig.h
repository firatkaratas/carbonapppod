//
//  CBCommentModelConfig.h
//  Carbon
//
//  Created by Semih Cihan on 20/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@interface CBCommentModelConfig : CBBaseModelConfig

/**
 Id for the comment.
 */
@property (strong, nonatomic) NSString *commentId;

/**
 Content of the comment.
 */
@property (strong, nonatomic) NSString *comment;

/**
 The date that comment has been made.
 */
@property (strong, nonatomic) NSString *date;

/**
 Username of the commentator.
 */
@property (strong, nonatomic) NSString *username;

@end
