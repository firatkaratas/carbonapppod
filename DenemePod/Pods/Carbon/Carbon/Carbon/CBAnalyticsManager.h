//
//  CBAnalyticsManager.h
//  Carbon
//
//  Created by Semih Cihan on 19/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBAnalyticsManager : NSObject

/**
 Start analytics tracking. Uses the tracking id values in AppConfig.plist
 */
+ (void)startTracking;

/**
 Track comment button tapped action. Category is set to "ui_action", action is set to "comment_press".
 */
+ (void)commentButtonTapped;

/**
 Track share button tapped action. Category is set to "ui_action", action is set to "share_press".
 */
+ (void)shareButtonTapped;

/**
 Track video play button tapped action. Category is set to "ui_action", action is set to "video_play_press : videoCategory"
 @param videoCategory Category of the video.
 */
+ (void)videoPlayButtonTappedWithVideoCategory:(NSString *)videoCategory;

/**
 Track live stream play button tapped action. Category is set to "ui_action", action is set to "live_stream_press"
 */
+ (void)liveStreamPlayButtonTapped;

/**
 Track detail page opening action.
 @param feedCategory Category of the feed in which the news is shown.
 @param detailCategory Category of the news detail.
 @param newsType Type of the news: news, video, gallery
 */
+ (void)feedDetailActionWithCategoryOfTheFeed:(NSString *)feedCategory
                          categoryOfTheDetail:(NSString *)detailCategory
                                     newsType:(NSString *)newsType;
/**
 Track feed page opening action.
 @param feedCategory Category of the feed.
 */
+ (void)feedActionWithCategoryOfTheFeed:(NSString *)feedCategory;

/**
 Used to set the category name of the last opened feed page to be used in analytics.
 @param feedCategoryName category name of the last opened feed page.
 */
+ (void)setLastOpenedFeedCategoryName:(NSString *)feedCategoryName;

@end
