//
//  CBVideoDetailLogic.h
//  Carbon
//
//  Created by Semih Cihan on 06/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseNewsDetailLogic.h"
#import "CBVideoDetailModel.h"

@class CBVideoDetailModel;


@interface CBVideoDetailLogic : CBBaseNewsDetailLogic

/**
 CBVideoDetailModel object loaded by the logic.
 */
@property (nonatomic, strong) CBVideoDetailModel *newsDetail;

@end
