//
//  NSBundle+Additions.h
//  Carbon
//
//  Created by Necati Aydın on 17/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//


@interface NSBundle (Additions)

/**
 Bundle for the carbon framework. If the bundle is not loaded, it loads the bundle.
 */
+ (NSBundle *)frameworkBundle;

@end
