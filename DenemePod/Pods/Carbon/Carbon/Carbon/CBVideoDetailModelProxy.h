//
//  CBVideoDetailModelProxy.h
//  Carbon
//
//  Created by Semih Cihan on 16/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelProxy.h"

@protocol CBVideoDetailModelProxyProtocol <NSObject>

@optional

- (NSString *)detailIdJSONTransformerOverridden:(id)object;
- (NSString *)dateJSONTransformerOverridden:(id)object;
- (NSString *)categoryJSONTransformerOverridden:(id)object;
- (NSString *)titleJSONTransformerOverridden:(id)object;
- (NSString *)spotJSONTransformerOverridden:(id)object;
- (NSString *)urlJSONTransformerOverridden:(id)object;
- (NSString *)videoUrlJSONTransformerOverridden:(id)object;
- (NSString *)durationJSONTransformerOverridden:(id)object;
- (NSNumber *)viewCountJSONTransformerOverridden:(id)object;
- (NSString *)imageUrlJSONTransformerOverridden:(id)object;
- (NSString *)contentJSONTransformerOverridden:(id)object;
- (NSArray *)suggestedVideosJSONTransformerOverridden:(id)object;

@end


@interface CBVideoDetailModelProxy : CBBaseModelProxy <CBVideoDetailModelProxyProtocol>

+ (instancetype)sharedInstance;

- (id)detailIdJSONTransformer:(id)object;
- (id)dateJSONTransformer:(id)object;
- (id)categoryJSONTransformer:(id)object;
- (id)titleJSONTransformer:(id)object;
- (id)spotJSONTransformer:(id)object;
- (id)urlJSONTransformer:(id)object;
- (id)videoUrlJSONTransformer:(id)object;
- (id)durationJSONTransformer:(id)object;
- (id)viewCountJSONTransformer:(id)object;
- (id)imageUrlJSONTransformer:(id)object;
- (id)contentJSONTransformer:(id)object;
- (id)suggestedVideosJSONTransformer:(id)object;

@end
