//
//  CBGalleryPhotoModelProxy.m
//  Carbon
//
//  Created by Semih Cihan on 17/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGalleryPhotoModelProxy.h"

@implementation CBGalleryPhotoModelProxy

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[CBGalleryPhotoModelProxy alloc] init];
    });
    return sharedInstance;
}

- (id)photoIdJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(photoIdJSONTransformerOverridden:)]) {
        return [self photoIdJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)photoDescriptionJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(photoDescriptionJSONTransformerOverridden:)]) {
        return [self photoDescriptionJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)imageUrlJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(imageUrlJSONTransformerOverridden:)]) {
        return [self imageUrlJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

@end
