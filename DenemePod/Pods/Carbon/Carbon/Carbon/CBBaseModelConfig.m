//
//  CBBaseModelConfig.m
//  Carbon
//
//  Created by Semih Cihan on 16/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@implementation CBBaseModelConfig

- (instancetype)init
{
    return [super initByUsingDefaultsForNils];
}

@end
