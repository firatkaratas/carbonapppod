//
//  CBSeamlessViewController.h
//  Carbon
//
//  Created by Semih Cihan on 17/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"
#import <Seamless/Seamless.h>

/**
 This class is a base class for view controllers that shows seamless ads.
 */
@interface CBSeamlessViewController : CBBaseViewController <SLAdViewDelegate>

/**
 Collection view ad manager.
 */
@property (strong, nonatomic) SLCollectionViewAdManager *collectionViewAdManager;

/**
 Interstitial ad manager.
 */
@property (nonatomic, strong) SLInterstitialAdManager *interstitialAdManager;

/**
 MMA ad view.
 */
@property (nonatomic, strong) SLAdView *mmaView;

/**
 MRE ad view.
 */
@property (nonatomic, strong) SLAdView *mreView;

/**
 Creates and starts the interstitial ad manager.
 @param entityName Entity name to be concatenated at the end of the created entity name within the method.
 */
- (void)startInterstitialAdWithEntityName:(NSString *)entityName;

/**
 Creates and starts the MMA ad view.
 @param entityName Entity name to be concatenated at the end of the created entity name within the method.
 @code
 // Example SLAdViewDelegate methods for subclasses.
 
 - (void)adViewDidLoad:(SLAdView *)adView {
    [self.view addSubview:adView]; //add the view to the view hierarchy.
    [adView distance:0.f toBottomView:self.bottomBarView]; //CBSeamlessViewController class only gives height, width constraints of the view and centers it in the superview. So bottom or top constraint of the view should be given by the subclasses you implement.
    [super adViewDidLoad:adView]; //should call super at the end of the method.
 }
 
 - (void)adViewDidFailToLoad:(SLAdView *)adView {
    [super adViewDidFailToLoad:adView];
 }
 */
- (void)startMMAAdWithEntityName:(NSString *)entityName;

/**
 Creates and starts the MRE ad view.
 @param entityName Entity name to be concatenated at the end of the created entity name within the method.
 @code
 // Example SLAdViewDelegate methods for subclasses.
 
 - (void)adViewDidLoad:(SLAdView *)adView {
    [self.view addSubview:adView]; //add the view to the view hierarchy.
    [adView distance:0.f toBottomView:self.bottomBarView]; //CBSeamlessViewController class only gives height, width constraints of the view and centers it in the superview. So bottom or top constraint of the view should be given by the subclasses you implement.
    [super adViewDidLoad:adView]; //should call super at the end of the method.
 }
 
 - (void)adViewDidFailToLoad:(SLAdView *)adView {
    [super adViewDidFailToLoad:adView];
 }
 */
- (void)startMREAdWithEntityName:(NSString *)entityName;

/**
 Creates and starts the collection view ad manager.
 @param entityName Entity name to be concatenated at the end of the created entity name within the method.
 @param collectionView Collection view to be used by the manager.
 @param dataSource Data source to be used by the manager.
 */
- (void)startCollectionViewAdWithEntityName:(NSString *)entityName
                             collectionView:(UICollectionView *)collectionView
                                 dataSource:(NSMutableArray *)dataSource;

/**
 Presents the seamless video player modally.
 @param url Video url to be played.
 @param entityName Entity name to be concatenated at the end of the created entity name within the method.
 */
- (void)presentSeamlessVideoPlayerWithUrl:(NSURL *)url entityName:(NSString *)entityName;

@end
