//
//  CBSuggestedVideoModelProxy.h
//  Carbon
//
//  Created by Semih Cihan on 17/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelProxy.h"

@protocol CBSuggestedVideoModelProxyProtocol <NSObject>

@optional

- (NSString *)detailIdJSONTransformerOverridden:(id)object;
- (NSString *)categoryJSONTransformerOverridden:(id)object;
- (NSString *)titleJSONTransformerOverridden:(id)object;
- (NSString *)spotJSONTransformerOverridden:(id)object;
- (NSString *)imageUrlJSONTransformerOverridden:(id)object;

@end


@interface CBSuggestedVideoModelProxy : CBBaseModelProxy <CBSuggestedVideoModelProxyProtocol>

+ (instancetype)sharedInstance;

- (id)detailIdJSONTransformer:(id)object;
- (id)categoryJSONTransformer:(id)object;
- (id)titleJSONTransformer:(id)object;
- (id)spotJSONTransformer:(id)object;
- (id)imageUrlJSONTransformer:(id)object;

@end
