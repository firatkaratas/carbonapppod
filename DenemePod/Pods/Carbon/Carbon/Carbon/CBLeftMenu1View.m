//
//  CBLeftMenuView.m
//  Carbon
//
//  Created by naydin on 18.02.2015.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu1View.h"
#import "CBLeftMenu1TableViewCell.h"
#import "UIView+LayoutConstraints.h"
#import "UIView+NibLoading.h"
#import "CBLeftMenu1ContentView.h"


static const CGFloat kMaximumAllowedCellCount = 7;

@interface CBLeftMenu1View()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UIView *scrollViewContentView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *scrollViewContentWidth;

@property (nonatomic, strong) NSMutableArray *contents;

@property (nonatomic, copy) void (^scrollViewAnimationCompletion)();

@end

@implementation CBLeftMenu1View

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.contents = [NSMutableArray new];
    self.delegate = self;
}


#pragma mark - Public

- (void)selectFirstRowInFirstPage
{
    if (self.contents.count) {
        CBLeftMenu1ContentView *view = self.contents[0];
        if([view.tableView numberOfRowsInSection:0]) {
            [view.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
            [self.leftMenuDelegate leftMenuViewDidSelectFirstRow:self];
        }
    }
}

+ (CGFloat)height
{
    return [CBLeftMenu1ContentView heightForRowCount:kMaximumAllowedCellCount];
}

- (void)reload
{
    for(CBLeftMenu1ContentView *content in self.contents)
    {
        [content.tableView reloadData];
    }
}

- (void)pushContentViewAnimated:(BOOL)animated
{
    NSInteger pageCountBeforePush = self.contents.count;
    NSInteger pageCountAfterPush = pageCountBeforePush + 1;
    if(pageCountBeforePush != 0)
    {
        self.userInteractionEnabled = NO;
    }
    
    CBLeftMenu1ContentView *view = [CBLeftMenu1ContentView loadFromNIB];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    view.tableView.backgroundColor = [UIColor clearColor];
    [self.scrollViewContentView addSubview:view];
    
    [view distanceTopToSuperview:0];
    [view distanceBottomToSuperview:0];
    [view equalWidthWithSuperView:self];
    [view distanceLeftToSuperview:pageCountBeforePush * CGRectGetWidth(self.frame)];
    view.tableView.delegate = self;
    view.tableView.dataSource = self;
    
    [self.contents addObject:view];
    
    
    self.scrollViewContentWidth.constant = pageCountAfterPush * CGRectGetWidth(self.frame);
    __weak typeof(self) weakSelf = self;
    [self setContentOffset:CGPointMake(pageCountBeforePush * CGRectGetWidth(self.frame), 0)
                  animated:animated
                completion:^{

        weakSelf.userInteractionEnabled = YES;
        [weakSelf.leftMenuDelegate leftMenuView:weakSelf
                                  pageDisplayed:pageCountAfterPush
                             scrollingDirection:CBLeftMenu1ViewScrollingDirectionForward];
    }];
}

- (void)popContentViewAnimated:(BOOL)animated
{
    NSInteger pageCountBeforePop = self.contents.count;
    NSInteger pageCountAfterPop = pageCountBeforePop - 1;
    if(pageCountAfterPop <= 0 || [self shouldCancelPop])
    {
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [self setContentOffset:CGPointMake((pageCountAfterPop-1) * CGRectGetWidth(self.frame), 0)
                  animated:animated
                completion:^
    {
        CBLeftMenu1ContentView *view = [weakSelf.contents lastObject];
        [view removeFromSuperview];
        [weakSelf.contents removeLastObject];
        weakSelf.scrollViewContentWidth.constant = pageCountAfterPop * CGRectGetWidth(weakSelf.frame);
        [weakSelf.leftMenuDelegate leftMenuView:weakSelf
                                  pageDisplayed:pageCountAfterPop
                             scrollingDirection:CBLeftMenu1ViewScrollingDirectionBack];
    }];
}

#pragma mark - Helper

/**
 If a scrolling animation is going on, the page is somewhere in the middle. So the pop should be cancelled.
 */
- (BOOL)shouldCancelPop
{
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat page = self.contentOffset.x / width;
    NSInteger pageInt = page;
    
    return self.contentOffset.x != pageInt * width;
}

- (void)setContentOffset:(CGPoint)contentOffset animated:(BOOL)animated completion:(void (^)())completion
{
    if(animated)
    {
        if(CGPointEqualToPoint(contentOffset, self.contentOffset))
        {
            completion();
        }
        else
        {
            self.scrollViewAnimationCompletion = completion;
        }
    }
    else
    {
        completion();
    }


    [self setContentOffset:contentOffset animated:animated];
}

#pragma mark - Scroll View Delegate


//without dragging
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if(self.scrollViewAnimationCompletion)
    {
        self.scrollViewAnimationCompletion();
        self.scrollViewAnimationCompletion = nil;
    }

}

//with dragging
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == self)
    {
        NSInteger currentPage = scrollView.contentOffset.x / CGRectGetWidth(scrollView.frame);
        NSInteger contentCount = self.contents.count;
        
        for (NSInteger i = 0; i < contentCount - currentPage -1; i++)
        {
            CBLeftMenu1ContentView *view = [self.contents lastObject];
            [self.contents removeLastObject];
            [view removeFromSuperview];
        }
        
        self.scrollViewContentWidth.constant = self.contents.count * CGRectGetWidth(self.frame);
        

        if(currentPage + 1 < contentCount)//did go back
        {
            [self.leftMenuDelegate leftMenuView:self pageDisplayed:currentPage+1 scrollingDirection:CBLeftMenu1ViewScrollingDirectionBack];
        }
        else if(currentPage + 1 == contentCount)
        {
            [self.leftMenuDelegate leftMenuView:self pageDisplayed:currentPage+1 scrollingDirection:CBLeftMenu1ViewScrollingDirectionNone];
        }
        else
        {
            [self.leftMenuDelegate leftMenuView:self pageDisplayed:currentPage scrollingDirection:CBLeftMenu1ViewScrollingDirectionForward];
        }
    }
}

#pragma mark - Table View Delegate & Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kLeftMenuCellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.leftMenuDelegate leftMenuView:self numberOfRowsForPage:[self getIndexForTableView:tableView]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBLeftMenu1TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CBLeftMenu1TableViewCell class])];
    
    cell.titleLabel.text = [self.leftMenuDelegate leftMenuView:self
                                                  titleForPage:[self getIndexForTableView:tableView]
                                                           row:indexPath.row];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.stylingDelegate styleLeftMenuTableViewCell:cell];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.leftMenuDelegate leftMenuView:self didSelectRow:indexPath.row atPage:self.contents.count];
}

- (NSInteger)getIndexForTableView:(UITableView *)tableView
{
    for(NSInteger i = 0; i < self.contents.count; i++)
    {
        CBLeftMenu1ContentView *view  = self.contents[i];
        if(tableView == view.tableView)
        {
            return i;
        }
    }
    
    return -1;
}



@end
