//
//  CBBaseModel.m
//  Carbon
//
//  Created by Semih Cihan on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"
#import <limits.h>

@interface CBBaseModel ()

@property (nonatomic, strong) NSMutableDictionary *additionalValues;

@end

@implementation CBBaseModel

/**
 Default initializer is subclassed to use config's additional parameter mappings. If there is a config and it has additional parameter mappings, it is applied here.
 */
- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error
{
    self = [super initWithDictionary:dictionaryValue error:error];
    if(self)
    {
        CBBaseModelConfig *config = [self.class config];
        if(config)
        {
            NSDictionary *fields = [config additionalJSONKeyPathsByPropertyKey];
            if(fields)
            {
                [fields enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
                {
                    [self setAdditionalValue:dictionaryValue[obj] forKey:key];
                }];
            }

        }

    }
    return self;
}

+ (CBBaseModelConfig *)config
{
    //should be overriden in subclasses.
    return nil;
}

- (NSMutableDictionary *)additionalValues
{
    if(!_additionalValues)
    {
        _additionalValues = [NSMutableDictionary new];
    }
    
    return _additionalValues;
}

- (id)additionalValueForKey:(NSString *)key
{
    id ret = self.additionalValues[key];
    if(ret == [NSNull null])
    {
        return nil;
    }
    else
    {
        return ret;
    }
}

- (void)setAdditionalValue:(id)value forKey:(NSString *)key
{
    if(!value)
    {
        [self.additionalValues setObject:[NSNull null] forKey:key];
    }
    else
    {
        [self.additionalValues setObject:value forKey:key];
    }
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

+ (NSValueTransformer *)stringOrNumberToStringJSONTransformer
{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        return [self stringOrNumberToString:object];
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)stringOrNumberToNumberJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        return [self stringOrNumberToNumber:object];
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)stringBooleanNumberToNumberJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        return [self stringOrNumberToBoolNumber:object];
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSNumber *)stringOrNumberToBoolNumber:(id)object {
    
    if ([object isKindOfClass:[NSNumber class]]) {
        return object;
    } else if ([object isKindOfClass:[NSString class]]) {
        
        NSString *lowerCase = [object lowercaseString];
        NSArray *booleanYesStrings = @[@"yes", @"true", @"1"];
        
        if ([booleanYesStrings containsObject:lowerCase]) {
            return @YES;
        } else {
            return @NO;
        }
    } else {
        return @NO;
    }
    
}

+ (NSNumber *)stringOrNumberToNumber:(id)object {
    if ([object isKindOfClass:[NSString class]]) {
        return @([object integerValue]);
    } else if ([object isKindOfClass:[NSNumber class]]) {
        return object;
    } else {
        return nil;
    }
}

+ (NSString *)stringOrNumberToString:(id)object {
    if ([object isKindOfClass:[NSString class]]) {
        return object;
    } else if ([object isKindOfClass:[NSNumber class]]) {
        NSNumber *number = object;
        return [number stringValue];
    } else {
        return nil;
    }
}

- (void)setNilValueForKey:(NSString *)key
{
    //no super call to prevent crashes. (Trying to set a nil value crashes the app)
}

+ (NSString *)removeLinksInHtmlContent:(NSString *)content linkPattern:(NSString *)linkPattern {
    
    NSRange rangeOfPattern = [content rangeOfString:linkPattern];
    while (rangeOfPattern.location != NSNotFound) {
        
        NSRange rangeOfParagraphBeginning = [content rangeOfString:@"<a"
                                                           options:NSBackwardsSearch
                                                             range:NSMakeRange(0, rangeOfPattern.location)];
        if (rangeOfParagraphBeginning.location == NSNotFound) {
            break;
        }
        
        NSRange rangeOfParagraphEnding = [content rangeOfString:@"</a>"
                                                        options:0
                                                          range:NSMakeRange(rangeOfParagraphBeginning.location + rangeOfParagraphBeginning.length, content.length - rangeOfParagraphBeginning.location - rangeOfParagraphBeginning.length)];
        
        if (rangeOfParagraphEnding.location == NSNotFound) {
            break;
        }
        
        NSString *contentFirstPart = [content substringToIndex:rangeOfParagraphBeginning.location];
        
        NSString *contentLastPart = [content substringFromIndex:rangeOfParagraphEnding.location + rangeOfParagraphEnding.length];
        content = [NSString stringWithFormat:@"%@%@", contentFirstPart, contentLastPart];
        rangeOfPattern = [content rangeOfString:linkPattern];
        
    }
    
    return content;
}

+ (NSString *)removeLinksInHtmlContent:(NSString *)content linkPatternArray:(NSArray *)linkPatternArray {
    
    for (NSString *pattern in linkPatternArray) {
        content = [CBBaseModel removeLinksInHtmlContent:content linkPattern:pattern];
    }
    return content;
}

@end
