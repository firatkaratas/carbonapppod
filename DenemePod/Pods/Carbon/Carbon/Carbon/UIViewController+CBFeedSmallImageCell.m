//
//  UIViewController+CBFeedSmallImageCell.m
//  Carbon
//
//  Created by Semih Cihan on 10/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIViewController+CBFeedSmallImageCell.h"
#import "CBFeed4Cell.h"
#import "CBFeedModel.h"
#import "UIImageView+WebCache.h"
#import "CBSuggestedVideoModel.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIViewControllerCBFeedSmallImageCell)

@implementation UIViewController (CBFeedSmallImageCell)

- (void)configureFeedSmallImageCell:(CBFeed4Cell *)cell withFeedModel:(CBFeedModel *)feed
{
    cell.titleLabel.text = feed.title;
    cell.label2.text = feed.category.name ? feed.category.name : @"";
    cell.label3.text = feed.spot;
    [cell.imageView sd_setImageWithURL:feed.imageUrl];
}

- (void)configureFeedCell:(CBFeed4Cell *)cell withSuggestedVideoModel:(CBSuggestedVideoModel *)model
{
    cell.titleLabel.text = model.title;
    cell.label2.text = model.category.name ? model.category.name : @"";
    cell.label3.text = model.spot;
    
    __weak typeof(cell) weakCell = cell;
    [cell.imageView sd_setImageWithURL:model.imageUrl
                      placeholderImage:nil
                             completed:^(UIImage *image,
                                         NSError *error,
                                         SDImageCacheType cacheType,
                                         NSURL *imageURL)
     {
         BOOL useFetchedImage = !error && image;
         
         if (!useFetchedImage) {
             [weakCell.imageView setImage:[UIImage imageNamed:@"suggested_videos_placeholder" inBundle:[NSBundle frameworkBundle]]];
         }
     }];
}

@end
