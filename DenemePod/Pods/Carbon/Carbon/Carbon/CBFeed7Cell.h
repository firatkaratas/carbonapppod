//
//  CBFeed7Cell.h
//  Carbon
//
//  Created by Necati Aydın on 12/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedCell.h"

@interface CBFeed7Cell : CBFeedCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;


@end
