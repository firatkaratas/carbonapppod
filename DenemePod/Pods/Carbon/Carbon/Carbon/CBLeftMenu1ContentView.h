//
//  CBLeftMenuContentView.h
//  Carbon
//
//  Created by naydin on 19.02.2015.
//  Copyright (c) 2015 mobilike. All rights reserved.
//


static const CGFloat kLeftMenuCellHeight = 48;

/**
 Includes only a table view with the CBLeftMenu1TableViewCell.
 */
@interface CBLeftMenu1ContentView : UIView


@property (nonatomic, weak) IBOutlet UITableView *tableView;

/**
 Calculates the view height for the given row count.
 */
+ (CGFloat)heightForRowCount:(NSInteger)rowCount;

@end
