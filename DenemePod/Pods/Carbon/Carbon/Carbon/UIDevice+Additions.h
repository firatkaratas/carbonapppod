//
//  UIDevice+Additions.h
//  Carbon
//
//  Created by Necati Aydın on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice(Additions)

+ (BOOL)iOS8AndHigher;

@end
