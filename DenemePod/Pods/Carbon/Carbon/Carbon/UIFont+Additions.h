//
//  UIFont+Additions.h
//  Carbon
//
//  Created by Necati Aydın on 16/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIFont.h>

@interface UIFont(Additions)

/**
 @description Creates a font from the given string if it is in the format:"fontName,fontSize".
 @param fontString "fontName,fontSize".
 @return Created font.
 */
+ (UIFont *)fontWithCommaSeparatedString:(NSString *)fontString;

/**
 @description Creates a font from the given string if it is in the format:"paletteKey,fontSize".
 @param paletteKey The key to be used for CBFontPalette to fetch the font name.
 @return Created font.
 */
+ (UIFont *)fontByReadingFromPaletteWithString:(NSString *)paletteKey;


/**
 @description Creates a new font object by incrementing the font size as the given amount.
 @param fontSizeIncrementAmount Float to change the font size. This could be a negative integer to decrement the font size.
 @return A new UIFont object by updating the font size but same font family.
 */
- (UIFont *)fontByIncrementingSize:(CGFloat)fontSizeIncrementAmount;

@end
