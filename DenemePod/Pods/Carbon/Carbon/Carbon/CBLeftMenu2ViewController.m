//
//  CBLeftMenu2ViewController.m
//  Carbon
//
//  Created by Necati Aydın on 18/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu2ViewController.h"
#import "CBViewControllerBuilder.h"
#import "CBNavigationBarStyler.h"
#import "CBCategoryModel.h"
#import "UIImage+Additions.h"
#import "CBNavigationBarStyleConfig.h"
#import "CBLeftMenu2StyleConfig.h"

static const CGFloat kMenuOpacity = 0.85;
static const CGFloat kNavigationBarHeight = 44;
static const CGFloat kStatusBarHeight = 20;

@interface CBLeftMenu2ViewController () <CBLeftMenu2ContentViewControllerDelegate>

@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, strong) NSMutableArray *selectedCategories;
@property (nonatomic, strong) NSMutableArray *selectedViewControllers;
@property (nonatomic, strong) UIImageView *backgroundImageView;

@end

@implementation CBLeftMenu2ViewController

@synthesize rootCategory,leftMenuViewControllerDelegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [CBNavigationBarStyler styleWithTitleLogo:self.navigationItem];
    [CBNavigationBarStyler styleNavigationItemWithCloseIcon:self.navigationItem
                                                     target:self
                                                     action:@selector(closeButtonTapped:)];
    [CBNavigationBarStyler styleRightNavigationItem:self.navigationItem settingsButtonAction:@selector(settingsTapped:) infoButtonAction:@selector(infoTapped:) target:self];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:self.backgroundImage];
    CGRect imageViewFrame = self.view.frame;
    imageViewFrame.origin.y -= kNavigationBarHeight + kStatusBarHeight;
    imageView.frame = imageViewFrame;
    self.backgroundImageView = imageView;
    [self.view insertSubview:imageView atIndex:0];
    
    UIView *view = [[UIView alloc] initWithFrame:self.backgroundImageView.frame];
    view.backgroundColor = [[CBLeftMenu2StyleConfig sharedConfig].viewBackgroundColor colorWithAlphaComponent:kMenuOpacity];
    [self.view insertSubview:view aboveSubview:self.backgroundImageView];
    
    UIColor *clr = [[CBNavigationBarStyleConfig sharedConfig].backgroundColor colorWithAlphaComponent:kMenuOpacity];
    UIImage *img = [UIImage imageWithColor:clr];
    [self.navigationController.navigationBar setBackgroundImage:img
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    
    [self startViewControllersWithLogics];
}

- (void)startViewControllersWithLogics
{
    CBLeftMenu2ContentViewController *vc = (CBLeftMenu2ContentViewController *)[CBViewControllerBuilder leftMenu2ContentViewControllerWithDelegate:self rootCategory:self.rootCategory];
    UIViewController *dummyViewController = [[UIViewController alloc] init];
    [self setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [vc hideTopBar];
    
    self.selectedCategories = @[self.rootCategory,self.rootCategory.subcategories[0]].mutableCopy;
    self.selectedViewControllers = @[dummyViewController, vc].mutableCopy;

    self.currentPage = 1;
    
    [vc selectFirstRow];
}


- (void)leftMenuContent:(CBLeftMenu2ContentViewController *)leftMenuViewController didSelectCategory:(CBCategoryModel *)categoryModel
{
    //if a different category is selected from the same page, remove the extra ones
    self.selectedCategories = [self.selectedCategories subarrayWithRange:NSMakeRange(0, self.currentPage)].mutableCopy;
    
    [self.selectedCategories addObject:categoryModel];
    
    if([categoryModel hasSubcategory])
    {
        CBLeftMenu2ContentViewController *vc = (CBLeftMenu2ContentViewController *)[CBViewControllerBuilder leftMenu2ContentViewControllerWithDelegate:self rootCategory:categoryModel];
        [self.selectedViewControllers addObject:vc];
        
        [self setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionForward
                        animated:YES completion:nil];
        [vc.backButton setTitle:categoryModel.superCategory.name forState:UIControlStateNormal];
        self.currentPage++;
    }
    
    [self.leftMenuViewControllerDelegate leftMenu:self didSelectCategory:categoryModel];
}

- (void)leftMenuContentTappedBackButton:(CBLeftMenu2ContentViewController *)leftMenuViewController
{
    if(self.currentPage <= 1)
    {
        return;
    }

    //while going back, page number should be equal to selected categories and selected view controller count
    self.selectedCategories = [self.selectedCategories subarrayWithRange:NSMakeRange(0, self.currentPage)].mutableCopy;
    self.selectedViewControllers = [self.selectedViewControllers subarrayWithRange:NSMakeRange(0, self.currentPage)].mutableCopy;

    self.currentPage--;
    
    //get the previous view controller
    UIViewController *vc = self.selectedViewControllers[self.currentPage];

    [self setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
}

- (void)setBackgroundImage:(UIImage *)backgroundImage
{
    _backgroundImage = backgroundImage;
    self.backgroundImageView.image = backgroundImage;
}

#pragma mark - Action

- (void)closeButtonTapped:(id)sender
{
    [self.leftMenuViewControllerDelegate closeTappedFromViewController:self];
}

- (IBAction)infoTapped:(id)sender
{
    [self.leftMenuViewControllerDelegate infoTappedFromViewController:self];
}

- (IBAction)settingsTapped:(id)sender
{
    [self.leftMenuViewControllerDelegate settingsTappedFromViewController:self];
}

@end
