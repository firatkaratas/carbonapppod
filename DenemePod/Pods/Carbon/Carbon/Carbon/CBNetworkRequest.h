//
//  CBNetworkRequest.h
//  Carbon
//
//  Created by Necati Aydın on 26/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Network request model. Create instances of this method and then pass them to CBNetworManager to make network requests.
 */
@interface CBNetworkRequest : NSObject

/**
 ENUM for types of a request.
 */
typedef NS_ENUM(NSUInteger, RequestType) {
    RequestTypeGet,
    RequestTypePost
};

/**
 Format url of the request, with or without the base url. If the request needs any parameters for the url, you should construct the format url specifying the parameter locations with '%@'. The url will is formatted within the getter of formatUrl.
 */
@property (strong, nonatomic) NSString *formatUrl;

/**
 Parameters for the url. Every '%@' given in the format url will be replaced with these parameters one by one.
 */
@property (strong, nonatomic) NSArray *urlParameters;

/**
 This parameter is directly passed to AFNetworking, you can learn more from the AFNetworking documentations.
 */
@property (strong, nonatomic) id parameters;

/**
 Type of the request.
 */
@property (assign, nonatomic) RequestType requestType;

@end
