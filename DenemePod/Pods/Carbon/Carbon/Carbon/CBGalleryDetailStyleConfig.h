//
//  CBGalleryDetailStyleConfig.h
//  Carbon
//
//  Created by Necati Aydın on 04/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBGalleryDetailStyleConfig : CBBaseConfig

/**
 Background color for the complete view.
 */
@property (nonatomic, strong) UIColor *viewBackgroundColor;

/**
 Background color for the bottom bar.
 */
@property (nonatomic, strong) UIColor *bottomBarBackgroundColor;

/**
 Separator color for the bar which includes the control buttons.
 */
@property (nonatomic, strong) UIColor *bottomBarSeparatorColor;

/**
 Text color for the photo descriptions.
 */
@property (nonatomic, strong) NSString *descriptionTextColor;

/**
 Text color for the label which shows the current index and the total photo count.
 */
@property (nonatomic, strong) UIColor *bottomBarPhotoCountTextColor;

/**
 Font for the photo descriptions.
 */
@property (nonatomic, strong) UIFont *descriptionFont;

/**
 Font for the label which shows the current index and the total photo count.
 */
@property (nonatomic, strong) UIFont *bottomBarPhotoCountFont;

@end
