//
//  CBLeftMenuView.h
//  Carbon
//
//  Created by naydin on 18.02.2015.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

@class CBLeftMenu1View, CBLeftMenu1ContentView, CBLeftMenu1TableViewCell;

/**
 Scrolling directions for CBLeftMenu1View.
 */
typedef NS_ENUM(NSInteger, CBLeftMenu1ViewScrollingDirection)
{
    CBLeftMenu1ViewScrollingDirectionForward,
    CBLeftMenu1ViewScrollingDirectionBack,
    CBLeftMenu1ViewScrollingDirectionNone
};

/**
 Delegate protocol for left menu behaviours.
 */
@protocol CBLeftMenu1ViewDelegate <NSObject>

/**
 Text for the given page and row.
 */
- (NSString *)leftMenuView:(CBLeftMenu1View *)leftMenuView titleForPage:(NSInteger)page row:(NSInteger)row;

/**
 Row count for the given page.
 */
- (NSInteger)leftMenuView:(CBLeftMenu1View *)leftMenuView numberOfRowsForPage:(NSInteger)page;

/**
 Called when a scrolling animation is ended. By Both dragging and calling setContentOffset: . 
 @warning If the scroll view stays at the same position even if setContentOffset: is called, this delegate method is not called.
 */
- (void)leftMenuView:(CBLeftMenu1View *)leftMenuView pageDisplayed:(NSInteger)page scrollingDirection:(CBLeftMenu1ViewScrollingDirection)direction;

/**
 Called when a table view cell is selected at the given row for the given page.
 */
- (void)leftMenuView:(CBLeftMenu1View *)leftMenuView didSelectRow:(NSInteger)row atPage:(NSInteger)page;


/**
 When the left menu is first populated with the cells, the first cell of the first page is selected automatically. This method notifies it.
 */
- (void)leftMenuViewDidSelectFirstRow:(CBLeftMenu1View *)leftMenuView;

@end

/**
 Protocol for styling CBLeftMenu1View.
 */
@protocol CBLeftMenuStylingDelegate <NSObject>

/**
 Style the table view cell.
 */
- (void)styleLeftMenuTableViewCell:(CBLeftMenu1TableViewCell *)leftMenuTableViewCell;

@end


/**
 A scroll view which helps to display content in pages. It displays the CBLeftMenu1ContentView instances in pages.
 */
@interface CBLeftMenu1View : UIScrollView <UIScrollViewDelegate>

/**
 Delegate for the view.
 */
@property (nonatomic, weak) IBOutlet NSObject<CBLeftMenu1ViewDelegate> *leftMenuDelegate;

/**
 Delegate for styling the view.
 */
@property (nonatomic, weak) IBOutlet NSObject <CBLeftMenuStylingDelegate> *stylingDelegate;

/**
 Selects the first row in the first page.
 */
- (void)selectFirstRowInFirstPage;

/**
 Calculates the maximum height for maximum allowed cell count(which is 7).
 */
+ (CGFloat)height;

/**
 Reloads the tables in CBLeftMenu1ContentView instances.
 */
- (void)reload;

/**
 Creates and pushes a new CBLeftMenu1ContentView instance as a new page.
 */
- (void)pushContentViewAnimated:(BOOL)animated;

/**
 Pops the last CBLeftMenu1ContentView page.
 */
- (void)popContentViewAnimated:(BOOL)animated;

@end
