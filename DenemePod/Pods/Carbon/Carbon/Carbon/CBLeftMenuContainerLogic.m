//
//  CBLeftMenuContainerLogic.m
//  Carbon
//
//  Created by Necati Aydın on 19/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenuContainerLogic.h"
#import "CBCategoryStore.h"
#import "CBCategoryModel.h"

@implementation CBLeftMenuContainerLogic
@dynamic delegate;

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        _rootCategory = nil;
    }
    return self;
}

- (void)loadData
{
    [self.delegate dataLoading];
    __weak typeof(self) weakSelf = self;
    [[CBCategoryStore sharedStore] getCategoriesWithCompletion:^(NSArray *categories,NSError *error)
     {
         if(error)
         {
             weakSelf.rootCategory = nil;
             [weakSelf.delegate dataLoadedWithError:[error localizedDescription]];
             return;
         }
         
         if (!categories || categories.count == 0) {
             weakSelf.rootCategory = nil;
             [weakSelf.delegate dataLoaded];
             return;
         }
         
         weakSelf.rootCategory = [self dummyRootCategory];
         weakSelf.rootCategory.subcategories = categories;
         [weakSelf assignSuperCategories];
         [weakSelf.delegate dataLoaded];
     }];
    
}


- (CBCategoryModel *)dummyRootCategory
{
    CBCategoryModel *category = [CBCategoryModel new];
    category.name = NSLocalizedString(@"Ana Menü", nil);
    category.categoryId = @"0";
    category.subcategories = @[];
    return category;
}

- (CBCategoryModel *)mainPageCategory
{
    if(!self.rootCategory)
    {
        return nil;
    }
    
    return self.rootCategory.subcategories[0];
}

- (void)assignSuperCategories
{
    [self recursivelyAssignSuperCategories:self.rootCategory];
}

- (void)recursivelyAssignSuperCategories:(CBCategoryModel *)category
{
    for(CBCategoryModel *subcategory in category.subcategories)
    {
        subcategory.superCategory = category;
        [self recursivelyAssignSuperCategories:subcategory];
    }
}

@end
