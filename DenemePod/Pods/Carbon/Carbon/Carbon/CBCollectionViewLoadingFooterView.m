//
//  CBCollectionViewLoadingFooterView.m
//  Carbon
//
//  Created by Semih Cihan on 04/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBCollectionViewLoadingFooterView.h"

@implementation CBCollectionViewLoadingFooterView

// Declared in UICollectionViewCell
- (NSString *)reuseIdentifier {
    return [CBCollectionViewLoadingFooterView reuseIdentifier];
}

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

+ (CGSize)sizeForWidth:(CGFloat)width {
    return CGSizeMake(width, 50.f);
}

@end
