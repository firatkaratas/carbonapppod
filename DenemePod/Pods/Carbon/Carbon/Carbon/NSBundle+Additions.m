//
//  NSBundle+Additions.m
//  Carbon
//
//  Created by Necati Aydın on 17/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "NSBundle+Additions.h"
#import "CBAppDelegate.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(NSBundleAdditions)

@implementation NSBundle (Additions)

+ (NSBundle *)frameworkBundle
{
    static NSBundle* frameworkBundle = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        NSString* mainBundlePath = [[NSBundle bundleForClass:[CBAppDelegate class]] resourcePath];
        NSString* frameworkBundlePath = [mainBundlePath stringByAppendingPathComponent:@"Carbon.bundle"];
        frameworkBundle = [NSBundle bundleWithPath:frameworkBundlePath];
    });
    
    return frameworkBundle;
}

@end
