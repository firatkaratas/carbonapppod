//
//  CBLeftMenuContainerViewController.m
//  Carbon
//
//  Created by Necati Aydın on 17/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenuContainerViewController.h"
#import "CBLeftMenu1ViewController.h"
#import "CBFeedViewController.h"
#import "CBFeedLogic.h"
#import "CBViewControllerBuilder.h"
#import "CBAppConfig.h"
#import "CBLeftMenu2ContentViewController.h"
#import "UIViewController+Snapshot.h"
#import "UIImageEffects.h"
#import "CBCategoryModel.h"
#import "CBLeftMenu2ViewController.h"
#import "CBLeftMenuContainerLogic.h"
#import "CBAlertManager.h"
#import "CBAnalyticsManager.h"
#import "UIView+Loading.h"

@interface CBLeftMenuContainerViewController () <CBLeftMenuContainedDelegate,SLInterstitialAdManagerDelegate>

@property (nonatomic, strong) UINavigationController *leftMenu2;
@property (nonatomic, assign) BOOL menuHidden;
@property (nonatomic, assign) BOOL didChangeCategory;
@property (nonatomic, strong) MPInterstitialAdController *interstitial;
@property (nonatomic, strong) UINavigationController *feedViewController;

@end

@implementation CBLeftMenuContainerViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.menuHidden = YES;
    self.scaleMenuView = NO;
    self.parallaxEnabled = NO;
    
    self.contentViewController = [CBViewControllerBuilder feedViewControllerWithDelegate:self];
    self.feedViewController =(UINavigationController *)self.contentViewController;
    
    if([CBAppConfig sharedConfig].leftMenuType == CBLeftMenuType1)
    {
        self.leftMenuViewController = [CBViewControllerBuilder leftMenu1ViewControllerWithDelegate:self rootCategory:nil];
        self.contentViewShadowEnabled = YES;
        self.contentViewShadowColor = [UIColor blackColor];
        self.contentViewShadowOffset = CGSizeMake(0, 0);
        self.contentViewShadowOpacity = 0.6;
        self.contentViewShadowRadius = 12;
    }
    self.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.logic loadData];
    self.interstitial = nil;
    self.didChangeCategory = NO;
}

- (void)dataLoaded
{
    CBFeedViewController *vc = (CBFeedViewController *)((UINavigationController *)self.contentViewController).topViewController;
    [vc.view dismissLoadingView];
    
    [CBAnalyticsManager feedActionWithCategoryOfTheFeed:self.logic.mainPageCategory.name];
    
    //load the data for the main category.
    [self loadDataForFeedWithCategory:self.logic.mainPageCategory];
    
    if([CBAppConfig sharedConfig].leftMenuType == CBLeftMenuType1)
    {
        CBLeftMenu1ViewController *vc = (CBLeftMenu1ViewController *)self.leftMenuViewController;
        vc.rootCategory = self.logic.rootCategory;
        [vc dataLoaded];
    }
    else if([CBAppConfig sharedConfig].leftMenuType == CBLeftMenuType2)
    {
        UINavigationController *navigationController = (UINavigationController *)[CBViewControllerBuilder leftMenu2ViewControllerWithDelegate:self rootCategory:self.logic.rootCategory];
        self.leftMenu2 = navigationController;
    }
}

- (void)dataLoading
{
    CBFeedViewController *vc = (CBFeedViewController *)((UINavigationController *)self.contentViewController).topViewController;
    [vc.view showLoadingView];
}

- (void)dataLoadedWithError:(NSString *)errorMessage
{
    CBFeedViewController *vc = (CBFeedViewController *)((UINavigationController *)self.contentViewController).topViewController;
    [vc.view dismissLoadingView];
    
    [CBAlertManager showAlertWithTitle:nil
                               message:NSLocalizedString(@"Unable to fetch the news.", nil)
                     cancelButtonTitle:NSLocalizedString(@"Retry", nil)
                     otherButtonTitles:nil
                        viewController:self
                     completionHandler:^(NSInteger buttonClicked)
    {
        [self.logic loadData];
    }];
}

#pragma mark - RESideMenuDelegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    [self menuWillShown];
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    [self menuDidHidden];
}


#pragma mark - Left Menu Content View Controller Delegate

- (void)menuContentViewControllerTappedMenuButton:(UIViewController *)leftMenuContentViewController
{
    if(!self.logic.rootCategory)
    {
        return;
    }
    
    if([CBAppConfig sharedConfig].leftMenuType == CBLeftMenuType1)
    {
        if(self.leftMenuViewController)
        {
            [self presentLeftMenuViewController];
        }
    }
    else
    {
        if(self.leftMenu2)
        {
            UINavigationController *navigationController = self.leftMenu2;
            CBLeftMenu2ViewController *vc = (CBLeftMenu2ViewController *)navigationController.topViewController;
            vc.backgroundImage = [self takeMenuSnapshotFromViewController:leftMenuContentViewController];
            
            __weak typeof(self) weakSelf = self;
            [weakSelf menuWillShown];
            [self presentViewController:self.leftMenu2 animated:NO completion:^
             {
                 
             }];
        }
    }
}

- (UIImage *)takeMenuSnapshotFromViewController:(UIViewController *)viewController
{
    UIImage *inputImage = [viewController takeViewSnapshot];
    UIColor *tintColor = [UIColor colorWithWhite:0.97 alpha:0.82];
    return [UIImageEffects imageByApplyingBlurToImage:inputImage withRadius:8 tintColor:tintColor saturationDeltaFactor:1.8 maskImage:nil];
}

#pragma mark - Public

- (CGFloat)getCalculatedContentViewInPortraitWidth
{
    CGFloat width  = CGRectGetWidth(self.view.frame);
    CGFloat scaledWidth = width * self.contentViewScaleValue;
    CGFloat scaledWidthGap = (width - scaledWidth) /2;
    CGFloat offsetFromCenterX = width/2 - scaledWidthGap - self.contentViewInPortraitOffsetCenterX;
    
    return width/2 - offsetFromCenterX;
}

#pragma mark - LeftMenuDelegate

- (void)leftMenu:(UIViewController <CBLeftMenuViewControllerProtocol> *)leftMenuViewController didSelectCategory:(CBCategoryModel *)categoryModel
{
    [CBAnalyticsManager feedActionWithCategoryOfTheFeed:categoryModel.name];

    if(categoryModel.contentType == CBCategoryModelContentTypeFeed)
    {
        [self setContentViewController:self.feedViewController animated:NO];
        if([leftMenuViewController isKindOfClass:[CBLeftMenu1ViewController class]])
        {
            if(![categoryModel hasSubcategory])
            {
                [self hideMenu];
            }
            
            if([categoryModel hasFeed])
            {
                [self loadDataForFeedWithCategory:categoryModel];
            }
        }
        else if([leftMenuViewController isKindOfClass:[CBLeftMenu2ViewController class]])
        {
            if(![categoryModel hasSubcategory] /*|| [categoryModel hasFeed]*/)
            {
                [self hideMenu];
                [self loadDataForFeedWithCategory:categoryModel];
            }
        }
    }
    else if(categoryModel.contentType == CBCategoryModelContentTypeGuide)
    {
        UIViewController *vc = [CBViewControllerBuilder guideViewControllerWithDelegate:self];
        [self setContentViewController:vc animated:NO];
        [self hideMenu];
    }
}

- (void)settingsTappedFromViewController:(UIViewController *)viewController
{
    UIViewController *vc = [CBViewControllerBuilder settingsViewController];
    [viewController presentViewController:vc animated:YES completion:nil];
}

- (void)infoTappedFromViewController:(UIViewController *)viewController
{
    UIViewController *vc = [CBViewControllerBuilder infoViewController];
    [viewController presentViewController:vc animated:YES completion:nil];
}

- (void)closeTappedFromViewController:(UIViewController<CBLeftMenuViewControllerProtocol> *)viewController
{
    [self hideMenu];
}

#pragma mark -

- (void)loadDataForFeedWithCategory:(CBCategoryModel *)categoryModel
{
    self.didChangeCategory = YES;
    
    UINavigationController *navigationController = (UINavigationController *)self.contentViewController;
    for(UIViewController *vc in navigationController.viewControllers)
    {
        if([vc isKindOfClass:[CBFeedViewController class]])
        {
            CBFeedViewController *feedViewController = (CBFeedViewController *)vc;
            CBFeedLogic *feedLogic = [[CBFeedLogic alloc] init];
            feedLogic.category = categoryModel;
            feedLogic.delegate = feedViewController;
            feedViewController.logic = feedLogic;
            
            if(self.menuHidden)
            {
                [self startAdsInFeedViewController:feedViewController];
            }
            
            [feedViewController.logic loadData];
            break;
        }
    }
}

- (void)hideMenu
{
    if([CBAppConfig sharedConfig].leftMenuType == CBLeftMenuType1)
    {
        [self hideMenuViewController];
    }
    else if([CBAppConfig sharedConfig].leftMenuType == CBLeftMenuType2)
    {
        __weak typeof(self) weakSelf = self;
        [self dismissViewControllerAnimated:YES completion:^
        {
            [weakSelf menuDidHidden];
        }];
    }
}

- (void)menuDidHidden
{
    self.menuHidden = YES;
    
    
    UIViewController *viewController = ((UINavigationController *)self.contentViewController).topViewController;
    if([viewController isKindOfClass:[CBFeedViewController class]])
    {
        CBFeedViewController *vc = (CBFeedViewController *)viewController;
        if(self.didChangeCategory)
        {
            [self startAdsInFeedViewController:vc];
        }
        else
        {
            if(self.interstitial)
            {
                [self.interstitial showFromViewController:vc];
                self.interstitial = nil;
            }
        }
    }
}

- (void)menuWillShown
{
    self.menuHidden = NO;
    self.didChangeCategory = NO;
}

#pragma mark - Ads

- (void)startAdsInFeedViewController:(CBFeedViewController *)feedViewController
{
    if(!feedViewController)
    {
        feedViewController = (CBFeedViewController *)((UINavigationController *)self.contentViewController).topViewController;
    }
    [feedViewController startAds];
    feedViewController.interstitialAdManager.delegate = self;
}

- (void)interstitialDidLoad:(MPInterstitialAdController *)interstitial
{
    if(self.menuHidden)
    {
        CBFeedViewController *vc = (CBFeedViewController *)((UINavigationController *)self.contentViewController).topViewController;
        [interstitial showFromViewController:vc];
    }
    else
    {
        self.interstitial = interstitial;
    }
}

- (void)interstitialDidFailToLoad:(MPInterstitialAdController *)interstitial
{
    
}

#pragma mark -

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - 

- (void)interstitialWillPresent:(MPInterstitialAdController *)interstitial{}
- (void)interstitialDidPresent:(MPInterstitialAdController *)interstitial{}
- (void)interstitialWillDismiss:(MPInterstitialAdController *)interstitial{}
- (void)interstitialDidDismiss:(MPInterstitialAdController *)interstitial{}


@end
