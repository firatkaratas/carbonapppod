//
//  CBBaseTableViewCell.m
//  Carbon
//
//  Created by Semih Cihan on 18/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseTableViewCell.h"

@implementation CBBaseTableViewCell

+ (NSString *)reuseIdentifier
{
    return NSStringFromClass([self class]);
}

+ (CGFloat)cellHeight{
    return 0.f;
}

@end
