//
//  CBSettingsViewControllerTableViewController.h
//  Carbon
//
//  Created by Semih Cihan on 09/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CBSettingsLogic;

@interface CBSettingsViewController : UITableViewController

@end
