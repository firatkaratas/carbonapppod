//
//  CBFeed5Cell.h
//  Carbon
//
//  Created by Semih Cihan on 05/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedCell.h"

/**
 Feed Cell type 5.
 */
@interface CBFeed5Cell : CBFeedCell

@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UIImageView *typeImageView;

@end
