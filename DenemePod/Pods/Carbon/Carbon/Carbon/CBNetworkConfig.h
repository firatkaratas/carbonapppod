//
//  CBNetworkConfig.h
//  Carbon
//
//  Created by Semih Cihan on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

/**
 @description This config class feeds the framework with service endpoints.
 */
@interface CBNetworkConfig : CBBaseConfig

/**
 * @description Base url string of the server. Request urls are concatenated with this base url string. (BaseUrl+RequestUrl)
 */
@property (strong, nonatomic) NSString *baseUrl;

/**
 * @description Url string that will be used to fetch news detail data.
 */
@property (strong, nonatomic) NSString *newsDetailUrl;

/**
 * @description Url string that will be used to fetch video detail data.
 */
@property (strong, nonatomic) NSString *videoDetailUrl;

/**
 * @description Url string that will be used to fetch author detail data.
 */
@property (strong, nonatomic) NSString *authorDetailUrl;

/**
 * @description Url string that will be used to fetch comment data.
 */
@property (strong, nonatomic) NSString *commentsUrl;

/**
 * @description Url string that will be used to fetch categories.
 */
@property (strong, nonatomic) NSString *categoriesUrl;

/**
 * @description Url string that will be used to fetch category feeds.
 */
@property (strong, nonatomic) NSString *categoryFeedUrl;

/**
 * @description Url string that will be used to fetch author feeds for the given author.
 */
@property (strong, nonatomic) NSString *authorFeedUrl;

/**
 * @description Url string that will be used to fetch gallery detail data.
 */
@property (strong, nonatomic) NSString *galleryDetailUrl;

/**
 * @description Url string that will be used to fetch the live stream url.
 */
@property (strong, nonatomic) NSString *liveStreamUrl;

/**
 * @description Url string of the live stream, if this is not set then liveStreamUrl will be used to fetch the stream url. If this is set then this url is played.
 */
@property (strong, nonatomic) NSString *liveStreamDirectUrl;

/**
 @description Url string of the guide.
 */
@property (strong, nonatomic) NSString *guideCategoryFeedUrl;

/**
 Pattern array for html contents to match and remove the links that forward to mobile website.
 */
@property (nonatomic, strong) NSArray *htmlLinkDropPatternArray;

@end
