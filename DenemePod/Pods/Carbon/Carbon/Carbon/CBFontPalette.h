//
//  CBFontPalette.h
//  Carbon
//
//  Created by Necati Aydın on 16/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//


#import "CBPListReader.h"

@interface CBFontPalette : CBPListReader

+ (NSString *)fontNameWithKey:(NSString *)key;

@end
