//
//  UITableView+Additions.m
//  Carbon
//
//  Created by Necati Aydın on 26/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UITableView+Additions.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UITableViewAdditions)

@implementation UITableView (Additions)

- (void)registerClassForDefaultReuseIdentifier:(Class)cellClass
{
    UINib *nib = [UINib nibWithNibName:NSStringFromClass(cellClass) bundle:[NSBundle frameworkBundle]];
    [self registerNib:nib forCellReuseIdentifier:NSStringFromClass(cellClass)];
}

@end
