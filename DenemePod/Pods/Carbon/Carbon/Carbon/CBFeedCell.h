//
//  CBFeedCell.h
//  Carbon
//
//  Created by Necati Aydın on 31/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseCollectionViewCell.h"

static const CGFloat kFeedCellHorizontalSpace = 10;

@interface CBFeedCell : CBBaseCollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@property (strong, nonatomic) NSArray *separators;

+ (CGSize)sizeForWidth:(CGFloat)width;
+ (CGFloat)cellRatio;

@end
