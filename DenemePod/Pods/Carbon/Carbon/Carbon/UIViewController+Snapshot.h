//
//  UIViewController+Snapshot.h
//  Carbon
//
//  Created by Necati Aydın on 23/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

@interface UIViewController (Snapshot)

/**
 Takes the snapshot of the view of the view controller. (Does not include navigation bar if exists).
 */
- (UIImage *)takeViewSnapshot;

@end
