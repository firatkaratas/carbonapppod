//
//  CBSettingsLogic.m
//  Carbon
//
//  Created by Necati Aydın on 11/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBSettingsLogic.h"
#import "CBPushNotificationManager.h"
#import "NSUserDefaults+Additions.h"

@implementation CBSettingsLogic

+ (BOOL)notificationsTurnedOn
{
    return [CBPushNotificationManager notificationsTurnedOn];
}

+ (void)enableNotifications:(BOOL)enable
{
    if(enable)
    {
        [CBPushNotificationManager turnOnNotifications];
    }
    else
    {
        [CBPushNotificationManager turnOffNotifications];
    }
}

+ (void)setFontIncrementAmount:(NSInteger)fontIncrementAmount {
    [NSUserDefaults setFontIncrementAmount:fontIncrementAmount];
}

+ (NSInteger)getFontIncrementAmount {
    return [NSUserDefaults getFontIncrementAmount];
}

@end
