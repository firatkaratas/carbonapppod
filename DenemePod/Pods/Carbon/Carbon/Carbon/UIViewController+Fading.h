//
//  UIViewController+Fading.h
//  Carbon
//
//  Created by Necati Aydın on 13/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Fading)

/**
 Dismisses a modal view controller by adding fading animation.
 */
- (void)dismissViewControllerByFadingOut;

/**
 Shows a modal view controllery by adding fading animation.
 */
- (void)presentViewControllerByFadingIn:(UIViewController *)viewController;

/**
 Fade transition which is used while dismissing. It can be used in the upper view controller.
 @code Example usage in the view controller that presents this view controller:
 [self.view.window.layer addAnimation:[imageViewController fadeTransition] forKey:kCATransition];
 */
- (CATransition *)fadeTransition;

@end
