//
//  CBWebViewStyleConfig.h
//  Carbon
//
//  Created by Necati Aydın on 22/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBWebViewStyleConfig : CBBaseConfig

/**
 Background color for the complete view;
 */
@property (nonatomic, strong) UIColor *viewBackgroundColor;

/**
 Separator color for the bar.
 */
@property (nonatomic, strong) UIColor *barSeparatorColor;

@end
