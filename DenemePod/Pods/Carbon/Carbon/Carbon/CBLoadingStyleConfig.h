//
//  CBLoadingStyleConfig.h
//  Carbon
//
//  Created by Semih Cihan on 06/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBLoadingStyleConfig : CBBaseConfig

/**
 Background color of the loading view.
 */
@property (strong, nonatomic) UIColor *backgroundColor;

/**
 Activity indicatior color.
 */
@property (strong, nonatomic) UIColor *indicatorColor;

@end
