//
//  CBSettingsViewControllerTableViewController.m
//  Carbon
//
//  Created by Semih Cihan on 09/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBSettingsViewController.h"
#import "CBNavigationBarStyler.h"
#import "CBSettingsStyleConfig.h"
#import "CBSettingsLogic.h"

@interface CBSettingsViewController(Styling)

- (void)style;

@end

@interface CBSettingsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *notificationsLabel;
@property (weak, nonatomic) IBOutlet UISwitch *notificationsSwitch;
@property (weak, nonatomic) IBOutlet UILabel *fontSizeLabel;
@property (weak, nonatomic) IBOutlet UISlider *fontSizeSlider;
@property (weak, nonatomic) IBOutlet UITableViewCell *notificationsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *fontCell;

- (IBAction)notificationsSwitchValueChanged:(id)sender;
- (IBAction)fontSizeSliderValueChanged:(id)sender;

@end

@implementation CBSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [CBNavigationBarStyler styleNavigationItemWithCloseIcon:self.navigationItem target:self action:@selector(closeButtonTapped:)];
    [self style];
    
    self.notificationsSwitch.on = [CBSettingsLogic notificationsTurnedOn];
    
    [self.fontSizeSlider setValue:[CBSettingsLogic getFontIncrementAmount] animated:NO];
    [self.fontSizeLabel setFont:[self.fontSizeLabel.font fontWithSize:[CBSettingsStyleConfig sharedConfig].notificationsLabelFont.pointSize + [CBSettingsLogic getFontIncrementAmount]]];
}

#pragma mark - Actions

- (void)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)notificationsSwitchValueChanged:(id)sender
{
    UISwitch *sw = sender;
    [CBSettingsLogic enableNotifications:sw.on];
}

- (IBAction)fontSizeSliderValueChanged:(UISlider *)sender {
    [sender setValue:(int)(sender.value) animated:NO];
    [self.fontSizeLabel setFont:[self.fontSizeLabel.font fontWithSize:[CBSettingsStyleConfig sharedConfig].notificationsLabelFont.pointSize + (NSInteger)sender.value]];
    [CBSettingsLogic setFontIncrementAmount:(NSInteger)sender.value];
}


@end


@implementation CBSettingsViewController(Styling)

- (void)style
{
    CBSettingsStyleConfig *config = [CBSettingsStyleConfig sharedConfig];
    self.view.backgroundColor = config.backgroundColor;
    self.notificationsLabel.textColor = config.notificationsLabelColor;
    self.notificationsLabel.font = config.notificationsLabelFont;
    self.fontSizeLabel.textColor = config.fontSizeLabelColor;
    self.fontSizeLabel.font = config.fontSizeLabelFont;
    self.fontSizeSlider.tintColor = config.sliderTintColor;
    self.notificationsSwitch.tintColor = config.switchTintColor;
    self.fontCell.backgroundColor = config.cellBackgroundColor;
    self.notificationsCell.backgroundColor = config.cellBackgroundColor;
}

@end
