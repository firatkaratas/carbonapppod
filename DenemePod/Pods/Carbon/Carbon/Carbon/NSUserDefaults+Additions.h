//
//  NSUserDefaults+Additions.h
//  Carbon
//
//  Created by Semih Cihan on 11/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (Additions)

/**
 Saves font increment amount in nsuserdefaulst.
 @param fontIncrementAmount Font increment amount to be saved.
 */
+ (void)setFontIncrementAmount:(NSInteger)fontIncrementAmount;

/**
 Returns font increment amount saved in the user defaults.
 @return Font increment amount.
 */
+ (NSInteger)getFontIncrementAmount;

/**
 Saves the notification enabling preference.
 @param enabled Notification enabling preference.
 */
+ (void)setPushNotificationEnabled:(BOOL)enabled;

/**
 Returns the preference for notification.
 @return Preference for the notificaion.
 */
+ (NSNumber *)getPushNotificationEnabled;

@end
