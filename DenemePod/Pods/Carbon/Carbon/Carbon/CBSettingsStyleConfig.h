//
//  CBSettingsStyleConfig.h
//  Carbon
//
//  Created by Semih Cihan on 09/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBSettingsStyleConfig : CBBaseConfig

/**
 Background color of the view.
 */
@property (strong, nonatomic) UIColor *backgroundColor;

/**
 Notifications label color.
 */
@property (strong, nonatomic) UIColor *notificationsLabelColor;

/**
 Notifications label font.
 */
@property (strong, nonatomic) UIFont *notificationsLabelFont;

/**
 Font size label text color.
 */
@property (strong, nonatomic) UIColor *fontSizeLabelColor;

/**
 Font size label font.
 */
@property (strong, nonatomic) UIFont *fontSizeLabelFont;

/**
 Slider tint color.
 */
@property (strong, nonatomic) UIColor *sliderTintColor;

/**
 Switch tint color.
 */
@property (strong, nonatomic) UIColor *switchTintColor;

/**
 Cell background color.
 */
@property (strong, nonatomic) UIColor *cellBackgroundColor;

@end
