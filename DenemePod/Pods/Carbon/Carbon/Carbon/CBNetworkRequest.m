//
//  CBNetworkRequest.m
//  Carbon
//
//  Created by Necati Aydın on 26/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNetworkRequest.h"

@implementation CBNetworkRequest

- (NSString *)formatUrl {
 
    NSString *str = _formatUrl;
    for(NSInteger i = 0; i < self.urlParameters.count; i++) {
        
        NSRange firstOccurrenceRange = [str rangeOfString:@"%@"];
        if (firstOccurrenceRange.location != NSNotFound) {
            str = [str stringByReplacingCharactersInRange:firstOccurrenceRange
                                               withString:self.urlParameters[i]];
        }
    }
    
    return str;
}

@end
