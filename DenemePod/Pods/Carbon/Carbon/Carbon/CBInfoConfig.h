//
//  CBInfoConfig.h
//  Carbon
//
//  Created by Semih Cihan on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBInfoConfig : CBBaseConfig

/**
 Html Info url as a string.
 */
@property (strong, nonatomic) NSString *htmlInfoUrl;

/**
 Map image url as a string.
 */
@property (strong, nonatomic) NSString *mapImageUrl;

/**
 Titles array containing title strings.
 */
@property (strong, nonatomic) NSArray *titles;

/**
 Informations array containing information strings.
 */
@property (strong, nonatomic) NSArray *informations;

/**
 Facebook URL to be opened in web browswer.It is optional.
 */
@property (nonatomic, strong) NSString *facebookUrl;

/**
 Twitter URL to be opened in web browswer.It is optional.
 */
@property (nonatomic, strong) NSString *twitterUrl;

/**
 Google plus URL to be opened in web browswer.It is optional.
 */
@property (nonatomic, strong) NSString *googlePlusUrl;

/**
 Youtube URL to be opened in web browswer.It is optional.
 */
@property (nonatomic, strong) NSString *youTubeUrl;

@end
