//
//  CBDeviceType.m
//  Carbon
//
//  Created by Semih Cihan on 08/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBScreenSizeType.h"
#import <UIKit/UIKit.h>

static const CGFloat kIphone5ScreenHeight = 568.f;
static const CGFloat kIphone6ScreenHeight = 667.f;
static const CGFloat kIphone6PlusScreenHeight = 736.f;

@implementation CBScreenSizeType

+ (BOOL)isIpad {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

+ (BOOL)isIphone {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
}

+ (CGFloat)screenWidth {
    return ([[UIScreen mainScreen] bounds].size.width);
}

+ (CGFloat)screenHeight {
    return ([[UIScreen mainScreen] bounds].size.height);
}

+ (CGFloat)screenMaxLength {
    return (MAX([self screenWidth], [self screenHeight]));
}

+ (ScreenSizeType)screenSizeType {
    if ([self isIphone] && [self screenMaxLength] < kIphone5ScreenHeight) {
        return ScreenSizeTypeIphonePrior5;
    } else if ([self isIphone] && [self screenMaxLength] == kIphone5ScreenHeight) {
        return ScreenSizeTypeIphone5;
    } else if ([self isIphone] && [self screenMaxLength] == kIphone6ScreenHeight) {
        return ScreenSizeTypeIphone6;
    } else if ([self isIphone] && [self screenMaxLength] == kIphone6PlusScreenHeight) {
        return ScreenSizeTypeIphone6Plus;
    } else {
        return ScreenSizeTypeOther;
    }
}

@end
