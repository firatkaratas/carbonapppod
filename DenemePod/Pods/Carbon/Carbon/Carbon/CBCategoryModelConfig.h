//
//  CBCategoryModelConfig.h
//  Carbon
//
//  Created by Semih Cihan on 16/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@interface CBCategoryModelConfig : CBBaseModelConfig

/**
 Id of the category.
 */
@property (strong, nonatomic) NSString *categoryId;

/**
 Name of the category.
 */
@property (strong, nonatomic) NSString *name;

/**
 Shows if the category has its own feed.
 */
@property (strong, nonatomic) NSString *hasFeed;

/**
 Subcategory array of this category.
 */
@property (strong, nonatomic) NSString *subcategories;

/**
 The content type of the category. For ex: feed, guide.
 */
@property (nonatomic, strong) NSString *contentType;

@end
