//
//  CBPListReader.h
//  Carbon
//
//  Created by Necati Aydın on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 This class is reading the plist files. After creating the singleton instance, the corresponding dictionary can be read from plistDictionary property.
 */
@interface CBPListReader : NSObject

/**
 Singleton instance for the config. Singleton instances are created for subclasses. Subclasses does not need to re-implement singleton.
 
 @return singleton instance.
 */
+ (instancetype)sharedConfig;

@property (nonatomic, strong) NSDictionary *plistDictionary;

@end
