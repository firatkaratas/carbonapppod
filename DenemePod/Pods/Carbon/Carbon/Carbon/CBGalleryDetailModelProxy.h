//
//  CBGalleryDetailModelProxy.h
//  Carbon
//
//  Created by Semih Cihan on 17/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelProxy.h"

@protocol CBGalleryDetailModelProxyProtocol <NSObject>

@optional

- (NSString *)detailIdJSONTransformerOverridden:(id)object;
- (NSString *)categoryJSONTransformerOverridden:(id)object;
- (NSString *)dateJSONTransformerOverridden:(id)object;
- (NSString *)titleJSONTransformerOverridden:(id)object;
- (NSString *)urlJSONTransformerOverridden:(id)object;
- (NSArray *)photosJSONTransformerOverridden:(id)object;

@end

@interface CBGalleryDetailModelProxy : CBBaseModelProxy <CBGalleryDetailModelProxyProtocol>

+ (instancetype)sharedInstance;

- (id)detailIdJSONTransformer:(id)object;
- (id)categoryJSONTransformer:(id)object;
- (id)dateJSONTransformer:(id)object;
- (id)titleJSONTransformer:(id)object;
- (id)urlJSONTransformer:(id)object;
- (id)photosJSONTransformer:(id)object;

@end
