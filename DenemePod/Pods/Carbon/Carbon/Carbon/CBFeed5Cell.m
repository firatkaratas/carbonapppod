//
//  CBFeed5Cell.m
//  Carbon
//
//  Created by Semih Cihan on 05/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeed5Cell.h"

@implementation CBFeed5Cell

+ (CGFloat)cellRatio
{
    return 245.f/300.f;
}


@end
