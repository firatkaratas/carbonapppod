//
//  UIView+NibLoading.m
//  Carbon
//
//  Created by Necati Aydın on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIView+NibLoading.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIViewNibLoading)

@implementation UIView (NibLoading)

+ (instancetype)loadFromNIB
{
    return [self loadFromNIBWithName:NSStringFromClass([self class])];
}

+ (instancetype)loadFromNIBWithName:(NSString *)nibName
{
    NSArray* nibViews = [[NSBundle frameworkBundle] loadNibNamed:nibName
                                                           owner:self
                                                         options:nil];
    
    for (id view in nibViews)
    {
        if ([view isKindOfClass:[self class]])
        {
            return view;
        }
    }
    
    return nil;
}

@end
