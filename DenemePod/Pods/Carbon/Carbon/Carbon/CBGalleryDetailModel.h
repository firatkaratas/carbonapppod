//
//  CBGalleryDetailModel.h
//  Carbon
//
//  Created by Semih Cihan on 27/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"
#import "CBCategoryModel.h"
#import "CBGalleryPhotoModel.h"

@interface CBGalleryDetailModel : CBBaseModel

@property (strong, nonatomic) NSString *detailId;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) CBCategoryModel *category;
@property (copy, nonatomic) NSMutableArray *photos;
@property (copy, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *title;

/**
 Returns the photo count based on the photos array. If photos array nil, returns 0;
 */
- (NSInteger)photoCount;

@end
