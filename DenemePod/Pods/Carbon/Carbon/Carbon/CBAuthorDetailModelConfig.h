//
//  CBAuthorDetailConfig.h
//  Carbon
//
//  Created by Necati Aydın on 08/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@interface CBAuthorDetailModelConfig : CBBaseModelConfig

/**
 Id for the model.
 */
@property (strong, nonatomic) NSString *detailId;

/**
 Date of the news.
 */
@property (strong, nonatomic) NSString *date;

/**
 Category of the news.
 */
@property (strong, nonatomic) NSString *category;

/**
 Title of the news.
 */
@property (strong, nonatomic) NSString *title;

/**
 Secondary title of the news.
 */
@property (strong, nonatomic) NSString *spot;

/**
 Image url of the news.
 */
@property (strong, nonatomic) NSString *imageUrl;

/**
 Url of the news.
 */
@property (strong, nonatomic) NSString *url;

/**
 HTML content of the news.
 */
@property (strong, nonatomic) NSString *content;

/**
 Id of the author.
 */
@property (nonatomic, strong) NSString *authorId;

@end
