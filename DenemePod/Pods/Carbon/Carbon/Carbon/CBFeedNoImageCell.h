//
//  CBFeedNoImageCell.h
//  Carbon
//
//  Created by Necati Aydın on 26/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedCell.h"

@interface CBFeedNoImageCell : CBFeedCell

@property (nonatomic, weak) IBOutlet UILabel *categoryLabel;
@property (nonatomic, weak) IBOutlet UILabel *spotLabel;

@end
