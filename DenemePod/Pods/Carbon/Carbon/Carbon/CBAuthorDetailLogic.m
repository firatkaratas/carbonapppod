//
//  CBAuthorDetailLogic.m
//  Carbon
//
//  Created by Necati Aydın on 08/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBAuthorDetailLogic.h"
#import "CBNetworkManager.h"
#import "CBAuthorDetailModel.h"

@implementation CBAuthorDetailLogic
@dynamic newsDetail;

- (void)loadData
{
    [self.delegate dataLoading];
    
    __weak typeof(self) weakSelf = self;
    [[CBNetworkManager sharedInstance] getAuthorDetailById:self.feedId
                                             successBlock:^(CBAuthorDetailModel *authorDetail)
     {
         NSString *errorMessage = [self validateModel:authorDetail];
         
         if(errorMessage)
         {
             [weakSelf.delegate dataLoadedWithError:errorMessage];
         }
         else
         {
             weakSelf.newsDetail = authorDetail;
             [weakSelf.delegate dataLoaded];
         }
     } failureBlock:^(NSError *error) {
         [weakSelf.delegate dataLoadedWithError:NSLocalizedString(@"Unable to fetch the news.", nil)];
     }];
}

- (NSString *)validateModel:(CBAuthorDetailModel *)detailModel
{
    
    if(!detailModel.title)
    {
        DebugLog(@"%@ with feedId: %lu", NSLocalizedString(@"Unable to fetch the news.",nil), (unsigned long)self.feedId);
        return NSLocalizedString(@"Unable to fetch the news.",nil);
    }
    
    return nil;
}

@end
