//
//  CBNewsDetailConfig.h
//  Carbon
//
//  Created by Semih Cihan on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@interface CBNewsDetailModelConfig : CBBaseModelConfig

/**
 Id for the model.
 */
@property (strong, nonatomic) NSString *detailId;

/**
 Date of the news.
 */
@property (strong, nonatomic) NSString *date;

/**
 Category of the news.
 */
@property (strong, nonatomic) NSString *category;

/**
 Title of the news.
 */
@property (strong, nonatomic) NSString *title;

/**
 Secondary title of the news.
 */
@property (strong, nonatomic) NSString *spot;

/**
 Image url of the news.
 */
@property (strong, nonatomic) NSString *imageUrl;

/**
 Url of the news.
 */
@property (strong, nonatomic) NSString *url;

/**
 HTML content of the news.
 */
@property (strong, nonatomic) NSString *content;


@end
