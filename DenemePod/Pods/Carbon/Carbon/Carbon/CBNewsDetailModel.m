//
//  CBNewsDetailModel.m
//  Carbon
//
//  Created by Semih Cihan on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNewsDetailModel.h"
#import "CBNewsDetailModelConfig.h"
#import "CBCategoryStore.h"
#import "CBNetworkConfig.h"
#import "CBNewsDetailModelProxy.h"

@interface CBNewsDetailModel ()

@end

@implementation CBNewsDetailModel

+ (CBBaseModelConfig *)config
{
    return [CBNewsDetailModelConfig sharedConfig];
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"detailId" : [CBNewsDetailModelConfig sharedConfig].detailId,
             @"date" : [CBNewsDetailModelConfig sharedConfig].date,
             @"category" : [CBNewsDetailModelConfig sharedConfig].category,
             @"title" : [CBNewsDetailModelConfig sharedConfig].title,
             @"spot" : [CBNewsDetailModelConfig sharedConfig].spot,
             @"imageUrl" : [CBNewsDetailModelConfig sharedConfig].imageUrl,
             @"url" : [CBNewsDetailModelConfig sharedConfig].url,
             @"content" : [CBNewsDetailModelConfig sharedConfig].content,
             };
}

+ (NSValueTransformer *)contentJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBNewsDetailModelProxy sharedInstance] contentJSONTransformer:object];
        object = [CBBaseModel removeLinksInHtmlContent:object linkPatternArray:[CBNetworkConfig sharedConfig].htmlLinkDropPatternArray];
        
        return object;
        
    } reverseBlock:^id (id obj) {
        return obj;
    }];
}

+ (NSValueTransformer *)detailIdJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBNewsDetailModelProxy sharedInstance] detailIdJSONTransformer:object];
        return [CBBaseModel stringOrNumberToString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)categoryJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBNewsDetailModelProxy sharedInstance] categoryJSONTransformer:object];
        
        if (object) {
            NSString *categoryId = [self stringOrNumberToString:object];
            CBCategoryModel *cat = [[CBCategoryStore sharedStore] getCategoryWithId:categoryId];
            return cat;
        } else {
            return nil;
        }
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)titleJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBNewsDetailModelProxy sharedInstance] titleJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)spotJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBNewsDetailModelProxy sharedInstance] spotJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)dateJSONTransformer
{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id(id object) {
        
        object = [[CBNewsDetailModelProxy sharedInstance] dateJSONTransformer:object];
        return object;
    } reverseBlock:^id(id obj) {
        return obj;
    }];
}

+ (NSValueTransformer *)urlJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBNewsDetailModelProxy sharedInstance] urlJSONTransformer:object];
        return [NSURL URLWithString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)imageUrlJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBNewsDetailModelProxy sharedInstance] imageUrlJSONTransformer:object];
        return [NSURL URLWithString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

@end
