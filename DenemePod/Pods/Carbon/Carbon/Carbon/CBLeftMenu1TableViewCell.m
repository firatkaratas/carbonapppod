//
//  CBLeftMenuTableViewCell.m
//  Carbon
//
//  Created by naydin on 18.02.2015.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu1TableViewCell.h"

static const CGFloat kCellHighlightViewCornerRadius = 18;

@interface CBLeftMenu1TableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;

@end

@implementation CBLeftMenu1TableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.highlightView.layer.cornerRadius = kCellHighlightViewCornerRadius;
    [self.arrowImageView setImage:[[UIImage imageNamed:@"left_menu_arrow" inBundle:[NSBundle frameworkBundle]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    [self.arrowImageView setTintColor:self.arrowColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    self.highlightView.hidden = !selected;
    if (selected) {
        [self.titleLabel setTextColor:self.selectedTextColor];
        [self.titleLabel setFont:self.selectedTextFont];
        [self.arrowImageView setTintColor:self.selectedArrowColor];
    } else {
        [self.titleLabel setTextColor:self.textColor];
        [self.titleLabel setFont:self.textFont];
        [self.arrowImageView setTintColor:self.arrowColor];
    }

}

@end
