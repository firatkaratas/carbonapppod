//
//  CBInfoLabelCell.m
//  Carbon
//
//  Created by Semih Cihan on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBInfoCell.h"
#import "NSString+Additions.h"

@interface CBInfoCell () <UIWebViewDelegate>

@end

@implementation CBInfoCell

- (NSString *)reuseIdentifier {
    return [CBInfoCell reuseIdentifier];
}

@end
