//
//  UIViewController+CBFeedCell.m
//  Carbon
//
//  Created by Necati Aydın on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIViewController+CBFeedCell.h"
#import "CBFeed1Cell.h"
#import "CBFeed2Cell.h"
#import "CBFeed3Cell.h"
#import "CBFeed4Cell.h"
#import "CBFeed5Cell.h"
#import "CBFeed7Cell.h"
#import "CBFeedNoImageCell.h"
#import "CBFeedModel.h"
#import "UIImageView+WebCache.h"
#import "NimbusKitBasics.h"
#import "UIImage+Loading.h"

NI_FIX_CATEGORY_BUG(UIViewControllerCBFeedCell)

@implementation UIViewController (CBFeedCell)

- (void)configureFeedCell:(CBFeedCell *)cell withFeedModel:(CBFeedModel *)feed
{
    cell.titleLabel.text = feed.title;
    if([cell isKindOfClass:[CBFeed1Cell class]])
    {
        CBFeed1Cell *feed1Cell = (CBFeed1Cell *)cell;
        feed1Cell.categoryLabel.text = feed.category.name ? feed.category.name : @"";
        [feed1Cell.imageView sd_setImageWithURL:feed.imageUrl];

    }
    else if([cell isKindOfClass:[CBFeed2Cell class]])
    {
        CBFeed2Cell *feed2Cell = (CBFeed2Cell *)cell;
        feed2Cell.dateLabel.text = feed.date ? feed.date : @"";
        [feed2Cell.imageView sd_setImageWithURL:feed.imageUrl];
    }
    else if([cell isKindOfClass:[CBFeed3Cell class]])
    {
        CBFeed3Cell *feed3Cell = (CBFeed3Cell *)cell;
        [feed3Cell.imageView sd_setImageWithURL:feed.imageUrl];
        if(feed.type == FeedTypeArticle)
        {
            feed3Cell.typeImageView.hidden = NO;
            UIImage *image = [UIImage imageNamed:@"feed_cell_article" inBundle:[NSBundle frameworkBundle]];
            feed3Cell.typeImageView.image = image;
        }
        else if(feed.type == FeedTypeVideo)
        {
            feed3Cell.typeImageView.hidden = NO;
            UIImage *image = [UIImage imageNamed:@"feed_cell_video" inBundle:[NSBundle frameworkBundle]];
            feed3Cell.typeImageView.image = image;
        }
        else if(feed.type == FeedTypeGallery)
        {
            feed3Cell.typeImageView.hidden = NO;
            feed3Cell.typeImageView.image = [UIImage imageNamed:@"feed_cell_photo" inBundle:[NSBundle frameworkBundle]];
        }
        else
        {
            feed3Cell.typeImageView.hidden = YES;
        }
    }
    else if([cell isKindOfClass:[CBFeed4Cell class]])
    {
        CBFeed4Cell *feed4Cell = (CBFeed4Cell *)cell;
        feed4Cell.label2.text = feed.category.name ? feed.category.name : @"";
        feed4Cell.label3.text = feed.spot;
        [feed4Cell.imageView sd_setImageWithURL:feed.imageUrl];
    }
    else if([cell isKindOfClass:[CBFeed5Cell class]])
    {
        CBFeed5Cell *feed5Cell = (CBFeed5Cell *)cell;
        feed5Cell.categoryLabel.text = feed.category.name ? feed.category.name : @"";
        [feed5Cell.imageView sd_setImageWithURL:feed.imageUrl];
        if(feed.type == FeedTypeArticle)
        {
            feed5Cell.typeImageView.hidden = NO;
            UIImage *image = [UIImage imageNamed:@"feed_cell_article" inBundle:[NSBundle frameworkBundle]];
            feed5Cell.typeImageView.image = image;
        }
        else if(feed.type == FeedTypeVideo)
        {
            feed5Cell.typeImageView.hidden = NO;
            UIImage *image = [UIImage imageNamed:@"feed_cell_video" inBundle:[NSBundle frameworkBundle]];
            feed5Cell.typeImageView.image = image;
        }
        else if(feed.type == FeedTypeGallery)
        {
            feed5Cell.typeImageView.hidden = NO;
            feed5Cell.typeImageView.image = [UIImage imageNamed:@"feed_cell_photo" inBundle:[NSBundle frameworkBundle]];
        }
        else
        {
            feed5Cell.typeImageView.hidden = YES;
        }
    }
    else if([cell isKindOfClass:[CBFeed7Cell class]])
    {
        CBFeed7Cell *feed7Cell = (CBFeed7Cell *)cell;
        feed7Cell.label3.text = feed.spot;
        feed7Cell.label2.text = feed.date;
        [feed7Cell.imageView sd_setImageWithURL:feed.imageUrl];
    }
    else if([cell isKindOfClass:[CBFeedNoImageCell class]])
    {
        CBFeedNoImageCell *feedNoImageCell = (CBFeedNoImageCell *)cell;
        feedNoImageCell.categoryLabel.text = feed.category.name ? feed.category.name : feed.date;
        feedNoImageCell.spotLabel.text = feed.spot;
    }
}

@end
