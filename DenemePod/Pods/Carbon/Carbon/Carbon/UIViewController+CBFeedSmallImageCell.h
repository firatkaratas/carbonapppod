//
//  UIViewController+CBFeedSmallImageCell.h
//  Carbon
//
//  Created by Semih Cihan on 10/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CBFeed4Cell;
@class CBFeedModel;
@class CBSuggestedVideoModel;

@interface UIViewController (CBFeedSmallImageCell)

- (void)configureFeedSmallImageCell:(CBFeed4Cell *)cell withFeedModel:(CBFeedModel *)feed;
- (void)configureFeedCell:(CBFeed4Cell *)cell withSuggestedVideoModel:(CBSuggestedVideoModel *)model;

@end
