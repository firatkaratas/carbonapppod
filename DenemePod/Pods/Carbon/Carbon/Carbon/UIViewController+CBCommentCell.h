//
//  UIViewController+CBCommentCell.h
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CBCommentModel, CBCommentCell;

@interface UIViewController (CBCommentCell)

- (void)configureCommentCell:(CBCommentCell *)cell withCommentModel:(CBCommentModel *)model;

@end
