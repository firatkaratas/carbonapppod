//
//  CBGalleryContentViewController.h
//  Carbon
//
//  Created by Necati Aydın on 09/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBImageViewController.h"

/**
 Image view controller subclass with a text view, which has webview in it.
 */
@interface CBGalleryContentViewController : CBImageViewController

/**
 Sets text view hidden property to YES.
 */
- (void)disableTextView;

/**
 Sets text view alpha.
 */
- (void)hideTextView:(BOOL)hide;

/**
 Loads the given html string to text view.
 @apram htmlString A string with html content in it.
 */
- (void)loadHtmlString:(NSString *)htmlString;

@end
