//
//  CBAlertManager.m
//  Haber3
//
//  Created by Necati Aydın on 26/01/15.
//  Copyright (c) 2014 Mobilike. All rights reserved.
//

#import "CBAlertManager.h"
#import "UIDevice+Additions.h"

@interface CBAlertManager()<UIAlertViewDelegate>

@property (nonatomic, strong) UIAlertView *alertView;
@property (nonatomic, copy) void (^completionHandler)(NSInteger buttonClicked);

@property (nonatomic, strong) UIAlertController *alertController;

@end

@implementation CBAlertManager

+ (instancetype)sharedInstance
{
    static CBAlertManager *manager = nil;
    static dispatch_once_t token;

    dispatch_once(&token, ^{
        manager = [CBAlertManager new];
    });
    
    return manager;
}

+ (void) showAlertWithTitle:(NSString *)title
                    message:(NSString *)message
          cancelButtonTitle:(NSString *)cancelButtonTitle
          otherButtonTitles:(NSArray *)otherButtonTitles
             viewController:(UIViewController *)viewController
          completionHandler:(void (^)(NSInteger buttonClicked))completionHandler
{
    if([UIDevice iOS8AndHigher])
    {
        [CBAlertManager sharedInstance].alertController = [UIAlertController alertControllerWithTitle:title.copy
                                                                                            message:message.copy
                                                                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        if(cancelButtonTitle)
        {
            UIAlertAction *action = [UIAlertAction actionWithTitle:cancelButtonTitle
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction *action) {
                                                               completionHandler(0);
                                                           }];
            
            [[CBAlertManager sharedInstance].alertController addAction:action];
        }
        
        for(NSInteger i = 0; i < otherButtonTitles.count; i++)
        {
            UIAlertAction *action = [UIAlertAction actionWithTitle:otherButtonTitles[i]
                                                             style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction *action) {
                                                                     completionHandler(i+1);
                                                                 }];
            
            [[CBAlertManager sharedInstance].alertController addAction:action];
        }
        
        [viewController presentViewController:[CBAlertManager sharedInstance].alertController animated:YES completion:nil];
        
    }
    else
    {
        [[CBAlertManager sharedInstance].alertView dismissWithClickedButtonIndex:0 animated:YES];
        [CBAlertManager sharedInstance].alertView = [[UIAlertView alloc] initWithTitle:title.copy
                                                                             message:message.copy
                                                                            delegate:[CBAlertManager sharedInstance]
                                                                   cancelButtonTitle:cancelButtonTitle.copy
                                                                   otherButtonTitles:nil];
        
        for (NSString *title in otherButtonTitles)
        {
            [[CBAlertManager sharedInstance].alertView addButtonWithTitle:title];
        }
        [CBAlertManager sharedInstance].completionHandler = completionHandler;
        
        [[CBAlertManager sharedInstance].alertView show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.completionHandler) {
        self.completionHandler(buttonIndex);
    }
}

- (void)alertViewCancel:(UIAlertView *)alertView
{
    if (self.completionHandler) {
        self.completionHandler(0);
    }
}



@end
