//
//  CBLeftMenuContentView.m
//  Carbon
//
//  Created by naydin on 19.02.2015.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu1ContentView.h"
#import "CBLeftMenu1TableViewCell.h"
#import "UITableView+Additions.h"

@interface CBLeftMenu1ContentView()


@end

@implementation CBLeftMenu1ContentView

- (void)awakeFromNib
{
    [super awakeFromNib];

    [self.tableView registerClassForDefaultReuseIdentifier:[CBLeftMenu1TableViewCell class]];
}

+ (CGFloat)heightForRowCount:(NSInteger)rowCount
{
    return rowCount * kLeftMenuCellHeight;
}

@end
