//
//  CBGalleryContentViewController.m
//  Carbon
//
//  Created by Necati Aydın on 09/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGalleryContentViewController.h"

static const CGFloat kTextViewAlpha = 0.7;
static const CGFloat kTextViewHeight = 150;
static const CGFloat kBottomBarHeight = 44;

@interface CBGalleryContentViewController () <UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UIView *textView;

@end

@implementation CBGalleryContentViewController

- (void)disableTextView
{
    self.textView.hidden = YES;
}

- (void)hideTextView:(BOOL)hide
{
    self.textView.alpha = hide ? 0 : kTextViewAlpha;
}

- (void)loadHtmlString:(NSString *)htmlString
{
    [self.webView loadHTMLString:htmlString baseURL:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //in order to calculate the future height of the text view, height of the text view must be set to minimum non-zero value. So we set it to 0.
    CGRect textViewFrame = self.textView.frame;
    CGRect webViewFrame = self.webView.frame;
    
    textViewFrame.size.width = CGRectGetWidth(self.view.frame);
    textViewFrame.size.height = 1;
    webViewFrame.size.width = CGRectGetWidth(self.view.frame);
    webViewFrame.size.height = 1;
    
    webView.frame = webViewFrame;
    
    CGSize sz = [webView sizeThatFits:CGSizeZero];
    
    
    //after calculating the web view height with the given html and width, shrink it if possible
    if(sz.height < kTextViewHeight)
    {
        textViewFrame.size.height = sz.height;
        webViewFrame.size.height = sz.height;
        textViewFrame.origin.y = CGRectGetHeight(self.view.frame) - kBottomBarHeight  - CGRectGetHeight(textViewFrame);
    }
    else
    {
        textViewFrame.size.height = kTextViewHeight;
        webViewFrame.size.height = kTextViewHeight;
        textViewFrame.origin.y = CGRectGetHeight(self.view.frame) - kBottomBarHeight  - CGRectGetHeight(textViewFrame);
    }
    
    self.textView.frame = textViewFrame;
    self.webView.frame = webViewFrame;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self disableTextView];
}

@end
