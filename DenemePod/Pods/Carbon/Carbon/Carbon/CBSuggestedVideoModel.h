//
//  CBSuggestedVideoModel.h
//  Carbon
//
//  Created by Semih Cihan on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"
#import "CBCategoryModel.h"

@interface CBSuggestedVideoModel : CBBaseModel

@property (strong, nonatomic) NSString *detailId;
@property (strong, nonatomic) CBCategoryModel *category;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *spot;
@property (strong, nonatomic) NSURL *imageUrl;

@end
