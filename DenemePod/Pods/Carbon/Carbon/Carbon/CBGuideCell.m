//
//  CBGuideCell.m
//  Carbon
//
//  Created by Necati Aydın on 09/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGuideCell.h"
#import "UIView+Separator.h"

@implementation CBGuideCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.separators = [self.contentView addFrameWithLineWidth:kCellBorderLineWidth];
}

+ (CGSize)sizeForWidth:(CGFloat)width
{
    return CGSizeMake(width, width * [self cellRatio]);
}

+ (CGFloat)cellRatio
{
    return 170.f/300.f;
}

@end
