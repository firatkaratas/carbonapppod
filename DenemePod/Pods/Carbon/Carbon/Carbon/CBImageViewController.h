//
//  CBImageViewController.h
//  Carbon
//
//  Created by Necati Aydın on 27/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"

@class CBImageViewController;

@protocol CBImageViewControllerDelegate <NSObject>

/**
 Called when single tapped.
 */
- (void)imageViewControllerTapped:(CBImageViewController *)imageViewController;

@end


/**
 A view controller to view images. Zooming and scrolling enabled. Rotations can be done.
 */
@interface CBImageViewController : CBBaseViewController


//public

/**
 Creates an instance without giving the instance. The instance can be set later on by calling addImage:.
 */
+ (instancetype)instanceWithoutImage;

/**
 Delegate
 */
@property (nonatomic, weak) id<CBImageViewControllerDelegate> delegate;

/**
 If the view controller is created without an image, the image can be set with this method later on.
 */
- (void)addImage:(UIImage *)image;


//protected

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImageView *imageView;

- (void)singleTap:(id)sender;
- (CGRect)initialImageViewFrame;

@end
