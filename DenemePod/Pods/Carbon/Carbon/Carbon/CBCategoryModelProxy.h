//
//  CBCategoryModelProxy.h
//  Carbon
//
//  Created by Semih Cihan on 16/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelProxy.h"

@protocol CBCategoryModelProxyProtocol <NSObject>

@optional

- (NSString *)categoryIdJSONTransformerOverridden:(id)object;
- (NSString *)nameJSONTransformerOverridden:(id)object;
- (NSNumber *)hasFeedNumberJSONTransformerOverridden:(id)object;
- (NSArray *)subcategoriesJSONTransformerOverridden:(id)object;
- (NSNumber *)contentTypeNumberJSONTransformerOverridden:(id)object;

@end

@interface CBCategoryModelProxy : CBBaseModelProxy <CBCategoryModelProxyProtocol>

+ (instancetype)sharedInstance;

- (id)categoryIdJSONTransformer:(id)object;
- (id)nameJSONTransformer:(id)object;
- (id)hasFeedNumberJSONTransformer:(id)object;
- (id)subcategoriesJSONTransformer:(id)object;
- (id)contentTypeNumberJSONTransformer:(id)object;

@end
