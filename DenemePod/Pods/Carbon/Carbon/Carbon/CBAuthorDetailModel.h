//
//  CBAuthorDetail.h
//  Carbon
//
//  Created by Necati Aydın on 08/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNewsDetailModel.h"

@interface CBAuthorDetailModel : CBNewsDetailModel

@property (nonatomic, strong) NSString *authorId;

@end
