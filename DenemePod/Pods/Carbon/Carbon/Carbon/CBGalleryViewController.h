//
//  CBGalleryViewController.h
//  Carbon
//
//  Created by Necati Aydın on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"
#import "CBGalleryLogic.h"
#import "CBDetailContainerViewController.h"

@class CBGalleryDetailModel;

@interface CBGalleryViewController : CBBaseViewController <CBDataLoaderDelegate>

@property (nonatomic, strong) CBGalleryLogic *logic;
@property (strong, nonatomic) UIImage *backgroundImage;

@property (nonatomic, assign) BOOL startedWithPush;

@end
