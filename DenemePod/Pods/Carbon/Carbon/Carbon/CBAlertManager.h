//
//  CBAlertManager.h
//  Haber3
//
//  Created by Necati Aydın on 26/01/15.
//  Copyright (c) 2014 Mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 A facade for the Old UIAlertView and new UIAlertController. UIAlertView is used if iOS 7 and UIAlertController is used if iOS 8 and higher.
 */
@interface CBAlertManager : NSObject

/**
 Shows an alert with the givene inputs. For iOS 7, internally, it uses alert view. For iOS 8 and higher it uses the new aler controller.
 @param title Title of the alert.
 @param message Message of the alert.
 @param cancelButtonTitle Title of the default destructive alert button.
 @param otherButtonTitle Titles of the non-destructive alert buttons.
 @param viewController The view contoller in which the alert is going to be shown. This field is not used for iOS7. It is used for iOS8 and higher.
 @param completionHandler The index of the clicked button is returned in the block parameters. 0 is used for the destructive button.
 */
+ (void) showAlertWithTitle:(NSString *)title
                    message:(NSString *)message
          cancelButtonTitle:(NSString *)cancelButtonTitle
          otherButtonTitles:(NSArray *)otherButtonTitles
             viewController:(UIViewController *)viewController
          completionHandler:(void (^)(NSInteger buttonClicked))completionHandler;
@end
