//
//  CBGuideStyleConfig.h
//  Carbon
//
//  Created by Necati Aydın on 15/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBGuideStyleConfig : CBBaseConfig

/**
 Background color of the view.
 */
@property (strong, nonatomic) UIColor *backgroundColor;

/**
 Text color of the day labels.
 */
@property (strong, nonatomic) UIColor *daysTextColor;

/**
 Font of the day labels.
 */
@property (strong, nonatomic) UIFont *daysTextFont;

/**
 Background color of the feed cells.
 */
@property (nonatomic, strong) UIColor *cellBackgroundColor;

/**
 Color of the borders of the cells.
 */
@property (nonatomic, strong) UIColor *cellBorderColor;

/**
 Color of the title of the feed cells.
 */
@property (nonatomic, strong) UIColor *cellTitleColor;

/**
 Font of the title of the feed cells.
 */
@property (nonatomic, strong) UIFont *cellTitleFont;

/**
 Color of the date label.
 */
@property (nonatomic, strong) UIColor *cellDateColor;

/**
 Font of the date label.
 */
@property (nonatomic, strong) UIFont *cellDateFont;


@end
