//
//  CBDetailViewController.m
//  Carbon
//
//  Created by Necati Aydın on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNewsDetailViewController.h"
#import "CBNewsDetailStyleConfig.h"
#import "UIFont+Additions.h"
#import "UIView+LayoutConstraints.h"
#import "CBNewsDetailImageViewController.h"
#import "UIViewController+Fading.h"
#import "CBAppConfig.h"
#import "CBNavigationBarStyler.h"

static const CGFloat kMinimumImageHeight = 100;
static const CGFloat kMinimumImageWidth = 100;
static const CGFloat kWebViewBottomContentSpace = 10;

@interface CBNewsDetailViewController ()

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imageViewSizeRatio;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *webviewBottomSpace;

@end

@implementation CBNewsDetailViewController
@dynamic logic;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([CBAppConfig sharedConfig].commentsEnabledForNewsDetail) {
        
        [CBNavigationBarStyler styleRightNavigationItem:self.parentViewController.navigationItem
                                    commentButtonAction:@selector(commentButtonTapped:)
                                      shareButtonAction:@selector(shareButtonTapped:)
                                                 target:self];
    } else {
        
        [CBNavigationBarStyler styleRightNavigationItemWithShareIcon:self.parentViewController.navigationItem target:self action:@selector(shareButtonTapped:)];
    }
}

- (BOOL)shouldHideNavigationBar
{
    return self.newsDetailType == CBNewsDetailType3;
}

- (void)handleImageLoading:(UIImage *)image error:(NSError *)error
{
    [super handleImageLoading:image error:error];
    
    BOOL imageShowingCondition = !error && (image.size.width > kMinimumImageWidth && image.size.height > kMinimumImageHeight);
    
    if(!imageShowingCondition)
    {
        [self.imageView removeConstraint:self.imageViewSizeRatio];
        [self.imageView setHeightConstraint:0];
        [self.imageView layoutIfNeeded];
    }
}


#pragma mark - AdView delegate

- (void)adViewDidLoad:(SLAdView *)adView
{
    [super adViewDidLoad:adView];
    
    if(adView == self.mreView)
    {
        self.webviewBottomSpace.constant = SLAdSizeMRect.height + kWebViewBottomContentSpace;
        [self.scrollView addSubview:adView];
        [adView distanceBottomToSuperview:kWebViewBottomContentSpace];
        [adView centerXInSuperview];
    }
}

- (void)adViewDidFailToLoad:(SLAdView *)adView
{
    [super adViewDidFailToLoad:adView];

    if(adView == self.mreView)
    {
        self.webviewBottomSpace.constant = kWebViewBottomContentSpace;
    }
}

#pragma mark - Action

- (IBAction)imageTapped:(id)sender
{
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    CGRect frame = [window convertRect:self.imageView.frame fromView:self.scrollView];
    
    CBNewsDetailImageViewController *vc = [CBNewsDetailImageViewController instanceWithInitialImage:self.imageView.image
                                                                                         imageFrame:frame];

    [self presentViewControllerByFadingIn:vc];
}

@end

#pragma mark - Styling

@implementation CBNewsDetailViewController(Styling)

- (void)styleByIncrementingFontBy:(CGFloat)fontIncrementAmount
{
    CBNewsDetailStyleConfig *config = [CBNewsDetailStyleConfig sharedConfig];
    
    self.view.backgroundColor = config.viewBackgroundColor;
    
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];
    
    self.titleLabel.textColor = config.titleColor;
    self.titleLabel.font = [config.titleFont fontByIncrementingSize:fontIncrementAmount];
    
    self.spotLabel.textColor = config.spotColor;
    self.spotLabel.font = [config.spotFont fontByIncrementingSize:fontIncrementAmount];
    
    self.dateLabel.textColor = config.dateColor;
    self.dateLabel.font = [config.dateFont fontByIncrementingSize:fontIncrementAmount];
}

- (NSString *)styleContent:(NSString *)content withCSSForMaximumWidth:(CGFloat)width fontIncrementAmount:(CGFloat)fontIncrementAmount
{
    CBNewsDetailStyleConfig *config = [CBNewsDetailStyleConfig sharedConfig];
    return [NSString stringWithFormat:
            @"<style>\
            p{font-family:\"%@\";\
            font-size:%f;\
            color:rgb(%@)}\
            a{font-family:\"%@\";\
            font-size:%f;}\
            iframe{\
            max-width:100%%;\
            height:auto;\
            }\
            img {\
            max-width:100%%;\
            height:auto;\
            }\
            </style>\
            <html>\
            <body width=%f>\
            %@\
            </body>\
            </html>",
            config.contentFont.fontName,
            (long)config.contentFont.pointSize + fontIncrementAmount,
            config.contentColor,
            config.contentFont.fontName,
            (long)config.contentFont.pointSize + fontIncrementAmount,
            width,
            content ? content : @""];
}

@end

