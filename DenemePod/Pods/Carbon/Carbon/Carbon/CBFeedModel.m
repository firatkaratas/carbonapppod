//
//  CBContentModel.m
//  Carbon
//
//  Created by Semih Cihan on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedModel.h"
#import "CBFeedModelConfig.h"
#import "CBCategoryStore.h"
#import "CBFeedModelProxy.h"

@interface CBFeedModel ()

@property (strong, nonatomic) NSNumber *typeNumber;
@property (strong, nonatomic) NSNumber *cellTypeNumber;

@end

@implementation CBFeedModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"type" : [NSNull null],
             @"typeNumber" : [CBFeedModelConfig sharedConfig].type,
             @"feedId" : [CBFeedModelConfig sharedConfig].feedId,
             @"category" : [CBFeedModelConfig sharedConfig].category,
             @"date" : [CBFeedModelConfig sharedConfig].date,
             @"title" : [CBFeedModelConfig sharedConfig].title,
             @"spot" : [CBFeedModelConfig sharedConfig].spot,
             @"url" : [CBFeedModelConfig sharedConfig].url,
             @"imageUrl" : [CBFeedModelConfig sharedConfig].imageUrl,
             @"imageWidth" : [CBFeedModelConfig sharedConfig].imageWidth,
             @"imageHeight" : [CBFeedModelConfig sharedConfig].imageHeight,
             @"cellTypeNumber": [CBFeedModelConfig sharedConfig].cellType,
             };
}

+ (CBBaseModelConfig *)config
{
    return [CBFeedModelConfig sharedConfig];
}

+ (NSValueTransformer *)typeNumberJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBFeedModelProxy sharedInstance] typeNumberJSONTransformer:object];
        return [CBBaseModel stringOrNumberToNumber:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)feedIdJSONTransformer {

    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBFeedModelProxy sharedInstance] feedIdJSONTransformer:object];
        return [CBBaseModel stringOrNumberToString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)categoryJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBFeedModelProxy sharedInstance] categoryJSONTransformer:object];
        NSString *categoryId = [self stringOrNumberToString:object];
        CBCategoryModel *cat = [[CBCategoryStore sharedStore] getCategoryWithId:categoryId];
        return cat;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)dateJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBFeedModelProxy sharedInstance] dateJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)titleJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBFeedModelProxy sharedInstance] titleJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)spotJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBFeedModelProxy sharedInstance] spotJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)urlJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBFeedModelProxy sharedInstance] urlJSONTransformer:object];
        return [NSURL URLWithString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)imageUrlJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBFeedModelProxy sharedInstance] imageUrlJSONTransformer:object];
        return [NSURL URLWithString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)imageWidthJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBFeedModelProxy sharedInstance] imageWidthJSONTransformer:object];
        return [CBBaseModel stringOrNumberToNumber:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)imageHeightJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBFeedModelProxy sharedInstance] imageHeightJSONTransformer:object];
        return [CBBaseModel stringOrNumberToNumber:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

#pragma mark - other methods

- (void)setType:(FeedType)type
{
    self.typeNumber = @(type);
}

- (FeedType)type {
    
    return self.typeNumber ? self.typeNumber.integerValue : FeedTypeArticle;
}

- (NSInteger)cellType
{
    return [self.cellTypeNumber integerValue];
}

- (void)setCellType:(NSInteger)cellType
{
    _cellTypeNumber = @(cellType);
}

+ (NSString *)feedTypeStringOfFeedType:(FeedType)type {

    switch (type) {
        case FeedTypeArticle:
            return @"News";
            
        case FeedTypeGallery:
            return @"Gallery";
            
        case FeedTypeVideo:
            return @"Video";

        case FeedTypeAuthor:
            return @"Author";
            
        default:
            return @"Undefined";
    }
}

- (BOOL)hasImage
{
    return self.imageUrl != nil && [self.imageUrl absoluteString].length > 0;
}

@end
