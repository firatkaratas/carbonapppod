//
//  CBBaseCollectionViewCell.m
//  Carbon
//
//  Created by Semih Cihan on 08/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseCollectionViewCell.h"

@implementation CBBaseCollectionViewCell

+ (NSString *)reuseIdentifier
{
    return NSStringFromClass([self class]);
}

@end
