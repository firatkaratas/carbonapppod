//
//  CBFeedCell.h
//  Carbon
//
//  Created by Semih Cihan on 08/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedCell.h"

/**
 Feed Cell type 1.
 */
@interface CBFeed1Cell : CBFeedCell

@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
