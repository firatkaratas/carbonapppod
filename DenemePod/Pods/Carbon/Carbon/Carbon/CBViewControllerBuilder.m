//
//  CBViewControllerBuilder.m
//  Carbon
//
//  Created by Necati Aydın on 23/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBViewControllerBuilder.h"
#import "CBLeftMenuContainerLogic.h"
#import "CBLeftMenuContainerViewController.h"
#import "CBAppConfig.h"
#import "CBLeftMenu1ViewController.h"
#import "CBLeftMenu2ContentViewController.h"
#import "CBGalleryViewController.h"
#import "CBGalleryLogic.h"
#import "CBDetailContainerViewController.h"
#import "CBDetailContainerLogic.h"
#import "CBNewsDetailViewController.h"
#import "CBVideoDetailViewController.h"
#import "CBCommentViewController.h"
#import "CBSettingsViewController.h"
#import "CBInfoViewController.h"
#import "CBWebViewController.h"
#import "CBLeftMenu2ViewController.h"
#import "CBRotationlessNavigationController.h"
#import "CBAppConfig.h"
#import "CBGuideViewController.h"
#import "CBGuideContentViewController.h"
#import "CBAuthorDetailViewController.h"
#import "CBAuthorFeedLogic.h"


@implementation CBViewControllerBuilder

+ (UIViewController *)rootViewController
{
    CBLeftMenuContainerViewController *vc = [[UIStoryboard storyboardWithName:@"CBLeftMenuContainerViewController" bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
    vc.logic = [[CBLeftMenuContainerLogic alloc] init];
    vc.logic.delegate = vc;
    vc.startedWithPush = NO;
    return vc;
}

+ (UIViewController *)rootViewControllerForPush
{
    CBLeftMenuContainerViewController *vc = (CBLeftMenuContainerViewController *)[CBViewControllerBuilder rootViewController];
    vc.startedWithPush = YES;
    return vc;
}

+ (UIViewController *)feedViewControllerWithDelegate:(id<CBMenuContentViewControllerDelegate>)delegate
{
    UINavigationController *navigationController = [[UIStoryboard storyboardWithName:[CBFeedViewController storyboardName] bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
    CBFeedViewController *feedViewController = (CBFeedViewController *)navigationController.topViewController;
    feedViewController.delegate = delegate;
    CBFeedLogic *feedLogic = [[CBFeedLogic alloc] init];
    feedLogic.delegate = feedViewController;
    feedViewController.logic = feedLogic;
    
    return navigationController;
}

+ (UIViewController *)authorFeedViewControllerWithAuthorId:(NSString *)authorId
{
    UINavigationController *navigationController = [[UIStoryboard storyboardWithName:[CBFeedViewController storyboardName] bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
    CBFeedViewController *feedViewController = (CBFeedViewController *)navigationController.topViewController;
    CBAuthorFeedLogic *logic = [[CBAuthorFeedLogic alloc] init];
    logic.authorId = authorId;
    feedViewController.logic = logic;
    logic.delegate = feedViewController;
    
    return feedViewController;
}

+ (UIViewController *)leftMenu1ViewControllerWithDelegate:(id<CBLeftMenuContainedDelegate>)delegate rootCategory:(CBCategoryModel *)categoryModel
{
    CBLeftMenu1ViewController *vc = [[UIStoryboard storyboardWithName:[CBLeftMenu1ViewController storyboardName]
                                                               bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
    vc.leftMenuViewControllerDelegate = delegate;
    vc.rootCategory = categoryModel;
    return vc;
}

+ (UIViewController *)leftMenu2ViewControllerWithDelegate:(id<CBLeftMenuContainedDelegate>)delegate rootCategory:(CBCategoryModel *)categoryModel
{
    CBLeftMenu2ViewController *vc = [[UIStoryboard storyboardWithName:[CBLeftMenu2ViewController storyboardName] bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
    vc.rootCategory = categoryModel;
    vc.leftMenuViewControllerDelegate = delegate;
    CBRotationlessNavigationController *navigationController = [[CBRotationlessNavigationController alloc] initWithRootViewController:vc];
    return navigationController;
}

+ (UIViewController *)leftMenu2ContentViewControllerWithDelegate:(id<CBLeftMenu2ContentViewControllerDelegate>)delegate rootCategory:(CBCategoryModel *)categoryModel
{
    CBLeftMenu2ContentViewController *vc = (CBLeftMenu2ContentViewController *)[[UIStoryboard storyboardWithName:[CBLeftMenu2ContentViewController storyboardName] bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
    vc.delegate = delegate;
    vc.rootCategory = categoryModel;
    return vc;
}

+ (UIViewController *)galleryViewControllerWithLogic:(CBGalleryLogic *)logic
{
    CBGalleryViewController *vc  = [[UIStoryboard storyboardWithName:[CBGalleryViewController storyboardName] bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
    vc.logic = logic;
    logic.delegate = vc;
    return vc;
}

+ (UIViewController *)galleryViewControllerForPushWithFeedId:(NSString *)feedId
{
    CBGalleryLogic *logic = [CBGalleryLogic new];
    logic.feedId = feedId;
    CBGalleryViewController *vc = (CBGalleryViewController *)[self galleryViewControllerWithLogic:logic];
    vc.startedWithPush = YES;
    return vc;
}

+ (UIViewController *)detailContainerViewControllerWithLogic:(CBDetailContainerLogic *)logic
                                               startingIndex:(NSInteger)index
{
    CBDetailContainerViewController *vc = [[UIStoryboard storyboardWithName:[CBDetailContainerViewController storyboardName] bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
    vc.logic = logic;
    logic.delegate = vc;
    vc.currentIndex = index;
    
    return vc;
}

+ (UIViewController *)newsDetailViewControllerWithLogic:(CBBaseNewsDetailLogic *)logic
{
    CBNewsDetailViewController *vc = nil;
    if([CBAppConfig sharedConfig].newsDetailType == CBNewsDetailType2)
    {
        vc = [[UIStoryboard storyboardWithName:@"CBNewsDetail2ViewController" bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
        vc.newsDetailType = CBNewsDetailType2;
    }
    else if([CBAppConfig sharedConfig].newsDetailType == CBNewsDetailType3)
    {
        vc = [[UIStoryboard storyboardWithName:@"CBNewsDetail3ViewController" bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
        vc.newsDetailType = CBNewsDetailType3;
    }
    
    vc.logic = logic;
    vc.logic.delegate = vc;
    
    return vc;
}

+ (UIViewController *)newsDetailViewControllerForPushWithFeedId:(NSString *)feedId
{
    CBBaseNewsDetailLogic *logic = [[CBBaseNewsDetailLogic alloc] initWithFeedId:feedId];
    CBNewsDetailViewController *vc = (CBNewsDetailViewController *)[self newsDetailViewControllerWithLogic:logic];
    vc.startedWithPush = YES;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    
    navigationController.navigationBarHidden = [CBAppConfig sharedConfig].newsDetailType == CBNewsDetailType2;
    
    return navigationController;
}

+ (UIViewController *)videoDetailViewControllerWithLogic:(CBVideoDetailLogic *)logic
{
    CBVideoDetailViewController *vc = nil;
    if([CBAppConfig sharedConfig].videoDetailType == CBVideoDetailType3)
    {
        vc = [[UIStoryboard storyboardWithName:@"CBVideoDetail3ViewController" bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
        vc.videoDetailType = CBVideoDetailType3;
    }
    else if([CBAppConfig sharedConfig].videoDetailType == CBVideoDetailType2)
    {
        vc = [[UIStoryboard storyboardWithName:@"CBVideoDetail2ViewController" bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
        vc.videoDetailType = CBVideoDetailType2;
    }
    
    vc.logic = logic;
    vc.logic.delegate = vc;
    
    return vc;
}

+ (UIViewController *)videoDetailViewControllerForPushWithFeedId:(NSString *)feedId
{
    CBVideoDetailLogic *logic = [[CBVideoDetailLogic alloc] initWithFeedId:feedId];
    CBVideoDetailViewController *vc = (CBVideoDetailViewController *)[self videoDetailViewControllerWithLogic:logic];
    vc.startedWithPush = YES;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    
    navigationController.navigationBarHidden = [CBAppConfig sharedConfig].videoDetailType == CBVideoDetailType3;
    
    return navigationController;
}

+ (UIViewController *)authorDetailViewControllerWithLogic:(CBAuthorDetailLogic *)logic
{
    CBAuthorDetailViewController *vc = nil;
    if([CBAppConfig sharedConfig].authorDetailType == CBAuthorDetailType2)
    {
        vc = [[UIStoryboard storyboardWithName:@"CBAuthorDetail2ViewController" bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
        vc.authorDetailType = CBAuthorDetailType2;
    }
    
    vc.logic = logic;
    vc.logic.delegate = vc;
    
    return vc;
}

+ (UIViewController *)authorDetailViewControllerForPushWithFeedId:(NSString *)feedId
{
    CBAuthorDetailLogic *logic = [[CBAuthorDetailLogic alloc] initWithFeedId:feedId];
    CBAuthorDetailViewController *vc = (CBAuthorDetailViewController *)[self authorDetailViewControllerWithLogic:logic];
    vc.startedWithPush = YES;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    
    return navigationController;
}

+ (UIViewController *)commentViewControllerWithLogic:(CBCommentLogic *)logic
{
    CBCommentViewController *vc = [UIStoryboard storyboardWithName:[CBCommentViewController storyboardName]
                                                            bundle:[NSBundle frameworkBundle]].instantiateInitialViewController;
    vc.logic = logic;
    vc.logic.delegate = vc;
    
    return vc;
}

+ (UIViewController *)settingsViewController
{
    UINavigationController *navigationController = [UIStoryboard storyboardWithName:[CBSettingsViewController storyboardName] bundle:[NSBundle frameworkBundle]].instantiateInitialViewController;
    
    return navigationController;
}

+ (UIViewController *)infoViewController
{
    UINavigationController *navigationController = [UIStoryboard storyboardWithName:[CBInfoViewController storyboardName]
                                     bundle:[NSBundle frameworkBundle]].instantiateInitialViewController;
    
    CBInfoViewController *vc = (CBInfoViewController *)(navigationController.topViewController);
    vc.logic = [[CBInfoLogic alloc] init];
    
    return navigationController;
}

+ (UIViewController *)webViewWithUrlRequest:(NSURLRequest *)request title:(NSString *)title
{
    UIStoryboard * sb= [UIStoryboard storyboardWithName:[CBWebViewController storyboardName]
                                                 bundle:[NSBundle frameworkBundle]];
    
    CBWebViewController *vc = [sb instantiateInitialViewController];
    vc.request = request;
    vc.shareTitle = title;
    
    return vc;
}

+ (UIViewController *)guideViewControllerWithDelegate:(UIViewController <CBMenuContentViewControllerDelegate> *)delegate
{
    CBGuideViewController *vc = [[UIStoryboard storyboardWithName:@"CBGuideViewController" bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
    vc.logic = [CBGuideLogic new];
    vc.logic.delegate = vc;
    vc.delegate = delegate;
    return [[CBRotationlessNavigationController alloc] initWithRootViewController:vc];
}

+ (UIViewController *)guideContentViewControllerWithGuideDayModel:(CBGuideDayModel *)guideDayModel
{
    CBGuideContentViewController *vc = [[UIStoryboard storyboardWithName:[CBGuideContentViewController storyboardName] bundle:[NSBundle frameworkBundle]] instantiateInitialViewController];
    vc.guideDayModel = guideDayModel;
    return vc;
}

@end
