//
//  CBAuthorDetailLogic.h
//  Carbon
//
//  Created by Necati Aydın on 08/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseNewsDetailLogic.h"
#import "CBAuthorDetailModel.h"

@class CBAuthorDetailModel;

@interface CBAuthorDetailLogic : CBBaseNewsDetailLogic

/**
 CBAuthorDetailModel object loaded by the logic.
 */
@property (nonatomic, strong) CBAuthorDetailModel *newsDetail;

@end
