//
//  ReachabilityManager.m
//  Carbon
//
//  Created by Semih Cihan on 09/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBReachabilityManager.h"
#import "CBAlertManager.h"

@interface CBReachabilityManager ()

+ (void)reachabilityChanged:(NSNotification *)notification;

@end

@implementation CBReachabilityManager

+ (void)startReachability {
    Reachability *reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    [reach startNotifier];
}

+ (void)reachabilityChanged:(NSNotification *)notification {
    Reachability *reachability = notification.object;
    if (!reachability.isReachable) {
        DebugLog(@"Unreachable");
        __weak typeof(self) weakSelf = self;
        [CBAlertManager showAlertWithTitle:nil
                                   message:NSLocalizedString(@"NoConnectionMessage", nil)
                         cancelButtonTitle:nil
                         otherButtonTitles:@[NSLocalizedString(@"Retry", nil)]
                            viewController:[[[UIApplication sharedApplication] delegate] window].rootViewController
                         completionHandler:^(NSInteger buttonClicked) {
                             [weakSelf reachabilityChanged:notification];
                         }];
    }
}

@end
