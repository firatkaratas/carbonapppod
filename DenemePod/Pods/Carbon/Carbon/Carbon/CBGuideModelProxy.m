//
//  CBGuideModelProxy.m
//  Carbon
//
//  Created by Semih Cihan on 17/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGuideModelProxy.h"

@implementation CBGuideModelProxy

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[CBGuideModelProxy alloc] init];
    });
    return sharedInstance;
}

- (id)titleJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(titleJSONTransformerOverridden:)]) {
        return [self titleJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)imageUrlJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(imageUrlJSONTransformerOverridden:)]) {
        return [self imageUrlJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)tagJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(tagJSONTransformerOverridden:)]) {
        return [self tagJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)timeJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(timeJSONTransformerOverridden:)]) {
        return [self timeJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

@end
