//
//  CBNavigationBarStyler.h
//  Carbon
//
//  Created by Necati Aydın on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CBNavigationBarStyler : NSObject

/**
 Styles navigation bar colors using CBNavigationBarConfig.
 */
+ (void)styleNavigationBarColors;

/**
 Styles navigation bar title font and color using CBNavigationBarConfig.
 */
+ (void)styleNavigationBarTitle;

/**
 Styles left navigation item so that it has a menu button with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
+ (void)styleLeftNavigationItemWithMenuIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles right navigation item so that it has a share button with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
+ (void)styleRightNavigationItemWithShareIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles right navigation item so that it has a live stream button with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
+ (void)styleRightNavigationItemWithStreamIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles navigation item so that it has a back button on the left with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
+ (void)styleNavigationItemWithBackIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles navigation item so that it has a close button on the left with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
+ (void)styleNavigationItemWithCloseIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles navigation item so that it has a settings button and info button on the right with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param settingsAction Settings button action selector.
 @param infoAction Info button action selector.
 @param target Action target.
 */
+ (void)styleRightNavigationItem:(UINavigationItem *)navigationItem
            settingsButtonAction:(SEL)settingsAction
                infoButtonAction:(SEL)infoAction
                          target:(id)target;

/**
 Styles navigation item so that it has a comment button and share button on the right with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param commentAction Comment button action selector.
 @param shareAction share button action selector.
 @param target Action target.
 */
+ (void)styleRightNavigationItem:(UINavigationItem *)navigationItem
             commentButtonAction:(SEL)commentAction
               shareButtonAction:(SEL)shareAction
                          target:(id)target;

/**
 Styles navigation item so that it has a title logo.
 @param navigationItem Navigation item to be styled.
 */
+ (void)styleWithTitleLogo:(UINavigationItem *)navigationItem;

@end
