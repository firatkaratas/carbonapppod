//
//  CBCommentCell.m
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBCommentCell.h"
#import "UIView+Separator.h"
#import "NSString+Additions.h"

static const CGFloat kCommentLabelVerticalSpaceToContainerConstraint = 14.f;
static const CGFloat kDetailLabelVerticalSpaceToCommentLabelConstraint = 7.f;
static const CGFloat kDetailLabelVerticalSpaceToContainerConstraint = 12.f;

static const CGFloat kTextHorizontalSpace = 20.f;

@interface CBCommentCell()


@end

@implementation CBCommentCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.separators = [self addFrameWithLineWidth:kCellBorderLineWidth];
    
    [self.commentLabelVerticalSpaceToContainerConstraint setConstant:kCommentLabelVerticalSpaceToContainerConstraint];
    [self.detailLabelVerticalSpaceToCommentLabelConstraint setConstant:kDetailLabelVerticalSpaceToCommentLabelConstraint];
}

+ (CGSize)cellSizeForWidth:(CGFloat)width commentText:(NSString *)commentText commentFont:(UIFont *)commentFont detailText:(NSString *)detailText detailFont:(UIFont *)detailFont
{
    CGFloat textWidth = width - 2 * kTextHorizontalSpace;
    CGFloat height = [commentText calculateBoundingRectHeightWithinLabelWithConstrainedWidth:textWidth font:commentFont];
    height += [detailText calculateBoundingRectHeightWithinLabelWithConstrainedWidth:textWidth font:detailFont];
    height += kCommentLabelVerticalSpaceToContainerConstraint + kDetailLabelVerticalSpaceToCommentLabelConstraint + kDetailLabelVerticalSpaceToContainerConstraint;
    return CGSizeMake(width, height);
}

@end
