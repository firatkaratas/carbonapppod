//
//  CBFeedModelConfig.h
//  Carbon
//
//  Created by Semih Cihan on 14/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@interface CBFeedModelConfig : CBBaseModelConfig

/**
 Type of the feed. For example: News, advertorial or video.
 */
@property (strong, nonatomic) NSString *type;

/**
 Id of the feed.
 */
@property (strong, nonatomic) NSString *feedId;

/**
 Category of the feed.
 */
@property (strong, nonatomic) NSString *category;

/**
 Date of the feed.
 */
@property (strong, nonatomic) NSString *date;

/**
 Title of the feed.
 */
@property (strong, nonatomic) NSString *title;

/**
 Sub title of the feed.
 */
@property (strong, nonatomic) NSString *spot;

/**
 Web url if exists.
 */
@property (strong, nonatomic) NSString *url;

/**
 URL of the feed image.
 */
@property (strong, nonatomic) NSString *imageUrl;

/**
 Original width of the feed image.
 */
@property (strong, nonatomic) NSString *imageWidth;

/**
 Original height of the feed image.
 */
@property (strong, nonatomic) NSString *imageHeight;

/**
 Cell type of the feed.
 */
@property (strong, nonatomic) NSString *cellType;

@end
