//
//  CBPushModel.h
//  Carbon
//
//  Created by Necati Aydın on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedModel.h"

@interface CBPushModel : CBBaseModel <MTLJSONSerializing>

/**
 Alert text for the push notification.
 */
@property (nonatomic, strong) NSString *alert;

/**
 feedId for the feed to be opened. It can be nil.
 */
@property (nonatomic, strong) NSString *feedId;

/**
 feedType for the feed to be opened.
 */
@property (nonatomic, assign) FeedType feedType;

@end
