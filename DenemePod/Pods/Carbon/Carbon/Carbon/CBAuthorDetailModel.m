//
//  CBAuthorDetail.m
//  Carbon
//
//  Created by Necati Aydın on 08/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBAuthorDetailModel.h"
#import "CBAuthorDetailModelConfig.h"

@implementation CBAuthorDetailModel

+ (CBBaseModelConfig *)config
{
    return [CBAuthorDetailModelConfig sharedConfig];
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"detailId" : [CBAuthorDetailModelConfig sharedConfig].detailId,
             @"date" : [CBAuthorDetailModelConfig sharedConfig].date,
             @"category" : [CBAuthorDetailModelConfig sharedConfig].category,
             @"title" : [CBAuthorDetailModelConfig sharedConfig].title,
             @"spot" : [CBAuthorDetailModelConfig sharedConfig].spot,
             @"imageUrl" : [CBAuthorDetailModelConfig sharedConfig].imageUrl,
             @"url" : [CBAuthorDetailModelConfig sharedConfig].url,
             @"content" : [CBAuthorDetailModelConfig sharedConfig].content,
             @"authorId" : [CBAuthorDetailModelConfig sharedConfig].authorId,};
}

@end
