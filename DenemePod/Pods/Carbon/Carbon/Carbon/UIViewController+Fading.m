//
//  UIViewController+Fading.m
//  Carbon
//
//  Created by Necati Aydın on 13/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIViewController+Fading.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIViewControllerFading)

@implementation UIViewController (Fading)

- (void)dismissViewControllerByFadingOut
{
    [self.view.window.layer addAnimation:[self fadeTransition]
                                  forKey:kCATransition];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)presentViewControllerByFadingIn:(UIViewController *)viewController
{
    [self.view.window.layer addAnimation:[self fadeTransition] forKey:kCATransition];
    [self presentViewController:viewController animated:NO completion:nil];
}

- (CATransition *)fadeTransition
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromTop;
    
    return transition;
}

@end
