//
//  CBPushNotificationManager.h
//  Carbon
//
//  Created by Necati Aydin on 5.3.15.
//  Copyright (c) 2014 Mobilike. All rights reserved.
//

#import "CBPushNotificationManager.h"
#import <Parse/Parse.h>
#import "UIDevice+Additions.h"
#import "CBAlertManager.h"
#import "NSUserDefaults+Additions.h"
#import "CBAppDelegate.h"
#import "CBFeedModel.h"
#import "CBViewControllerBuilder.h"
#import "CBPushModel.h"
#import "CBAppConfig.h"

@protocol CBPushNotificationManagerAppDelegateProtocol

- (void)startAppWithPush:(CBPushModel *)pushModel;

@end

@implementation CBPushNotificationManager

+ (BOOL)didRegisterForNotificationsAtAll
{
    NSNumber *pushNotificationsEnabled = [NSUserDefaults getPushNotificationEnabled];
    return pushNotificationsEnabled != nil;
}

+ (BOOL)notificationsTurnedOn
{
    NSNumber *pushNotificationsEnabled = [NSUserDefaults getPushNotificationEnabled];
    return pushNotificationsEnabled && [pushNotificationsEnabled isEqualToNumber:@YES];
}

+ (void)turnOnNotifications
{
    [self registerPushNotifications];
    
    [NSUserDefaults setPushNotificationEnabled:YES];
}

+ (void)turnOffNotifications
{
    [self unregisterPushNotifications];
    
    [NSUserDefaults setPushNotificationEnabled:NO];
}

+ (void)registerPushNotifications
{
    if([UIDevice iOS8AndHigher])
    {
        UIApplication *application = [UIApplication sharedApplication];
        
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
        
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
}

+ (void)unregisterPushNotifications
{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}

+ (BOOL)registerParse
{
    if([CBAppConfig sharedConfig].parseApplicationId && ![[CBAppConfig sharedConfig].parseApplicationId isEqualToString:@""])
    {
        [Parse setApplicationId:[CBAppConfig sharedConfig].parseApplicationId
                      clientKey:[CBAppConfig sharedConfig].parseClientKey];
        return YES;
    }
    else
    {
        return NO;
    }

}

#pragma mark - App Delegate

+ (BOOL)handleDidFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    //if the application start without a push notification
    if (userInfo != nil)
    {
        [CBPushNotificationManager handleDidReceiveRemoteNotification:userInfo applicationOffline:YES];
    }
    
    BOOL registeredPush = [CBPushNotificationManager registerParse];
    if(!registeredPush)
    {
        return YES;
    }
    
    if(![CBPushNotificationManager didRegisterForNotificationsAtAll])
    {
        [CBPushNotificationManager turnOnNotifications];
    }
    else
    {
        if([CBPushNotificationManager notificationsTurnedOn])
        {
            [CBPushNotificationManager turnOnNotifications];
        }
    }
    
    CBPushModel *push = [MTLJSONAdapter modelOfClass:[CBPushModel class] fromJSONDictionary:[userInfo objectForKey:@"aps"]  error:nil];
    return (userInfo == nil) || (!push.feedId);
}

+ (void)handleDidReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self handleDidReceiveRemoteNotification:userInfo applicationOffline:NO];
}

+ (void)handleDidRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

#pragma mark -

+ (void)handleDidReceiveRemoteNotification:(NSDictionary *)userInfo applicationOffline:(BOOL)applicationOffline
{
    NSError *pushError;
    CBPushModel *push = [MTLJSONAdapter modelOfClass:[CBPushModel class] fromJSONDictionary:[userInfo objectForKey:@"aps"] error:&pushError];

    if(pushError || !push || !push.feedId)
    {
        [PFPush handlePush:userInfo];
        return;
    }
    
    if(!applicationOffline)
    {
        CBPushModel __block *pushInBlock = push;
        __weak typeof(self) weakSelf = self;
        [CBAlertManager showAlertWithTitle:nil
                                   message:push.alert
                         cancelButtonTitle:NSLocalizedString(@"Close", nil)
                         otherButtonTitles:@[NSLocalizedString(@"GoToNews", nil)]
                            viewController:[[[UIApplication sharedApplication] delegate] window].rootViewController
                         completionHandler:^(NSInteger buttonClicked)
         {
             if(buttonClicked == 1)
             {
                 [weakSelf handlePush:pushInBlock];
                 
             }
         }];
    }
    else
    {
        [self handlePush:push];
    }
    
}

+ (void)handlePush:(CBPushModel *)pushModel
{
    [[UIApplication sharedApplication].delegate performSelector:@selector(startAppWithPush:) withObject:pushModel];
}


@end
