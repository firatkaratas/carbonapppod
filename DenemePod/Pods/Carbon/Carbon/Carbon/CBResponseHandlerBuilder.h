//
//  CBResponseHandlerBuilder.h
//  Carbon
//
//  Created by Necati Aydın on 26/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"

@class CBBaseModelConfig;

typedef void (^SuccessBlockWithOperation)(AFHTTPRequestOperation *operation, id responseObject);
typedef void (^ErrorBlockWithOperation)(AFHTTPRequestOperation *operation, NSError *error);

/**
 Response builder class. You can write a category for ResponseHandlerBuilder, an empty subclass of this class, to override the  methods of this class.
 */
@interface CBResponseHandlerBuilder : NSObject

/**
 The most generic success handler builder method.
 @param rootKey If not nil, the object with the root key is extracted. If nil nothing happens.
 @param proxyBlock It is executed after root handling. It is the perfect place to apply service specific coding.
 @param modelConfig It not nil, given config is used for mapping. If it is nil, the config plist is used.
@param isArray Boolean to indicate if the response should be an array or a single object of type classToMap.
 @param classToMap Model class to map the response. Should be a subclass of MTLModel and should conform to MTLJSONSerializing. If classToMap is nil, then no mapping is done and the completionBlock is called immediately.
 @param completionBlock Completion block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)successHandlerWithRootKey:(NSString *)rootKey
                                            proxyBlock:(id (^)(id response))proxyBlock
                                           modelConfig:(CBBaseModelConfig *)config
                                               isArray:(BOOL)isArray
                                            classToMap:(Class)classToMap
                                       completionBlock:(void (^)(id response))completionBlock;

/**
 A generic success handler builder method.
 @param classToMap Model class to map the response. Should be a subclass of MTLModel and should conform to MTLJSONSerializing. If classToMap is nil, then no mapping is done and the completionBlock is called immediately.
 @param config Model config object for custom mapping of objects.
 @param isArray Boolean to indicate if the response should be an array or a single object of type classToMap.
 @param completionBlock Completion block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)successHandlerWithClassToMap:(Class)classToMap
                                              modelConfig:(CBBaseModelConfig *)config
                                                  isArray:(BOOL)isArray
                                          completionBlock:(void (^)(id response))completionBlock;

/**
 A generic success handler builder method.
 @param classToMap Model class to map the response. Should be a subclass of MTLModel and should conform to MTLJSONSerializing. If classToMap is nil, then no mapping is done and the self.config is called immediately.
 @param isArray Boolean to indicate if the response should be an array or a single object of type classToMap.
 @param completionBlock Completion block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)successHandlerWithClassToMap:(Class)classToMap
                                                  isArray:(BOOL)isArray
                                          completionBlock:(void (^)(id response))completionBlock;

/**
 A generic error handler builder method.
 @param errorBlock Failure block to be called on failures.
 @return Returns the error handler built with the given parameters.
 */
+ (ErrorBlockWithOperation)errorHandlerWithFailureBlock:(void (^)(NSError *error))errorBlock;

/**
 Success handler builder method for news detail request.
 @param completionBlock Success block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)newsDetailSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock;

/**
 Success handler builder method for video detail request.
 @param completionBlock Success block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)videoDetailSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock;

/**
 Success handler builder method for author detail request.
 @param completionBlock Success block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)authorDetailSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock;

/**
 Success handler builder method for comments request.
 @param completionBlock Success block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)commentsSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock;

/**
 Success handler builder method for gallery detail request.
 @param completionBlock Success block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)galleryDetailSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock;

/**
 Success handler builder method for categories request.
 @param completionBlock Success block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)categoriesSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock;

/**
 Success handler builder method for category feed request.
 @param completionBlock Success block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)categoryFeedSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock;

/**
 Success handler builder method for author feed request.
 @param completionBlock Success block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)authorFeedSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock;

/**
 Success handler builder method for live stream request.
 @param completionBlock Success block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)liveStreamSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock;

/**
 Success handler builder method for guide request.
 @param completionBlock Success block to be called after successful network requests.
 @return Returns the success handler built with the given parameters.
 */
+ (SuccessBlockWithOperation)guideSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock;

@end
