//
//  CBGuideModelProxy.h
//  Carbon
//
//  Created by Semih Cihan on 17/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelProxy.h"

@protocol CBGuideDayModelProxyProtocol <NSObject>

@optional

- (NSArray *)guideItemsJSONTransformerOverridden:(id)object;
- (NSString *)dateJSONTransformerOverridden:(id)object;
- (NSNumber *)dayOfTheWeekJSONTransformerOverridden:(id)object;

@end

@interface CBGuideDayModelProxy : CBBaseModelProxy <CBGuideDayModelProxyProtocol>

+ (instancetype)sharedInstance;

- (id)guideItemsJSONTransformer:(id)object;
- (id)dateJSONTransformer:(id)object;
- (id)dayOfTheWeekJSONTransformer:(id)object;

@end
