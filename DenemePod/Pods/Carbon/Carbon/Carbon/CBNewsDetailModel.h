//
//  CBNewsDetailModel.h
//  Carbon
//
//  Created by Semih Cihan on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"
#import "CBCategoryModel.h"

/**
 @description This class is the model for news detail data.
 */
@interface CBNewsDetailModel : CBBaseModel

@property (strong, nonatomic) NSString *detailId;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) CBCategoryModel *category;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *spot;
@property (strong, nonatomic) NSURL *imageUrl;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *content;

@end
