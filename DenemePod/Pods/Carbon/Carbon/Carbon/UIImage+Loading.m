//
//  UIImage+Loading.m
//  Carbon
//
//  Created by Necati Aydın on 17/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIImage+Loading.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIImageLoading)

@implementation UIImage (Loading)

+ (UIImage *)imageNamed:(NSString *)name inBundle:(NSBundle *)bundle
{
    UIImage *imageFromMainBundle = [UIImage imageNamed:name];
    if(imageFromMainBundle)
    {
        return imageFromMainBundle;
    }
    
    NSString *imagePath = [bundle pathForResource:name ofType:@"png"];
    return [UIImage imageWithContentsOfFile:imagePath];
}

@end
