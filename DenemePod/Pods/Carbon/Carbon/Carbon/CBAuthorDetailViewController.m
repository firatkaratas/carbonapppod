//
//  CBAuthorDetailViewController.m
//  Carbon
//
//  Created by Necati Aydın on 06/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBAuthorDetailViewController.h"
#import "CBAuthorDetailStyleConfig.h"
#import "UIFont+Additions.h"
#import "CBNavigationBarStyler.h"
#import "CBViewControllerBuilder.h"
#import "CBFeedViewController.h"
#import "CBFeedStyleConfig.h"

@interface CBAuthorDetailViewController ()

@end

@implementation CBAuthorDetailViewController
@dynamic logic;

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.imageView.layer.cornerRadius = CGRectGetHeight(self.imageView.frame)/2;
    self.imageView.clipsToBounds = YES;
    
    self.parentViewController.navigationItem.rightBarButtonItem = nil;
}

- (BOOL)shouldHideNavigationBar
{
    return NO;
}

- (FeedType)feedType
{
    return FeedTypeAuthor;
}

- (void)handleImageLoading:(UIImage *)image error:(NSError *)error
{
    [super handleImageLoading:image error:error];
    
    if(!image || error)
    {
        self.imageView.hidden = YES;
    }
}

- (IBAction)imageTapped:(id)sender
{
    //do nothing
}

- (IBAction)allArticlesButtonTapped:(id)sender
{
    CBFeedViewController *vc = (CBFeedViewController *)[CBViewControllerBuilder authorFeedViewControllerWithAuthorId:self.logic.newsDetail.authorId];
    vc.cellType = CBFeedCellType7;
    [vc.logic loadData];
    [self.parentViewController.navigationController pushViewController:vc animated:YES];
}

- (void)dataLoaded
{
    [super dataLoaded];
    
    if(self.logic.newsDetail.url)
    {
        [CBNavigationBarStyler styleRightNavigationItemWithShareIcon:self.parentViewController. navigationItem target:self  action:@selector(shareButtonTapped:)];
    }
}


@end

#pragma mark - Styling

@implementation CBAuthorDetailViewController(Styling)

- (void)styleByIncrementingFontBy:(CGFloat)fontIncrementAmount
{
    CBAuthorDetailStyleConfig *config = [CBAuthorDetailStyleConfig sharedConfig];
    
    self.view.backgroundColor = config.viewBackgroundColor;
    
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];
    
    self.titleLabel.textColor = config.titleColor;
    self.titleLabel.font = [config.titleFont fontByIncrementingSize:fontIncrementAmount];
    
    self.spotLabel.textColor = config.spotColor;
    self.spotLabel.font = [config.spotFont fontByIncrementingSize:fontIncrementAmount];
    
    self.dateLabel.textColor = config.dateColor;
    self.dateLabel.font = [config.dateFont fontByIncrementingSize:fontIncrementAmount];
}

- (NSString *)styleContent:(NSString *)content withCSSForMaximumWidth:(CGFloat)width fontIncrementAmount:(CGFloat)fontIncrementAmount
{
    CBAuthorDetailStyleConfig *config = [CBAuthorDetailStyleConfig sharedConfig];
    return [NSString stringWithFormat:
            @"<style>\
            p{font-family:\"%@\";\
            font-size:%f;\
            color:rgb(%@)}\
            a{font-family:\"%@\";\
            font-size:%f;}\
            iframe{\
            max-width:100%%;\
            height:auto;\
            }\
            img {\
            max-width:100%%;\
            height:auto;\
            }\
            </style>\
            <html>\
            <body width=%f>\
            %@\
            </body>\
            </html>",
            config.contentFont.fontName,
            (long)config.contentFont.pointSize + fontIncrementAmount,
            config.contentColor,
            config.contentFont.fontName,
            (long)config.contentFont.pointSize + fontIncrementAmount,
            width,
            content ? content : @""];
}

@end
