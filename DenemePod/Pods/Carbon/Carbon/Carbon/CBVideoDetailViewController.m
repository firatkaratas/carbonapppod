//
//  CBVideoDetailViewController.m
//  Carbon
//
//  Created by Semih Cihan on 06/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBVideoDetailViewController.h"
#import "CBVideoDetailModel.h"
#import "UIImageView+WebCache.h"
#import "UIFont+Additions.h"
#import "UIView+LayoutConstraints.h"
#import "CBVideoDetailStyleConfig.h"
#import "CBFeed4Cell.h"
#import "UIViewController+CBFeedSmallImageCell.h"
#import "CBSuggestedVideoModel.h"
#import "CBNavigationBarStyler.h"
#import "CBViewControllerBuilder.h"
#import "UICollectionView+Additions.h"
#import "CBAnalyticsManager.h"
#import "CBAppConfig.h"
#import "NSString+Additions.h"

static const CGFloat kMinimumImageHeight = 100;
static const CGFloat kMinimumImageWidth = 100;
static const CGFloat kCollectionViewSpacing = 10;
static const CGFloat kStatusBarHeight = 20;

@interface CBVideoDetailViewController(Styling)


- (void)styleFeedSmallImageCell:(CBFeed4Cell *)cell;

@end

@interface CBVideoDetailViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SLCollectionViewAdManagerDelegate>

//layout constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *suggestedVideosTopSpaceConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *scrollViewTopViewTopSpace;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *webViewToCollectionViewSpace;

//play view
@property (nonatomic, strong) IBOutlet UIView *thumbnailView;
@property (weak, nonatomic) IBOutlet UIImageView *playImageView;
@property (nonatomic, weak) IBOutlet UIButton *backButton;

//scroll view
@property (weak, nonatomic) IBOutlet UIImageView *viewCountImageView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *viewCountLabel;

//suggested videos
@property (weak, nonatomic) IBOutlet UIView *suggestedVideosTitleView;
@property (weak, nonatomic) IBOutlet UILabel *suggestedVideosLabel;

@end

@implementation CBVideoDetailViewController

@dynamic logic;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.collectionView registerClassForDefaultReuseIdentifier:[CBFeed4Cell class]];

    [CBNavigationBarStyler styleWithTitleLogo:self.navigationItem];
    [CBNavigationBarStyler styleNavigationItemWithBackIcon:self.navigationItem target:self action:@selector(backButtonTapped:)];
    [self.suggestedVideosLabel setText:NSLocalizedString(@"suggestedVideos", @"İlgili Videolar")];

    
    if([CBVideoDetailStyleConfig sharedConfig].thumbnailScrollsWithContent)
    {
        [self moveThumbnailToScrollView];
    }
    
    if ([CBAppConfig sharedConfig].commentsEnabledForVideoDetail) {
        
        [CBNavigationBarStyler styleRightNavigationItem:self.parentViewController.navigationItem
                                    commentButtonAction:@selector(commentButtonTapped:)
                                      shareButtonAction:@selector(shareButtonTapped:)
                                                 target:self];
    } else {
        
        [CBNavigationBarStyler styleRightNavigationItemWithShareIcon:self.parentViewController.navigationItem target:self action:@selector(shareButtonTapped:)];
    }
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swiped:)];
    [self.view addGestureRecognizer:recognizer];
}

- (void)moveThumbnailToScrollView
{
    [self.thumbnailView removeFromSuperview];
    if(self.videoDetailType == CBVideoDetailType3)
    {
        [self.scrollView distanceTopToSuperview:kStatusBarHeight];
    }
    else if(self.videoDetailType == CBVideoDetailType2)
    {
        [self.scrollView distanceTopToSuperview:0];
    }
    
    [self.scrollView addSubview:self.thumbnailView];
    [self.thumbnailView distanceTopToSuperview:0];
    [self.thumbnailView distanceRightToSuperview:0];
    [self.thumbnailView distanceLeftToSuperview:0];
    
    [self.thumbnailView layoutIfNeeded];
    self.scrollViewTopViewTopSpace.constant = self.scrollViewTopViewTopSpace.constant + CGRectGetHeight(self.thumbnailView.frame);
}

- (void)dealloc
{
    _collectionView.delegate = nil;
    _collectionView.dataSource = nil;
}

- (BOOL)shouldHideNavigationBar
{
    return self.videoDetailType == CBVideoDetailType3;
}

- (void)configure
{
    [super configure];
    [self.playImageView setHidden:NO];
    
    [self.collectionViewHeightConstraint setConstant:[self calculateCollectionViewHeight:self.collectionView dataSource:self]];
    [self.collectionView reloadData];
}

- (CGFloat)calculateCollectionViewHeight:(UICollectionView *)collectionView
                           dataSource:(id<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>)delegateDataSource {
    
    CGFloat collectionViewExpectedHeight = 0;
    UIEdgeInsets insetsForSection = [delegateDataSource collectionView:collectionView layout:collectionView.collectionViewLayout insetForSectionAtIndex:0];
    collectionViewExpectedHeight += insetsForSection.top + insetsForSection.bottom;
    CGFloat lineSpacing = [delegateDataSource collectionView:collectionView layout:collectionView.collectionViewLayout minimumLineSpacingForSectionAtIndex:0];
    for (int i = 0; i < [delegateDataSource collectionView:collectionView numberOfItemsInSection:0]; i++) {
        collectionViewExpectedHeight += [delegateDataSource collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]].height + lineSpacing;
    }
    collectionViewExpectedHeight -= lineSpacing;
    
    return collectionViewExpectedHeight;
}

- (void)configureWithModel
{
    [super configureWithModel];
    
    CBVideoDetailModel *detailModel = self.logic.newsDetail;

    if (detailModel.viewCount && [detailModel.viewCount integerValue] > 0) {
        self.viewCountLabel.text = [[detailModel.viewCount stringValue] groupedStringForNumber];
    } else {
        [self.viewCountImageView setHidden:YES];
        [self.viewCountLabel setHidden:YES];
    }
    
    if ([detailModel.suggestedVideos count] == 0) {
        [self.collectionView setHidden:YES];
        [self.suggestedVideosTitleView setHidden:YES];
    }
}

- (void)handleImageLoading:(UIImage *)image error:(NSError *)error
{
    [super handleImageLoading:image error:error];
    
    BOOL useFetchedImage = !error && (image.size.width > kMinimumImageWidth && image.size.height > kMinimumImageHeight);
    
    if (!useFetchedImage) {
        [self.imageView setImage:[UIImage imageNamed:@"placeholder" inBundle:[NSBundle frameworkBundle]]];
    }
}

#pragma mark - Ads

- (void)startAds
{
    [super startAds];
    
    [self startCollectionViewAdWithEntityName:[self entityNameWithoutType] collectionView:self.collectionView dataSource:self.logic.newsDetail.suggestedVideos];
}

#pragma mark - SLCollectionViewAdManagerDelegate

- (void)adInsertedCollectionViewAtIndexPath:(NSIndexPath *)indexPath {
    [self configure];
}

#pragma mark - Logic Delegate

- (void)dataLoaded
{
    [super dataLoaded];
    
    self.playImageView.hidden = NO;
    self.viewCountImageView.hidden = NO;
    self.backButton.hidden = NO;
}

- (FeedType)feedType
{
    return FeedTypeVideo;
}

- (void)dataLoadedWithError:(NSString *)errorMessage
{
    [super dataLoadedWithError:errorMessage];
    
    [self.playImageView setHidden:YES];
    [self.viewCountImageView setHidden:YES];
}

#pragma mark - collection view delegate & data source

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10.f;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10.f, 0.f, 10.f, 0.f);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.logic.newsDetail.suggestedVideos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.collectionViewAdManager shouldShowAdAtIndexPath:indexPath]) {
        return [self.collectionViewAdManager cellForItemAtIndexPath:indexPath];
    } else {
        CBFeed4Cell *feedCell = (CBFeed4Cell *)[collectionView dequeueReusableCellWithReuseIdentifier:[CBFeed4Cell reuseIdentifier] forIndexPath:indexPath];
        
        CBSuggestedVideoModel *model = self.logic.newsDetail.suggestedVideos[indexPath.row];
        [self configureFeedCell:feedCell withSuggestedVideoModel:model];
        [self styleFeedSmallImageCell:feedCell];
        
        return feedCell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.collectionViewAdManager shouldShowAdAtIndexPath:indexPath]) {
        return [self.collectionViewAdManager sizeForItemAtIndexPath:indexPath];
    } else {
        //BUGFIX: MIN is called to handle rotation cases. We want to make sure that width is the true width.
        CGFloat width = MIN(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))  - 2 * kCollectionViewSpacing;
        return [CBFeed4Cell sizeForWidth:width];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.collectionViewAdManager shouldShowAdAtIndexPath:indexPath]){
        [self.collectionViewAdManager didSelectItemAtIndexPath:indexPath];
    } else {
        
        CBVideoDetailViewController *vc = (CBVideoDetailViewController *)[CBViewControllerBuilder videoDetailViewControllerWithLogic:[[CBVideoDetailLogic alloc] initWithFeedId:((CBSuggestedVideoModel *)self.logic.newsDetail.suggestedVideos[indexPath.row]).detailId]];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - Action

- (void)swiped:(UISwipeGestureRecognizer *)recognizer
{
    if(recognizer.direction == UISwipeGestureRecognizerDirectionRight)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)imageTapped:(id)sender
{
    [CBAnalyticsManager videoPlayButtonTappedWithVideoCategory:self.logic.newsDetail.category.name];
    
    [self presentSeamlessVideoPlayerWithUrl:self.logic.newsDetail.videoUrl entityName:[self entityNameWithoutType]];
}

#pragma mark - AdView delegate methods

- (void)adViewDidLoad:(SLAdView *)adView
{
    [super adViewDidLoad:adView];
    
    if(adView == self.mreView)
    {
        self.suggestedVideosTopSpaceConstraint.constant = SLAdSizeMRect.height + 10;
        self.webViewToCollectionViewSpace.constant = SLAdSizeMRect.height + 8 + CGRectGetHeight(self.suggestedVideosTitleView.bounds);
        [self.scrollView addSubview:adView];
        [adView distance:0 toTopView:self.webView];
        [adView centerXInSuperview];
    }
}

- (void)adViewDidFailToLoad:(SLAdView *)adView
{
    [super adViewDidFailToLoad:adView];
    
    if(adView == self.mreView)
    {
        self.webViewToCollectionViewSpace.constant = 25;
        self.suggestedVideosTopSpaceConstraint.constant = 0;
    }
}

@end

#pragma mark - Styling

@implementation CBVideoDetailViewController(Styling)

- (void)styleByIncrementingFontBy:(CGFloat)fontIncrementAmount
{
    CBVideoDetailStyleConfig *config = [CBVideoDetailStyleConfig sharedConfig];
    
    self.view.backgroundColor = config.viewBackgroundColor;
    
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];
    
    self.dateLabel.textColor = config.dateColor;
    self.dateLabel.font = [config.dateFont fontByIncrementingSize:fontIncrementAmount];
    
    self.titleLabel.textColor = config.titleColor;
    self.titleLabel.font = [config.titleFont fontByIncrementingSize:fontIncrementAmount];
    
    self.spotLabel.textColor = config.spotColor;
    self.spotLabel.font = [config.spotFont fontByIncrementingSize:fontIncrementAmount];
    
    self.viewCountLabel.textColor = config.viewCountColor;
    self.viewCountLabel.font = [config.viewCountFont fontByIncrementingSize:fontIncrementAmount];
    
    [self.suggestedVideosTitleView setBackgroundColor:config.suggestedVideosBackgroundColor];
    [self.suggestedVideosLabel setTextColor:config.suggestedVideosTitleColor];
    [self.suggestedVideosLabel setFont:config.suggestedVideosTitleFont];
}

- (NSString *)styleContent:(NSString *)content withCSSForMaximumWidth:(CGFloat)width fontIncrementAmount:(CGFloat)fontIncrementAmount
{
    CBVideoDetailStyleConfig *config = [CBVideoDetailStyleConfig sharedConfig];
    return [NSString stringWithFormat:
            @"<style>\
            p{font-family:\"%@\";\
            font-size:%f;\
            color:rgb(%@)}\
            a{font-family:\"%@\";\
            font-size:%f;}\
            iframe{\
            max-width:100%%;\
            height:auto;\
            }\
            img {\
            max-width:100%%;\
            height:auto;\
            }\
            </style>\
            <html>\
            <body width=%f>\
            %@\
            </body>\
            </html>",
            config.contentFont.fontName,
            (long)config.contentFont.pointSize + fontIncrementAmount,
            config.contentColor,
            config.contentFont.fontName,
            (long)config.contentFont.pointSize + fontIncrementAmount,
            width,
            content ? content : @""];
    return nil;
}

- (void)styleFeedSmallImageCell:(CBFeed4Cell *)cell
{
    CBVideoDetailStyleConfig *config = [CBVideoDetailStyleConfig sharedConfig];
    for(UIView *separator in cell.separators)
    {
        separator.backgroundColor = config.cellBorderColor;
    }
    
    cell.backgroundColor = config.cellBackgroundColor;
    [cell.titleLabel setTextColor:config.cellTitleColor];
    [cell.label2 setTextColor:config.cellCategoryColor];
    [cell.label3 setTextColor:config.cellSpotColor];
    [cell.titleLabel setFont:config.cellTitleFont];
    [cell.label2 setFont:config.cellCategoryFont];
    [cell.label3 setFont:config.cellSpotFont];
}


@end