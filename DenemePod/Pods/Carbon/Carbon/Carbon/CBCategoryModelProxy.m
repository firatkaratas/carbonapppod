//
//  CBCategoryModelProxy.m
//  Carbon
//
//  Created by Semih Cihan on 16/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBCategoryModelProxy.h"

@implementation CBCategoryModelProxy

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[CBCategoryModelProxy alloc] init];
    });
    return sharedInstance;
}

- (id)nameJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(nameJSONTransformerOverridden:)]) {
        return [self nameJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)categoryIdJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(categoryIdJSONTransformerOverridden:)]) {
        return [self categoryIdJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)subcategoriesJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(subcategoriesJSONTransformerOverridden:)]) {
        return [self subcategoriesJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)hasFeedNumberJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(hasFeedNumberJSONTransformerOverridden:)]) {
        return [self hasFeedNumberJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)contentTypeNumberJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(contentTypeNumberJSONTransformerOverridden:)]) {
        return [self contentTypeNumberJSONTransformerOverridden:object];
    } else {
        return object;
    }
}


@end
