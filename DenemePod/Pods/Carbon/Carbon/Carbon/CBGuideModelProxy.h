//
//  CBGuideModelProxy.h
//  Carbon
//
//  Created by Semih Cihan on 17/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelProxy.h"

@protocol CBGuideModelProxyProtocol <NSObject>

@optional

- (NSString *)titleJSONTransformerOverridden:(id)object;
- (NSString *)imageUrlJSONTransformerOverridden:(id)object;
- (NSString *)tagJSONTransformerOverridden:(id)object;
- (NSString *)timeJSONTransformerOverridden:(id)object;

@end

@interface CBGuideModelProxy : CBBaseModelProxy <CBGuideModelProxyProtocol>

+ (instancetype)sharedInstance;

- (id)titleJSONTransformer:(id)object;
- (id)imageUrlJSONTransformer:(id)object;
- (id)tagJSONTransformer:(id)object;
- (id)timeJSONTransformer:(id)object;

@end
