//
//  CBImageViewController.m
//  Carbon
//
//  Created by Necati Aydın on 27/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBImageViewController.h"

static const CGFloat kMaximumZoomScale = 3.0;
static const CGFloat kMinimumZoomScale = 1.0;
static const CGFloat kInitialZoomScale = 1.0;
static const CGFloat kSmartZoomScale = 2.0;

@interface CBImageViewController () <UIScrollViewDelegate>

@end

@implementation CBImageViewController

+ (instancetype)instanceWithoutImage
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[self storyboardName]
                                                         bundle:[NSBundle frameworkBundle]];
    
    return [storyboard instantiateInitialViewController];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self startGestureRecognizers];
    
    if(self.image)
    {
        [self addImageView];
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.scrollView.minimumZoomScale = kMinimumZoomScale;
    self.scrollView.maximumZoomScale = kMaximumZoomScale;
    self.scrollView.zoomScale = kInitialZoomScale;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3f animations:^
    {
        [weakSelf centerImageView];
    }];
}

- (void)startGestureRecognizers
{
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:doubleTapRecognizer];
    
    UITapGestureRecognizer *singleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapRecognizer.numberOfTapsRequired = 1;
    singleTapRecognizer.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:singleTapRecognizer];
    
    [singleTapRecognizer requireGestureRecognizerToFail:doubleTapRecognizer];
}

#pragma mark - Action

- (void)singleTap:(id)sender
{
    [self.delegate imageViewControllerTapped:self];
}

- (void)doubleTap:(id)sender
{
    [self.scrollView setZoomScale:(self.scrollView.zoomScale < kSmartZoomScale) ? kSmartZoomScale : kInitialZoomScale animated:YES];
    
}

#pragma mark - Image View

- (void)addImage:(UIImage *)image
{
    self.image = image;
    if(image)
    {
        [self addImageView];
        [self centerImageView];
    }
}

- (CGRect)initialImageViewFrame
{
    return [self imageViewFrameForSize:self.view.frame.size];
}

- (void)addImageView
{
    self.imageView = [[UIImageView alloc] initWithFrame:[self initialImageViewFrame]];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.image = self.image;
    
    [self.scrollView addSubview:self.imageView];
    self.scrollView.contentSize = self.imageView.frame.size;
}

- (void)centerImageView
{
    CGSize boundsSize = self.scrollView.bounds.size;
    self.imageView.frame = [self centeredImageViewFrameForSize:boundsSize];
}

- (CGRect)centeredImageViewFrameForSize:(CGSize)boundsSize
{
    CGRect frame = self.imageView.frame;
    
    if (frame.size.width < boundsSize.width)
    {
        frame.origin.x = (boundsSize.width - frame.size.width) / 2.0f;
    }
    else
    {
        frame.origin.x = 0.0f;
    }
    
    if (frame.size.height < boundsSize.height)
    {
        frame.origin.y = (boundsSize.height - frame.size.height) / 2.0f;
    }
    else
    {
        frame.origin.y = 0.0f;
    }
    
    return frame;
}

- (CGRect)imageViewFrameForSize:(CGSize)boundsSize
{
    CGFloat selfRatio = boundsSize.width / boundsSize.height;
    CGFloat imageRatio = self.image.size.width/self.image.size.height;
    
    if(imageRatio > selfRatio)
    {
        return CGRectMake(0, 0, boundsSize.width, boundsSize.width * self.image.size.height / self.image.size.width);
    }
    else
    {
        return CGRectMake(0, 0, boundsSize.height * self.image.size.width / self.image.size.height , boundsSize.height);
    }
}

#pragma mark - UIScrollView

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self centerImageView];
}

#pragma mark - Rotation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    self.scrollView.zoomScale = 1;
    
    CGFloat targetSelfWidth = 0, targetSelfHeight = 0;
    CGFloat currentWidth = CGRectGetWidth(self.view.frame);
    CGFloat currentHeight = CGRectGetHeight(self.view.frame);
    
    if(!UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
    {
        targetSelfWidth = MAX(currentWidth, currentHeight);
        targetSelfHeight = MIN(currentWidth, currentHeight);
    }
    else
    {
        targetSelfWidth = MIN(currentWidth, currentHeight);
        targetSelfHeight = MAX(currentWidth, currentHeight);
    }

    [UIView animateWithDuration:duration animations:^
    {
        self.imageView.frame = [self imageViewFrameForSize:CGSizeMake(targetSelfWidth, targetSelfHeight)];
        self.imageView.frame = [self centeredImageViewFrameForSize:CGSizeMake(targetSelfWidth, targetSelfHeight)];
    }];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    self.scrollView.contentSize = self.imageView.frame.size;
}

@end
