//
//  CBGuideModel.m
//  Carbon
//
//  Created by Semih Cihan on 08/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGuideDayModel.h"
#import "CBGuideDayModelConfig.h"
#import "CBGuideModel.h"
#import "CBNetworkConfig.h"
#import "CBGuideDayModelProxy.h"

@implementation CBGuideDayModel

+ (CBBaseModelConfig *)config
{
    return [CBGuideDayModelConfig sharedConfig];
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"date" : [CBGuideDayModelConfig sharedConfig].date,
             @"guideItems" : [CBGuideDayModelConfig sharedConfig].guideItems,
             @"dayOfTheWeek" : [CBGuideDayModelConfig sharedConfig].dayOfTheWeek
             };
}

+ (NSValueTransformer *)guideItemsJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        NSArray *rawGuideItemsArray = [[CBGuideDayModelProxy sharedInstance] guideItemsJSONTransformer:object];
        
        if (rawGuideItemsArray) {
            NSError *error;
            NSArray *modelArray = [MTLJSONAdapter modelsOfClass:CBGuideModel.class fromJSONArray:rawGuideItemsArray error:&error];
            if (error) {
                DebugLog(@"Error in suggestedVideosJSONTransformer in CBGuideDayModel, error description: %@", error.localizedDescription);
                return nil;
            }
            return modelArray;
        } else {
            return nil;
        }
        
    } reverseBlock:^id (id suggestedVideos) {
        return nil;
    }];
}

+ (NSValueTransformer *)dateJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGuideDayModelProxy sharedInstance] dateJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)dayOfTheWeekJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGuideDayModelProxy sharedInstance] dayOfTheWeekJSONTransformer:object];
        return [CBBaseModel stringOrNumberToNumber:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

@end
