//
//  CBVideoDetailModelProxy.m
//  Carbon
//
//  Created by Semih Cihan on 16/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBVideoDetailModelProxy.h"

@implementation CBVideoDetailModelProxy

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[CBVideoDetailModelProxy alloc] init];
    });
    return sharedInstance;
}

- (id)detailIdJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(detailIdJSONTransformerOverridden:)]) {
        return [self detailIdJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)dateJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(dateJSONTransformerOverridden:)]) {
        return [self dateJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)categoryJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(categoryJSONTransformerOverridden:)]) {
        return [self categoryJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)titleJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(titleJSONTransformerOverridden:)]) {
        return [self titleJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)spotJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(spotJSONTransformerOverridden:)]) {
        return [self spotJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)imageUrlJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(imageUrlJSONTransformerOverridden:)]) {
        return [self imageUrlJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)urlJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(urlJSONTransformerOverridden:)]) {
        return [self urlJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)contentJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(contentJSONTransformerOverridden:)]) {
        return [self contentJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)suggestedVideosJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(suggestedVideosJSONTransformerOverridden:)]) {
        return [self suggestedVideosJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)videoUrlJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(videoUrlJSONTransformerOverridden:)]) {
        return [self videoUrlJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)durationJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(durationJSONTransformerOverridden:)]) {
        return [self durationJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)viewCountJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(viewCountJSONTransformerOverridden:)]) {
        return [self viewCountJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

@end
