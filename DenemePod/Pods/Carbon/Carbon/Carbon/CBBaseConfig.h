//
//  CBPListReader.h
//  Carbon
//
//  Created by Necati Aydın on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CBPListReader.h"

/**
 New config files should subclass this one. For each config file, there should be a corresponding plist file.
 */
@interface CBBaseConfig : CBPListReader

/**
 This method used by the CBBaseModelConfig subclasses for which default values for properties (name of the property) are used when there is no matching plist key for that property in the plist.
 @return A new instance of the class.
 */
- (instancetype)initByUsingDefaultsForNils;

@end
