//
//  CBGuideModel.h
//  Carbon
//
//  Created by Semih Cihan on 08/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"

/**
 @description Represents a day in the guide.
 */
@interface CBGuideDayModel : CBBaseModel

/**
 Date of the model.
 */
@property (strong, nonatomic) NSString *date;

/**
 This array represents the schedule of the day. It holds CBGuideModel objects.
 */
@property (strong, nonatomic) NSArray *guideItems;

/**
 NSNumber as integers between 0 and 6. 0 indicates Monday, 6 indicates Sunday.
 */
@property (strong, nonatomic) NSNumber *dayOfTheWeek;

@end
