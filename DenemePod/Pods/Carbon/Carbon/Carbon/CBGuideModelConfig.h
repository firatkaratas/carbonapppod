//
//  CBGuideModelConfig.h
//  Carbon
//
//  Created by Semih Cihan on 08/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@interface CBGuideModelConfig : CBBaseModelConfig

/**
 Title of the item.
 */
@property (strong, nonatomic) NSString *title;

/**
 Image url of the item.
 */
@property (strong, nonatomic) NSString *imageUrl;

/**
 Tag of the item. To be used as Live, New Episode etc.
 */
@property (strong, nonatomic) NSString *tag;

/**
 Start time of the item.
 */
@property (strong, nonatomic) NSString *time;

@end
