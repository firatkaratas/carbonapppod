//
//  CBSettingsLogic.h
//  Carbon
//
//  Created by Necati Aydın on 11/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"

@interface CBSettingsLogic : CBBaseLogic

/**
 @return Returns YES if notifications are on, NO otherwise.
 */
+ (BOOL)notificationsTurnedOn;

/**
 Used to enable/disable push notifications.
 @param enable BOOL to turn on, off notifications.
 */
+ (void)enableNotifications:(BOOL)enable;

/**
 Used to set the font increment amount by saving it to NSUserDefaults.
 @param fontIncementAmount Font increment amount to be saved.
 */
+ (void)setFontIncrementAmount:(NSInteger)fontIncrementAmount;

/**
 Used to get the font increment amount from NSUserDefaults.
 @return font increment amount.
 */
+ (NSInteger)getFontIncrementAmount;

@end
