//
//  CBFeedModelProxy.h
//  Carbon
//
//  Created by Semih Cihan on 16/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelProxy.h"

@protocol CBFeedModelProxyProtocol <NSObject>

@optional

- (NSNumber *)typeNumberJSONTransformerOverridden:(id)object;
- (NSString *)feedIdJSONTransformerOverridden:(id)object;
- (NSString *)categoryJSONTransformerOverridden:(id)object;
- (NSString *)dateJSONTransformerOverridden:(id)object;
- (NSString *)titleJSONTransformerOverridden:(id)object;
- (NSString *)spotJSONTransformerOverridden:(id)object;
- (NSString *)urlJSONTransformerOverridden:(id)object;
- (NSString *)imageUrlJSONTransformerOverridden:(id)object;
- (NSNumber *)imageWidthJSONTransformerOverridden:(id)object;
- (NSNumber *)imageHeightJSONTransformerOverridden:(id)object;

@end


@interface CBFeedModelProxy : CBBaseModelProxy <CBFeedModelProxyProtocol>

+ (instancetype)sharedInstance;

- (id)typeNumberJSONTransformer:(id)object;
- (id)feedIdJSONTransformer:(id)object;
- (id)categoryJSONTransformer:(id)object;
- (id)dateJSONTransformer:(id)object;
- (id)titleJSONTransformer:(id)object;
- (id)spotJSONTransformer:(id)object;
- (id)urlJSONTransformer:(id)object;
- (id)imageUrlJSONTransformer:(id)object;
- (id)imageWidthJSONTransformer:(id)object;
- (id)imageHeightJSONTransformer:(id)object;

@end
