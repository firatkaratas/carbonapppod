//
//  CBHTTPRequestOperationManager.m
//  Carbon
//
//  Created by Semih Cihan on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNetworkManager.h"
#import "CBNetworkConfig.h"
#import "NetworkManager.h"
#import "CBNetworkRequest.h"
#import "ResponseHandlerBuilder.h"
#import "NetworkRequestBuilder.h"

@implementation CBNetworkManager

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[NetworkManager alloc] initWithBaseURL:[NSURL URLWithString:[CBNetworkConfig sharedConfig].baseUrl]];
        ((NetworkManager *)sharedInstance).responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/xml", @"text/html", @"application/json", @"text/json", @"text/javascript", @"text/plain"]];
        [((NetworkManager *)sharedInstance).requestSerializer setTimeoutInterval:30];
        [((NetworkManager *)sharedInstance) makeInitializationConfigurations];
    });
    return sharedInstance;
}

- (void)makeInitializationConfigurations {
    
}

#pragma mark - Cancel services

- (void)cancelRunningServices {
    [[self operationQueue] cancelAllOperations];
}

- (void)cancelRunningServicesWithRequest:(CBNetworkRequest *)request {
    NSString *baseUrl = [[self baseURL] absoluteString];
    NSString *requestUrlWithBase = [NSString stringWithFormat:@"%@%@", baseUrl, request.formatUrl];
    for (int i = 0; i < [self.operationQueue.operations count]; i++) {
        AFURLConnectionOperation *op = [self.operationQueue.operations objectAtIndex:i];
        if ( [[[op.request URL] absoluteString] rangeOfString:requestUrlWithBase].location != NSNotFound ) {
            [op cancel];
        }
    }
}

#pragma mark - Requests

- (AFHTTPRequestOperation *)GET:(NSString *)URLString
                     parameters:(id)parameters
                        success:(SuccessBlockWithOperation)success
                        failure:(ErrorBlockWithOperation)failure
{
    
    return [super GET:URLString
           parameters:parameters
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  [self handleGetWithOperation:operation
                                responseObject:responseObject
                                         error:nil
                                  successBlock:success
                                  failureBlock:failure];
                  
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  [self handleGetWithOperation:operation
                                responseObject:nil
                                         error:error
                                  successBlock:success
                                  failureBlock:failure];
              }];

}

- (void)handleGetWithOperation:(AFHTTPRequestOperation *)operation
                responseObject:(id)responseObject
                         error:(NSError *)error
                  successBlock:(SuccessBlockWithOperation)success
                  failureBlock:(ErrorBlockWithOperation)failure {
    
    if(error) {
        
        DebugLog(@"Network Error in get with url : %@ error : %@", [operation.request.URL absoluteString], [error localizedDescription]);
        failure(operation, error);
    } else {
        
        DebugLog(@"get operation success:%@", operation);
        DebugLog(@"%@", responseObject);
        success(operation, responseObject);
    }
}

- (AFHTTPRequestOperation *)POST:(NSString *)URLString
                      parameters:(id)parameters
                         success:(SuccessBlockWithOperation)success
                         failure:(ErrorBlockWithOperation)failure {
    
    return [super POST:URLString
            parameters:parameters
               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   
                   [self handlePostWithOperation:operation
                                  responseObject:responseObject
                                           error:nil
                                    successBlock:success
                                    failureBlock:failure];
                   
               }
               failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   
                   [self handlePostWithOperation:operation
                                  responseObject:nil
                                           error:error
                                    successBlock:success
                                    failureBlock:failure];
               }];
}

- (void)handlePostWithOperation:(AFHTTPRequestOperation *)operation
                 responseObject:(id)responseObject
                          error:(NSError *)error
                   successBlock:(SuccessBlockWithOperation)success
                   failureBlock:(ErrorBlockWithOperation)failure {
    
    if(error) {
        
        DebugLog(@"Network Error in post with url : %@ error : %@", [operation.request.URL absoluteString], [error localizedDescription]);
        failure(operation, error);
    } else {
        
        DebugLog(@"post operation success:%@", operation);
        DebugLog(@"%@", responseObject);
        success(operation, responseObject);
    }
}

- (void)makeRequest:(CBNetworkRequest *)request
       successBlock:(SuccessBlockWithOperation)success
       failureBlock:(ErrorBlockWithOperation)failure {
    
    if(request.requestType == RequestTypeGet) {
        
        [self GET:request.formatUrl parameters:request.parameters success:success failure:failure];
    } else {
        
        [self POST:request.formatUrl parameters:request.parameters success:success failure:failure];
    }
}

#pragma mark - Specific Requests

- (void)getNewsDetailById:(NSString *)detailId
             successBlock:(void (^)(CBNewsDetailModel *))successBlock
             failureBlock:(FailureBlock)failureBlock {
    
    CBNetworkRequest *request = [NetworkRequestBuilder newsDetailRequestWithId:detailId];
    
    [self makeRequest:request
         successBlock:[ResponseHandlerBuilder newsDetailSuccessHandlerWithCompletionBlock:successBlock]
         failureBlock:[ResponseHandlerBuilder errorHandlerWithFailureBlock:failureBlock]];
}

- (void)getVideoDetailById:(NSString *)detailId
              successBlock:(void (^)(CBVideoDetailModel *))successBlock
              failureBlock:(FailureBlock)failureBlock {
    
    CBNetworkRequest *request = [NetworkRequestBuilder videoDetailRequestWithId:detailId];
    
    [self makeRequest:request
         successBlock:[ResponseHandlerBuilder videoDetailSuccessHandlerWithCompletionBlock:successBlock]
         failureBlock:[ResponseHandlerBuilder errorHandlerWithFailureBlock:failureBlock]];
}

- (void)getAuthorDetailById:(NSString *)detailId
               successBlock:(void (^)(CBAuthorDetailModel *authorDetailModel))successBlock
               failureBlock:(FailureBlock)failureBlock {
    
    CBNetworkRequest *request = [NetworkRequestBuilder authorDetailRequestWithId:detailId];
    
    [self makeRequest:request
         successBlock:[ResponseHandlerBuilder authorDetailSuccessHandlerWithCompletionBlock:successBlock]
         failureBlock:[ResponseHandlerBuilder errorHandlerWithFailureBlock:failureBlock]];
}

- (void)getCommentsByDetailId:(NSString *)detailId
                 successBlock:(void (^)(NSArray *))successBlock
                 failureBlock:(FailureBlock)failureBlock {
    
    CBNetworkRequest *request = [NetworkRequestBuilder commentsRequestWithId:detailId];
    
    [self makeRequest:request
         successBlock:[ResponseHandlerBuilder commentsSuccessHandlerWithCompletionBlock:successBlock]
         failureBlock:[ResponseHandlerBuilder errorHandlerWithFailureBlock:failureBlock]];
}

- (void)getGalleryDetailById:(NSString *)feedId
                successBlock:(void (^)(CBGalleryDetailModel *))successBlock
                failureBlock:(FailureBlock)failureBlock
{
    
    CBNetworkRequest *request = [NetworkRequestBuilder galleryDetailRequestWithId:feedId];
    
    [self makeRequest:request
         successBlock:[ResponseHandlerBuilder galleryDetailSuccessHandlerWithCompletionBlock:successBlock]
         failureBlock:[ResponseHandlerBuilder errorHandlerWithFailureBlock:failureBlock]];
}

- (void)getCategoriesWithSuccessBlock:(void (^)(NSArray *categories))successBlock
                         failureBlock:(FailureBlock)failureBlock {
    
    CBNetworkRequest *request = [NetworkRequestBuilder cateogiresRequest];
    
    [self makeRequest:request
         successBlock:[ResponseHandlerBuilder categoriesSuccessHandlerWithCompletionBlock:successBlock]
         failureBlock:[ResponseHandlerBuilder errorHandlerWithFailureBlock:failureBlock]];
}

- (void)getCategoryFeedWithCategoryId:(NSString *)categoryId
                                 pageNumber:(NSString *)pageNumber
                                 pageSize:(NSString *)pageSize
                         successBlock:(void (^)(NSArray *))successBlock
                         failureBlock:(FailureBlock)failureBlock {
    
    CBNetworkRequest *request = [NetworkRequestBuilder categoryFeedRequestWithId:categoryId pageNumber:pageNumber pageSize:pageSize];
    
    [self makeRequest:request
         successBlock:[ResponseHandlerBuilder categoryFeedSuccessHandlerWithCompletionBlock:successBlock]
         failureBlock:[ResponseHandlerBuilder errorHandlerWithFailureBlock:failureBlock]];

}

- (void)getAuthorFeedWithAuthorId:(NSString *)authorId
                       pageNumber:(NSString *)pageNumber
                         pageSize:(NSString *)pageSize
                     successBlock:(void (^)(NSArray *))successBlock
                     failureBlock:(FailureBlock)failureBlock {
    
    CBNetworkRequest *request = [NetworkRequestBuilder authorFeedRequestWithId:authorId
                                                                    pageNumber:pageNumber
                                                                      pageSize:pageSize];
    
    [self makeRequest:request
         successBlock:[ResponseHandlerBuilder authorFeedSuccessHandlerWithCompletionBlock:successBlock]
         failureBlock:[ResponseHandlerBuilder errorHandlerWithFailureBlock:failureBlock]];
}

- (void)getLiveStreamWithSuccessBlock:(void (^)(NSURL *))successBlock
                         failureBlock:(FailureBlock)failureBlock {
    
    NSString *directStreamUrl = [CBNetworkConfig sharedConfig].liveStreamDirectUrl;
    if (directStreamUrl && directStreamUrl.length != 0) {
        successBlock([NSURL URLWithString:directStreamUrl]);
    } else {
        
        CBNetworkRequest *request = [NetworkRequestBuilder liveStreamRequest];
        
        [self makeRequest:request
             successBlock:[ResponseHandlerBuilder liveStreamSuccessHandlerWithCompletionBlock:successBlock]
             failureBlock:[ResponseHandlerBuilder errorHandlerWithFailureBlock:failureBlock]];
    }
}

- (void)getGuideWithSuccessBlock:(void (^)(NSArray *))successBlock
                    failureBlock:(FailureBlock)failureBlock {
    
    CBNetworkRequest *request = [NetworkRequestBuilder guideRequest];
    
    [self makeRequest:request
         successBlock:[ResponseHandlerBuilder guideSuccessHandlerWithCompletionBlock:successBlock]
         failureBlock:[ResponseHandlerBuilder errorHandlerWithFailureBlock:failureBlock]];
}

@end
