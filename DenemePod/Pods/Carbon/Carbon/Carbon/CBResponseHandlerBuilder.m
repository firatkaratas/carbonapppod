//
//  CBResponseHandlerBuilder.m
//  Carbon
//
//  Created by Necati Aydın on 26/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBResponseHandlerBuilder.h"
#import "Mantle.h"
#import "CBNewsDetailModel.h"
#import "CBVideoDetailModel.h"
#import "CBAuthorDetailModel.h"
#import "CBCommentModel.h"
#import "CBGalleryDetailModel.h"
#import "CBCategoryModel.h"
#import "CBFeedModel.h"
#import "CBGuideDayModel.h"
#import "CBJSONAdapter.h"

@implementation CBResponseHandlerBuilder

+ (SuccessBlockWithOperation)successHandlerWithRootKey:(NSString *)rootKey
                                            proxyBlock:(id (^)(id response))proxyBlock
                                           modelConfig:(CBBaseModelConfig *)config
                                               isArray:(BOOL)isArray
                                            classToMap:(Class)classToMap
                                       completionBlock:(void (^)(id response))completionBlock
{
    return ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        id responseWithoutRoot = (rootKey == nil) ? responseObject : responseObject[rootKey];
        id carbonResponse = (proxyBlock == nil) ? responseWithoutRoot : proxyBlock(responseWithoutRoot);
        
        if(config)
        {
            SuccessBlockWithOperation mappingOperation = [self successHandlerWithClassToMap:classToMap modelConfig:config isArray:isArray completionBlock:completionBlock];
            mappingOperation(operation,carbonResponse);
        }
        else
        {
            SuccessBlockWithOperation mappingOperation = [self successHandlerWithClassToMap:classToMap isArray:isArray completionBlock:completionBlock];
            mappingOperation(operation, carbonResponse);
        }
    };
}

+ (SuccessBlockWithOperation)successHandlerWithClassToMap:(Class)classToMap
                                              modelConfig:(CBBaseModelConfig *)config
                                                  isArray:(BOOL)isArray
                                         completionBlock:(void (^)(id response))completionBlock  {
    
    return ^(AFHTTPRequestOperation *operation, id responseObject) {
        
        id obj;
        if(isArray) {
            
            NSMutableArray *arr = [NSMutableArray new];
            for (NSDictionary *dict in responseObject)
            {
                NSError *error;
                CBJSONAdapter *adapter = [[CBJSONAdapter alloc] initWithJSONDictionary:dict modelClass:classToMap error:&error config:config];
                if (error == nil && adapter.model) {
                    [arr addObject:adapter.model];
                }
            }
            obj = arr;
        } else {
            
            NSError *error;
            CBJSONAdapter *adapter = [[CBJSONAdapter alloc] initWithJSONDictionary:responseObject modelClass:classToMap error:&error config:config];
            if (error == nil && adapter.model) {
                obj = adapter.model;
            }
        }
        
        if (completionBlock) {
            completionBlock(obj);
        }
    };
}

+ (SuccessBlockWithOperation)successHandlerWithClassToMap:(Class)classToMap
                                                  isArray:(BOOL)isArray
                                          completionBlock:(void (^)(id response))completionBlock {
    
    return ^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (classToMap == nil) {
            
            if (completionBlock) {
                completionBlock(responseObject);
            }
        } else {
            
            id obj;
            if(isArray) {
                
                obj = [MTLJSONAdapter modelsOfClass:classToMap fromJSONArray:responseObject error:nil];
            } else {
                
                obj = [MTLJSONAdapter modelOfClass:classToMap fromJSONDictionary:responseObject error:nil];
            }
            
            if (completionBlock) {
                completionBlock(obj);
            }

        }
    };
}

+ (ErrorBlockWithOperation)errorHandlerWithFailureBlock:(void (^)(NSError *error))errorBlock {
    
    return ^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (errorBlock) {
            errorBlock(error);
        }
    };
}

+ (SuccessBlockWithOperation)newsDetailSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock {
    return [self successHandlerWithClassToMap:[CBNewsDetailModel class] isArray:NO completionBlock:completionBlock];
}

+ (SuccessBlockWithOperation)videoDetailSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock {
    
    return [self successHandlerWithClassToMap:[CBVideoDetailModel class] isArray:NO completionBlock:completionBlock];
}

+ (SuccessBlockWithOperation)authorDetailSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock {
    
    return [self successHandlerWithClassToMap:[CBAuthorDetailModel class] isArray:NO completionBlock:completionBlock];
}
+ (SuccessBlockWithOperation)galleryDetailSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock {
    
    return [self successHandlerWithClassToMap:[CBGalleryDetailModel class] isArray:NO completionBlock:completionBlock];
}

+ (SuccessBlockWithOperation)commentsSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock {
    
    return [self successHandlerWithClassToMap:[CBCommentModel class] isArray:YES completionBlock:completionBlock];
}

+ (SuccessBlockWithOperation)categoriesSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock {
    
    return [self successHandlerWithClassToMap:[CBCategoryModel class] isArray:YES completionBlock:completionBlock];
}

+ (SuccessBlockWithOperation)categoryFeedSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock {

    return [self successHandlerWithClassToMap:[CBFeedModel class] isArray:YES completionBlock:completionBlock];
}

+ (SuccessBlockWithOperation)authorFeedSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock {
    
    return [self successHandlerWithClassToMap:[CBFeedModel class] isArray:YES completionBlock:completionBlock];
}

+ (SuccessBlockWithOperation)liveStreamSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock {
    
    return [self successHandlerWithClassToMap:nil isArray:NO completionBlock:^(id response) {
        completionBlock([NSURL URLWithString:response]);
    }];
}

+ (SuccessBlockWithOperation)guideSuccessHandlerWithCompletionBlock:(void (^)(id response))completionBlock {
    
    return [self successHandlerWithClassToMap:[CBGuideDayModel class] isArray:YES completionBlock:completionBlock];
}

@end
