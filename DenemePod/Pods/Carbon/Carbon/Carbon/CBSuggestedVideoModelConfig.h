//
//  CBSuggestedVideoModelConfig.h
//  Carbon
//
//  Created by Semih Cihan on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@interface CBSuggestedVideoModelConfig : CBBaseModelConfig

/**
 Id for the model.
 */
@property (strong, nonatomic) NSString *detailId;

/**
 Category of the news.
 */
@property (strong, nonatomic) NSString *category;

/**
 Title of the news.
 */
@property (strong, nonatomic) NSString *title;

/**
 Secondary title of the news.
 */
@property (strong, nonatomic) NSString *spot;

/**
 Image url of the news.
 */
@property (strong, nonatomic) NSString *imageUrl;

@end
