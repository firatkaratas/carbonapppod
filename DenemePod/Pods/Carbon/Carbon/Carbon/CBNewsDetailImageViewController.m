//
//  CBNewsDetailImageViewController.m
//  Carbon
//
//  Created by Necati Aydın on 09/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNewsDetailImageViewController.h"
#import "UIViewController+Fading.h"

static const CGFloat kInitialZoomScale = 1.0;

@interface CBNewsDetailImageViewController ()

@property (nonatomic, assign) CGRect originalImageViewFrame;

@end

@implementation CBNewsDetailImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (CGRect)initialImageViewFrame
{
    return self.originalImageViewFrame;
}

+ (instancetype)instanceWithInitialImage:(UIImage *)image imageFrame:(CGRect)imageFrame
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[self storyboardName]
                                                         bundle:[NSBundle frameworkBundle]];
    
    CBNewsDetailImageViewController *vc = [storyboard instantiateInitialViewController];
    vc.originalImageViewFrame = imageFrame;
    vc.image = image;
    vc.fadeTransitionEneabledForDismiss = YES;
    
    return vc;
}

- (void)singleTap:(id)sender
{
    [super singleTap:sender];
    
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^
     {
         [weakSelf.scrollView setZoomScale:kInitialZoomScale];
         
     } completion:^(BOOL finished)
     {
         [UIView animateWithDuration:0.3f animations:^
          {
              weakSelf.imageView.frame = weakSelf.originalImageViewFrame;
              
          } completion:^(BOOL finished)
          {
              if(self.fadeTransitionEneabledForDismiss)
              {
                  [self dismissViewControllerByFadingOut];
              }
              else
              {
                  [self dismissViewControllerAnimated:YES completion:nil];
              }

          }];
     }];
}

@end
