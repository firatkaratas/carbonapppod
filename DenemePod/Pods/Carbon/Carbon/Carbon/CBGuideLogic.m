//
//  CBGuideLogic.m
//  Carbon
//
//  Created by Necati Aydın on 08/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGuideLogic.h"
#import "CBNetworkManager.h"
#import "CBGuideDayModel.h"
#import "CBGuideModel.h"

@implementation CBGuideLogic

@dynamic delegate;

- (void)loadData
{
    [self.delegate dataLoading];
    
#ifdef DEBUG
    CBGuideModel *guide = [CBGuideModel new];
    guide.imageUrl = [NSURL URLWithString:@"https://s.dogannet.tv/ps/kanald/98/200x113/552e1a72e50aa90f4014ef3f.jpg"];
    guide.title = @"Yayin";
    guide.time = @"20:30";
    
    NSMutableArray *data = [NSMutableArray new];
    for(NSInteger i = 0; i < 7; i++)
    {
        CBGuideDayModel *guideDayModel = [CBGuideDayModel new];
        guideDayModel.date = [NSString stringWithFormat:@"21:30"];
        guideDayModel.dayOfTheWeek = @(i);
        guideDayModel.guideItems = @[guide];
        [data addObject:guideDayModel];
    }
    self.data = data;
    [self.delegate dataLoaded];
#else
    __weak typeof(self) weakSelf = self;
    [[CBNetworkManager sharedInstance] getGuideWithSuccessBlock:^(NSArray *guideDayModels)
     {
         weakSelf.data = guideDayModels;
         [weakSelf.delegate dataLoaded];
     } failureBlock:^(NSError *error) {
         [weakSelf.delegate dataLoadedWithError:NSLocalizedString(@"Unable to fetch the news.", nil)];
     }];
#endif
    
}

- (NSInteger)currentDayOfWeek
{
    NSDate *date = [NSDate new];
    
    NSTimeInterval interval = [date timeIntervalSinceReferenceDate]/(60.0*60.0*24.0);
    //mod 7 the number of days to identify day
    long dayix=((long)interval) % 7;
    return (dayix)%7;
}

@end
