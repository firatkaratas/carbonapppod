//
//  NSString+Additions.h
//  Carbon
//
//  Created by Necati Aydın on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Additions)

/**
 @description Removes the framework string at the beginning "CB" if exists.
 @return The substring of the given string which does not include the framework prefix at the beginning.
 */
- (NSString *)removeFrameworkPrefixIfExists;

/**
 Calculates string height with the given constrained width and font. Use for strings in UILabel.
 @param width Constrained width.
 @param font String font.
 @return Height of the string.
 */
- (CGFloat)calculateBoundingRectHeightWithinLabelWithConstrainedWidth:(CGFloat)width font:(UIFont *)font;

/**
 Calculates string height with the given constrained width and font. Use for strings in UITextView.
 @param width Constrained width.
 @param font String font.
 @return Height of the string.
 */
- (CGFloat)calculateBoundingRectHeightWithinTextViewWithConstrainedWidth:(CGFloat)width font:(UIFont *)font;

/**
 Converts the string to an integer and groups the digits by putting comma. For ex. For 1023 returns 1,023.
 */
- (NSString *)groupedStringForNumber;

@end
