//
//  CBPushModelProxy.h
//  Carbon
//
//  Created by Semih Cihan on 15/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelProxy.h"

@protocol CBPushModelProxyProtocol <NSObject>

@optional

- (NSString *)alertJSONTransformerOverridden:(id)object;
- (NSString *)feedIdJSONTransformerOverridden:(id)object;
- (NSNumber *)feedTypeNumberJSONTransformerOverridden:(id)object;

@end

@interface CBPushModelProxy : CBBaseModelProxy <CBPushModelProxyProtocol>

+ (instancetype)sharedInstance;

- (id)alertJSONTransformer:(id)object;
- (id)feedIdJSONTransformer:(id)object;
- (id)feedTypeNumberJSONTransformer:(id)object;

@end
