//
//  UINavigationBar+TapRecognizing.m
//  Carbon
//
//  Created by Necati Aydın on 04/02/15.
//  Copyright (c) 2015 Mobilike. All rights reserved.
//

#import "UINavigationBar+TapRecognizing.h"

@implementation UINavigationBar(TapRecognizing)

- (void)addTapGestureRecognizerByRemovingTheOldWithTarget:(id)target action:(SEL)action
{
    for(UIGestureRecognizer *recognizer in self.gestureRecognizers)
    {
        if([recognizer isKindOfClass:[UITapGestureRecognizer class]])
        {
            [self removeGestureRecognizer:recognizer];
        }
    }
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:target
                                                                                 action:action];
    [self addGestureRecognizer:recognizer];
}

@end
