//
//  CBColorPalette.h
//  Carbon
//
//  Created by Necati Aydın on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBPListReader.h"

@interface CBColorPalette : CBPListReader

+ (UIColor *)colorWithKey:(NSString *)key;

@end
