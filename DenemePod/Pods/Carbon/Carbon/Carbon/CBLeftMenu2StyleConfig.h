//
//  CBLeftMenuStyleConfig.h
//  Carbon
//
//  Created by Semih Cihan on 18/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBLeftMenu2StyleConfig : CBBaseConfig

/**
 Background color of the view. Some amount of alpha is given to the color before using it.
 */
@property (nonatomic, strong) UIColor *viewBackgroundColor;

/**
 Label color.
 */
@property (nonatomic, strong) UIColor *categoryLabelColor;

/**
 Label color when selected.
 */
@property (nonatomic, strong) UIColor *categoryLabelSelectedColor;

/**
Label font when selected.
 */
@property (nonatomic, strong) UIFont *categoryLabelSelectedFont;

/**
 Selected cell background color.
 */
@property (nonatomic, strong) UIColor *selectedCellBackgroundColor;

/**
 Cell separator color.
 */
@property (nonatomic, strong) UIColor *separatorColor;

/**
 Label font.
 */
@property (nonatomic, strong) UIFont *categoryLabelFont;

/**
 Back title text color;
 */
@property (nonatomic, strong) UIColor *backTitleColor;

/**
 Back button title font.
 */
@property (nonatomic, strong) UIFont *backFont;

/**
 Arrow color;
 */
@property (nonatomic, strong) UIColor *arrowColor;

@end
