//
//  CBFeedDetailLogic.m
//  Carbon
//
//  Created by Necati Aydın on 20/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseNewsDetailLogic.h"
#import "CBNetworkManager.h"
#import "CBCommentLogic.h"
#import "CBNewsDetailModel.h"

@interface CBBaseNewsDetailLogic()

@end

@implementation CBBaseNewsDetailLogic
@dynamic delegate;

- (instancetype)initWithFeedId:(NSString *)feedId
{
    self = [super init];
    if(self)
    {
        _feedId = feedId;
        _newsDetail = nil;
    }
    return self;
}

- (void)loadData
{
    [self.delegate dataLoading];
    
    __weak typeof(self) weakSelf = self;
    [[CBNetworkManager sharedInstance] getNewsDetailById:self.feedId
                                            successBlock:^(CBNewsDetailModel *newsDetail)
    {
        NSString *errorMessage = [self validateModel:newsDetail];
        
        if(errorMessage)
        {
            [weakSelf.delegate dataLoadedWithError:errorMessage];
        }
        else
        {
            weakSelf.newsDetail = newsDetail;
            [weakSelf.delegate dataLoaded];
        }
    } failureBlock:^(NSError *error) {
        [weakSelf.delegate dataLoadedWithError:NSLocalizedString(@"Unable to fetch the news.", nil)];
    }];
}

- (CBCommentLogic *)getCommentLogic
{
    CBCommentLogic *commentLogic = [[CBCommentLogic alloc] initWithDetailId:self.newsDetail.detailId];
    return commentLogic;
}

- (NSString *)validateModel:(CBNewsDetailModel *)detailModel
{
    if(!detailModel.title && !detailModel.spot && !detailModel.content)
    {
        DebugLog(@"%@ with feedId: %lu", NSLocalizedString(@"Unable to fetch the news.",nil), (unsigned long)self.feedId);
        return NSLocalizedString(@"Unable to fetch the news.",nil);
    }
    
    return nil;
}

@end
