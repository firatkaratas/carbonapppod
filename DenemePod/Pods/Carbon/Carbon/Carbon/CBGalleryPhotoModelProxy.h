//
//  CBGalleryPhotoModelProxy.h
//  Carbon
//
//  Created by Semih Cihan on 17/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelProxy.h"

@protocol CBGalleryPhotoModelProxyProtocol <NSObject>

@optional

- (NSString *)photoIdJSONTransformerOverridden:(id)object;
- (NSString *)photoDescriptionJSONTransformerOverridden:(id)object;
- (NSString *)imageUrlJSONTransformerOverridden:(id)object;

@end

@interface CBGalleryPhotoModelProxy : CBBaseModelProxy <CBGalleryPhotoModelProxyProtocol>

+ (instancetype)sharedInstance;

- (id)photoIdJSONTransformer:(id)object;
- (id)photoDescriptionJSONTransformer:(id)object;
- (id)imageUrlJSONTransformer:(id)object;

@end
