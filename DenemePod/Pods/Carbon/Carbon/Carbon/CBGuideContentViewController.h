//
//  CBGuideContentViewController.h
//  Carbon
//
//  Created by Necati Aydın on 09/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"

@class CBGuideDayModel;

@interface CBGuideContentViewController : CBBaseViewController

@property (nonatomic, strong) CBGuideDayModel *guideDayModel;

@end
