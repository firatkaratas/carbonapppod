//
//  CBPListReader.m
//  Carbon
//
//  Created by Necati Aydın on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBPListReader.h"
#import "NSString+Additions.h"

static NSMutableDictionary *singletons = nil;

@implementation CBPListReader

+ (instancetype)sharedConfig
{
    NSString *className = NSStringFromClass(self);
    CBPListReader *reader = [singletons objectForKey:className];
    reader.plistDictionary = [reader loadPlistDictionary];
    return reader;
}


+ (void)initialize
{
    if(!singletons)
    {
        singletons = [NSMutableDictionary dictionary];
    }
    
    if([self class] == [CBPListReader class])
    {
        return;
    }
    
    NSString *className = NSStringFromClass(self);
    
    if(![singletons objectForKey:className])
    {
        [singletons setObject:[[NSClassFromString(className) alloc] init] forKey:className];
    }
}

/**
 @return Returns the dictionary from the plist file with the same name as the class (with or without the project prefix)
 */
- (NSDictionary *)loadPlistDictionary
{
    NSString *resourceName = [NSStringFromClass([self class]) removeFrameworkPrefixIfExists];
    
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *path = [bundle pathForResource:resourceName ofType:@"plist"];
    NSDictionary *plistDictionary = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    return plistDictionary;
}

@end
