//
//  CBPushModelProxy.m
//  Carbon
//
//  Created by Semih Cihan on 15/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBPushModelProxy.h"

@implementation CBPushModelProxy

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[CBPushModelProxy alloc] init];
    });
    return sharedInstance;
}

- (id)alertJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(alertJSONTransformerOverridden:)]) {
        return [self alertJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)feedIdJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(feedIdJSONTransformerOverridden:)]) {
        return [self feedIdJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)feedTypeNumberJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(feedTypeNumberJSONTransformerOverridden:)]) {
        return [self feedTypeNumberJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

@end
