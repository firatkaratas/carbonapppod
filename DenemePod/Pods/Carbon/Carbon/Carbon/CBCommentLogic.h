//
//  CBCommentLogic.h
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"

@interface CBCommentLogic : CBBaseLogic <CBDataLoaderProtocol>

- (instancetype)initWithDetailId:(NSString *)detailId;

/**
 @description Data for CommentViewController instances.
 */
@property (strong, nonatomic) NSArray *data;

/**
 @description Delegate of CBFeedLogic.
 */
@property (weak, nonatomic) id<CBDataLoaderDelegate> delegate;

@end
