//
//  UIDevice+Additions.m
//  Carbon
//
//  Created by Necati Aydın on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIDevice+Additions.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIDeviceAdditions)

@implementation UIDevice(Additions)

+ (BOOL)iOS8AndHigher
{
    return [[UIDevice currentDevice].systemVersion floatValue] >= 8;
}

@end
