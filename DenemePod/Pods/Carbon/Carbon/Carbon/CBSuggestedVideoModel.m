//
//  CBSuggestedVideoModel.m
//  Carbon
//
//  Created by Semih Cihan on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBSuggestedVideoModel.h"
#import "CBSuggestedVideoModelConfig.h"
#import "CBCategoryStore.h"
#import "CBSuggestedVideoModelProxy.h"

@implementation CBSuggestedVideoModel

+ (CBBaseModelConfig *)config
{
    return [CBSuggestedVideoModelConfig sharedConfig];
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"detailId" : [CBSuggestedVideoModelConfig sharedConfig].detailId,
             @"category" : [CBSuggestedVideoModelConfig sharedConfig].category,
             @"title" : [CBSuggestedVideoModelConfig sharedConfig].title,
             @"spot" : [CBSuggestedVideoModelConfig sharedConfig].spot,
             @"imageUrl" : [CBSuggestedVideoModelConfig sharedConfig].imageUrl,
             };
}

+ (NSValueTransformer *)detailIdJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBSuggestedVideoModelProxy sharedInstance] detailIdJSONTransformer:object];
        return [CBBaseModel stringOrNumberToString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)categoryJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBSuggestedVideoModelProxy sharedInstance] categoryJSONTransformer:object];
        
        if (object) {
            NSString *categoryId = [self stringOrNumberToString:object];
            CBCategoryModel *cat = [[CBCategoryStore sharedStore] getCategoryWithId:categoryId];
            return cat;
        } else {
            return nil;
        }
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)titleJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBSuggestedVideoModelProxy sharedInstance] titleJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)spotJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBSuggestedVideoModelProxy sharedInstance] spotJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)imageUrlJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBSuggestedVideoModelProxy sharedInstance] imageUrlJSONTransformer:object];
        return [NSURL URLWithString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

@end
