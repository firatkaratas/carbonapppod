//
//  UIImage+Additions.h
//  Carbon
//
//  Created by Necati Aydın on 06/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Additions)

/**
 Creates an image with the given size. Default size for the image is 1x1.
 */
+ (UIImage *)imageWithColor:(UIColor *)color;

@end
