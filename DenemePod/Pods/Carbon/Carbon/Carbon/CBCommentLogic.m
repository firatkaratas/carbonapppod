//
//  CBCommentLogic.m
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBCommentLogic.h"
#import "CBNetworkManager.h"

@interface CBCommentLogic()

@property (nonatomic, strong) NSString *detailId;

@end

@implementation CBCommentLogic
@dynamic delegate;

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        _data = [NSArray new];
        _detailId = 0;
    }
    return self;
}

- (instancetype)initWithDetailId:(NSString *)detailId
{
    self = [super init];
    if(self)
    {
        _data = [NSArray new];
        _detailId = detailId;
    }
    return self;
}

- (void)loadData
{
    [self.delegate dataLoading];
    
    __weak typeof(self) weakSelf = self;
    [[CBNetworkManager sharedInstance] getCommentsByDetailId:self.detailId
                                                successBlock:^(NSArray *comments)
     {
         weakSelf.data = comments;
         [weakSelf.delegate dataLoaded];
     } failureBlock:^(NSError *error) {
         [weakSelf.delegate dataLoadedWithError:NSLocalizedString(@"Unable to fetch the news.", nil)];
     }];
}

@end
