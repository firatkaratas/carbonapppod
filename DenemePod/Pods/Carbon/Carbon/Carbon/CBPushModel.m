//
//  CBPushModel.m
//  Carbon
//
//  Created by Necati Aydın on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBPushModel.h"
#import "CBPushModelProxy.h"

@interface CBPushModel ()

@property (nonatomic, strong) NSNumber *feedTypeNumber;

@end

@implementation CBPushModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"alert":@"alert",
             @"feedId":@"feedId",
             @"feedTypeNumber":@"feedType"};
}

+ (CBBaseModelConfig *)config
{
    return nil;
}

- (FeedType)feedType
{
    return [self.feedTypeNumber integerValue];
}

+ (NSValueTransformer *)alertJSONTransformer
{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {

        object = [[CBPushModelProxy sharedInstance] alertJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)feedIdJSONTransformer
{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBPushModelProxy sharedInstance] feedIdJSONTransformer:object];
        return [CBBaseModel stringOrNumberToString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)feedTypeNumberJSONTransformer
{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBPushModelProxy sharedInstance] feedTypeNumberJSONTransformer:object];
        return [CBBaseModel stringOrNumberToNumber:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

@end

