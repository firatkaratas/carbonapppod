//
//  NSObject+Property.h
//  Carbon
//
//  Created by Necati Aydın on 07/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 A category with utility methods to handle properties of an object.
 */
@interface NSObject (Property)

/**
 Get the property names of an object. It does not return the property names of the super class.
 
 @return NSArray including the names of all the properties.
 */
- (NSArray *)allPropertyNames;

/**
 For getting the type of a property belonging to some class. If the class A has an NSString property B, it returns NSString.
 
 @param propertyName Class type of the property.
 
 @return Class of a property. If the property has a primitive type, it returns nil.
 */
- (Class)classOfPropertyNamed:(NSString *)propertyName;

/**
 Returns the object properties and values in a dictionoary. Primitive types are converted to NSNumber.
 @return Dictionary representation of the object.
 */
- (NSDictionary *)propertyDictionary;

/**
 Returns the object properties and values in a dictionoary. Primitive types are converted to NSNumber. Nil values are given the key string as a value.
 @return Dictionary representation of the object.
 */
- (NSDictionary *)propertyDictionaryByUsingDefaultForNils;

@end
