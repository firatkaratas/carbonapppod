//
//  CBGuideCell.h
//  Carbon
//
//  Created by Necati Aydın on 09/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseCollectionViewCell.h"

@interface CBGuideCell : CBBaseCollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *typeLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@property (nonatomic, strong) NSArray *separators;

+ (CGSize)sizeForWidth:(CGFloat)width;

@end
