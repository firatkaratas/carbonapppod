//
//  CBVideoDetailModel.h
//  Carbon
//
//  Created by Semih Cihan on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNewsDetailModel.h"

@interface CBVideoDetailModel : CBNewsDetailModel

@property (strong, nonatomic) NSURL *videoUrl;
@property (strong, nonatomic) NSString *duration;
@property (strong, nonatomic) NSNumber *viewCount;
@property (strong, nonatomic) NSMutableArray *suggestedVideos;

@end
