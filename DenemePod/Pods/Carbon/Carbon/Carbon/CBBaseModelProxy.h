//
//  CBBaseModelProxy.h
//  Carbon
//
//  Created by Semih Cihan on 21/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Model proxy class that interferes the dictionaries before passing them to the related JSONTransformer method implemented by the subclass. The methods of this class check if the related model proxy protocol method is implemented and if so call the method. Otherwise they do nothing but returning the input as is. If you want to interfere the dictionaries passed to transformers you need to create a category of the model proxy class and implement its protocol.
 */
@interface CBBaseModelProxy : NSObject

@end
