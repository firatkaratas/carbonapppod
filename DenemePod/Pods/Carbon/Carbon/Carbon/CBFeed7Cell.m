//
//  CBFeed7Cell.m
//  Carbon
//
//  Created by Necati Aydın on 12/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeed7Cell.h"
#import "UIView+Separator.h"

@implementation CBFeed7Cell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.separators = [self.contentView addFrameWithLineWidth:kCellBorderLineWidth];
    
    self.imageView.clipsToBounds = YES;
    self.imageView.layer.borderWidth = 1;
}

+ (CGFloat)cellRatio
{
    return 100.f/300.f;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.layer.cornerRadius = CGRectGetHeight(self.imageView.bounds)/2;
}


@end
