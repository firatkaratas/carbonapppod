//
//  CBGalleryLogic.m
//  Carbon
//
//  Created by Necati Aydın on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGalleryLogic.h"
#import "CBNetworkManager.h"

@implementation CBGalleryLogic
@dynamic delegate;

- (void)loadData
{
    [self.delegate dataLoading];
    
    __weak typeof(self) weakSelf = self;
    [[CBNetworkManager sharedInstance] getGalleryDetailById:self.feedId
                                                      successBlock:^(CBGalleryDetailModel *detailModel)
    {
        weakSelf.data = detailModel;
        [weakSelf.delegate dataLoaded];

    } failureBlock:^(NSError *error)
    {
        [weakSelf.delegate dataLoadedWithError:NSLocalizedString(@"Unable to fetch the news.", nil)];
    }];
}

@end
