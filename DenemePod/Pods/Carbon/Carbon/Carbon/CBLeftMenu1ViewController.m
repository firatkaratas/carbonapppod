//
//  CBLeftMenuViewController.m
//  Carbon
//
//  Created by Necati Aydın on 17/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenuContainerViewController.h"
#import "CBLeftMenu1ViewController.h"
#import "CBLeftMenu1View.h"
#import "CBLeftMenu1ContentView.h"
#import "CBLeftMenu1TableViewCell.h"
#import "CBLeftMenuStyleConfig.h"
#import "CBCategoryModel.h"

@interface CBLeftMenu1ViewController (Styling) <CBLeftMenuStylingDelegate>

- (void)style;
- (void)styleLeftMenuTableViewCell:(CBLeftMenu1TableViewCell *)leftMenuTableViewCell;

@end

@interface CBLeftMenu1ViewController () <CBLeftMenu1ViewDelegate>

@property (nonatomic, weak) IBOutlet CBLeftMenu1View *leftMenuView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *scrollViewHeight;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *scrollViewRightSpace;
@property (weak, nonatomic) IBOutlet UIImageView *appIconImageView;
@property (nonatomic, weak) IBOutlet UIButton *backButton;
@property (nonatomic, weak) IBOutlet UIView *separatorView;
@property (nonatomic, weak) IBOutlet UIButton *headerTitleButton;
@property (nonatomic, strong) NSMutableArray *selectedCategories;

@end



@implementation CBLeftMenu1ViewController

@synthesize leftMenuViewControllerDelegate,rootCategory;

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self style];
    
    CBLeftMenuContainerViewController *vc = (CBLeftMenuContainerViewController *)self.parentViewController;
    self.scrollViewRightSpace.constant = [vc getCalculatedContentViewInPortraitWidth];
    [self.leftMenuView layoutIfNeeded];
   
    self.scrollViewHeight.constant = [CBLeftMenu1View height];
}

- (void)dealloc
{
    _leftMenuView.delegate = nil;
}

#pragma mark - Left Menu View

/**
 @warning page is always one-indexed. It is zero only for dummy root category.
 */
- (NSString *)leftMenuView:(CBLeftMenu1View *)leftMenuView titleForPage:(NSInteger)page row:(NSInteger)row
{
    CBCategoryModel *currentCategory = self.selectedCategories[page];
    CBCategoryModel *subCategory = currentCategory.subcategories[row];
    return subCategory.name;

}
- (NSInteger)leftMenuView:(CBLeftMenu1View *)leftMenuView numberOfRowsForPage:(NSInteger)page
{
    CBCategoryModel *currentCategory = self.selectedCategories[page];
    return currentCategory.subcategories.count;
}

- (void)leftMenuView:(CBLeftMenu1View *)leftMenuView pageDisplayed:(NSInteger)page scrollingDirection:(CBLeftMenu1ViewScrollingDirection)direction
{
    self.backButton.hidden = (page == 0 || page == 1);
    self.appIconImageView.hidden = !self.backButton.hidden;
    
    CBCategoryModel *currentCategory = self.selectedCategories[page-1];
    [self.headerTitleButton setTitle:currentCategory.name forState:UIControlStateNormal];
    
    if(direction == CBLeftMenu1ViewScrollingDirectionBack)
    {
        self.selectedCategories = [self.selectedCategories subarrayWithRange:NSMakeRange(0, page + 1)].mutableCopy;
    }
}

- (void)leftMenuView:(CBLeftMenu1View *)leftMenuView didSelectRow:(NSInteger)row atPage:(NSInteger)page
{
    //if a category is selected at the same page, remove extra
    self.selectedCategories = [self.selectedCategories subarrayWithRange:NSMakeRange(0, page)].mutableCopy;

    CBCategoryModel *parentCategory = self.selectedCategories[page-1];
    CBCategoryModel *selectedCategory = parentCategory.subcategories[row];
    [self.selectedCategories addObject:selectedCategory];
    if([selectedCategory hasSubcategory])
    {
        [leftMenuView pushContentViewAnimated:YES];
    }

    CBCategoryModel *lastCategory = [self.selectedCategories lastObject];
    [self.leftMenuViewControllerDelegate leftMenu:self didSelectCategory:lastCategory];
}

- (void)leftMenuViewDidSelectFirstRow:(CBLeftMenu1View *)leftMenuView
{
    self.backButton.hidden = YES;
    self.appIconImageView.hidden = NO;
    CBCategoryModel *category = self.rootCategory;
    [self.headerTitleButton setTitle:category.name forState:UIControlStateNormal];
    CBCategoryModel *parentCategory = self.selectedCategories[0];
    CBCategoryModel *selectedCategory = parentCategory.subcategories[0];
    [self.selectedCategories addObject:selectedCategory];
}

#pragma mark - Action

- (IBAction)backButtonTapped:(id)sender
{
    [self.leftMenuView popContentViewAnimated:YES];
}

- (IBAction)infoTapped:(id)sender
{
    [self.leftMenuViewControllerDelegate infoTappedFromViewController:self];
}

- (IBAction)settingsTapped:(id)sender
{
    [self.leftMenuViewControllerDelegate settingsTappedFromViewController:self];
}

#pragma mark -

- (void)dataLoaded
{
    if (self.rootCategory)
    {
        self.selectedCategories = [NSMutableArray arrayWithObject:self.rootCategory];
        [self.leftMenuView pushContentViewAnimated:NO];
        [self.leftMenuView reload];
        [self.leftMenuView selectFirstRowInFirstPage];
    }
}

@end

@implementation CBLeftMenu1ViewController (Styling)

- (void)style
{
    self.view.backgroundColor = [CBLeftMenuStyleConfig sharedConfig].viewBackgroundColor;
    [self.headerTitleButton setTitleColor:[CBLeftMenuStyleConfig sharedConfig].headerTitleColor forState:UIControlStateNormal];
    self.headerTitleButton.titleLabel.font = [CBLeftMenuStyleConfig sharedConfig].headerTitleFont;
    self.separatorView.backgroundColor = [CBLeftMenuStyleConfig sharedConfig].separatorColor;
    [self.appIconImageView setImage:[UIImage imageNamed:@"left_menu_app_icon" inBundle:[NSBundle frameworkBundle]]];
}

- (void)styleLeftMenuTableViewCell:(CBLeftMenu1TableViewCell *)leftMenuTableViewCell
{
    leftMenuTableViewCell.titleLabel.textColor = [CBLeftMenuStyleConfig sharedConfig].menuItemTextColor;
    leftMenuTableViewCell.titleLabel.font = [CBLeftMenuStyleConfig sharedConfig].menuItemTextFont;
    leftMenuTableViewCell.highlightView.backgroundColor = [CBLeftMenuStyleConfig sharedConfig].highlightViewColor;
    leftMenuTableViewCell.selectedTextColor = [CBLeftMenuStyleConfig sharedConfig].menuItemHighlightTextColor;
    leftMenuTableViewCell.textColor = [CBLeftMenuStyleConfig sharedConfig].menuItemTextColor;
    leftMenuTableViewCell.selectedTextFont = [CBLeftMenuStyleConfig sharedConfig].menuItemHighlightTextFont;
    leftMenuTableViewCell.textFont = [CBLeftMenuStyleConfig sharedConfig].menuItemTextFont;
    leftMenuTableViewCell.arrowColor = [CBLeftMenuStyleConfig sharedConfig].arrowColor;
    leftMenuTableViewCell.selectedArrowColor = [CBLeftMenuStyleConfig sharedConfig].highlightArrowColor;
}


@end