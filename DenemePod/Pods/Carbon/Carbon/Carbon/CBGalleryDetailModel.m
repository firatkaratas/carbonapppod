//
//  CBGalleryDetailModel.m
//  Carbon
//
//  Created by Semih Cihan on 27/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGalleryDetailModel.h"
#import "CBGalleryDetailModelConfig.h"
#import "CBGalleryPhotoModel.h"
#import "CBCategoryStore.h"
#import "CBGalleryDetailModelProxy.h"

@implementation CBGalleryDetailModel

+ (CBBaseModelConfig *)config
{
    return [CBGalleryDetailModelConfig sharedConfig];
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"detailId" : [CBGalleryDetailModelConfig sharedConfig].detailId,
             @"date" : [CBGalleryDetailModelConfig sharedConfig].date,
             @"category" : [CBGalleryDetailModelConfig sharedConfig].category,
             @"photos" : [CBGalleryDetailModelConfig sharedConfig].photos,
             @"url" : [CBGalleryDetailModelConfig sharedConfig].url,
             @"title" : [CBGalleryDetailModelConfig sharedConfig].title
             };
}

+ (NSValueTransformer *)detailIdJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGalleryDetailModelProxy sharedInstance] detailIdJSONTransformer:object];
        return [CBBaseModel stringOrNumberToString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)categoryJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGalleryDetailModelProxy sharedInstance] categoryJSONTransformer:object];
        
        if (object) {
            NSString *categoryId = [self stringOrNumberToString:object];
            CBCategoryModel *cat = [[CBCategoryStore sharedStore] getCategoryWithId:categoryId];
            return cat;
        } else {
            return nil;
        }
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)dateJSONTransformer
{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGalleryDetailModelProxy sharedInstance] dateJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)photosJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        NSArray *rawPhotosArray = [[CBGalleryDetailModelProxy sharedInstance] photosJSONTransformer:object];
        
        if (rawPhotosArray) {
            NSError *error;
            NSArray *modelArray = [MTLJSONAdapter modelsOfClass:CBGalleryPhotoModel.class fromJSONArray:rawPhotosArray error:&error];
            if (error) {
                DebugLog(@"Error in photosJSONTransformer in CBGalleryDetailModel, error description: %@", error.localizedDescription);
                return nil;
            }
            return modelArray;
        } else {
            return nil;
        }

    } reverseBlock:^id (CBGalleryDetailModel *galleryDetail) {
        return nil;
    }];
}

+ (NSValueTransformer *)titleJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGalleryDetailModelProxy sharedInstance] titleJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)urlJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGalleryDetailModelProxy sharedInstance] urlJSONTransformer:object];
        return [NSURL URLWithString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

- (NSInteger)photoCount
{
    if(!self.photos)
    {
        return 0;
    }
    
    return self.photos.count;
}

@end
