//
//  CarbonViewController.m
//  Carbon
//
//  Created by Necati Aydın on 29/12/14.
//  Copyright (c) 2014 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"
#import "CBAppConfig.h"

@interface CBBaseViewController ()

@end

@implementation CBBaseViewController

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc
{
    _logic.delegate = nil;
}

- (NSString *)appName
{
    return [CBAppConfig sharedConfig].appName;
}

@end
