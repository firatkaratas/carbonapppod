//
//  CBLeftMenuViewController.h
//  Carbon
//
//  Created by Necati Aydın on 17/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu.h"

/**
 Type 1 left menu.
 */
@interface CBLeftMenu1ViewController : UIViewController <CBLeftMenuViewControllerProtocol>

/**
 Should be called when categories are loaded.
 */
- (void)dataLoaded;

@end
