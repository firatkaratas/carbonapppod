//
//  UIImageView+LayoutConstraints.m
//  Carbon
//
//  Created by Necati Aydın on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIImageView+LayoutConstraints.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIImageViewLayoutConstraints)

@implementation UIImageView(LayoutConstraints)

- (NSLayoutConstraint *)setAspectFitLayoutConstraint
{
    if(!self.image)
    {
        return nil;
    }
    
    CGFloat ratio = self.image.size.width / self.image.size.height;
    NSLayoutConstraint *constraint =
    [NSLayoutConstraint constraintWithItem:self
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self
                                 attribute:NSLayoutAttributeHeight
                                multiplier:ratio
                                  constant:0];
    [self addConstraint:constraint];
    return constraint;
}

@end
