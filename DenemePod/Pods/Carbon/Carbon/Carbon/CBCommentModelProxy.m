//
//  CBCommentModelProxy.m
//  Carbon
//
//  Created by Semih Cihan on 17/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBCommentModelProxy.h"

@implementation CBCommentModelProxy

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[CBCommentModelProxy alloc] init];
    });
    return sharedInstance;
}

- (id)commentIdJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(commentIdJSONTransformerOverridden:)]) {
        return [self commentIdJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)commentJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(commentJSONTransformerOverridden:)]) {
        return [self commentJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)dateJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(dateJSONTransformerOverridden:)]) {
        return [self dateJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)usernameJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(usernameJSONTransformerOverridden:)]) {
        return [self usernameJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

@end
