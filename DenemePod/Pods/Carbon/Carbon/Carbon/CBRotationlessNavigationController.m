//
//  CBRotationlessNavigationController.m
//  Carbon
//
//  Created by Necati Aydın on 13/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBRotationlessNavigationController.h"

@interface CBRotationlessNavigationController ()

@end

@implementation CBRotationlessNavigationController

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
