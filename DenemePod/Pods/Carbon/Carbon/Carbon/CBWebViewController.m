//
//  CBWebViewController.m
//  Carbon
//
//  Created by Necati Aydın on 22/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBWebViewController.h"
#import "CBWebViewStyleConfig.h"
#import "UIView+Separator.h"
#import "CBAnalyticsManager.h"
#import "CBNavigationBarStyler.h"
#import "UIDevice+Additions.h"

@interface CBWebViewController () <UIWebViewDelegate>

@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) IBOutlet UIView *bottomBar;

@property (nonatomic, strong) IBOutlet UIButton *backButton;
@property (nonatomic, strong) IBOutlet UIButton *nextButton;
@property (nonatomic, strong) IBOutlet UIButton *refreshButton;
@property (nonatomic, strong) IBOutlet UIButton *shareButton;
@property (nonatomic, strong) IBOutlet UIButton *closeButton;

@end

@implementation CBWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.backButton.enabled = NO;
    self.nextButton.enabled = NO;
    [self.webView loadRequest:self.request];
    self.view.backgroundColor = [CBWebViewStyleConfig sharedConfig].viewBackgroundColor;
    UIView *separator = [self.bottomBar addSeparatorAtTopWithLineWidth:1];
    separator.backgroundColor = [CBWebViewStyleConfig sharedConfig].barSeparatorColor;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.backButton.enabled = webView.canGoBack;
    self.nextButton.enabled = webView.canGoForward;
}

#pragma mark - Action

- (IBAction)backButtonTapped:(id)sender
{
    [self.webView goBack];
}

- (IBAction)nextButtonTapped:(id)sender
{
    [self.webView goForward];
}

- (IBAction)refreshButtonTapped:(id)sender
{
    [self.webView reload];
}

- (IBAction)shareButtonTapped:(id)sender
{
    [CBAnalyticsManager commentButtonTapped];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[self.shareTitle ? self.shareTitle : @"", self.webView.request.URL]
                                                                                         applicationActivities:nil];
    if(IPAD)
    {
        if([UIDevice iOS8AndHigher])
        {
            activityViewController.popoverPresentationController.sourceView = self.shareButton;
            activityViewController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
        }
        else
        {
            return;
        }
    }
    
    [self presentViewController:activityViewController animated:YES completion:^{}];
}

- (IBAction)closeButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
