//
//  UIViewController+Utility.h
//  Carbon
//
//  Created by Semih Cihan on 06/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utility)

/**
 Used to get the name of the view controller defined by NSStringFromClass([self class]).
 @return NSString Returns the name of the view controller.
 */
+ (NSString *)storyboardName;


@end
