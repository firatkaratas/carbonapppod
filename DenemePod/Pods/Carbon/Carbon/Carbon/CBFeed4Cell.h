//
//  CBFeedSmallImageCell.h
//  Carbon
//
//  Created by Semih Cihan on 10/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedCell.h"

@interface CBFeed4Cell : CBFeedCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;

@end
