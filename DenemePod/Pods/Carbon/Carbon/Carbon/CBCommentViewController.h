//
//  CBCommentViewController.h
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"
#import "CBCommentLogic.h"

@interface CBCommentViewController : CBBaseViewController <CBDataLoaderDelegate>

@property (strong, nonatomic) CBCommentLogic *logic;

@end
