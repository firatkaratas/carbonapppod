//
//  CBCommentViewController.m
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBCommentViewController.h"
#import "CBCommentCell.h"
#import "UIViewController+CBCommentCell.h"
#import "CBCommentStyleConfig.h"
#import "CBCommentModel.h"
#import "CBNavigationBarStyler.h"
#import "UIView+Loading.h"
#import "UICollectionView+Additions.h"

static const CGFloat kCellHorizontalMargin = 10;

#pragma mark - Styling

@interface CBCommentViewController (Styling)

- (void)style;

@end

@implementation CBCommentViewController (Styling)

- (void)style
{
    CBCommentStyleConfig *config = [CBCommentStyleConfig sharedConfig];
    
    self.view.backgroundColor = config.viewBackgroundColor;
}

- (void)styleCommentCell:(CBCommentCell *)cell
{
    CBCommentStyleConfig *config = [CBCommentStyleConfig sharedConfig];
    
    for(UIView *separator in cell.separators)
    {
        separator.backgroundColor = config.cellBorderColor;
    }
    
    cell.backgroundColor = config.cellBackgroundColor;
    [cell.commentLabel setTextColor:config.commentColor];
    [cell.detailLabel setTextColor:config.detailColor];
    [cell.commentLabel setFont:config.commentFont];
    [cell.detailLabel setFont:config.detailFont];
}

@end

#pragma mark - Class

@interface CBCommentViewController () <UICollectionViewDataSource, UICollectionViewDelegate, CBLoadingActionProtocol>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation CBCommentViewController
@dynamic logic;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView registerClassForDefaultReuseIdentifier:[CBCommentCell class]];
    [self setTitle:NSLocalizedString(@"Comments", @"")];
    
    [self style];
    [CBNavigationBarStyler styleNavigationItemWithBackIcon:self.navigationItem
                                                        target:self
                                                        action:@selector(backButtonTapped:)];
    [self.logic loadData];
}

- (void)dealloc
{
    _collectionView.delegate = nil;
    _collectionView.dataSource = nil;
}

#pragma mark - collection view delegate & data source

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10.f, 0.f, 10.f, 0.f);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.logic.data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CBCommentCell *cell = (CBCommentCell *)[collectionView dequeueReusableCellWithReuseIdentifier:[CBCommentCell reuseIdentifier] forIndexPath:indexPath];
    [self styleCommentCell:cell];
    
    CBCommentModel *model = self.logic.data[indexPath.row];
    [self configureCommentCell:cell withCommentModel:model];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CBCommentModel *model = self.logic.data[indexPath.row];
    CGFloat width = CGRectGetWidth(self.view.frame) - 2 * kCellHorizontalMargin;
    return [CBCommentCell cellSizeForWidth:width commentText:model.comment commentFont:[CBCommentStyleConfig sharedConfig].commentFont detailText:[NSString stringWithFormat:@"%@ - %@", model.username, model.date] detailFont:[CBCommentStyleConfig sharedConfig].detailFont];
}

#pragma mark - CommentLogicDelegate

- (void)dataLoading {
    [self.view showLoadingView];
}

- (void)dataLoaded
{
    if (self.logic.data.count > 0) {

        [self.view dismissLoadingView];
        [self.collectionView reloadData];
    } else {
        
        [self.view showErrorMessage:NSLocalizedString(@"No comments", nil) actionTarget:nil];
    }

}

- (void)dataLoadedWithError:(NSString *)errorMessage {
    [self.view showErrorMessage:errorMessage actionTarget:self];
}

#pragma mark - CBLoadingActionProtocol

- (void)errorViewTapped:(UIGestureRecognizer *)recognizer {
    [self.view dismissErrorView];
    [self.logic loadData];
}

#pragma mark - Action

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
