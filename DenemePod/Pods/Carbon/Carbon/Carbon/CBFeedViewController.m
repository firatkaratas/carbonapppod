//
//  CBHomeViewController.m
//  Carbon
//
//  Created by Semih Cihan on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedViewController.h"
#import "CBFeed1Cell.h"
#import "CBFeed2Cell.h"
#import "CBFeed3Cell.h"
#import "CBFeed4Cell.h"
#import "CBFeed5Cell.h"
#import "CBFeed7Cell.h"
#import "CBFeedModel.h"
#import "CBNavigationBarStyler.h"
#import "ADLivelyCollectionView.h"
#import "UIViewController+CBFeedCell.h"
#import "CBDetailContainerLogic.h"
#import "CBFeedStyleConfig.h"
#import "UIView+Loading.h"
#import "RESideMenu.h"
#import "CBViewControllerBuilder.h"
#import "UICollectionView+Additions.h"
#import "CBGalleryViewController.h"
#import "UIImageEffects.h"
#import "UIViewController+Snapshot.h"
#import "CBCollectionViewLoadingFooterView.h"
#import "UIView+LayoutConstraints.h"
#import "CBAnalyticsManager.h"
#import "UINavigationBar+TapRecognizing.h"
#import "CBFeedNoImageCell.h"
#import "UIView+NibLoading.h"
#import "CBAppConfig.h"
#import "CBAlertManager.h"

#pragma mark - Styling

@interface CBFeedViewController(Styling)

@end

@implementation CBFeedViewController(Styling)

- (void)style
{
    CBFeedStyleConfig *config = [CBFeedStyleConfig sharedConfig];
    self.view.backgroundColor = config.viewBackgroundColor;
}

- (void)styleCell:(CBFeedCell *)cell
{
    CBFeedStyleConfig *config = [CBFeedStyleConfig sharedConfig];
    cell.backgroundColor = config.cellBackgroundColor;
    cell.titleLabel.textColor = config.cellTitleColor;
    cell.titleLabel.font = config.cellTitleFont;
    
    for(UIView *separator in cell.separators)
    {
        separator.backgroundColor = config.cellBorderColor;
    }
    
    if([cell isKindOfClass:[CBFeedNoImageCell class]])
    {
        CBFeedNoImageCell *noImageCell = (CBFeedNoImageCell *)cell;
        noImageCell.titleLabel.textColor = config.noImageCellTitleColor;
        noImageCell.titleLabel.font = config.noImageCellTitleFont;
        noImageCell.categoryLabel.textColor = config.noImageCellCategoryColor;
        noImageCell.categoryLabel.font = config.noImageCellCategoryFont;
        noImageCell.spotLabel.textColor = config.noImageCellSpotColor;
        noImageCell.spotLabel.font = config.noImageCellSpotFont;
    }
    else if([cell isKindOfClass:[CBFeed1Cell class]])
    {
        CBFeed1Cell *feed1Cell = (CBFeed1Cell *)cell;
        [feed1Cell.categoryLabel setTextColor:config.cellCategoryColor];
        [feed1Cell.categoryLabel setFont:config.cellCategoryFont];
    }
    else if([cell isKindOfClass:[CBFeed2Cell class]])
    {
        CBFeed2Cell *feed2Cell = (CBFeed2Cell *)cell;
        [feed2Cell.dateLabel setTextColor:config.cellDateColor];
        [feed2Cell.dateLabel setFont:config.cellDateFont];
    }
    //nothing specific for CBFeed3Cell
    else if([cell isKindOfClass:[CBFeed4Cell class]])
    {
        CBFeed4Cell *feed4Cell = (CBFeed4Cell *)cell;
        [feed4Cell.label2 setTextColor:config.cellCategoryColor];
        [feed4Cell.label2 setFont:config.cellCategoryFont];
        [feed4Cell.label3 setTextColor:config.cellSpotColor];
        [feed4Cell.label3 setFont:config.cellSpotFont];
    }
    else if([cell isKindOfClass:[CBFeed5Cell class]])
    {
        CBFeed5Cell *feed5Cell = (CBFeed5Cell *)cell;
        [feed5Cell.categoryLabel setTextColor:config.cellCategoryColor];
        [feed5Cell.categoryLabel setFont:config.cellCategoryFont];
    }
    else if([cell isKindOfClass:[CBFeed7Cell class]])
    {
        CBFeed7Cell *feed7Cell = (CBFeed7Cell *)cell;
        feed7Cell.imageView.layer.borderColor = config.cellBorderColor.CGColor;
        [feed7Cell.label2 setTextColor:config.cellDateColor];
        [feed7Cell.label2 setFont:config.cellDateFont];
        [feed7Cell.label3 setTextColor:config.cellSpotColor];
        [feed7Cell.label3 setFont:config.cellSpotFont];
    }
}


@end

#pragma mark - View Controller

@interface CBFeedViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CBLoadingActionProtocol>

@property (weak, nonatomic) IBOutlet ADLivelyCollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewBottomSpaceConstraint;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSDate *lastPull;
@property (strong, nonatomic) CBCollectionViewLoadingFooterView *footerView;

@end

@implementation CBFeedViewController
@dynamic logic;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.collectionView setInitialCellTransformBlock:ADLivelyTransformFade];
    
    NSArray *cellClasses = @[[CBFeed1Cell class], [CBFeedNoImageCell class], [CBFeed2Cell class],
                             [CBFeed3Cell class], [CBFeed4Cell class], [CBFeed5Cell class],[CBFeed7Cell class]];
    for(Class class in cellClasses)
    {
        [self.collectionView registerClassForDefaultReuseIdentifier:class];
    }
    
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CBCollectionViewLoadingFooterView class]) bundle:[NSBundle frameworkBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:[CBCollectionViewLoadingFooterView reuseIdentifier]];
    
    [self.collectionView setHidden:YES];
    [self style];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh:)
             forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];

    if(self.delegate)
    {
        [CBNavigationBarStyler styleLeftNavigationItemWithMenuIcon:self.navigationItem target:self action:@selector(menuButtonTapped)];
    }
    else
    {
        [CBNavigationBarStyler styleNavigationItemWithBackIcon:self.navigationItem target:self action:@selector(backButtonTapped)];
    }

    [CBNavigationBarStyler styleWithTitleLogo:self.navigationItem];
    
    if ([CBAppConfig sharedConfig].liveStreamEnabled) {
        [CBNavigationBarStyler styleRightNavigationItemWithStreamIcon:self.navigationItem target:self action:@selector(streamButtonTapped)];
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
    }
    
    [self.navigationController.navigationBar addTapGestureRecognizerByRemovingTheOldWithTarget:self action:@selector(navigationBarTapped:)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)dealloc
{
    _collectionView.delegate = nil;
    _collectionView.dataSource = nil;
}

- (void)setLogic:(CBFeedLogic *)logic
{
    //BUGFIX: while switching among categories from left menu, an empty data set should be reloaded for scrolling to top
    [self.collectionViewAdManager cleanDataSource];
    [self.collectionViewAdManager clean];
    self.collectionViewAdManager = nil;
    self.logic.data = @[].mutableCopy;
    [self.collectionView reloadData];
    //
    
    super.logic = logic;
    self.lastPull = [NSDate dateWithTimeIntervalSince1970:0];
    [CBAnalyticsManager setLastOpenedFeedCategoryName:self.logic.category.name];
}

- (NSInteger)cellTypeForFeed:(CBFeedModel *)feed
{
    if(feed.cellType == CBFeedCellTypeNotSpecified)
    {
        if(self.cellType == CBFeedCellTypeNotSpecified)
        {
            return [CBFeedStyleConfig sharedConfig].cellType;
        }
        else
        {
            return self.cellType;
        }
    }
    else
    {
        return feed.cellType;
    }

}

- (Class)cellClassForFeed:(CBFeedModel *)feedModel
{
    if([feedModel hasImage])
    {
        CBFeedCellType cellType = [self cellTypeForFeed:feedModel];
        if(cellType == CBFeedCellType1)
        {
            return [CBFeed1Cell class];
        }
        else if(cellType == CBFeedCellType2)
        {
            return [CBFeed2Cell class];
        }
        else if(cellType == CBFeedCellType3)
        {
            return [CBFeed3Cell class];
        }
        else if(cellType == CBFeedCellType4)
        {
            return [CBFeed4Cell class];
        }
        else if(cellType == CBFeedCellType5)
        {
            return [CBFeed5Cell class];
        }
        else if(cellType == CBFeedCellType7)
        {
            return [CBFeed7Cell class];
        }
        else
        {
            return NULL;
        }
    }
    else
    {
        return [CBFeedNoImageCell class];
    }
}

#pragma mark - Private methods

- (void)startRefresh:(UIRefreshControl *)refreshControl {
    if (!self.logic.isLoading && [[NSDate date] timeIntervalSinceDate:self.lastPull] > 30) { //refreshing is not allowed before half minute from the last refresh because otherwise seamless causes problems in collectionview
        [self.logic refreshData];
        self.lastPull = [NSDate date];
    } else {
        [refreshControl endRefreshing];
    }
}

#pragma mark - collection view delegate & data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2; // first section is for cells, second section is for loading supplementary view footer.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return (section == 0) ? self.logic.data.count : 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.collectionViewAdManager shouldShowAdAtIndexPath:indexPath]) {
        return [self.collectionViewAdManager cellForItemAtIndexPath:indexPath];
    }

    CBFeedModel *feedModel = self.logic.data[indexPath.row];
    CBFeedCell *cell = nil;

    Class cellClass = [self cellClassForFeed:feedModel];
    if([cellClass respondsToSelector:@selector(reuseIdentifier)])
    {
        NSString *reuseIdentifier = [cellClass performSelector:@selector(reuseIdentifier) withObject:nil];
        cell = (CBFeedCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    }
    [self styleCell:cell];
    [self configureFeedCell:cell withFeedModel:feedModel];

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.collectionViewAdManager shouldShowAdAtIndexPath:indexPath]) {
        return [self.collectionViewAdManager sizeForItemAtIndexPath:indexPath];
    }
    else
    {
        CBFeedModel *feedModel = self.logic.data[indexPath.row];
        
        if([feedModel hasImage])
        {
            CGFloat width = 0;
            if(IPAD)
            {
                width = (MIN(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) - 3 * kFeedCellHorizontalSpace) / 2;
            }
            else
            {
                width = MIN(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) - 2 * kFeedCellHorizontalSpace;
            }
            
            Class cellClass = [self cellClassForFeed:feedModel];
            if([cellClass respondsToSelector:@selector(sizeForWidth:)])
            {
                return [cellClass sizeForWidth:width];
            }
            else
            {
                return CGSizeZero;
            }
        }
        else
        {
            return [self sizeForNoImageCellForFeedModel:feedModel];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.collectionViewAdManager shouldShowAdAtIndexPath:indexPath])
    {
        [self.collectionViewAdManager didSelectItemAtIndexPath:indexPath];
    }
    else
    {
        CBBaseLogic *logic = [self.logic getLogicWithIndex:indexPath.row];
        if([logic isKindOfClass:[CBDetailContainerLogic class]])
        {
            CBDetailContainerLogic *containerLogic = (CBDetailContainerLogic *)logic;
            CBFeedModel *feed = self.logic.data[indexPath.row];
            NSInteger startingIndex = [self.logic findFeedId:feed.feedId
                                                inFeedModels:containerLogic.feedModels];
            
            UIViewController *vc = [CBViewControllerBuilder detailContainerViewControllerWithLogic:containerLogic
                                                                                     startingIndex:startingIndex];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            CBGalleryViewController *galleryViewController = (CBGalleryViewController *)[CBViewControllerBuilder galleryViewControllerWithLogic:(CBGalleryLogic *)logic];
            [galleryViewController setBackgroundImage:[self.navigationController takeViewSnapshot]];
            [self presentViewController:galleryViewController animated:YES completion:nil];            
        }
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionFooter) {
        CBCollectionViewLoadingFooterView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:[CBCollectionViewLoadingFooterView reuseIdentifier] forIndexPath:indexPath];
        self.footerView = footer;
        return footer;
    }
    
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
referenceSizeForFooterInSection:(NSInteger)section {
    
    CGFloat width = MIN(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) - 2 * kFeedCellHorizontalSpace;
    
    if (section == 0) {
        
        return CGSizeMake(width, 0.f);
    } else {
        
        if (!self.logic.hasMorePageToLoad) {
            
            return CGSizeMake(width, 0.f);
        } else {
            
            return [CBCollectionViewLoadingFooterView sizeForWidth:width];
        }
    }

}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    
    if (section == 0) {
        return UIEdgeInsetsMake(10.f, 0.f, 10.f, 0.f);
    } else {
        return UIEdgeInsetsZero;
    }
}

- (void)showLoadingIndicatorForPaging
{
    if(self.footerView)
    {
        self.footerView.hidden = NO;
    }
}

- (void)dismissLoadingIndicatorForPaging
{
    if(self.footerView)
    {
        self.footerView.hidden = YES;
    }
}

#pragma mark - ScrollView Delegate

- (BOOL)shouldLoadMore
{
    static const CGFloat kLoadingMargin = 20.f;
    CGFloat collectionViewBottomY = self.collectionView.contentOffset.y + CGRectGetHeight(self.collectionView.bounds) - self.collectionView.contentInset.bottom;
    return (collectionViewBottomY + kLoadingMargin > self.collectionView.contentSize.height);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self shouldLoadMore]) {
        //checking content offset to differentiate between paging and refreshing
        if (self.logic.hasMorePageToLoad && scrollView.contentOffset.y > 0) {
            [self.logic loadNextPage];
        }
    }
}

#pragma mark - FeedLogicDelegate

- (void)dataLoading
{
    [self.view dismissLoadingView];
    [self.view showLoadingView];
    [self.collectionView setHidden:YES];
    
    if ([CBAppConfig sharedConfig].liveStreamEnabled) {
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
    }
}

- (void)dataLoadingWithPaging
{
    [self showLoadingIndicatorForPaging];
    
    if ([CBAppConfig sharedConfig].liveStreamEnabled) {
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
    }
}

- (void)dataLoaded
{
 
}

- (void)dataLoadedForRange:(NSRange)range
{
    [self dismissLoadingIndicatorForPaging];
    [self.refreshControl endRefreshing];
    [self.collectionView setHidden:NO];
    
    if (range.location == 0) { //no insertion, reload the whole collectionView (refresh or first time data loaded)
        
        [self.collectionView reloadData];
        [self startCollectionViewAdWithEntityName:[self entityNameWithoutType] collectionView:self.collectionView dataSource:self.logic.data];
    } else  {
        
        if (range.length > 0) { //insertion after paging
            NSMutableArray *indexesToAdd = [@[] mutableCopy];
            int i = 0;
            for (i = 0; i < range.length; i++) {
                [indexesToAdd addObject:[NSIndexPath indexPathForRow:i + range.location inSection:0]];
            }
            [self.collectionView insertItemsAtIndexPaths:indexesToAdd];
        }
        
        if (!self.logic.hasMorePageToLoad) { //remove the footer loading
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:1]];
        }
    }
    
    [self.view dismissLoadingView];
    
    if ([CBAppConfig sharedConfig].liveStreamEnabled) {
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }
}

- (void)dataLoadedWithError:(NSString *)errorMessage {
    [self.refreshControl endRefreshing];
    [self.collectionView setHidden:YES];
    [self.view showErrorMessage:errorMessage actionTarget:self];
    
    if ([CBAppConfig sharedConfig].liveStreamEnabled) {
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }
}

#pragma mark - Ads

- (NSString *)entityNameLiveStream
{
    return @"LiveStream";
}

- (NSString *)entityNameWithoutType
{
    return [NSString stringWithFormat:@"%@-Feed", self.logic.category.name];
}

- (void)startAds
{
    [self startInterstitialAdWithEntityName:[self entityNameWithoutType]];
    [self startMMAAdWithEntityName:[self entityNameWithoutType]];
}

#pragma mark - Action

- (void)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)streamButtonTapped {
    [self.view showLoadingView];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [self.logic getLiveStreamUrl];
}

- (void)menuButtonTapped
{
    [self.delegate menuContentViewControllerTappedMenuButton:self];
}

- (void)navigationBarTapped:(id)sender
{
    [self.collectionView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - CBLiveStreamDelegate

- (void)liveStreamUrlLoaded:(NSURL *)liveStreamUrl {
    [self.view dismissLoadingView];
    [self presentSeamlessVideoPlayerWithUrl:liveStreamUrl entityName:[self entityNameLiveStream]];
    [CBAnalyticsManager liveStreamPlayButtonTapped];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
}

- (void)liveStreamLoadedWithError:(NSString *)errorMessage {
    [self.view dismissLoadingView];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    
    __weak typeof(self) weakSelf = self;
    [CBAlertManager showAlertWithTitle:nil
                               message:errorMessage
                     cancelButtonTitle:NSLocalizedString(@"Close", nil)
                     otherButtonTitles:@[NSLocalizedString(@"Retry", nil)]
                        viewController:self
                     completionHandler:^(NSInteger buttonClicked)
     {
         if(buttonClicked == 1)
         {
             [weakSelf streamButtonTapped];
         }
     }];
}

#pragma mark - CBLoadingActionProtocol

- (void)errorViewTapped:(UIGestureRecognizer *)recognizer {
    [self.view dismissErrorView];
    [self.logic loadData];
}

#pragma mark - AdView delegate methods

- (void)adViewDidLoad:(SLAdView *)adView {
    [super adViewDidLoad:adView];
    self.collectionViewBottomSpaceConstraint.constant = SLAdSizeMMA.height;
    [self.view addSubview:adView];
    [adView centerXInSuperview];
    [adView distanceBottomToSuperview:0];
}

- (void)adViewDidFailToLoad:(SLAdView *)adView {
    [super adViewDidFailToLoad:adView];
    self.collectionViewBottomSpaceConstraint.constant = 0;
}

#pragma mark - Helper


- (CGSize)sizeForNoImageCellForFeedModel:(CBFeedModel *)feedModel
{
    static CBFeedNoImageCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
    {
        CGFloat width = MIN(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) - 2 * kFeedCellHorizontalSpace;

        if(IPAD)
        {
            width -= kFeedCellHorizontalSpace;
            width /= 2;//2 columns
        }

        sizingCell = [CBFeedNoImageCell loadFromNIB];
        sizingCell.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        sizingCell.bounds = CGRectZero;
        [sizingCell setNeedsLayout];
        [sizingCell layoutIfNeeded];

        //Insert a dummy view to block the srinking of the view for the width.
        UIView *dummyWidthView = [UIView new];
        dummyWidthView.translatesAutoresizingMaskIntoConstraints = NO;
        dummyWidthView.backgroundColor = [UIColor clearColor];
        [sizingCell.contentView addSubview:dummyWidthView];
        [dummyWidthView setWidthConstraint:width];
        [dummyWidthView setHeightConstraint:1];
        [dummyWidthView distanceLeftToSuperview:0];
        [dummyWidthView distanceRightToSuperview:0];
        [dummyWidthView distanceTopToSuperview:0];

        [sizingCell setNeedsLayout];
        [sizingCell layoutIfNeeded];
    });
    
    [self configureFeedCell:sizingCell withFeedModel:feedModel];
    [self styleCell:sizingCell];
    
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize sz = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingExpandedSize];
    return sz;
}

@end


