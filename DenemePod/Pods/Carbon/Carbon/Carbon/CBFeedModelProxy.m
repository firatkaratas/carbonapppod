//
//  CBFeedModelProxy.m
//  Carbon
//
//  Created by Semih Cihan on 16/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedModelProxy.h"

@implementation CBFeedModelProxy

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[CBFeedModelProxy alloc] init];
    });
    return sharedInstance;
}

- (id)typeNumberJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(typeNumberJSONTransformerOverridden:)]) {
        return [self typeNumberJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)feedIdJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(feedIdJSONTransformerOverridden:)]) {
        return [self feedIdJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)categoryJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(categoryJSONTransformerOverridden:)]) {
        return [self categoryJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)dateJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(dateJSONTransformerOverridden:)]) {
        return [self dateJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)titleJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(titleJSONTransformerOverridden:)]) {
        return [self titleJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)spotJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(spotJSONTransformerOverridden:)]) {
        return [self spotJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)urlJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(urlJSONTransformerOverridden:)]) {
        return [self urlJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)imageUrlJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(imageUrlJSONTransformerOverridden:)]) {
        return [self imageUrlJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)imageWidthJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(imageWidthJSONTransformerOverridden:)]) {
        return [self imageWidthJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)imageHeightJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(imageHeightJSONTransformerOverridden:)]) {
        return [self imageHeightJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

@end
