//
//  UIViewController+Utility.m
//  Carbon
//
//  Created by Semih Cihan on 06/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIViewController+Utility.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIViewControllerUtility)

@implementation UIViewController (Utility)

+ (NSString *)storyboardName {
    return NSStringFromClass([self class]);
}

@end
