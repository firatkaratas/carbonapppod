//
//  UICollectionView+Additions.h
//  Carbon
//
//  Created by Necati Aydın on 26/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (Additions)

/**
 Registers the cell nib with the default reuse identifier. Class name is used for default reuse identifier.
 */
- (void)registerClassForDefaultReuseIdentifier:(Class)cellClass;

@end
