//
//  CBVideoDetailLogic.m
//  Carbon
//
//  Created by Semih Cihan on 06/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBVideoDetailLogic.h"
#import "CBNetworkManager.h"
#import "CBVideoDetailModel.h"

@implementation CBVideoDetailLogic
@dynamic delegate,newsDetail;

- (void)loadData
{
    [self.delegate dataLoading];
    
    __weak typeof(self) weakSelf = self;
    [[CBNetworkManager sharedInstance] getVideoDetailById:self.feedId
                                            successBlock:^(CBVideoDetailModel *videoDetail)
     {
         NSString *errorMessage = [self validateModel:videoDetail];
         
         if(errorMessage)
         {
             [weakSelf.delegate dataLoadedWithError:errorMessage];
         }
         else
         {
             weakSelf.newsDetail = videoDetail;
             [weakSelf.delegate dataLoaded];
         }
     } failureBlock:^(NSError *error) {
         [weakSelf.delegate dataLoadedWithError:NSLocalizedString(@"Unable to fetch the news.", nil)];
     }];
}


- (NSString *)validateModel:(CBVideoDetailModel *)detailModel
{

    if(!detailModel.videoUrl)
    {
        DebugLog(@"%@ with feedId: %lu", NSLocalizedString(@"Unable to fetch the news.",nil), (unsigned long)self.feedId);
        return NSLocalizedString(@"Unable to fetch the news.",nil);
    }
    
    return nil;
}

@end
