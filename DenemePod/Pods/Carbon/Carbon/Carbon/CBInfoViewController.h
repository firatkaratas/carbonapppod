//
//  CBInfoViewController.h
//  Carbon
//
//  Created by Semih Cihan on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBInfoLogic.h"

@interface CBInfoViewController : UITableViewController

@property (strong, nonatomic) CBInfoLogic *logic;

@end
