//
//  UIView+Separator.m
//  Carbon
//
//  Created by Necati Aydın on 12/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIView+Separator.h"
#import "UIView+LayoutConstraints.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIViewSeparator)

@implementation UIView(Separator)

#pragma mark - Helper

- (UIView *)addSeparatorSubview
{
    UIView *separator = [UIView new];
    separator.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:separator];
    
    return separator;
}


#pragma mark -

- (NSArray *)addFrameWithLineWidth:(CGFloat)lineWidth
{
    NSMutableArray *separators = [[NSMutableArray alloc] initWithCapacity:4];
    
    //top separator
    UIView *separator = [self addSeparatorSubview];
    [separator distanceTopToSuperview:0];
    [separator distanceLeftToSuperview:0];
    [separator distanceRightToSuperview:0];
    [separator setHeightConstraint:lineWidth];

    [separators addObject:separator];
    
    //left separator
    separator = [self addSeparatorSubview];
    [separator distanceTopToSuperview:lineWidth];
    [separator distanceLeftToSuperview:0];
    [separator distanceBottomToSuperview:lineWidth];
    [separator setWidthConstraint:lineWidth];
    
    [separators addObject:separator];
    
    //bottom separator
    separator = [self addSeparatorSubview];
    [separator distanceBottomToSuperview:0];
    [separator distanceLeftToSuperview:0];
    [separator distanceRightToSuperview:0];
    [separator setHeightConstraint:lineWidth];
    
    [separators addObject:separator];
    
    //right separator
    separator = [self addSeparatorSubview];
    [separator distanceTopToSuperview:lineWidth];
    [separator distanceRightToSuperview:0];
    [separator distanceBottomToSuperview:lineWidth];
    [separator setWidthConstraint:lineWidth];
    
    [separators addObject:separator];
    
    return separators;
}

- (UIView *)addSeparatorAtTopWithLineWidth:(CGFloat)lineWidth
{
    UIView *separator = [self addSeparatorSubview];

    //autolayout constraints
    [separator distanceTopToSuperview:0];
    [separator distanceLeftToSuperview:0];
    [separator distanceRightToSuperview:0];
    [separator setHeightConstraint:lineWidth];
    
    return separator;
}

- (UIView *)addSeparatorAtBottomWithLineWidth:(CGFloat)lineWidth
{
    UIView *separator = [self addSeparatorSubview];

    //autolayout constraints
    [separator distanceBottomToSuperview:0];
    [separator distanceLeftToSuperview:0];
    [separator distanceRightToSuperview:0];
    [separator setHeightConstraint:lineWidth];
    
    return separator;
}

- (UIView *)addSeparatorAtLeftWithLineWidth:(CGFloat)lineWidth
{
    UIView *separator = [self addSeparatorSubview];

    //autolayout constraints
    [separator distanceTopToSuperview:0];
    [separator distanceLeftToSuperview:0];
    [separator distanceBottomToSuperview:0];
    [separator setWidthConstraint:lineWidth];
    
    return separator;
}

- (UIView *)addSeparatorAtRightWithLineWidth:(CGFloat)lineWidth
{
    UIView *separator = [self addSeparatorSubview];

    //autolayout constraints
    [separator distanceTopToSuperview:0];
    [separator distanceRightToSuperview:0];
    [separator distanceBottomToSuperview:0];
    [separator setWidthConstraint:lineWidth];
    
    return separator;
}

@end