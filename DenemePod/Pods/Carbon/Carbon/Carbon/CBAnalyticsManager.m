//
//  CBAnalyticsManager.m
//  Carbon
//
//  Created by Semih Cihan on 19/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBAnalyticsManager.h"
#import "CBAppConfig.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "CBFeedModel.h"
#import "Flurry.h"

static NSString * const kActionCategory = @"ui_action";
static NSString * const kActionComment = @"comment_press";
static NSString * const kActionLiveStream = @"live_stream_press";
static NSString * const kActionShare = @"share_press";
static NSString * const kActionVideoPlay = @"video_play_press : %@";
static NSString * const kActionVideoPlayWithoutCategory = @"video_play_press";
static NSString * const kFeedDetailCategory = @"feed_detail";
static NSString * const kFeedCategory = @"feed";
static NSString * const kFeedDetailAction = @"Feed Category : %@ - News Category : %@ - Detail Type : %@";
static NSString * const kFeedDetailActionWithoutDetail = @"Feed Category : %@ - Detail Type : %@";
static NSString * const kAction = @"action";

static BOOL isGAEnabled;
static BOOL isFlurryEnabled;
static NSString *lastOpenedFeedName;

@implementation CBAnalyticsManager

+ (void)startTracking {
    
    isGAEnabled = [CBAnalyticsManager notNilOrEmptyString:[CBAppConfig sharedConfig].googleAnalyticsTrackingId];
    isFlurryEnabled = [CBAnalyticsManager notNilOrEmptyString:[CBAppConfig sharedConfig].flurryAnalyticsTrackingId];
    
    if (isGAEnabled) {
        
        [GAI sharedInstance].dispatchInterval = 20;
        [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
        [[GAI sharedInstance] trackerWithTrackingId:[CBAppConfig sharedConfig].googleAnalyticsTrackingId];
        [[GAI sharedInstance] defaultTracker].allowIDFACollection = YES;
        
    }
    
    if(isFlurryEnabled) {
        
        [Flurry startSession:[CBAppConfig sharedConfig].flurryAnalyticsTrackingId];
    }
}

+ (void)commentButtonTapped {
    
    if (isGAEnabled) {
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kActionCategory
                                                                                            action:kActionComment
                                                                                             label:nil
                                                                                             value:nil] build]];
    }
    
    if (isFlurryEnabled) {
        
        [Flurry logEvent:kActionCategory withParameters:@{kAction : kActionComment}];
    }
}

+ (void)shareButtonTapped {
    
    if (isGAEnabled) {
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kActionCategory
                                                                                            action:kActionShare
                                                                                             label:nil
                                                                                             value:nil] build]];
    }
    
    if (isFlurryEnabled) {
        
        [Flurry logEvent:kActionCategory withParameters:@{kAction : kActionShare}];
    }
}

+ (void)videoPlayButtonTappedWithVideoCategory:(NSString *)videoCategory {
    
    if (isGAEnabled) {
        
        if ([CBAnalyticsManager notNilOrEmptyString:videoCategory]) {
            
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kActionCategory
                                                                                                action:[NSString stringWithFormat:kActionVideoPlay, videoCategory]
                                                                                                 label:nil
                                                                                                 value:nil] build]];
        } else {
            
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kActionCategory
                                                                                                action:kActionVideoPlayWithoutCategory
                                                                                                 label:nil
                                                                                                 value:nil] build]];
        }
        
    }
    
    if (isFlurryEnabled) {
        
        if ([CBAnalyticsManager notNilOrEmptyString:videoCategory]) {
            
            [Flurry logEvent:kActionCategory withParameters:@{kAction : [NSString stringWithFormat:kActionVideoPlay, videoCategory]}];
        } else {
            
            [Flurry logEvent:kActionCategory withParameters:@{kAction : kActionVideoPlayWithoutCategory}];
        }
        
    }
}

+ (void)liveStreamPlayButtonTapped {
    
    if (isGAEnabled) {
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kActionCategory
                                                                                            action:kActionLiveStream
                                                                                             label:nil
                                                                                             value:nil] build]];
    }
    
    if (isFlurryEnabled) {
        
        [Flurry logEvent:kActionCategory withParameters:@{kAction : kActionLiveStream}];
    }
}

+ (void)feedDetailActionWithCategoryOfTheFeed:(NSString *)feedCategory
                          categoryOfTheDetail:(NSString *)detailCategory
                                     newsType:(NSString *)newsType {
    
    if (feedCategory == nil) {
        feedCategory = lastOpenedFeedName;
    }
    
    if (isGAEnabled) {
        
        if ([CBAnalyticsManager notNilOrEmptyString:detailCategory]) {
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kFeedDetailCategory
                                                                                                action:[NSString stringWithFormat:kFeedDetailAction, feedCategory, detailCategory, newsType]
                                                                                                 label:nil
                                                                                                 value:nil] build]];
        } else {
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kFeedDetailCategory
                                                                                                action:[NSString stringWithFormat:kFeedDetailActionWithoutDetail, feedCategory, newsType]
                                                                                                 label:nil
                                                                                                 value:nil] build]];
            
        }
        
    }
    
    if (isFlurryEnabled) {
        
        if ([CBAnalyticsManager notNilOrEmptyString:detailCategory]) {
            
            [Flurry logEvent:kFeedDetailCategory withParameters:@{kAction : [NSString stringWithFormat:kFeedDetailAction, feedCategory, detailCategory, newsType]}];
        } else {
            
            [Flurry logEvent:kFeedDetailCategory withParameters:@{kAction : [NSString stringWithFormat:kFeedDetailActionWithoutDetail, feedCategory, newsType]}];
        }
        
    }
}

+ (void)feedActionWithCategoryOfTheFeed:(NSString *)feedCategory {
    
    if (isGAEnabled) {
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kFeedCategory
                                                                                            action:feedCategory
                                                                                             label:nil
                                                                                             value:nil] build]];
    }
    
    if (isFlurryEnabled) {
        
        [Flurry logEvent:kFeedCategory withParameters:@{kAction : feedCategory}];
    }
}

+ (void)setLastOpenedFeedCategoryName:(NSString *)feedCategoryName {
    lastOpenedFeedName = feedCategoryName;
}

#pragma mark - private methods

+ (BOOL)notNilOrEmptyString:(NSString *)string {
    return (string && string.length > 0);
}

@end
