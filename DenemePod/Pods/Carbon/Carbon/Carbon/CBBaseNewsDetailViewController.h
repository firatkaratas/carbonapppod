//
//  CBBaseNewsDetailViewController.h
//  Carbon
//
//  Created by Necati Aydın on 11/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBSeamlessViewController.h"
#import "CBDetailContainerViewController.h"
#import "CBBaseNewsDetailLogic.h"
#import "CBFeedModel.h"

@interface CBBaseNewsDetailViewController : CBSeamlessViewController <CBDetailContainedViewControllerProtocol, UIWebViewDelegate, CBDataLoaderDelegate>

@property (nonatomic, assign) BOOL startedWithPush;
@property (weak, nonatomic) IBOutlet UIView *statusBarView;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *spotLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIButton *shareButton;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *webViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomSpace;

@property (nonatomic, strong) CBBaseNewsDetailLogic *logic;

- (void)backButtonTapped:(id)sender;

- (IBAction)shareButtonTapped:(id)sender;

- (void)commentButtonTapped:(id)sender;

- (void)handleImageLoading:(UIImage *)image error:(NSError *)error;

- (void)configure;

- (NSString *)entityNameWithoutType;

- (void)startAds;

- (FeedType)feedType;

/**
 Should be implemented in subclasses to map the model to the UI.
 */
- (void)configureWithModel;

@end

@interface CBBaseNewsDetailViewController(Styling)

- (void)styleByIncrementingFontBy:(CGFloat)fontIncrementAmount;
- (NSString *)styleContent:(NSString *)content withCSSForMaximumWidth:(CGFloat)width fontIncrementAmount:(CGFloat)fontIncrementAmount;

@end