//
//  CBErrorMessageStyleConfig.h
//  Carbon
//
//  Created by Semih Cihan on 06/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

@interface CBErrorMessageStyleConfig : CBBaseConfig

/**
 Text color of the error messages.
 */
@property (strong, nonatomic) UIColor *errorTextColor;

/**
 Font of the error messages.
 */
@property (strong, nonatomic) UIFont *errorTextFont;

@end
