//
//  CBCollectionViewLoadingFooterView.h
//  Carbon
//
//  Created by Semih Cihan on 04/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBCollectionViewLoadingFooterView : UICollectionReusableView

+ (NSString *)reuseIdentifier;
+ (CGSize)sizeForWidth:(CGFloat)width;

@end
