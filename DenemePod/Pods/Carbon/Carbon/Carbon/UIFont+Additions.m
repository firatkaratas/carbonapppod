//
//  UIFont+Additions.m
//  Carbon
//
//  Created by Necati Aydın on 16/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIFont+Additions.h"
#import "CBFontPalette.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIFontAdditions)

@implementation UIFont(Additions)

+ (UIFont *)fontWithCommaSeparatedString:(NSString *)fontString
{
    NSArray *arr = [fontString componentsSeparatedByString:@","];
    
    if(arr.count != 2)
    {
        return nil;
    }
    
    NSString *fontNameString = arr[0];
    NSString *fontName = [fontNameString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *fontSizeString = arr[1];
    CGFloat fontSize = [[fontSizeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] floatValue];
    
    return [UIFont fontWithName:fontName size:fontSize];
}

+ (UIFont *)fontByReadingFromPaletteWithString:(NSString *)paletteKey
{
    NSArray *arr = [paletteKey componentsSeparatedByString:@","];
    
    NSAssert(arr.count == 2, @"fontString parameter should have 2 elements separated by commas.");
    
    NSString *fontNameKey = arr[0];
    fontNameKey = [fontNameKey stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *fontName = [CBFontPalette fontNameWithKey:fontNameKey];
    
    NSString *fontSizeString = arr[1];
    CGFloat fontSize = [[fontSizeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] floatValue];
    
    return [UIFont fontWithName:fontName size:fontSize];
}

- (UIFont *)fontByIncrementingSize:(CGFloat)fontSizeIncrementAmount
{
    return [self fontWithSize:self.pointSize + fontSizeIncrementAmount];
}

@end
