//
//  CBVideoDetailModel.m
//  Carbon
//
//  Created by Semih Cihan on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBVideoDetailModel.h"
#import "CBVideoDetailModelConfig.h"
#import "CBSuggestedVideoModel.h"
#import "CBCategoryStore.h"
#import "CBNetworkConfig.h"
#import "CBVideoDetailModelProxy.h"

@interface CBVideoDetailModel ()

@end

@implementation CBVideoDetailModel

+ (CBBaseModelConfig *)config
{
    return [CBVideoDetailModelConfig sharedConfig];
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"detailId" : [CBVideoDetailModelConfig sharedConfig].detailId,
             @"category" : [CBVideoDetailModelConfig sharedConfig].category,
             @"date" : [CBVideoDetailModelConfig sharedConfig].date,
             @"title" : [CBVideoDetailModelConfig sharedConfig].title,
             @"spot" : [CBVideoDetailModelConfig sharedConfig].spot,
             @"imageUrl" : [CBVideoDetailModelConfig sharedConfig].imageUrl,
             @"url" : [CBVideoDetailModelConfig sharedConfig].url,
             @"videoUrl" : [CBVideoDetailModelConfig sharedConfig].videoUrl,
             @"content" : [CBVideoDetailModelConfig sharedConfig].content,
             @"duration" : [CBVideoDetailModelConfig sharedConfig].duration,
             @"viewCount" : [CBVideoDetailModelConfig sharedConfig].viewCount,
             @"suggestedVideos" : [CBVideoDetailModelConfig sharedConfig].suggestedVideos,
             };
}

+ (NSValueTransformer *)contentJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id content) {
        
        content = [[CBVideoDetailModelProxy sharedInstance] contentJSONTransformer:content];
        
        NSArray *htmlPatternDropArray = [CBNetworkConfig sharedConfig].htmlLinkDropPatternArray;
        for (NSString *pattern in htmlPatternDropArray) {
            content = [CBBaseModel removeLinksInHtmlContent:content linkPattern:pattern];
        }
        return content;
        
    } reverseBlock:^id (id obj) {
        return obj;
    }];
}

+ (NSValueTransformer *)detailIdJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBVideoDetailModelProxy sharedInstance] detailIdJSONTransformer:object];
        return [CBBaseModel stringOrNumberToString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)categoryJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBVideoDetailModelProxy sharedInstance] categoryJSONTransformer:object];
        
        if (object) {
            NSString *categoryId = [self stringOrNumberToString:object];
            CBCategoryModel *cat = [[CBCategoryStore sharedStore] getCategoryWithId:categoryId];
            return cat;
        } else {
            return nil;
        }
        
    } reverseBlock:^id (CBCategoryModel *category) {
        return category.categoryId;
    }];
}

+ (NSValueTransformer *)titleJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBVideoDetailModelProxy sharedInstance] titleJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)spotJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBVideoDetailModelProxy sharedInstance] spotJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)durationJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBVideoDetailModelProxy sharedInstance] durationJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)viewCountJSONTransformer {

    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBVideoDetailModelProxy sharedInstance] viewCountJSONTransformer:object];
        object = [CBBaseModel stringOrNumberToNumber:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)urlJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBVideoDetailModelProxy sharedInstance] urlJSONTransformer:object];
        return [NSURL URLWithString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)imageUrlJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBVideoDetailModelProxy sharedInstance] imageUrlJSONTransformer:object];
        return [NSURL URLWithString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)videoUrlJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBVideoDetailModelProxy sharedInstance] videoUrlJSONTransformer:object];
        return [NSURL URLWithString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)suggestedVideosJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        NSArray *rawSuggestedVideosArray = [[CBVideoDetailModelProxy sharedInstance] suggestedVideosJSONTransformer:object];
        
        if (rawSuggestedVideosArray) {
            NSError *error;
            NSArray *modelArray = [MTLJSONAdapter modelsOfClass:CBSuggestedVideoModel.class fromJSONArray:rawSuggestedVideosArray error:&error];
            if (error) {
                DebugLog(@"Error in suggestedVideosJSONTransformer in CBVideoDetailModel, error description: %@", error.localizedDescription);
                return nil;
            }
            return modelArray;
        } else {
            return nil;
        }
        
    } reverseBlock:^id (CBSuggestedVideoModel *suggestedVideos) {
        return nil;
    }];
}

+ (NSValueTransformer *)dateJSONTransformer
{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id(id object) {
        
        object = [[CBVideoDetailModelProxy sharedInstance] dateJSONTransformer:object];
        return object;
    } reverseBlock:^id(id obj) {
        return obj;
    }];
    
}

@end
