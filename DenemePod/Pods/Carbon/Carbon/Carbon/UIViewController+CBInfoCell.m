//
//  UIViewController+CBInfoLabelCell.m
//  Carbon
//
//  Created by Semih Cihan on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIViewController+CBInfoCell.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIViewControllerCBInfoCell)

@implementation UIViewController (CBInfoCell)

- (void)configureCell:(CBInfoCell *)cell text:(NSString *)text {
    
    cell.webView.scrollView.scrollEnabled = NO;
    cell.webView.opaque = NO;
    
    NSString *styledText = [NSString stringWithFormat:@"<style>\
    body{font-family:\"%@\";\
    font-size:%f;\
    color:rgb(%@)}\
    a{font-family:\"%@\";\
    font-size:%f;}\
    </style>\
    <html>\
    <body>\
    %@\
    </body>\
    </html>",
    cell.font.fontName,
    cell.font.pointSize,
    cell.textColor,
    cell.font.fontName,
    cell.font.pointSize,
                            text];
    
    [cell.webView loadHTMLString:styledText baseURL:nil];
}

@end
