//
//  CBGalleryPhotoModel.m
//  Carbon
//
//  Created by Semih Cihan on 27/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGalleryPhotoModel.h"
#import "CBGalleryPhotoModelConfig.h"
#import "CBNetworkConfig.h"
#import "CBGalleryPhotoModelProxy.h"

@implementation CBGalleryPhotoModel

+ (CBBaseModelConfig *)config
{
    return [CBGalleryPhotoModelConfig sharedConfig];
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"photoId" : [CBGalleryPhotoModelConfig sharedConfig].photoId,
             @"photoDescription" : [CBGalleryPhotoModelConfig sharedConfig].photoDescription,
             @"imageUrl" : [CBGalleryPhotoModelConfig sharedConfig].imageUrl
             };
}

+ (NSValueTransformer *)photoDescriptionJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        NSString *content = [[CBGalleryPhotoModelProxy sharedInstance] photoDescriptionJSONTransformer:object];
        
        for (NSString *pattern in [CBNetworkConfig sharedConfig].htmlLinkDropPatternArray) {
            content = [CBBaseModel removeLinksInHtmlContent:content linkPattern:pattern];
        }
        return content;
        
    } reverseBlock:^id (id obj) {
        return obj;
    }];
}

+ (NSValueTransformer *)imageUrlJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGalleryPhotoModelProxy sharedInstance] imageUrlJSONTransformer:object];
        return [NSURL URLWithString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)photoIdJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBGalleryPhotoModelProxy sharedInstance] photoIdJSONTransformer:object];
        return [CBBaseModel stringOrNumberToString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

- (BOOL)hasDescription;
{
    return self.photoDescription && ![self.photoDescription isEqualToString:@""];
}

@end
