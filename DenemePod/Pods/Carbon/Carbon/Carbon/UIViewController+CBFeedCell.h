//
//  UIViewController+CBFeedCell.h
//  Carbon
//
//  Created by Necati Aydın on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CBFeedCell, CBFeedModel;

@interface UIViewController(CBFeedCell)

- (void)configureFeedCell:(CBFeedCell *)cell withFeedModel:(CBFeedModel *)feed;

@end
