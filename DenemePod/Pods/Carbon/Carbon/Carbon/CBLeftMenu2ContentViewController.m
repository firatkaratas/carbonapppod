//
//  CBLeftMenu2ContentViewController.m
//  Carbon
//
//  Created by Semih Cihan on 18/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu2ContentViewController.h"
#import "CBNavigationBarStyler.h"
#import "CBLeftMenu2TableViewCell.h"
#import "CBLeftMenu2StyleConfig.h"
#import "CBCategoryModel.h"
#import "UIViewController+CBLeftMenu2TableViewCell.h"
#import "UITableView+Additions.h"

#pragma mark - Styling

@interface CBLeftMenu2ContentViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopSpace;
@property (weak, nonatomic) IBOutlet UIView *topSeparatorView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorHeight;

@end

@interface CBLeftMenu2ContentViewController(Styling)

- (void)style;
- (void)styleCell:(CBLeftMenu2TableViewCell *)cell;
- (void)styleSelectedCell:(CBLeftMenu2TableViewCell *)cell;

@end

@implementation CBLeftMenu2ContentViewController(Styling)

- (void)style
{
    self.view.backgroundColor = [UIColor clearColor];
    [self.backButton setTitleColor:[CBLeftMenu2StyleConfig sharedConfig].backTitleColor forState:UIControlStateNormal];
    [self.backButton.titleLabel setFont:[CBLeftMenu2StyleConfig sharedConfig].backFont];
    self.topSeparatorView.backgroundColor = [CBLeftMenu2StyleConfig sharedConfig].separatorColor;
}

- (void)styleCell:(CBLeftMenu2TableViewCell *)cell
{
    CBLeftMenu2StyleConfig *config = [CBLeftMenu2StyleConfig sharedConfig];
    cell.separator.backgroundColor = config.separatorColor;
    
    cell.backgroundColor = [UIColor clearColor];
    [cell.titleLabel setTextColor:config.categoryLabelColor];
    [cell.titleLabel setFont:config.categoryLabelFont];
    cell.arrowImageView.tintColor = config.arrowColor;
}

- (void)styleSelectedCell:(CBLeftMenu2TableViewCell *)cell
{
    CBLeftMenu2StyleConfig *config = [CBLeftMenu2StyleConfig sharedConfig];
    cell.separator.backgroundColor = config.separatorColor;
    
    cell.backgroundColor = config.selectedCellBackgroundColor;
    [cell.titleLabel setTextColor:config.categoryLabelSelectedColor];
    [cell.titleLabel setFont:config.categoryLabelSelectedFont];
    cell.arrowImageView.tintColor = config.arrowColor;
}

@end

@implementation CBLeftMenu2ContentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self style];
    
    [self.tableView registerClassForDefaultReuseIdentifier:[CBLeftMenu2TableViewCell class]];
    self.separatorHeight.constant = 0.5f;
}

- (void)dealloc
{
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
}

- (void)selectFirstRow
{
    if(self.tableView && self.rootCategory.subcategories.count)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionTop];
    }
}

- (void)hideTopBar
{
    self.tableViewTopSpace.constant = 10;
    self.backButton.hidden = YES;
    self.topSeparatorView.hidden = YES;
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.rootCategory.subcategories.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CBLeftMenu2TableViewCell cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBLeftMenu2TableViewCell *cell = (CBLeftMenu2TableViewCell *)[tableView dequeueReusableCellWithIdentifier:[CBLeftMenu2TableViewCell reuseIdentifier] forIndexPath:indexPath];
    
    cell.separator.hidden = indexPath.row == 0;
    
    CBCategoryModel *model = self.rootCategory.subcategories[indexPath.row];
    [self configureCell:cell withModel:model];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(cell.selected)
    {
        [self styleSelectedCell:cell];
    }
    else
    {
        [self styleCell:cell];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBLeftMenu2TableViewCell *cell = (CBLeftMenu2TableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [self styleCell:cell];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBLeftMenu2TableViewCell *cell = (CBLeftMenu2TableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [self styleSelectedCell:cell];
    [self.delegate leftMenuContent:self didSelectCategory:self.rootCategory.subcategories[indexPath.row]];
}

#pragma mark - Actions

- (IBAction)backButtonTapped:(id)sender
{
    [self.delegate leftMenuContentTappedBackButton:self];
}



@end
