//
//  CBAuthorDetailViewController.h
//  Carbon
//
//  Created by Necati Aydın on 06/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNewsDetailViewController.h"
#import "CBAuthorDetailLogic.h"

typedef NS_ENUM(NSInteger, CBAuthorDetailType)
{
    CBAuthorDetailType2 = 2,
};

@interface CBAuthorDetailViewController : CBNewsDetailViewController

@property (nonatomic, strong) CBAuthorDetailLogic *logic;
@property (nonatomic, assign) CBAuthorDetailType authorDetailType;

@end
