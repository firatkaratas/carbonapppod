//
//  CBBaseNewsDetailViewController.m
//  Carbon
//
//  Created by Necati Aydın on 11/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseNewsDetailViewController.h"
#import "CBNavigationBarStyler.h"
#import "CBViewControllerBuilder.h"
#import "CBNavigationBarStyleConfig.h"
#import "UIView+Separator.h"
#import "UIView+Loading.h"
#import "CBBaseModel.h"
#import "NSUserDefaults+Additions.h"
#import "CBNewsDetailModel.h"
#import "UIImageView+WebCache.h"
#import "UIView+LayoutConstraints.h"
#import "CBAnalyticsManager.h"
#import "CBFeedModel.h"
#import "UIDevice+Additions.h"

static const CGFloat kPopoverHorizontalSpace = 37;

@interface CBBaseNewsDetailViewController () <CBLoadingActionProtocol>

@end

@implementation CBBaseNewsDetailViewController
@dynamic logic;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.statusBarView.backgroundColor = [CBNavigationBarStyleConfig sharedConfig].backgroundColor;
    
    if(self.startedWithPush)
    {
        [CBNavigationBarStyler styleWithTitleLogo:self.navigationItem];
        [CBNavigationBarStyler styleNavigationItemWithBackIcon:self.navigationItem target:self action:@selector(backButtonTapped:)];
        self.navigationController.navigationBarHidden = [self shouldHideNavigationBar];
    }
    
    self.webView.delegate = self;
    self.webView.scrollView.scrollEnabled = NO;
    
    [self.logic loadData];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //BUGFIX: when the view controller opened from split by swiping, the webview cannot be layed out correctly, so web view should be layed out again.
    if(self.logic.newsDetail)
    {
        [self configure];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.interstitialAdManager.delegate = nil;
}

- (void)backButtonTapped:(id)sender
{
    if(self.startedWithPush)
    {
        [UIApplication sharedApplication].delegate.window.rootViewController = [CBViewControllerBuilder rootViewControllerForPush];
        return;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldHideNavigationBar
{
    return NO;
}

- (void)configure
{
    [self configureWithModel];
    [self styleByIncrementingFontBy:[NSUserDefaults getFontIncrementAmount]];
}

- (void)configureWithModel
{
    CBNewsDetailModel *detailModel = self.logic.newsDetail;
    self.titleLabel.text = detailModel.title;
    self.spotLabel.text = detailModel.spot;
    self.dateLabel.text = detailModel.date;
    
    [self.webView loadHTMLString:[self styleContent:detailModel.content
                             withCSSForMaximumWidth:self.view.frame.size.width
                                fontIncrementAmount:[NSUserDefaults getFontIncrementAmount]]
                         baseURL:[NSURL URLWithString:@"http://"]];
    
    __weak typeof(self) weakSelf = self;
    [self.imageView sd_setImageWithURL:detailModel.imageUrl
                             completed:^(UIImage *image,
                                         NSError *error,
                                         SDImageCacheType cacheType,
                                         NSURL *imageURL)
     {
         [weakSelf handleImageLoading:image error:error];
     }];
}

- (void)handleImageLoading:(UIImage *)image error:(NSError *)error
{
    
}

- (void)dealloc
{
    _webView.delegate = nil;
    _scrollView.delegate = nil;
}

#pragma mark - Logic Delegate

- (void)dataLoading {
    [self.view showLoadingView];
}

- (void)dataLoaded
{
    [self.view dismissLoadingView];
    
    [self configure];
    
    if (!self.interstitialAdManager || !self.mmaView || !self.mreView) {
        [self startAds];
    }
    
    //analytics
    [CBAnalyticsManager feedDetailActionWithCategoryOfTheFeed:(self.startedWithPush) ? @"From Push" : nil
                                          categoryOfTheDetail:self.logic.newsDetail.category.name
                                                     newsType:[CBFeedModel feedTypeStringOfFeedType:[self feedType]]];
}

- (FeedType)feedType
{
    return FeedTypeArticle;
}

- (void)dataLoadedWithError:(NSString *)errorMessage
{
    [self.view showErrorMessage:errorMessage actionTarget:self];
}

#pragma mark - CBLoadingActionProtocol

- (void)errorViewTapped:(UIGestureRecognizer *)recognizer
{
    [self.view dismissErrorView];
    [self.logic loadData];
}

#pragma mark - Web view delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    CGSize calculatedSize = [webView sizeThatFits:CGSizeZero];
    self.webViewHeight.constant = calculatedSize.height;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if(navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        UIViewController *vc = [CBViewControllerBuilder webViewWithUrlRequest:request title:self.logic.newsDetail.title];
        [self presentViewController:vc animated:YES completion:nil];
        return NO;
    }
    else
    {
        return YES;
    }
}

#pragma mark - Action

- (void)commentButtonTapped:(id)sender
{
    [CBAnalyticsManager commentButtonTapped];
    
    CBCommentLogic *logic = [self.logic getCommentLogic];
    UIViewController *vc = [CBViewControllerBuilder commentViewControllerWithLogic:logic];
    [self.parentViewController.navigationController pushViewController:vc animated:YES];
}

- (void)shareButtonTapped:(id)sender
{
    [CBAnalyticsManager shareButtonTapped];
    
    NSMutableArray *activityItems = [NSMutableArray new];
    if(self.logic.newsDetail.title)
    {
        [activityItems addObject:self.logic.newsDetail.title];
    }
    if(self.logic.newsDetail.url)
    {
        [activityItems addObject:self.logic.newsDetail.url];
    }
    
    if(activityItems.count)
    {
        UIActivityViewController *controller =
        [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        controller.excludedActivityTypes = @[UIActivityTypeAddToReadingList, UIActivityTypeCopyToPasteboard];
        
        if(IPAD)
        {
            if([UIDevice iOS8AndHigher])
            {
                controller.popoverPresentationController.sourceView = self.view;
                controller.popoverPresentationController.sourceRect = CGRectMake(CGRectGetWidth(self.view.frame) - kPopoverHorizontalSpace, 0,  1, 1);
                controller.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
            }
            else
            {
                return;
            }
        }
        
        
        if(self.logic.newsDetail.title)
        {
            [controller setValue:self.logic.newsDetail.title forKey:@"subject"];
        }
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - Ads

- (NSString *)entityNameWithoutType
{
    return [NSString stringWithFormat:@"%@-Detail",self.logic.newsDetail.category.name];
}

- (void)startAds
{
    [self startInterstitialAdWithEntityName:[self entityNameWithoutType]];
    [self startMMAAdWithEntityName:[self entityNameWithoutType]];
    [self startMREAdWithEntityName:[self entityNameWithoutType]];
}

- (void)adViewDidLoad:(SLAdView *)adView
{
    [super adViewDidLoad:adView];
    
    if(adView == self.mmaView)
    {
        self.scrollViewBottomSpace.constant = SLAdSizeMMA.height;
        [self.view addSubview:adView];
        [adView distanceBottomToSuperview:0];
        [adView centerXInSuperview];
    }
}

- (void)adViewDidFailToLoad:(SLAdView *)adView
{
    [super adViewDidFailToLoad:adView];
    
    if(adView == self.mmaView)
    {
        self.scrollViewBottomSpace.constant = 0;
    }
}

@end

@implementation CBBaseNewsDetailViewController(Styling)

- (void)styleByIncrementingFontBy:(CGFloat)fontIncrementAmount
{
    //Should be implemented in subclasses.
}

- (NSString *)styleContent:(NSString *)content withCSSForMaximumWidth:(CGFloat)width fontIncrementAmount:(CGFloat)fontIncrementAmount
{
    //Should be implemented in subclasses.
    return nil;
}

@end




