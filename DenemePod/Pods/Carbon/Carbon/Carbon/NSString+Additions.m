//
//  NSString+Additions.m
//  Carbon
//
//  Created by Necati Aydın on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "NSString+Additions.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(NSStringAdditions)

@implementation NSString (Additions)

- (NSString *)removeFrameworkPrefixIfExists
{
    if([[self substringToIndex:2] isEqualToString:@"CB"])
    {
        return [self substringFromIndex:2];
    }
    
    return self;
}

- (CGFloat)calculateBoundingRectHeightWithinLabelWithConstrainedWidth:(CGFloat)width font:(UIFont *)font {
    CGFloat height = [self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{
                                                  NSFontAttributeName: font
                                                  }
                                        context:nil].size.height;
    return ceilf(height);
}

- (NSString *)groupedStringForNumber
{
    NSInteger integer = [self integerValue];
    NSNumber *number = @(integer);
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.usesGroupingSeparator = YES;
    formatter.groupingSize = 3;
    formatter.groupingSeparator = @",";
    return [formatter stringFromNumber:number];
}

- (CGFloat)calculateBoundingRectHeightWithinTextViewWithConstrainedWidth:(CGFloat)width font:(UIFont *)font {
    
    UITextView *textView = [[UITextView alloc] init];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self];
    [attributedText addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, self.length)];
    [textView setAttributedText:attributedText];
    CGSize size = [textView sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
    return ceilf(size.height);
}

@end
