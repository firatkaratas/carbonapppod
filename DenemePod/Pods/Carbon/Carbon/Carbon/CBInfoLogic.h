//
//  CBInfoLogic.h
//  Carbon
//
//  Created by Semih Cihan on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"

typedef NS_ENUM(NSInteger, CBInfoSocialMedia) {
    CBInfoSocialMediaFacebook,
    CBInfoSocialMediaTwitter,
    CBInfoSocialMediaGooglePlus,
    CBInfoSocialMediaYouTube,
    
};

@interface CBInfoLogic : CBBaseLogic

/**
 If this is not nil, then we show a webview for info page, otherwise we use other properties to make a static info page.
 */
@property (strong, nonatomic) NSURL *htmlInfoUrl;

/**
 If not nil and if no htmlInfoUrl then we show this url at the bottom of the info page. Intended to show a map.
 */
@property (strong, nonatomic) NSURL *mapImageUrl;

/**
 Section headers.
 */
@property (strong, nonatomic) NSArray *titles;

/**
 Cell label texts.
 */
@property (strong, nonatomic) NSArray *informations;

/**
 Title for the given section.
 @param section Section for the title.
 @return Title for the given section.
 */
- (NSString *)titleForSection:(NSInteger)section;

/**
 Information for the given section.
 @param section Section for the title.
 @return Information for the given section.
 */
- (NSString *)informationForSection:(NSInteger)section;

/**
 Checks if there are social media urls and returns no if there is none.
 */
- (BOOL)socialMediaEnabled;

/**
 Returns the url for the given social media type.
 */
- (NSURL *)urlForSocialMedia:(CBInfoSocialMedia)socialMediaType;

/**
 Returns all available social media types.
 */
- (NSMutableArray *)socialMediaTypes;

@end
