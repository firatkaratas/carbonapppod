//
//  CBFontPalette.m
//  Carbon
//
//  Created by Necati Aydın on 16/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFontPalette.h"
#import "NSString+Additions.h"

@implementation CBFontPalette

+ (NSString *)fontNameWithKey:(NSString *)key
{
    CBFontPalette *palette = [self sharedConfig];
    return [palette.plistDictionary objectForKey:key][0];
}

@end