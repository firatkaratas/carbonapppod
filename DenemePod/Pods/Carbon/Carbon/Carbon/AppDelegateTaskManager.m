//
//  AppDelegateTaskManager.m
//  Carbon
//
//  Created by Semih Cihan on 09/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "AppDelegateTaskManager.h"
#import "CBFontManager.h"
#import "CBNavigationBarStyler.h"
#import "CBReachabilityManager.h"
#import "CBCrashManager.h"
#import <Seamless/Seamless.h>
#import "CBAppConfig.h"
#import "CBAnalyticsManager.h"

@implementation AppDelegateTaskManager

+ (void)applicationDidFinishLaunchingTasks {
    
    //style
    [CBFontManager loadFonts];
    [CBNavigationBarStyler styleNavigationBarColors];
    [CBNavigationBarStyler styleNavigationBarTitle];
    
    //reachability
    [CBReachabilityManager startReachability];
    
    //crashlytics
    if([CBCrashManager respondsToSelector:@selector(startCrashlytics)])
    {
        [CBCrashManager startCrashlytics];
    }

    
    //seamless
    NSString *seamlessAppToken = IPAD ? [CBAppConfig sharedConfig].seamlessAppTokenIPad : [CBAppConfig sharedConfig].seamlessAppToken;
    if(seamlessAppToken && ![seamlessAppToken isEqualToString:@""])
    {
        [[SLManager sharedManager] setAppToken:seamlessAppToken];
        [[SLManager sharedManager] setLocationEnabled:[CBAppConfig sharedConfig].seamlessLocationEnabled];
    }
    
    //analytics
    [CBAnalyticsManager startTracking];
}

@end