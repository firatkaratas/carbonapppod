//
//  CBJSONAdapter.m
//  Carbon
//
//  Created by Necati Aydın on 28/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBJSONAdapter.h"
#import "CBBaseModelConfig.h"
#import "CBBaseModel.h"

@interface CBJSONAdapter ()

@property (nonatomic, strong) CBBaseModelConfig *config;

@end

@implementation CBJSONAdapter

- (id)initWithJSONDictionary:(NSDictionary *)JSONDictionary modelClass:(Class)modelClass error:(NSError *__autoreleasing *)error config:(CBBaseModelConfig *)config
{
    _config = config;
    
    CBJSONAdapter *adapter = [super initWithJSONDictionary:JSONDictionary modelClass:modelClass error:error];
    
    if([adapter.model isKindOfClass:[CBBaseModel class]])
    {
        CBBaseModel *model = (CBBaseModel *)adapter.model;
        NSDictionary *fields = [config additionalJSONKeyPathsByPropertyKey];
        if(fields)
        {
            [fields enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
             {
                 [model setAdditionalValue:JSONDictionary[obj] forKey:key];
             }];
        }
    }
    
    return adapter;
}

- (NSString *)JSONKeyPathForPropertyKey:(NSString *)key
{
    return self.config.plistDictionary[key];
}

@end
