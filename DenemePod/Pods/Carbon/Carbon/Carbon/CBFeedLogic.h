//
//  CBHomeLogic.h
//  Carbon
//
//  Created by Semih Cihan on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"

@class CBCategoryModel;


#pragma mark - CBDataLoaderWithPagingAndRefreshProtocol

@protocol CBDataLoaderWithPagingAndRefreshProtocol <CBDataLoaderProtocol>

/**
 Call to initiate refreshing.
 */
- (void)refreshData;

/**
 Call to initiate data loading of the next page.
 @return Yes if there are more data to load, No otherwise.
 */
- (BOOL)loadNextPage;

/**
 @description NO if there is no more data to load, YES otherwise.
 */
@property (assign, nonatomic, readonly) BOOL hasMorePageToLoad;

/**
 @description YES if loading in progress, NO otherwise.
 */
@property (assign, nonatomic, readonly) BOOL isLoading;

@end


#pragma mark - CBDataLoaderWithPagingAndRefreshDelegate

@protocol CBDataLoaderWithPagingAndRefreshDelegate <CBDataLoaderDelegate>

/**
 Called to inform the delegate that the data is loaded for the given range.
 @param range Range of the loaded data.
 */
- (void)dataLoadedForRange:(NSRange)range;

/**
 Called when the data loading is triggered by paging. 
 */
- (void)dataLoadingWithPaging;

@end

#pragma mark - CBLiveStreamDelegate

@protocol CBLiveStreamDelegate

/**
 Called to inform the delegate that the live stream url loaded.
 @param liveStreamUrl Live stream url that will be played.
 */
- (void)liveStreamUrlLoaded:(NSURL *)liveStreamUrl;

/**
 Called to inform the delegate that there is an error occured with loading the live stream url.
 */
- (void)liveStreamLoadedWithError:(NSString *)errorMessage;

@end


#pragma mark - CBFeedLogic

@interface CBFeedLogic : CBBaseLogic <CBDataLoaderWithPagingAndRefreshProtocol>

/**
 Category of the feed models.
 */
@property (strong, nonatomic) CBCategoryModel *category;

/**
 @description Data for FeedViewController instances.
 */
@property (strong, nonatomic) NSMutableArray *data;

/**
 @description Delegate of CBFeedLogic.
 */
@property (weak, nonatomic) id<CBDataLoaderWithPagingAndRefreshDelegate, CBLiveStreamDelegate> delegate;

/**
 @description Shows the current page index.
 */
@property (assign, nonatomic) NSInteger currentPage;

/**
 Select the feed at given index.
 */
- (CBBaseLogic *)getLogicWithIndex:(NSInteger)index;

/**
 Finds the feed index in the given feeds array.
 @return Index of the feed id. If not found, returns -1.
 */
- (NSInteger)findFeedId:(NSString *)feedId inFeedModels:(NSArray *)feeds;

/**
 Called to initiate fetching of live stream url.
 */
- (void)getLiveStreamUrl;

/**
 Network fetch request for logic to be overriden by subclasses. It should not be used directly.
 */
- (void)makeFetchRequestWithId:(NSString *)categoryId page:(NSInteger)page successBlock:(void (^)(NSArray *feeds))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

@end
