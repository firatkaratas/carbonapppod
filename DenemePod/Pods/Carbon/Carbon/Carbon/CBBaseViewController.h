//
//  CarbonViewController.h
//  Carbon
//
//  Created by Necati Aydın on 29/12/14.
//  Copyright (c) 2014 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBBaseLogic.h"
#import "UIViewController+Utility.h"

/**
 @description This a base class for UIViewControllers used in the framework.
 */
@interface CBBaseViewController : UIViewController

/**
 Base logic of the view controller.
 */
@property (nonatomic, strong) CBBaseLogic *logic;

/**
 Returns the name of the application using the app config.
 */
- (NSString *)appName;

@end

