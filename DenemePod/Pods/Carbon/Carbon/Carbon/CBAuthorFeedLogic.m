//
//  CBAuthorFeedLogic.m
//  Carbon
//
//  Created by Necati Aydın on 11/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBAuthorFeedLogic.h"
#import "CBNetworkManager.h"

@implementation CBAuthorFeedLogic

- (void)makeFetchRequestWithId:(NSString *)categoryId page:(NSInteger)page successBlock:(void (^)(NSArray *))successBlock failureBlock:(void (^)(NSError *))failureBlock
{
    [[CBNetworkManager sharedInstance] getAuthorFeedWithAuthorId:self.authorId
                                                      pageNumber:[@(page) stringValue]
                                                        pageSize:nil
                                                    successBlock:successBlock
                                                    failureBlock:failureBlock];
}

@end
