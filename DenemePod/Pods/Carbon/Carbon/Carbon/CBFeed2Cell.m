//
//  CBFeed2Cell.m
//  Carbon
//
//  Created by Necati Aydın on 30/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeed2Cell.h"

@implementation CBFeed2Cell

+ (CGFloat)cellRatio
{
    return 193.f/300.f;
}

@end
