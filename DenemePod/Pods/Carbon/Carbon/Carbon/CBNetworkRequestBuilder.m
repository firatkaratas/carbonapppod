//
//  CBNetworkRequestBuilder.m
//  Carbon
//
//  Created by Necati Aydın on 26/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNetworkRequestBuilder.h"
#import "CBNetworkConfig.h"

static CBNetworkConfig *config;

@implementation CBNetworkRequestBuilder

+ (void)setConfig:(CBNetworkConfig *)config
{
    config = config;
}

+ (CBNetworkConfig *)config
{
    if(!config)
    {
        return [CBNetworkConfig sharedConfig];
    }
    
    return config;
}

+ (CBNetworkRequest *)newsDetailRequestWithId:(NSString *)detailId
{
    CBNetworkRequest *request = [CBNetworkRequest new];
    request.urlParameters = @[detailId];
    request.formatUrl = self.config.newsDetailUrl;
    request.requestType = RequestTypeGet;
    
    return request;
}

+ (CBNetworkRequest *)videoDetailRequestWithId:(NSString *)detailId
{
    CBNetworkRequest *request = [CBNetworkRequest new];
    request.urlParameters = @[detailId];
    request.formatUrl = self.config.videoDetailUrl;
    request.requestType = RequestTypeGet;
    
    return request;
}

+ (CBNetworkRequest *)authorDetailRequestWithId:(NSString *)detailId
{
    CBNetworkRequest *request = [CBNetworkRequest new];
    request.urlParameters = @[detailId];
    request.formatUrl = self.config.authorDetailUrl;
    request.requestType = RequestTypeGet;
    
    return request;
}

+ (CBNetworkRequest *)commentsRequestWithId:(NSString *)detailId
{
    CBNetworkRequest *request = [CBNetworkRequest new];
    request.urlParameters = @[detailId];
    request.formatUrl = self.config.commentsUrl;
    request.requestType = RequestTypeGet;
    
    return request;
}

+ (CBNetworkRequest *)galleryDetailRequestWithId:(NSString *)detailId
{
    CBNetworkRequest *request = [CBNetworkRequest new];
    request.urlParameters = @[detailId];
    request.formatUrl = self.config.galleryDetailUrl;
    request.requestType = RequestTypeGet;
    
    return request;
}

+ (CBNetworkRequest *)cateogiresRequest
{
    CBNetworkRequest *request = [CBNetworkRequest new];
    request.urlParameters = nil;
    request.formatUrl = self.config.categoriesUrl;
    request.requestType = RequestTypeGet;
    
    return request;
}

+ (CBNetworkRequest *)categoryFeedRequestWithId:(NSString *)categoryId
                                     pageNumber:(NSString *)pageNumber
                                       pageSize:(NSString *)pageSize {
    
    CBNetworkRequest *request = [CBNetworkRequest new];
    
    if (pageNumber && pageSize) {
        
        request.urlParameters = @[categoryId, pageNumber, pageSize];
    } else if (pageNumber) {
        
        request.urlParameters = @[categoryId, pageNumber];
    } else {
        
        request.urlParameters = @[categoryId];
    }
    
    request.formatUrl = self.config.categoryFeedUrl;
    request.requestType = RequestTypeGet;
    
    return request;
}

+ (CBNetworkRequest *)authorFeedRequestWithId:(NSString *)authorId
                                   pageNumber:(NSString *)pageNumber
                                     pageSize:(NSString *)pageSize {
    
    CBNetworkRequest *request = [CBNetworkRequest new];

    if (pageNumber && pageSize) {
        
        request.urlParameters = @[authorId, pageNumber, pageSize];
    } else if (pageNumber) {
        
        request.urlParameters = @[authorId, pageNumber];
    } else {
        
        request.urlParameters = @[authorId];
    }
    
    request.formatUrl = self.config.authorFeedUrl;
    request.requestType = RequestTypeGet;
    
    return request;
}

+ (CBNetworkRequest *)liveStreamRequest
{
    CBNetworkRequest *request = [CBNetworkRequest new];
    request.urlParameters = nil;
    request.formatUrl = self.config.liveStreamUrl;
    request.requestType = RequestTypeGet;
    
    return request;
}

+ (CBNetworkRequest *)guideRequest
{
    CBNetworkRequest *request = [CBNetworkRequest new];
    request.urlParameters = nil;
    request.formatUrl = self.config.guideCategoryFeedUrl;
    request.requestType = RequestTypeGet;
    
    return request;
}


@end
