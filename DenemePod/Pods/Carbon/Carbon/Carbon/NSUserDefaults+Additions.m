//
//  NSUserDefaults+Additions.m
//  Carbon
//
//  Created by Semih Cihan on 11/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "NSUserDefaults+Additions.h"
#import "NimbusKitBasics.h"

static NSString * const kFontIncrementAmountKey = @"fontIncrementAmount";
static NSString * const pushNotificationEnabledKey = @"pushNotificationEnabled";

NI_FIX_CATEGORY_BUG(NSUserDefaultsAdditions)

@implementation NSUserDefaults (Additions)

+ (void)setFontIncrementAmount:(NSInteger)fontIncrementAmount {
    [[NSUserDefaults standardUserDefaults] setObject:@(fontIncrementAmount) forKey:kFontIncrementAmountKey];
}

+ (NSInteger)getFontIncrementAmount {
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kFontIncrementAmountKey] integerValue];
}

+ (void)setPushNotificationEnabled:(BOOL)enabled
{
    [[NSUserDefaults standardUserDefaults] setObject:@(enabled) forKey:pushNotificationEnabledKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSNumber *)getPushNotificationEnabled
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:pushNotificationEnabledKey];
}

@end
