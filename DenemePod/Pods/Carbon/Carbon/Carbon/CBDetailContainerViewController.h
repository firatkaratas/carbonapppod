//
//  CBDetailContainerViewController.h
//  Carbon
//
//  Created by Necati Aydın on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+Utility.h"

@class CBDetailContainerLogic;

/**
 Contained view controllers should conform to this protocol;
 */
@protocol CBDetailContainedViewControllerProtocol <NSObject>

/**
 Contained view controllers decide the navigation bar should be displayed or not wheh swiped among other ones.
 */
- (BOOL)shouldHideNavigationBar;

@end

@interface CBDetailContainerViewController : UIPageViewController

/**
 Logic class of the view controller.
 */
@property (nonatomic, strong) CBDetailContainerLogic *logic;

/**
 Index of the view controller to be shown.
 */
@property (assign, nonatomic) NSInteger currentIndex;

@end
