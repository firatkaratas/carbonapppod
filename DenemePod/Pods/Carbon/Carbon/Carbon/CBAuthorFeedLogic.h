//
//  CBAuthorFeedLogic.h
//  Carbon
//
//  Created by Necati Aydın on 11/05/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedLogic.h"

@interface CBAuthorFeedLogic : CBFeedLogic

@property (nonatomic, strong) NSString *authorId;

@end
