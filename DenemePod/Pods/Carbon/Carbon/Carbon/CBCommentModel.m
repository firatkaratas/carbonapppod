//
//  CBCommentModel.m
//  Carbon
//
//  Created by Semih Cihan on 20/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBCommentModel.h"
#import "CBCommentModelConfig.h"
#import "CBCommentModelProxy.h"

@implementation CBCommentModel

+ (CBBaseModelConfig *)config
{
    return [CBCommentModelConfig sharedConfig];
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"commentId" : [CBCommentModelConfig sharedConfig].commentId,
             @"comment" : [CBCommentModelConfig sharedConfig].comment,
             @"date" : [CBCommentModelConfig sharedConfig].date,
             @"username" : [CBCommentModelConfig sharedConfig].username
             };
}

+ (NSValueTransformer *)commentIdJSONTransformer {

    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBCommentModelProxy sharedInstance] commentIdJSONTransformer:object];
        return [CBBaseModel stringOrNumberToString:object];
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)commentJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBCommentModelProxy sharedInstance] commentJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)dateJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBCommentModelProxy sharedInstance] dateJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

+ (NSValueTransformer *)usernameJSONTransformer {
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id (id object) {
        
        object = [[CBCommentModelProxy sharedInstance] usernameJSONTransformer:object];
        return object;
        
    } reverseBlock:^id (id object) {
        return nil;
    }];
}

@end
