//
//  CBAppConfig.h
//  Carbon
//
//  Created by Necati Aydın on 23/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"

typedef NS_ENUM(NSInteger, CBLeftMenuType)
{
    CBLeftMenuType1=1,
    CBLeftMenuType2=2,
};

@interface CBAppConfig : CBBaseConfig

/**
 Name of the application.
 */
@property (nonatomic, strong) NSString *appName;

/**
 Type for the left menu which shows categories. It can be 1 or 2.
 */
@property (nonatomic, assign) CBLeftMenuType leftMenuType;

/**
 UI type for the news detail.
 */
@property (nonatomic, assign) NSInteger newsDetailType;

/**
 UI type for the video detail.
 */
@property (nonatomic, assign) NSInteger videoDetailType;

/**
 Author detail type for the author detail.
 */
@property (nonatomic, assign) NSInteger authorDetailType;

/**
 Application id, given by Parse for push notification purposes.
 */
@property (nonatomic, strong) NSString *parseApplicationId;

/**
 Client key, given by Parse for push notification purposes.
 */
@property (nonatomic, strong) NSString *parseClientKey;

/**
 Seamless app token for iPad.
 */
@property (nonatomic, strong) NSString *seamlessAppTokenIPad;

/**
 Seamless app token for iPhone.
 */
@property (strong, nonatomic) NSString *seamlessAppToken;

/**
 If YES, enables seamless location based targeting.
 @warning If set to YES, you should add "NSLocationWhenInUseUsageDescription" key into your project's Info.plist file.
 */
@property (assign, nonatomic) BOOL seamlessLocationEnabled;

/**
 Google analytics tracking id.
 */
@property (strong, nonatomic) NSString *googleAnalyticsTrackingId;

/**
 Flurry analytics tracking id.
 */
@property (strong, nonatomic) NSString *flurryAnalyticsTrackingId;

/**
 Indicates if the app has comments in news details.
 */
@property (assign, nonatomic) BOOL commentsEnabledForNewsDetail;

/**
 Indicates if the app has comments in video details.
 */
@property (assign, nonatomic) BOOL commentsEnabledForVideoDetail;

/**
 Indicates if the app has live stream feature.
 */
@property (assign, nonatomic) BOOL liveStreamEnabled;


@end
