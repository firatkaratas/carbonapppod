//
//  UIViewController+CBCommentCell.m
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIViewController+CBCommentCell.h"
#import "CBCommentModel.h"
#import "CBCommentCell.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UIViewControllerCBCommentCell)

@implementation UIViewController (CBCommentCell)

- (void)configureCommentCell:(CBCommentCell *)cell withCommentModel:(CBCommentModel *)model
{
    [cell.commentLabel setText:model.comment];
    [cell.detailLabel setText:[NSString stringWithFormat:@"%@ - %@", model.username, @"date"]];
}

@end
