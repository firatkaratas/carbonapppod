//
//  CBHomeConfig.h
//  Carbon
//
//  Created by Necati Aydın on 06/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseConfig.h"


typedef NS_ENUM(NSInteger, CBFeedCellType)
{
    CBFeedCellTypeNotSpecified = 0,
    CBFeedCellType1 = 1,
    CBFeedCellType2 = 2,
    CBFeedCellType3 = 3,
    CBFeedCellType4 = 4,
    CBFeedCellType5 = 5,
    CBFeedCellType7 = 7,
};

@interface CBFeedStyleConfig : CBBaseConfig

/**
 Background color of the main feed view.
 */
@property (nonatomic, strong) UIColor *viewBackgroundColor;

/**
 Background color of the feed cells.
 */
@property (nonatomic, strong) UIColor *cellBackgroundColor;

/**
 Color of the borders of the cells.
 */
@property (nonatomic, strong) UIColor *cellBorderColor;

/**
 Color of the title of the feed cells.
 */
@property (nonatomic, strong) UIColor *cellTitleColor;

/**
 Font of the title of the feed cells.
 */
@property (nonatomic, strong) UIFont *cellTitleFont;

/**
 Color of the category label in the feed cell.
 */
@property (nonatomic, strong) UIColor *cellCategoryColor;

/**
 Font of the category label in the feed cell.
 */
@property (nonatomic, strong) UIFont *cellCategoryFont;

/**
 Color of the spot label if it is included in a cell.
 */
@property (nonatomic, strong) UIColor *cellSpotColor;

/**
 Font of the spot label if it is included in a cell.
 */
@property (nonatomic, strong) UIFont *cellSpotFont;

/**
 UI Type of the cell. Selected by the client.
 */
@property (nonatomic, assign) CBFeedCellType cellType;

/**
 Color of the date label.
 */
@property (nonatomic, strong) UIColor *cellDateColor;

/**
 Font of the date label.
 */
@property (nonatomic, strong) UIFont *cellDateFont;

/**
 Background color of the no image feed cells.
 */
@property (nonatomic, strong) UIColor *noImageCellBackgroundColor;

/**
 Color of the borders of the no image cells.
 */
@property (nonatomic, strong) UIColor *noImageCellBorderColor;

/**
 Color of the title of the feed no image cells.
 */
@property (nonatomic, strong) UIColor *noImageCellTitleColor;

/**
 Font of the title of the feed no image cells.
 */
@property (nonatomic, strong) UIFont *noImageCellTitleFont;

/**
 Color of the category label in the no image feed cell.
 */
@property (nonatomic, strong) UIColor *noImageCellCategoryColor;

/**
 Font of the category label in the no image feed cell.
 */
@property (nonatomic, strong) UIFont *noImageCellCategoryFont;

/**
 Color of the spot label if it is included in a no image cell.
 */
@property (nonatomic, strong) UIColor *noImageCellSpotColor;

/**
 Font of the spot label if it is included in a no image cell.
 */
@property (nonatomic, strong) UIFont *noImageCellSpotFont;

/**
 Color of the date label in no image cell.
 */
@property (nonatomic, strong) UIColor *noImageCellDateColor;

/**
 Font of the date label in no image cell.
 */
@property (nonatomic, strong) UIFont *noImageCellDateFont;


@end
