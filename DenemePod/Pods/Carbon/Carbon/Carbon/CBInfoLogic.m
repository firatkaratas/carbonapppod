//
//  CBInfoLogic.m
//  Carbon
//
//  Created by Semih Cihan on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBInfoLogic.h"
#import "CBInfoConfig.h"

@implementation CBInfoLogic

- (instancetype)init {
    
    self = [super init];
    if (self) {
        if ([CBInfoConfig sharedConfig].mapImageUrl && [CBInfoConfig sharedConfig].mapImageUrl.length > 0) {
            _mapImageUrl = [NSURL URLWithString:[CBInfoConfig sharedConfig].mapImageUrl];
        }
        if ([CBInfoConfig sharedConfig].htmlInfoUrl && [CBInfoConfig sharedConfig].htmlInfoUrl.length > 0) {
            _htmlInfoUrl = [NSURL URLWithString:[CBInfoConfig sharedConfig].htmlInfoUrl];
        }
        _titles = [CBInfoConfig sharedConfig].titles;
        _informations = [CBInfoConfig sharedConfig].informations;
    }
    return self;
}

- (NSString *)titleForSection:(NSInteger)section {
    return self.titles[section];
}

- (NSString *)informationForSection:(NSInteger)section {
    return self.informations[section];
}

- (BOOL)socialMediaEnabled
{
    return [self socialMediaTypes] != nil;
}

- (NSURL *)urlForSocialMedia:(CBInfoSocialMedia)socialMediaType
{
    NSString *urlString = nil;
    if(socialMediaType == CBInfoSocialMediaFacebook)
    {
        urlString = [CBInfoConfig sharedConfig].facebookUrl;
    }
    else if(socialMediaType == CBInfoSocialMediaTwitter)
    {
        urlString = [CBInfoConfig sharedConfig].twitterUrl;
    }
    else if(socialMediaType == CBInfoSocialMediaGooglePlus)
    {
        urlString = [CBInfoConfig sharedConfig].googlePlusUrl;
    }
    else if(socialMediaType == CBInfoSocialMediaYouTube)
    {
        urlString = [CBInfoConfig sharedConfig].youTubeUrl;
    }
    
    if(urlString)
    {
        return [NSURL URLWithString:urlString];
    }
    else
    {
        return nil;
    }
}

- (NSMutableArray *)socialMediaTypes
{
    NSMutableArray *urls = [NSMutableArray new];
    if([CBInfoConfig sharedConfig].facebookUrl && ![[CBInfoConfig sharedConfig].facebookUrl isEqualToString:@""])
    {
        [urls addObject:@(CBInfoSocialMediaFacebook)];
    }
    if([CBInfoConfig sharedConfig].twitterUrl  && ![[CBInfoConfig sharedConfig].twitterUrl isEqualToString:@""])
    {
        [urls addObject:@(CBInfoSocialMediaTwitter)];
    }
    if([CBInfoConfig sharedConfig].googlePlusUrl && ![[CBInfoConfig sharedConfig].googlePlusUrl isEqualToString:@""])
    {
        [urls addObject:@(CBInfoSocialMediaGooglePlus)];
    }
    if([CBInfoConfig sharedConfig].youTubeUrl  && ![[CBInfoConfig sharedConfig].youTubeUrl isEqualToString:@""])
    {
        [urls addObject:@(CBInfoSocialMediaYouTube)];
    }
    
    if(urls.count)
    {
        return urls;
    }
    else
    {
        return nil;
    }
}

@end
