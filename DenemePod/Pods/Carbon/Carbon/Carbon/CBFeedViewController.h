//
//  CBHomeViewController.h
//  Carbon
//
//  Created by Semih Cihan on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBSeamlessViewController.h"
#import "CBFeedLogic.h"
#import "CBLeftMenu.h"

@class ADLivelyCollectionView,CBFeedViewController, CBLeftMenuContainerViewController;

@interface CBFeedViewController : CBSeamlessViewController <CBDataLoaderWithPagingAndRefreshDelegate,CBLiveStreamDelegate>

/**
 Logic of the view controller.
 */
@property (strong, nonatomic) CBFeedLogic *logic;

/**
 Cell type for the view controller.
 */
@property (assign, nonatomic) NSInteger cellType;

/**
 Delegate of the view controller.
 */
@property (weak, nonatomic) id<CBMenuContentViewControllerDelegate> delegate;

/**
 Starts interstitial and MMA ads.
 */
- (void)startAds;

@end
