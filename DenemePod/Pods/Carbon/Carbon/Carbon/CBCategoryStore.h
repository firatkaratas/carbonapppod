//
//  CBCategoryStore.h
//  Carbon
//
//  Created by Necati Aydın on 24/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

@class CBCategoryModel;

@interface CBCategoryStore : NSObject

/**
 Singleton object.
 */
+ (id)sharedStore;

/**
 Fetches categories on the first call using the networkmanager and caches the data. For the following calls returns the cached data.
 @param completion Completion block to be called. NSArray holds categories on successfull calls, nil otherwise.
 */
- (void)getCategoriesWithCompletion:(void (^)(NSArray *categories, NSError *error))completion;

/**
 Using the data fetched from the service, searches the array of category models (and their subcategories) for the given categoryId and returns the category if found.
 @param categoryId Searched categoryId.
 @return Model object if found, nil otherwise.
 */
- (CBCategoryModel *)getCategoryWithId:(NSString *)categoryId;

@end
