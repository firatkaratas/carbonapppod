//
//  CBBaseModel.h
//  Carbon
//
//  Created by Semih Cihan on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mantle.h"
#import "CBBaseModelConfig.h"

/**
 @description This a base class for models used in the framework. CBBaseModel is a subclass of MTLModel and conforms to MTLJSONSerializing.
 */
@interface CBBaseModel : MTLModel<MTLJSONSerializing>

/**
 Returns the additional parameters set separately.
 */
- (id)additionalValueForKey:(NSString *)key;

/**
 Additional values can be set to model with this function.
 */
- (void)setAdditionalValue:(id)value forKey:(NSString *)key;

/**
 Returns the config file used for mapping the network dictionary to model. It should be overriden in subclasses.
 */
+ (CBBaseModelConfig *)config;

/**
 Converts the input to an NSNumber.
 @param object The object to be converted. If the object is of type NSString or NSNumber the conversion is successful, otherwise returns nil.
 @return NSNumber
 */
+ (NSNumber *)stringOrNumberToNumber:(id)object;

/**
 Converts the input to an NSNumber.
 @param object The object to be converted. If the object is of type NSString (check the implementation for details) or NSNumber the conversion is successful, otherwise returns nil.
 @return NSNumber
 */
+ (NSNumber *)stringOrNumberToBoolNumber:(id)object;

/**
 Converts the input to an NSString.
 @param object The object to be converted. If the object is of type NSString (check the implementation for details) or NSNumber the conversion is successful, otherwise returns nil.
 @return NSString
 */
+ (NSString *)stringOrNumberToString:(id)object;

/**
 Used to remove links from html contents. Matches all the linkPattern string in the content and removes the part beginning with '<a' and ending with '</a>' containing the pattern.
 @param content Html content to remove the links.
 @param lingPattern Link pattern to remove.
 @return Returns a new string after removing the links with the given pattern.
 */
+ (NSString *)removeLinksInHtmlContent:(NSString *)content linkPattern:(NSString *)linkPattern;

/**
 Used to remove links from html contents. Matches all the linkPattern strings in the linkPatternArray in the content and removes the part beginning with '<a' and ending with '</a>' containing the pattern.
 @param content Html content to remove the links.
 @param lingPatternArray Link patterns to remove.
 @return Returns a new string after removing the links with the given pattern.
 */
+ (NSString *)removeLinksInHtmlContent:(NSString *)content linkPatternArray:(NSArray *)linkPatternArray;


@end
