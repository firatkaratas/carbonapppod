//
//  UICollectionView+Additions.m
//  Carbon
//
//  Created by Necati Aydın on 26/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UICollectionView+Additions.h"
#import "NimbusKitBasics.h"

NI_FIX_CATEGORY_BUG(UICollectionViewAdditions)

@implementation UICollectionView (Additions)

- (void)registerClassForDefaultReuseIdentifier:(Class)cellClass
{
    UINib *nib = [UINib nibWithNibName:NSStringFromClass(cellClass) bundle:[NSBundle frameworkBundle]];
    [self registerNib:nib forCellWithReuseIdentifier:NSStringFromClass(cellClass)];
}

@end
