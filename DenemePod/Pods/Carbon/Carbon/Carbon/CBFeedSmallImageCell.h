//
//  CBFeedSmallImageCell.h
//  Carbon
//
//  Created by Semih Cihan on 10/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseCollectionViewCell.h"

@interface CBFeedSmallImageCell : CBBaseCollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (strong, nonatomic) NSArray *separators;

+ (CGSize)sizeForWidth:(CGFloat)width;

@end
