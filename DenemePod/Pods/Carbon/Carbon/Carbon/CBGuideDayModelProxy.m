//
//  CBGuideModelProxy.m
//  Carbon
//
//  Created by Semih Cihan on 17/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGuideDayModelProxy.h"

@implementation CBGuideDayModelProxy

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[CBGuideDayModelProxy alloc] init];
    });
    return sharedInstance;
}

- (id)guideItemsJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(guideItemsJSONTransformerOverridden:)]) {
        return [self guideItemsJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)dateJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(dateJSONTransformerOverridden:)]) {
        return [self dateJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

- (id)dayOfTheWeekJSONTransformer:(id)object {
    
    if ([self respondsToSelector:@selector(dayOfTheWeekJSONTransformerOverridden:)]) {
        return [self dayOfTheWeekJSONTransformerOverridden:object];
    } else {
        return object;
    }
}

@end
