//
//  CBSeamlessViewController.m
//  Carbon
//
//  Created by Semih Cihan on 17/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBSeamlessViewController.h"
#import "UIView+LayoutConstraints.h"

@interface CBSeamlessViewController ()

@end

@implementation CBSeamlessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self setNeedsStatusBarAppearanceUpdate];

    if(!self.presentedViewController && self.interstitialAdManager)//if not interstitial is dismissing
    {
        self.interstitialAdManager.delegate = self;
        [self.interstitialAdManager loadAd];
        self.interstitialAdManager = nil;
    }
}

- (void)dealloc
{
    _collectionViewAdManager.delegate = nil;
    _interstitialAdManager.delegate = nil;
    _mreView.delegate = nil;
    _mmaView.delegate = nil;
}

#pragma mark - Public methods

- (void)startInterstitialAdWithEntityName:(NSString *)entityName {
    self.interstitialAdManager = [[SLInterstitialAdManager alloc]
                                  initWithEntity:[CBSeamlessViewController interstitialEntityNameWithEntityName:entityName]
                                  category:SLCategoryNews];
    self.interstitialAdManager.delegate = self;
    [self.interstitialAdManager loadAd];
}

- (void)startMMAAdWithEntityName:(NSString *)entityName {
    self.mmaView = [[SLAdView alloc] initWithEntity:[CBSeamlessViewController mmaEntityNameWithEntityName:entityName]
                                           category:SLCategoryNews
                                             adSize:SLAdSizeMMA
                                 rootViewController:self];
    self.mmaView.delegate = self;
    [self.mmaView loadAd];
}

- (void)startMREAdWithEntityName:(NSString *)entityName {
    self.mreView = [[SLAdView alloc] initWithEntity:[CBSeamlessViewController mreEntityNameWithEntityName:entityName]
                                           category:SLCategoryNews
                                             adSize:SLAdSizeMRect
                                 rootViewController:self];
    self.mreView.delegate = self;
    [self.mreView loadAd];
}

- (void)startCollectionViewAdWithEntityName:(NSString *)entityName
                             collectionView:(UICollectionView *)collectionView
                                 dataSource:(NSMutableArray *)dataSource {
    
    self.collectionViewAdManager = [[SLCollectionViewAdManager alloc] initWithCollectionView:collectionView
                                                                                  dataSource:dataSource
                                                                              viewController:self];
    [self.collectionViewAdManager setDelegate:self];
    
    [self.collectionViewAdManager getAdsWithEntity:[CBSeamlessViewController collectionViewEntityNameWithEntityName:entityName]
                                          category:SLCategoryNews
                                      successBlock:^{}
                                      failureBlock:^(NSError *error) {}
     ];
}

- (void)presentSeamlessVideoPlayerWithUrl:(NSURL *)url entityName:(NSString *)entityName {
    [[SLPlayerManager sharedManager] presentPlayerWithUrl:url entity:entityName];
}

#pragma mark - Interstitial delegate

- (void)interstitialDidLoad:(MPInterstitialAdController *)interstitial {
    [interstitial showFromViewController:self];
}

- (void)interstitialDidFailToLoad:(MPInterstitialAdController *)interstitial {
    
}

#pragma mark - AdView delegate methods

- (void)adViewDidLoad:(SLAdView *)adView {

    if(adView == self.mmaView) {
        
        adView.translatesAutoresizingMaskIntoConstraints = NO;
        [adView setWidthConstraint:SLAdSizeMMA.width];
        [adView setHeightConstraint:SLAdSizeMMA.height];
    } else if (adView == self.mreView) {
        
        adView.translatesAutoresizingMaskIntoConstraints = NO;
        [adView setWidthConstraint:SLAdSizeMRect.width];
        [adView setHeightConstraint:SLAdSizeMRect.height];
    }
    
}

- (void)adViewDidFailToLoad:(SLAdView *)adView {
    [adView removeFromSuperview];
}

#pragma mark - Entity names

+ (NSString *)interstitialEntityNameWithEntityName:(NSString *)entityName
{
    NSString *str = [[NSString stringWithFormat:@"%@-Interstitial", entityName] stringByReplacingOccurrencesOfString:@" " withString:@""];
    return str;
}

+ (NSString *)mmaEntityNameWithEntityName:(NSString *)entityName
{
    NSString *str = [[NSString stringWithFormat:@"%@-MMA",entityName]stringByReplacingOccurrencesOfString:@" " withString:@""];
    return str;
}

+ (NSString *)collectionViewEntityNameWithEntityName:(NSString *)entityName
{
    NSString *str = [[NSString stringWithFormat:@"%@-Collection",entityName]stringByReplacingOccurrencesOfString:@" " withString:@""];
    return str;
}

+ (NSString *)mreEntityNameWithEntityName:(NSString *)entityName
{
    NSString *str = [[NSString stringWithFormat:@"%@-MRE",entityName]stringByReplacingOccurrencesOfString:@" " withString:@""];
    return str;
}

+ (NSString *)videoPlayerEntityNameWithEntityName:(NSString *)entityName
{
    NSString *str = [[NSString stringWithFormat:@"%@-Video",entityName]stringByReplacingOccurrencesOfString:@" " withString:@""];
    return str;
}

@end
