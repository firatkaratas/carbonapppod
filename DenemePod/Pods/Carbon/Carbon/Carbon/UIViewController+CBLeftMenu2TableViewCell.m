//
//  UIViewController+CBLeftMenu2TableViewCell.m
//  Carbon
//
//  Created by Semih Cihan on 18/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "UIViewController+CBLeftMenu2TableViewCell.h"
#import "CBLeftMenu2TableViewCell.h"
#import "CBCategoryModel.h"

@implementation UIViewController (CBLeftMenu2TableViewCell)

- (void)configureCell:(CBLeftMenu2TableViewCell *)cell withModel:(CBCategoryModel *)model {
    [cell.titleLabel setText:model.name];
}

@end
