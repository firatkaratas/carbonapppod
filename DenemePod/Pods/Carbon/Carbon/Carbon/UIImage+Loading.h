//
//  UIImage+Loading.h
//  Carbon
//
//  Created by Necati Aydın on 17/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//


@interface UIImage (Loading)

/**
 Loads the image for the given bundle but it first looks if the image exists in the main bundle, if not it looks for the given bundle.
 @oaram name Name of the image. Does not include 'png' extension.
 @param bundle The bundle that the image is loaded from.
 @return Newly created image.
 */
+ (UIImage *)imageNamed:(NSString *)name inBundle:(NSBundle *)bundle;

@end
