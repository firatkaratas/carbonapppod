//
//  CBGuideContentViewController.m
//  Carbon
//
//  Created by Necati Aydın on 09/04/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGuideContentViewController.h"
#import "UICollectionView+Additions.h"
#import "UIImageView+WebCache.h"
#import "CBGuideCell.h"
#import "CBGuideModel.h"
#import "CBGuideDayModel.h"
#import "CBGuideStyleConfig.h"

static const CGFloat kGuideCellHorizontalSpace = 10;

@interface CBGuideContentViewController (Styling)

@end

@implementation CBGuideContentViewController (Styling)

- (void)styleGuideCell:(CBGuideCell *)cell
{
    CBGuideStyleConfig *config = [CBGuideStyleConfig sharedConfig];
    self.view.backgroundColor = config.backgroundColor;
    cell.backgroundColor = config.backgroundColor;
    cell.titleLabel.textColor = config.cellTitleColor;
    cell.titleLabel.font = config.cellTitleFont;
    cell.timeLabel.textColor = config.cellDateColor;
    cell.timeLabel.font = config.cellDateFont;
    for(UIView *separator in cell.separators)
    {
        separator.backgroundColor = config.cellBorderColor;
    }
}

@end

@interface CBGuideContentViewController ()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;


@end

@implementation CBGuideContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.collectionView registerClassForDefaultReuseIdentifier:[CBGuideCell class]];
}

#pragma mark - Collection View

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.guideDayModel.guideItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CBGuideCell *cell = (CBGuideCell *)[collectionView dequeueReusableCellWithReuseIdentifier:[CBGuideCell reuseIdentifier] forIndexPath:indexPath];
    
    CBGuideModel *guideModel = self.guideDayModel.guideItems[indexPath.row];
    [self configureGuideCell:cell withGuideModel:guideModel];
    [self styleGuideCell:cell];
    
    return cell;
}

- (void)configureGuideCell:(CBGuideCell *)cell withGuideModel:(CBGuideModel *)guideModel
{
    cell.titleLabel.text = guideModel.title;
    cell.typeLabel.text = guideModel.tag;
    cell.timeLabel.text = guideModel.time;
    
    [cell.imageView sd_setImageWithURL:guideModel.imageUrl];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = CGRectGetWidth(self.view.frame) - 2 * kGuideCellHorizontalSpace;
    return [CBGuideCell sizeForWidth:width];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 10, 0);
}


@end
