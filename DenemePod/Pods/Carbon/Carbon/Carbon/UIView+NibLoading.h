//
//  UIView+NibLoading.h
//  Carbon
//
//  Created by Necati Aydın on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UIView(NibLoading)

/**
 Creates an instance by loading it from the nib.
 */
+ (instancetype)loadFromNIB;

@end
