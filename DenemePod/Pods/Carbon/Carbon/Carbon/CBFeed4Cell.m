//
//  CBFeedSmallImageCell.m
//  Carbon
//
//  Created by Semih Cihan on 10/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeed4Cell.h"
#import "UIView+Separator.h"

@implementation CBFeed4Cell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.separators = [self.contentView addFrameWithLineWidth:kCellBorderLineWidth];
}

+ (CGFloat)cellRatio
{
    return 100.f/300.f;
}

@end
