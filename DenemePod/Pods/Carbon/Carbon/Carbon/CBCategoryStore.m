//
//  CBCategoryStore.m
//  Carbon
//
//  Created by Necati Aydın on 24/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBCategoryStore.h"
#import "CBNetworkManager.h"
#import "CBCategoryModel.h"

@interface CBCategoryStore()

@property (nonatomic, strong) NSArray *categories;

@end

@implementation CBCategoryStore

+ (id)sharedStore
{
    static CBCategoryStore *sharedMyStore = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
    {
        sharedMyStore = [self new];
    });
    
    return sharedMyStore;
}


- (void)getCategoriesWithCompletion:(void (^)(NSArray *categories, NSError *error))completion
{
    if(self.categories)
    {
        completion(self.categories,nil);
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [[CBNetworkManager sharedInstance] getCategoriesWithSuccessBlock:^(NSArray *categories)
    {
        weakSelf.categories = categories;
        completion(categories,nil);
        
    } failureBlock:^(NSError *error) {
        
        completion(nil,error);
    }];
}

- (CBCategoryModel *)getCategoryWithId:(NSString *)categoryId {
    
    CBCategoryModel *foundCategory;
    for (CBCategoryModel *model in self.categories) {
        foundCategory = [model searchCategoryForCategoryId:categoryId];
        if (foundCategory) {
            return foundCategory;
        }
    }
    return nil;
}


@end
