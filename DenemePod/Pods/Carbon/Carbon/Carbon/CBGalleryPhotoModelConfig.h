//
//  CBGalleryPhotoModelConfig.h
//  Carbon
//
//  Created by Semih Cihan on 27/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModelConfig.h"

@interface CBGalleryPhotoModelConfig : CBBaseModelConfig

/**
 Id for the model.
 */
@property (strong, nonatomic) NSString *photoId;

/**
 Description of the photo.
 */
@property (strong, nonatomic) NSString *photoDescription;

/**
 URL of the image.
 */
@property (strong, nonatomic) NSString *imageUrl;

@end
