Pod::Spec.new do |s|

s.name         = "Carbon"
s.version      = "0.0.1"
s.summary      = "A content based application framework for Mobilike iOS applications."

s.homepage     = "http://www.mobilike.com"
s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
s.author             = { "Semih Cihan" => "semih@mobilike.com" }
s.platform     = :ios
s.ios.deployment_target = "7.1"
s.source       = { :git => "https://bitbucket.org/mobilike/carbon.git", :tag => s.version }

s.source_files  = "Carbon/Carbon/*.{h,m}", "Carbon/Carbon/External/**/*.{h,m}"
s.prefix_header_file = 'Carbon/Carbon/Carbon-Prefix.pch'
s.resources = "Carbon/CarbonResources/*.{png,xib,storyboard,strings}"
s.frameworks = "Security", "CoreData", "AdSupport", "Foundation", "UIKit", "CoreGraphics", "CoreText", "MediaPlayer"
s.libraries = "sqlite3", "z"
s.vendored_frameworks = "Carbon/Parse.framework", "Carbon/ParseUI.framework", "Carbon/ParseFacebookUtils.framework", "Carbon/FBAudienceNetwork.framework", "Carbon/GoogleMobileAds.framework", "Carbon/Seamless.framework"
s.vendored_libraries = "Carbon/libGoogleAnalyticsServices.a", "Carbon/libAdIdAccess.a", "Carbon/Carbon/External/Flurry/libFlurry_6.4.0.a"

s.requires_arc = ['Carbon/Carbon/*.{h,m}', 'Carbon/**/External/AFNetworking/**/*.{h,m}', 'Carbon/**/External/Bolts/**/*.{h,m}', 'Carbon/**/External/Flurry/**/*.{h,m}', 'Carbon/**/External/GoogleAnalytics/**/*.{h,m}', 'Carbon/**/External/Mantle/**/*.{h,m}', 'Carbon/**/External/MBProgressHUD/**/*.{h,m}', 'Carbon/**/External/OrderedDictionary/**/*.{h,m}', 'Carbon/**/External/Reachability/**/*.{h,m}', 'Carbon/**/External/RESideMenu/**/*.{h,m}', 'Carbon/**/External/SDWebImage/**/*.{h,m}', 'Carbon/**/External/UIImageEffects/**/*.{h,m}', 'Carbon/**/External/NimbusKitBasics/*.{h,m}']

end
