//
//  main.m
//  DenemePod
//
//  Created by Fırat Karataş on 22/06/15.
//  Copyright (c) 2015 Mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Carbon/Carbon.h>

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CBAppDelegate class]));
    }
}
